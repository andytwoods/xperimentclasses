﻿package com.bulkloader{
	import com.bulkloader.*;
	import com.bulkloader.loadingtypes.XMLItem;
	import com.startMenu.saveFilesLocally;
	
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.*;
	
	public class preloadFilesFromWeb extends Sprite {
		public var loader:BulkLoader;
		public var files:Array = new Array;
		public var maximumTries:String="5";
		public var remoteDirectory:String="";
		private var localDirectory:String="";
		
		public function kill():void{
			//trace("getFilesFromWeb.kill(): nothing to kill");
		}
		
		public function isFinished():Boolean
		{
			return loader.isFinished;
		}
		
		public function informWhenPreloaderFinished(funct:Function):void{ //calls the provided function when the loader has loaded.
			loader.addEventListener(BulkLoader.COMPLETE,function(e:Event):void{
				e.currentTarget.removeEventListener(e.type, arguments.callee);
				funct();
			},false,0,true);
		}
		
		
		//public function 
		
		public function giveBulkloader():BulkLoader{
			return loader;
		}
		
		public function preloadFilesFromWeb(trialInfo:XML,suffix:String):void{
			localDirectory=suffix;
			var arr:Array;
			var test:String;//I anticipate needing to test for other object types in the future.  Maybe for containers (background?) or behaviours? 
			for each(var child:XML in trialInfo..@["filename"]){
				test=child.parent().localName().substr(0,3);
				if(child.parent().parent().parent().@numberTrials.toString()!="0" && child.parent().@howMany!="0" && test=="add"){
					//fix the ";" business so only 'used' objects are loaded.
					arr=child.parent().@["filename"].toString().replace(/---/g,",").replace(/&/g,",").replace(/;/g,",").split(",");
					for (var i:uint=0;i<arr.length;i++){
						if(files.indexOf(arr[i])==-1)files.push(suffix+arr[i]);
					}
				}
			}
			
			
			if(trialInfo.SETUP.loadFilesInAdvance && trialInfo.SETUP.loadFilesInAdvance.@maximumTries.toString().length!=0)maximumTries=trialInfo.SETUP.loadFilesInAdvance.@["maximumTries"].toString();
			getFilesFromWeb();
		}
		
		public function give(str:String):*{
			return(loader.getContent(localDirectory+str));
		}
		
		public function giveBinary(str:String):*{
			return(loader.getBinary(localDirectory+str));
		}
		
		
		public function getFilesFromWeb():void{

			//BulkLoader.registerNewType("svg","TYPE_TEXT",SVGItem as Class);
			if(BulkLoader._allLoaders["stimulusPreloader"]!=undefined){
				loader=BulkLoader._allLoaders["stimulusPreloader"];
			}
			else{
				loader=new BulkLoader("stimulusPreloader");
				loader.addEventListener(BulkLoader.ERROR, onAllError);
				loader.addEventListener(BulkLoader.PROGRESS, onAllItemsProgress);
				loader.addEventListener(BulkLoader.COMPLETE, onFilesLoaded);
				var typ:String;
				var fil:String;
				for (var i:uint=0;i<files.length;i++){
					fil=files[i];
					typ="image"
					if(fil.toLowerCase().indexOf(".svg")!=-1)typ="text";
					//below, bit odd, but FLVs start playing immediately without special consideration...
					if(fil.toLowerCase().indexOf(".flv")!=-1){typ="video";loader.add(remoteDirectory+fil,{id:fil, maxTries:maximumTries, type:typ,"pausedAtStart":true});}
					if(typ!="video")loader.add(remoteDirectory+fil,{id:fil, maxTries:maximumTries, type:typ});
				}
				
				loader.start();
			}
		}
		
		public function onFilesLoaded(evt : BulkProgressEvent):void {
			loader.removeEventListener(BulkLoader.ERROR, onAllError);
			loader.removeEventListener(BulkLoader.PROGRESS, onAllItemsProgress);
			loader.removeEventListener(BulkLoader.COMPLETE, onFilesLoaded);
			trace("all files loaded");
			
			//ACTION: need to tell Runner that all files have been loaded before experiment can begin.
			
			/*var putFilesOnPhone:saveFilesLocally = new saveFilesLocally();
			this.addChild(putFilesOnPhone);
			putFilesOnPhone.setup(loader,exptFilesObj,actionAfterSaving);
			this.removeChild(putFilesOnPhone);
			
			var storageLocationOnPhone:String = putFilesOnPhone.storageLocation;
			putFilesOnPhone=null;*/
		}

		public function onAllItemsProgress(evt : BulkProgressEvent):void {
			//trace(evt.loadingStatus());
		}
		
		private function onAllError(evt :ErrorEvent ): void{
     	   trace ("error loading files unfortunately: "+evt); // outputs more information 
		}
		
		
	}
	
}
