﻿package com.startMenu{
	import flash.filesystem.File;
	import flash.filesystem.FileStream;
	import flash.filesystem.FileMode;
	import com.bulkloader.*;
	import flash.net.URLRequest;
	import flash.events.*;
	import flash.display.Loader;
	import flash.events.Event;
	import flash.display.Sprite;
	import com.mobile.saveFilesInternally;

	public class saveFilesLocally extends Sprite{
		var folder:File;
		var file:File;
		var fileStream:FileStream;
		var loader:BulkLoader;
		var exptFilesXML:XML;
		var objLdr:Loader;
		public var storageLocation:String;
		private var actionAfterSaving:String;
		var saveLocally:saveFilesInternally;

		public function kill(){
			trace("saveFilesLocally.kill(): nothing to kill");
		}

		public function setup(loaded:BulkLoader, instruct:XML,act) {
			loader=loaded;
			exptFilesXML=instruct;
			actionAfterSaving=act;
			saveLocally=new saveFilesInternally(exptFilesXML.localDir[0]);
			
			var files:Array=exptFilesXML.files[0].split(",");
			
			var allsaved:Boolean=true;
			for (var i:uint=0; i<files.length; i++) {
				allsaved;
				saveLocally.saveFile(files[i],loader.getBinary(files[i]));
			}

				if(allsaved && actionAfterSaving!="doNothing"){
				this.dispatchEvent(new Event("here's a script!",true));
				}
			if (allsaved==false) {
				trace("PROBLEM: saving was not successful: "+files[i]);
			}
			saveLocally=null;
		}

		public function deleteDirectory(dir:String) {
			saveLocally=new saveFilesInternally(exptFilesXML.localDir[0]);
			saveLocally.deleteDirectory(dir);
			saveLocally=null;
		}
	}
}