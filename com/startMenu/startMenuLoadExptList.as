﻿package com.startMenu{
	import flash.display.Stage;
	import flash.events.Event;
	import com.startmenu.touchlist.renderers.TouchListItemRenderer;
	import com.startmenu.touchlist.events.ListItemEvent;
	import com.startmenu.touchlist.controls.TouchList;
	import com.startmenu.touchlist.controls.MainItem;
	import importExptScript;

	public class startMenuLoadExptList extends startMenuItem {
		private var tempData:importExptScript;
		private var messageWhilstWaitingForMenuToPopulate:String="...no luck afraid, no web...click to retry?";

		function startMenuLoadExptList(sta:Stage, m:XML) {
			super(sta,m);
			m.appendChild(<menuItem action="tryToConnectAgain"><theTitle>{messageWhilstWaitingForMenuToPopulate}</theTitle></menuItem>);
		}

		public function tryToConnectAgain() {
			trace("trying to connect again");
			tempData.removeEventListener("dataLoaded",continueAfterLoadingScripts);
			tempData.removeEventListener("dataNotLoaded",continueAfterNotLoadingScripts);
			the_Sta.removeChild(tempData);
			tempData.kill();
			getScript();
		}

		private function getScript(){
					tempData=new importExptScript("http://www.xPeriment.mobi/Scripts/Experiments.xml");
					tempData.addEventListener("dataLoaded",continueAfterLoadingScripts);
					tempData.addEventListener("dataNotLoaded",continueAfterNotLoadingScripts);
					the_Sta.addChild(tempData);
		}

		override public function showList() {
			if (this.numChildren==1) {
				addMenu();
				if (tempData) {
					tempData.kill();
					tempData.removeEventListener("dataLoaded",continueAfterLoadingScripts);
					tempData.removeEventListener("dataNotLoaded",continueAfterNotLoadingScripts);
					if (the_Sta.contains(tempData)) the_Sta.removeChild(tempData);
				}
				
				else  {
					getScript();
				}
			}
			else {
				removeList();
			}
		}
		
		override public function setupEachMenuItem(){
			var listOfPastExpts:Array = new Array;
			if(String(menuInfo.exptsInMemory[0]).length!=0)listOfPastExpts = String(menuInfo.exptsInMemory[0]).split(";");
			
			for (var count:uint=0; count<menuInfo.menuItem.length();count++){
				var item:TouchListItemRenderer=new TouchListItemRenderer(menuInfo.menuItems[0]);
				
				if(listOfPastExpts.indexOf(String(menuInfo.menuItem[count].theTitle[0]))!=-1){
					item.colour=menuInfo.menuItems.hitColour[0];
					item.fontColour=menuInfo.menuItems.dullFontColour[0];
					item.data="---download again?---"+String.fromCharCode(13)+menuInfo.menuItem[count].theTitle[0];
				}
				else{
					item.colour=menuInfo.menuItems.colour[0];
					item.fontColour=menuInfo.menuItems.fontColour[0];
					item.data=menuInfo.menuItem[count].theTitle[0];
				}
				item.index=count;
				item.itemHeight=menuInfo.menuItems.height[0];
				item.itemWidth=the_Sta.stageWidth;
				touchList.addListItem(item);
			}
		}
		
		
		

		function continueAfterLoadingScripts(e:Event) {
			tempData.removeEventListener("dataLoaded",continueAfterLoadingScripts);
			tempData.removeEventListener("dataNotLoaded",continueAfterNotLoadingScripts);
 			continueAfter_()
			the_Sta.addChild(tempData);
			var myXMLList:XMLList=e.target.giveMeData().children();
			for (var i:uint=0; i<myXMLList.length();i++){
				myXMLList[i].theTitle=String(myXMLList[i].theTitle+": "+myXMLList[i].description);
			}
			
			super.menuInfo.appendChild(myXMLList);
			initialTouchList();
		}
		
		function continueAfterNotLoadingScripts(e:Event) {
			super.menuInfo.appendChild(<menuItem action="tryToConnectAgain"><theTitle>cannot connect :( Retry?</theTitle></menuItem>);
			continueAfter_()
			initialTouchList();
		}
		
		function continueAfter_(){
			if (super.menuInfo.menuItem.(theTitle==messageWhilstWaitingForMenuToPopulate)) {
				delete super.menuInfo.menuItem.(theTitle==messageWhilstWaitingForMenuToPopulate)[0];
			}
			if (super.contains(super.touchList)) super.removeChild(super.touchList);	
			else (showMenu=false);
			super.touchList=null;
		}


	}
}