﻿package com.startMenu{
	import flash.display.Sprite;
	import flash.display.BitmapData;
	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.events.Event;
	import flash.net.URLRequest;
	
	public class getImage extends Sprite{
		var myImageLoader:Loader = new Loader(); 
		var myHeight:uint;
		var myWidth:uint;


		public function getImage(img:String,hei:uint,wid:uint) {
			myImageLoader = new Loader(); 
			myHeight=hei;
			myWidth=wid;

			var imageURLRequest:URLRequest = new URLRequest(img); 
			
			myImageLoader.load(imageURLRequest); 
			myImageLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, imageLoaded); 
		}
		
		function imageLoaded(e:Event):void { 
			 myImageLoader.contentLoaderInfo.removeEventListener(Event.COMPLETE, imageLoaded); 
 		 	 var myBitmapData:BitmapData = new BitmapData(myImageLoader.width, myImageLoader.height,true,0x00000000); 
 		 	 myBitmapData.draw(myImageLoader); 
 			 var myBitmap:Bitmap = new Bitmap; 
 			 myBitmap.bitmapData = myBitmapData; 
			 myBitmap.alpha=1;

			 if (myHeight!=0) myBitmap.height=myHeight;
			 if (myWidth !=0) myBitmap.width=myWidth;
  			 addImage(myBitmap);
		} 

		public function addImage(bmp:Bitmap){
			this.addChild(bmp);
		}
	}
	
}
