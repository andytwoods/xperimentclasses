﻿package com.startmenu.touchlist.renderers{
	import flash.display.Shape;
	import flash.geom.Point;
	import flash.events.TimerEvent;
	import flash.utils.Timer;

	public class wiggleShape {

		var point1:Point;
		var point2:Point;
		var point3:Point;
		var point4:Point;
		var point1_5:Point;
		var point2_5:Point;
		var point3_5:Point;
		var point4_5:Point;
		var shape:Shape;
		var colour:Number;
		var xOffset:uint;
		var maxHor:uint;
		var maxVer:uint;
		var wiggleHorMultiplier:Number;
		var wiggleVerMultiplier:Number;
		var myWidth:uint;
		var myHeight:uint;
		
		public function kill(){
			
		}

		public function wiggleShape(col:Number, width:uint, height:uint) {
			colour=col;
			xOffset=width*.0125;
			myWidth=width*.975;
			myHeight=height;
			
			wiggleHorMultiplier=.01;
			wiggleVerMultiplier=.1;
		}
		
		private function generateShape(){
			point1=new Point(myWidth,0);
			point2=new Point(myWidth,myHeight);
			point3=new Point(0,myHeight);
			point4=new Point(0,0);
				
			point1_5=Point.interpolate(point4,point1,Math.random());
			point2_5=Point.interpolate(point1,point2,Math.random());
			point3_5=Point.interpolate(point2,point3,Math.random());
			point4_5=Point.interpolate(point3,point4,Math.random());

			maxHor=point1.x-point4.x;
			maxVer=point2.y-point1.y;

			maxHor*=wiggleHorMultiplier;
			maxVer*=wiggleVerMultiplier;
			point1_5.offset(maxHor-Math.random()*maxHor*2,maxVer-Math.random()*maxVer*2);
			point2_5.offset(maxHor-Math.random()*maxHor*2,maxVer-Math.random()*maxVer*2);
			point3_5.offset(maxHor-Math.random()*maxHor*2,maxVer-Math.random()*maxVer*2);
			point4_5.offset(maxHor-Math.random()*maxHor*2,maxVer-Math.random()*maxVer*2);
		}
	
		public function setWiggles(ver:Number,hor:Number){
			wiggleHorMultiplier=hor;
			wiggleVerMultiplier=ver;
			
		}
		
		public function provideShape():Shape{
			return renderShape(colour,.9);
		}
		
		
		public function renderShape(col:Number,trans:Number):Shape{
			var shape:Shape = new Shape();
			shape.graphics.clear();
			
			shape.graphics.lineStyle(1,0xFFFFFF,.1);
			
			for(var i:uint=0;i<5;i++){
			generateShape();
			if(i==0)shape.graphics.beginFill(col,trans);
			shape.graphics.curveTo(point1_5.x,point1_5.y,point1.x,point1.y);
			shape.graphics.curveTo(point2_5.x,point2_5.y,point2.x,point2.y);
			shape.graphics.curveTo(point3_5.x,point3_5.y,point3.x,point3.y);
			shape.graphics.curveTo(point4_5.x,point4_5.y,point4.x,point4.y);
			}
			if(i==0)shape.graphics.endFill();
			shape.x=xOffset;
			

			return (shape);
		}
		
		var sha:Shape;
		
		public function renderFadeShape(col:Number, trans:Number):Shape{
			sha = renderShape(col, trans);
			var fadeTimer:Timer=new Timer(30,5);
			fadeTimer.addEventListener( TimerEvent.TIMER, onfadeTimer);
			fadeTimer.start();
			return (sha);
		}
		
		private function onfadeTimer(e:TimerEvent){
			sha.alpha-=.2;
			if (sha.alpha==0){e.target.removeEventListener(TimerEvent.TIMER, onfadeTimer);e.target.stop;};
		}

	}

}