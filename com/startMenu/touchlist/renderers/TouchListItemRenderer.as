﻿/**
 * @author Michael Ritchie
 * @blog http://www.thanksmister.com
 * @twitter Thanksmister
 * Copyright (c) 2010
 * 
 * TouchListItemRendeer is the default item renderere, it implements ITouchListRenderer.  
 * You can make your own custom renderer by implementing ITouchListRenderer.
 * */
package com.startmenu.touchlist.renderers{
	import flash.display.Sprite;
	import flash.display.Shape;
	import flash.events.Event;
	import flash.events.MouseEvent;
	//import flash.filters.DropShadowFilter;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import com.startmenu.touchlist.events.ListItemEvent;
	import com.startmenu.touchlist.renderers.wiggleShape;

	public class TouchListItemRenderer extends Sprite implements ITouchListItemRenderer {
		protected var _data:Object;
		protected var _index:Number=0;
		protected var _itemWidth:Number=0;
		protected var _itemHeight:Number=0;
		protected var _colour:Number=0;
		protected var _fontColour:Number=0;

		protected var initialized:Boolean=false;
		protected var textField:TextField;
		var itemInfo:XML;
		//protected var shadowFilter:DropShadowFilter;

		//-------- properites -----------
		
		public function kill():void{
			removeEventListener(MouseEvent.MOUSE_UP, selectHandler);
			removeEventListener(MouseEvent.MOUSE_DOWN, pressHandler);
			if(myWiggleShape){myWiggleShape=null;}
			for (var i:uint=0;i<this.numChildren;i++){
				this.removeChildAt(i);
			}
			this.removeChild(textField);
			this.graphics.clear();
		
			trace("TouchListItemRenderer.kill(): dead");
		}

		public function get itemWidth():Number {
			return _itemWidth;
		}
		public function set itemWidth(value:Number):void {
			_itemWidth=value;
			draw();
		}

		public function get itemHeight():Number {
			return _itemHeight;
		}
		public function set itemHeight(value:Number):void {
			_itemHeight=value;
			//draw();
		}

		public function get data():Object {
			return _data;
		}
		public function set data(value:Object):void {
			_data=value;
			//draw();
		}

		public function get index():Number {
			return _index;
		}
		public function set index(value:Number):void {
			_index=value;
		}
		
		public function get colour():Number {
			return _colour;
		}
		public function set colour(value:Number):void {
			_colour=value;
		}
		
		public function get fontColour():Number {
			return _fontColour;
		}
		public function set fontColour(value:Number):void {
			_fontColour=value;
		}

		public function TouchListItemRenderer(info:XML) {
			itemInfo=info;
			addEventListener(MouseEvent.MOUSE_DOWN, pressHandler);

			createChildren();
		}

		
		 var selectedShape:Shape;
		 var colourChange:Number;
		 var fontColourChange:Number;
		 
		public function selectItem():void {
			addEventListener(MouseEvent.MOUSE_UP, selectHandler);
			selectedShape=myWiggleShape.renderFadeShape(itemInfo.hitColour,.9)
			this.addChild(selectedShape);
		}

		public function changeColour(col:Number){
			colourChange=col;
		}

		protected function createChildren():void {
			initialized=true;

		}

		var newHeight:uint;
		var myWiggleShape:wiggleShape;

		protected function draw():void {
			
			if (! initialized) {
				return;
			}
			else {
				if (!textField) {
				var textFormat:TextFormat = new TextFormat();
				textFormat.color=fontColour;
				textFormat.size=itemInfo.fontSize[0];
				textFormat.font="DroidSans";

				textField = new TextField();
				//textField.height = 22;
				textField.autoSize="center";
				textField.mouseEnabled=false;
				textField.defaultTextFormat=textFormat;
				textField.wordWrap=true;

				this.addChild(textField);
			}
				textField.x=5;
				textField.text=String(data);

				textField.width=itemWidth-10;


				if (itemHeight>textField.height) {
					newHeight=itemHeight;
				}
				else {
					newHeight=textField.height;
				}
				
				if (!colourChange)colourChange=colour;
				
				
				myWiggleShape=new wiggleShape(colourChange,itemWidth,newHeight);
				this.addChildAt(myWiggleShape.provideShape(),0);
			
				this.graphics.beginFill(0x000000, .5);
				this.graphics.drawRoundRect(0, -itemInfo.distanceBetween-1, itemWidth, .5,itemWidth/8);
				this.graphics.endFill();
			}

		}


		// ----- event handlers --------

		/**
		 * Dispatched when item is first pressed on tap or mouse down.
		 * */
		protected function pressHandler(e:Event):void {
			this.dispatchEvent( new ListItemEvent(ListItemEvent.ITEM_PRESS, this) );
		}

		/**
		 * Dispatched when item is selected, usually on touch end or mouse up.
		 * */
		protected function selectHandler(e:Event):void {
			this.dispatchEvent( new ListItemEvent(ListItemEvent.ITEM_SELECTED, this) );
			
		}
	}
}