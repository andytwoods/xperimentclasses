﻿/**
 * @author Michael Ritchie
 * @blog http://www.thanksmister.com
 * @twitter Thanksmister
 * Copyright (c) 2010
 * 
 * TouchList is an ActionScript 3 scrolling list for Android mobile phones. I used and modified some code
 * within the component from other Flex/Flash examples for scrolling lists by the following people or location:
 * 
 * Dan Florio ( polyGeek )
 * polygeek.com/2846_flex_adding-physics-to-your-gestures
 * 
 * James Ward
 * www.jamesward.com/2010/02/19/flex-4-list-scrolling-on-android-with-flash-player-10-1/
 * 
 * FlepStudio
 * www.flepstudio.org/forum/flepstudio-utilities/4973-tipper-vertical-scroller-iphone-effect.html
 * 
 * You may use this code for your personal or professional projects, just be sure to give credit where credit is due.
 * */
package com.startmenu.touchlist.controls{
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	import com.startmenu.touchlist.renderers.ITouchListItemRenderer;
	import com.startmenu.touchlist.events.ListItemEvent;


	public class TouchList extends Sprite {
		//------- List --------

		private var listHitArea:Shape;
		private var list:Sprite=new Sprite  ;
		private var listHeight:Number=100;
		private var listWidth:Number=100;
		private var scrollListHeight:Number;
		private var scrollAreaHeight:Number;
		private var listTimer:Timer;// timer for all events
		private var fadeTimer:Timer;// timer for all events

		//------ Scrolling ---------------

		private var lastY:Number=0;// last touch position
		private var firstY:Number=0;// first touch position
		private var listY:Number=0;// initial list position on touch 
		private var diffY:Number=0;
		private var inertiaY:Number=40;
		private var minY:Number=0;
		private var maxY:Number=0;
		private var totalY:Number;
		private var scrollRatio:Number=40;// how many pixels constitutes a touch

		//------- Touch Events --------

		private var isTouching:Boolean=false;
		private var tapDelayTime:Number=0;
		private var maxTapDelayTime:Number=5;// change this to increase or descrease tap sensitivity
		private var tapItem:ITouchListItemRenderer;
		private var tapEnabled:Boolean=false;
		private var generalListInfo:XML;
		private var listInfo:XML;
		private var headerBottom:uint;
		private var onScreenStuff:Array = new Array;
		// ------ Constructor --------
		
		public function kill(){
			this.removeEventListener( MouseEvent.MOUSE_UP, onMouseUp);
			this.removeEventListener( MouseEvent.MOUSE_MOVE, onMouseMove);
			if (!listItem) listItem.removeEventListener(Event.ADDED_TO_STAGE, boingWhenEnter);
			for (var i:uint=0;i<onScreenStuff.length;i++){
				if (onScreenStuff[i]){
					onScreenStuff[i].removeEventListener(ListItemEvent.ITEM_SELECTED, handleItemSelected);
					onScreenStuff[i].removeEventListener(ListItemEvent.ITEM_PRESS, handleItemPress);
					onScreenStuff[i]=null;
				}
			}
			onScreenStuff=null;
			if(listTimer)listTimer=null;
			trace("TouchList.kill(): Dead");
			
		}
		
		public function TouchList(hb:uint, width:uint,height:uint, hi:XML,hii:XML) {
			generalListInfo=hi;
			listInfo=hii;
			headerBottom=hb;

			scrollListHeight=uint(generalListInfo.distanceFromHeader[0]);
			listWidth=width;
			listHeight=height;
			scrollAreaHeight=listHeight;
			this.addEventListener(Event.ADDED_TO_STAGE, init);
		}



		private function init(e:Event):void {
			removeEventListener(Event.ADDED_TO_STAGE, init);
			listItem.addEventListener(Event.ADDED_TO_STAGE, boingWhenEnter);
			addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown );

			listTimer=new Timer(33);
			listTimer.addEventListener( TimerEvent.TIMER, onListTimer);
			listTimer.start();

			scrollListHeight=uint(generalListInfo.distanceFromHeader[0]);

			creatList();

		}
		
		private function boingWhenEnter(e:Event):void{
			inertiaY=40;
		}


		/**
		 * Create an empty list an the list hit area, which is also its mask.
		 * */
		private function creatList():void {
			if (! listHitArea) {
				listHitArea = new Shape();
				this.addChild(listHitArea);
				onScreenStuff.push(listHitArea);
			}

			listHitArea.graphics.clear();
			listHitArea.graphics.beginFill(listInfo.Colour[0], 1);
			listHitArea.graphics.drawRect(0, 0, listWidth, listHeight);
			listHitArea.graphics.endFill();
			if (! list) {
				list = new Sprite();
				this.addChild(list);
				onScreenStuff.push(list);
			}

			list.graphics.clear();
			list.graphics.beginFill(0xFFFFFF, 0);
			list.graphics.drawRect(0, 0, listWidth, listHeight);
			list.graphics.endFill();
			list.mask=listHitArea;
		}



		private var listItem:DisplayObject;

		public function addListItem(item:ITouchListItemRenderer):void {

			this.addChild(list);
			onScreenStuff.push(list);

			listItem=item as DisplayObject;
			listItem.y=scrollListHeight;
			listItem.addEventListener(ListItemEvent.ITEM_SELECTED, handleItemSelected);
			listItem.addEventListener(ListItemEvent.ITEM_PRESS, handleItemPress);

			//ITouchListItemRenderer(listItem).itemWidth=listWidth;

			scrollListHeight=scrollListHeight+listItem.height+uint(generalListInfo.distanceBetween);

			list.addChild(listItem);
			onScreenStuff.push(listItem);


		}

		var listOfRemoved:Array = new Array();

		public function removeListItem(index:Number):void {
			var counter:uint=0;
			for (var i:uint=0; i<=listOfRemoved.length; i++) {
				if (listOfRemoved[i]<index) {
					counter++;
				}
			}

			listOfRemoved.push(index);
			var item:DisplayObject=list.removeChildAt(index-counter);
			item.removeEventListener(ListItemEvent.ITEM_SELECTED, handleItemSelected);
			item.removeEventListener(ListItemEvent.ITEM_PRESS, handleItemPress);
		}

		/**
		 * Clear the list of all item renderers.
		 * */
		public function removeListItems():void {
			tapDelayTime=0;

			listTimer.stop();
			isTouching=false;
			scrollAreaHeight=0;
			scrollListHeight=0;

			while (list.numChildren > 0) {
				var item:DisplayObject=list.removeChildAt(0);
				item.removeEventListener(ListItemEvent.ITEM_SELECTED, handleItemSelected);
				item.removeEventListener(ListItemEvent.ITEM_PRESS, handleItemPress);
			}
		}

		// ------ private methods -------

		/**
		 * Detects frist mouse or touch down position.
		 * */
		protected function onMouseDown( e:Event ):void {
			addEventListener( MouseEvent.MOUSE_MOVE, onMouseMove );
			addEventListener( MouseEvent.MOUSE_UP, onMouseUp );
			removeEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);

			inertiaY=0;
			firstY=mouseY;
			listY=list.y;
			minY=Math.min(- list.y,- scrollListHeight+listHeight-list.y);
			maxY=- list.y;
		}

		/**
		 * List moves with mouse or finger when mouse down or touch activated. 
		 * If we move the list moves more than the scroll ratio then we 
		 * clear the selected list item. 
		 * */
		protected function onMouseMove( e:MouseEvent ):void {
			totalY=mouseY-firstY;

			if (Math.abs(totalY)>scrollRatio) {
				isTouching=true;
			}

			if (isTouching) {

				diffY=mouseY-lastY;
				lastY=mouseY;

				if (totalY < minY) {
					totalY = minY - (minY - totalY);
				}

				if (totalY > maxY) {
					totalY = maxY + (totalY - maxY);
				}

				list.y=listY+totalY;

				onTapDisabled();
			}
		}

		/**
		 * Handles mouse up and begins animation. This also deslects
		 * any currently selected list items. 
		 * */
		protected function onMouseUp( e:MouseEvent ):void {
			addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown );
			removeEventListener( MouseEvent.MOUSE_MOVE, onMouseMove );
			removeEventListener(MouseEvent.MOUSE_UP, onMouseUp);

			if (isTouching) {
				isTouching=false;
				inertiaY=diffY;
			}


		}

		/**
		 * Timer event handler.  This is always running keeping track
		 * of the mouse movements and updating any scrolling or
		 * detecting any tap events.
		 * 
		 * Mouse x,y coords come through as negative integers when this out-of-window tracking happens. 
		 * The numbers usually appear as -107374182, -107374182. To avoid having this problem we can 
		 * test for the mouse maximum coordinates.
		 * */
		private function onListTimer(e:Event):void {
			// test for touch or tap event
			if (tapEnabled) {
				onTapDelay();
			}

			// scroll the list on mouse up
			if (! isTouching) {

				if (list.y>0) {
					inertiaY=3;
					
					//list.y = 0; //added this
					list.y*=-0.1;  //AW the cup

					if (list.y<1) {
						inertiaY=-1.5;
						//list.y = 0;
						list.y*=-1 //added this
					}
				}
				else if (scrollListHeight >= listHeight && list.y < listHeight - scrollListHeight) {
					inertiaY=0;

					var diff:Number = (listHeight - scrollListHeight ) - list.y;


					if (diff > 1) {
						diff*=0.1;
					}

					list.y+=diff;
				}
				else if (scrollListHeight > listHeight && list.y > 0) { //AW flipped the greaterthan/lessthan signs here
					inertiaY=0;
					list.y*=0.8;

					if (list.y>-1) {
						list.y = 0;
					}
				}

				if (Math.abs(inertiaY)>1) {
					list.y+=inertiaY;
					inertiaY*=0.9;
				}
				else {
					inertiaY=0;
				}

			}
		}

		/**
		 * The ability to tab is disabled if the list scrolls.
		 * */
		protected function onTapDisabled():void {
			if (tapItem) {
				//tapItem.unselectItem();
				tapEnabled=false;
				tapDelayTime=0;
			}
		}

		/**
		 * We set up a tap delay timer that only selects a list
		 * item if the tap occurs for a set amount of time.
		 * */
		protected function onTapDelay():void {
			tapDelayTime++;

			if (tapDelayTime > maxTapDelayTime ) {
				tapItem.selectItem();
				tapDelayTime=0;
				tapEnabled=false;
				if (eBodge) {//sorry, crap coding.  Tried to remove this annoying custom EventListener but no luck.
					this.dispatchEvent(new ListItemEvent(ListItemEvent.ITEM_SELECTED, eBodge.renderer));
				}
			}

		}

		/**
		 * On item press we clear any previously selected item. We only
		 * allow an item to be pressed if the list is not scrolling.
		 * */

		var activateButton:Boolean=false;

		var eBodge:ListItemEvent;

		protected function handleItemPress(e:ListItemEvent):void {
			if (tapItem) {
				//tapItem.unselectItem();
				eBodge=null;
			}
			//unselect the prev item if it exists;

			e.stopPropagation();
			tapItem=e.renderer;

			tapDelayTime=0;
			tapEnabled=true;
			eBodge=e;




		}

		/**
		 * Item selection event fired from a item press.  This event does
		 * not fire if list is scrolling or scrolled after press.
		 * */
		protected function handleItemSelected(e:ListItemEvent):void {
			e.stopPropagation();
			tapItem=e.renderer;
			tapDelayTime=0;
			tapEnabled=false;
			//tapItem.unselectItem();
			eBodge=null;
			//removeListItem(tapItem.index);

		}


	}
}