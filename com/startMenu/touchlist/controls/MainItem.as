﻿package com.startmenu.touchlist.controls{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	//import flash.events.TimerEvent;
	//import flash.utils.Timer;
	import com.startmenu.touchlist.renderers.wiggleShape;

	public class MainItem extends Sprite {

		public var index:Number=0;
		var itemWidth:Number=0;
		var itemHeight:Number=0;
		public var data:Array = new Array();
		var headInfo:XML;



		private var listHeight:Number=100;
		private var listWidth:Number=100;
		private var sha:Sprite=new Sprite  ;

		private var textField:TextField;

		public function kill(){
			myWiggleShape=null;
			sha=null;
			trace("localDatabase.kill(): dead");
		}


		function createItem():void {

			var textFormat:TextFormat = new TextFormat();
			textFormat.color=data.fontColour;
			textFormat.size=data.fontSize;
			textFormat.font="DroidSans";

			textField = new TextField();
			//textField.autoSize="center";
			textField.mouseEnabled=false;
			textField.defaultTextFormat=textFormat;
			textField.wordWrap=true;
			
			textField.x=5;
			textField.text=headInfo.theTitle[0];
			textField.height=textField.textHeight;
			textField.width=headInfo.width[0]-10;
			textField.y=itemHeight/2-textField.textHeight/2;



			this.addChild(sha);
			this.addChild(textField);
			draw();
		}

		public function MainItem(hi:XML) {
			headInfo=hi;
			itemWidth=headInfo.width[0];
			itemHeight=headInfo.height[0];
			data["shapeColour"]=headInfo.colour[0];
			data["fontSize"]=headInfo.fontSize[0];
			data["fontColour"]=headInfo.fontColour[0];

			addEventListener(Event.ADDED_TO_STAGE, init);

		}


		private function init(e:Event):void {
			removeEventListener(Event.ADDED_TO_STAGE, init);
			createItem();

		}


		var myWiggleShape:wiggleShape;

		function draw():void {

				myWiggleShape=new wiggleShape(data.shapeColour,itemWidth,itemHeight);
			myWiggleShape.setWiggles(wiggleHor,wiggleVer);
			sha.addChildAt(myWiggleShape.provideShape(),0);
			

		}

		public function setWiggles(hor:Number,ver:Number) {
			wiggleVer=ver;
			wiggleHor=hor;
		}
		var wiggleHor:Number=.15;
		var wiggleVer:Number=.015;


	}

}