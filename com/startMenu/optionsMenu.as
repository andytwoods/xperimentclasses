﻿
package com.startMenu{
	import flash.display.Stage;
	import flash.events.Event;
	import flash.text.TextField;
	import flash.events.MouseEvent;
	import flash.display.Sprite;
	import flash.display.Shape;
	import com.startmenu.touchlist.controls.MainItem;
	import flash.events.TimerEvent;
	import flash.utils.Timer;

	public class optionsMenu extends Sprite {
		private var the_Sta:Stage;
		public var menuInfo:XML;
		private var sp:Shape;
		private var listTimer:Timer;// timer for all events
		private var fadeTimer:Timer;
		private var fadeInTimer:Boolean=true;

		public var data:Array=new Array  ;

		public function kill(){
			sp=null;
			for (var i:uint=0;i<itemHolder.length;i++){
				tempMainItem.removeEventListener(MouseEvent.MOUSE_DOWN, optionClicked);
				tempMainItem.removeEventListener(MouseEvent.CLICK, optionReleased);
			}
			fadeTimer=null;
			trace("optionsMenu.kill: dead");
		}

		public function optionsMenu(sta:Stage, m:XML) {
			the_Sta=sta;
			menuInfo=m;
		}

		public var header:MainItem;

		var i:uint=0;
		var tempMainItem:MainItem;


		var list:Array=new Array();
		var itemHolder:Array;

		public function init() {
			this.x=-the_Sta.stageWidth*1.3;

			sp=new Shape  ;
			sp.graphics.lineStyle(55,menuInfo.backgroundColour[0],.8);
			sp.graphics.beginFill(Number(menuInfo.backgroundColour[0]),Number(menuInfo.backgroundTransparency[0]));
			sp.graphics.lineTo(the_Sta.stageWidth, 0);
			sp.graphics.lineTo(the_Sta.stageWidth*2, the_Sta.stageHeight/2);
			sp.graphics.lineTo(the_Sta.stageWidth, the_Sta.stageHeight);
			sp.graphics.lineTo(0, the_Sta.stageHeight);
			sp.graphics.lineTo(0, 0);
			sp.graphics.endFill();
			this.addChild(sp);
			
			itemHolder = new Array;

			for (i=0; i<menuInfo.menu.length(); i++) {
				tempMainItem=new MainItem(menuInfo.menu[i]);
				tempMainItem.setWiggles(.06,.025);
				tempMainItem.index=i;
				if (menuInfo.menu[i].x=="") {
					tempMainItem.x=(the_Sta.stageWidth-menuInfo.menu[i].width)/2;
				}
				else {
					tempMainItem.x=uint(menuInfo.menu[i].x);
				}
				tempMainItem.y=uint(menuInfo.menu[i].y);
				tempMainItem.addEventListener(MouseEvent.MOUSE_DOWN, optionClicked);
				tempMainItem.addEventListener(MouseEvent.CLICK, optionReleased);
				this.addChild(tempMainItem);
				itemHolder.push(tempMainItem);
			}
			fadeInTimer=true;
			doTimer();
		}
		
		private function doTimer(){
			fadeTimer=new Timer(30,uint(this.width/5));
			fadeTimer.addEventListener(TimerEvent.TIMER, fadeTimerAction);
			fadeTimer.start();
		}


		private function fadeTimerAction(e:TimerEvent) {
			if (this.x<0 && fadeInTimer) {
				this.x+=the_Sta.stageWidth*1.3/5;
			}
			else if(this.x>-this.width && !fadeInTimer) {
				this.x-=the_Sta.stageWidth*1.3/5;
			}
			else if (fadeTimer) {
				fadeTimer.stop();
				fadeTimer.removeEventListener(TimerEvent.TIMER, fadeTimerAction);
				fadeInTimer=true;
			}
		}

		public function removeFromScreen() {
			fadeInTimer=false;
			doTimer();
		}



		private function optionClicked(e:MouseEvent) {
			tempMainItem=e.target.parent;
			listTimer=new Timer(500);
			listTimer.addEventListener(TimerEvent.TIMER, onListTimer);
			listTimer.start();
		}

		private function optionReleased(e:MouseEvent) {
			this.removeEventListener( TimerEvent.TIMER, onListTimer);
			listTimer.stop();
		}

		private function onListTimer(e:TimerEvent) {
			listTimer.stop();
			listTimer.addEventListener(TimerEvent.TIMER, onListTimer);
			tempMainItem.removeEventListener(MouseEvent.CLICK, optionReleased);
			buttonSelected=tempMainItem.index;
			handlelistItemSelected(tempMainItem.index);
		}

		public function optionSelected():String {
			return menuInfo.menu[buttonSelected].theTitle;
			trace("Options Menu Button Clicked");
		}

		var buttonSelected:uint;

		var tempString:String;
		private function handlelistItemSelected(e:uint):void {
			tempString="";
			//trace(menuInfo.menu[e]);

			switch (tempString) {

				case "" :
					this.dispatchEvent(new Event("optionsMenuButtonPressed"));
					trace("event optionsMenuButtonPressed dispatched");
					break;

				case "nothing" :
					trace("unclickable");
					break;



			}
		}

	}
}