﻿package com.startMenu{
	import flash.filesystem.File;
	import flash.filesystem.FileStream;
	import flash.filesystem.FileMode;
	import com.bulkloader.*;
	import com.startMenu.saveFilesLocally;
	import flash.events.*
	import flash.display.Stage;
	import flash.display.Sprite;
	
	public class getFilesFromWeb extends Sprite {
		var loader:BulkLoader;
		var exptFilesXML:XML;
		public var dir_Str:String;
		public var actionAfterSaving:String;
		
		public function kill(){
			trace("getFilesFromWeb.kill(): nothing to kill");
		}
		
		
		function getFilesFromWeb(myXML:XML,act:String){
			exptFilesXML=myXML;
			actionAfterSaving=act;
			loader=new BulkLoader(myXML.localDir);
			loader.addEventListener(BulkLoader.ERROR, onAllError);
			loader.addEventListener(BulkLoader.PROGRESS, onAllItemsProgress);
			loader.addEventListener(BulkLoader.COMPLETE, onFilesLoaded);
			
			var remoteDirectory:String=exptFilesXML.remoteDir[0];
			var maximumTries:String=exptFilesXML.maxTriesAtSaving[0];
			var files:Array = exptFilesXML.files[0].split(",");
			
			for (var i:uint=0;i<files.length;i++){
				loader.add(remoteDirectory+files[i],{id:files[i], maxTries:maximumTries, type:BulkLoader.TYPE_BINARY});
			}
			
			loader.start();			
		}
		
		function onFilesLoaded(evt : BulkProgressEvent):void {
			loader.removeEventListener(BulkLoader.ERROR, onAllError);
			loader.removeEventListener(BulkLoader.PROGRESS, onAllItemsProgress);
			loader.removeEventListener(BulkLoader.COMPLETE, onFilesLoaded);
			
			var putFilesOnPhone:saveFilesLocally = new saveFilesLocally();
			this.addChild(putFilesOnPhone);
			putFilesOnPhone.setup(loader,exptFilesXML,actionAfterSaving);
			this.removeChild(putFilesOnPhone);
			
			var storageLocationOnPhone:String = putFilesOnPhone.storageLocation;
			putFilesOnPhone=null;
		}

		public function onAllItemsProgress(evt : BulkProgressEvent):void {
			trace(evt.loadingStatus());
		}
		
		function onAllError(evt :ErrorEvent ) : void{
     	   trace ("error loading files unfortunately: "+evt); // outputs more information 
		}
		
		
	}
	
}
