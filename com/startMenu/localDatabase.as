﻿package com.startMenu{
	import flash.data.SQLConnection;
	import flash.data.SQLStatement;
	import flash.data.SQLMode;
	import flash.data.SQLResult;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.display.StageAlign;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	import flash.filesystem.File;
	import flash.events.Event;
	import com.startMenu.getFilesFromWeb;
	import com.startMenu.saveFilesLocally;
	import flash.filesystem.File;


	public class localDatabase extends Sprite {
		private var _connection:SQLConnection;
		private var file:File;
		private var actionAfterSaving:String;
		private var exptFilesXML:XML;
		private var mostRecentScriptXML:XML;
		private var getAndSaveFiles:getFilesFromWeb;
		private var putFilesOnPhone:saveFilesLocally;

		public function kill() {
			try {
				_connection.close();
				getAndSaveFiles.kill();
				putFilesOnPhone.kill();
				getAndSaveFiles=null;
				putFilesOnPhone=null;
				trace("localDatabase.kill(): dead");

			} catch (error:Error) {
				trace(error.message);
			}

		}

		public function init():void {

			_connection = new SQLConnection();
			_connection.addEventListener(SQLEvent.OPEN, openHandler);
			_connection.addEventListener(SQLErrorEvent.ERROR, errorHandler);

			// create/open database;
			file=File.applicationStorageDirectory.resolvePath("myData.db");
			if (file.exists) {
				trace("db already exists, ready to be used");
				_connection.open(file);
			}
			else {
				trace("db did not exist so has been created");
				createTable();
			}
		}

		private function createTable():void {
			_connection.open(file);
			var statement:SQLStatement = new SQLStatement();
			statement.sqlConnection=_connection;
			var request:String =
			"CREATE TABLE IF NOT EXISTS experiments ("
			+ "id INTEGER PRIMARY KEY AUTOINCREMENT, exptID TEXT, url TEXT, name TEXT, description TEXT, script TEXT )";

			statement.text=request;

			try {
				statement.execute();

			} catch (error:Error) {
				trace(error.message);
			}
		}

		public function mostRecentScript(action:String) {

			if (action=="saved") {
				return mostRecentScriptXML;
			}
		}

		public function retrieveScript(row):XML {
			var statement:SQLStatement = new SQLStatement();
			var returnXML:XML=new XML  ;
			statement.sqlConnection=_connection;
			statement.text="SELECT * FROM experiments where exptID = :id";

			statement.parameters[":id"]=listOfExptIDs[row];

			try {
				statement.execute();

				var result:SQLResult=statement.getResult();
				if (result.data!=null) {
					returnXML=XML(result.data[0].script);
				}
			} catch (error:Error) {
				trace("error: ",error.message);
			}
			return returnXML;
		}



		function openHandler(event:SQLEvent):void {
			_connection.removeEventListener(SQLEvent.OPEN, openHandler);
			_connection.removeEventListener(SQLErrorEvent.ERROR, errorHandler);
			try {

				trace("connection opened");
				//createTable();
				checkTable();
			} catch (error:Error) {
				trace(error.message);
			}
		}

		function errorHandler(event:SQLErrorEvent):void {
			_connection.removeEventListener(SQLEvent.OPEN, openHandler);
			_connection.removeEventListener(SQLErrorEvent.ERROR, errorHandler);
			trace("Error message:", event.error.message);
			trace("Details:", event.error.details);
		}

		public function action(Str:String) {
			actionAfterSaving=Str;
		}

		public function addExpt(exptID:String, URL:String, theTitle:String, Description:String, Script:XML):void {

			exptFilesXML=Script.SETUP[0];
			if (String(Script.SETUP).length!=0&&exptFilesXML.remoteDir.length()==0) {
				var newURL:String="";
				var tempArr:Array=URL.split("/");
				for (var i:uint = 0; i < tempArr.length-1; i++) {
					newURL=(newURL+tempArr[i]+"/");
					exptFilesXML.remoteDir=newURL;
				}
			}
			
			//adding to the Script where it's files are stored
			if (String(Script.SETUP).length!=0&&exptFilesXML.localDir.length()==0) {
				exptFilesXML.localDir=exptID;
			}
			
			Script.SETUP.explicitLocalDir=File.applicationStorageDirectory.resolvePath(exptFilesXML.localDir[0]).nativePath+String.fromCharCode(47);

			var statement:SQLStatement = new SQLStatement();
			statement.sqlConnection=_connection;
			var insert:String="INSERT INTO experiments (exptID, url, name, description, script) VALUES ('"+
			exptID+"', '"+URL+"', '"+theTitle+"', '"+Description+"', '"+escape(String(Script))+"')";
			statement.text=insert;
			//trace(insert);
			try {
				statement.execute();
				checkTable();

				trace("added Item to Database");
				if (String(Script.SETUP).length!=0) {
					getAndSaveFiles=new getFilesFromWeb(exptFilesXML,actionAfterSaving);
					mostRecentScriptXML=Script;
					this.addChild(getAndSaveFiles);
				}
			} catch (error:Error) {
				mostRecentScriptXML=null;
				trace(error.message);

			}
		}

		public function checkTable():void {
			var statement:SQLStatement = new SQLStatement();
			statement.sqlConnection=_connection;
			statement.text="SELECT * FROM experiments";

			statement.addEventListener(SQLEvent.RESULT, selectionReceived);
			statement.execute();
		}


		public var listOfExperimentNamesAndDescriptions:Array=new Array  ;
		var tempArr:Array;

		private function selectionReceived(event:SQLEvent):void {
			var stat:SQLStatement=event.currentTarget as SQLStatement;
			stat.removeEventListener(SQLEvent.RESULT, selectionReceived);
			listOfExptIDs=new Array  ;
			listOfExperimentNamesAndDescriptions=new Array  ;
			var rows:int=0;
			var result:SQLResult=stat.getResult();

			if (result != null) {

				if (result.data!=null) {
					rows=result.data.length;
				}

				for (var i:int = 0; i < rows; i++) {
					tempArr=new Array  ;
					var row:Object=result.data[i];
					//trace("loaded: "+row.exptID);
					listOfExperimentNamesAndDescriptions.push(row.name);
					listOfExptIDs.push(row.exptID);
				}

			}
			//_connection.close();
			this.dispatchEvent(new Event("exptListGen"));
		}

		var listOfExptIDs:Array;

		public function deleteItem(idDescription:String,row:uint):void {
			var statement:SQLStatement = new SQLStatement();
			statement.sqlConnection=_connection;
			statement.text="DELETE FROM experiments where name = :nm";
			statement.parameters[":nm"]=idDescription;
			try {
				deleteFiles(row);
				statement.execute();
				trace("experiment "+idDescription+ " removed");
				//_connection.close();
			} catch (error:Error) {
				trace("item",error.message);
			}
		}

		private function deleteFiles(row:uint) {
			var mylocation:String=retrieveScript(row).SETUP.localDir;
			putFilesOnPhone = new saveFilesLocally();
			putFilesOnPhone.deleteDirectory(mylocation);
			putFilesOnPhone=null;
		}
	}
}