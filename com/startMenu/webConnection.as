﻿package com.startMenu{
	import flash.display.Sprite;
	import flash.display.BitmapData;
	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.events.Event;
	import flash.net.URLRequest;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	import flash.display.Stage;
	
	public class webConnection extends getImage{
		var myBmp:Bitmap;
		var addAfterLoaded:Boolean=false;
		var onScreen:Boolean=false;
		private var webTimer:Timer;
		private var fadeTimer:Timer;
		private var the_Sta:Stage;
		private var yPos:uint;
		private var xPos:uint;

		public function webConnection(img:String,hei:String,wid:String,the_Stage:Stage,xP:String, yP:String) {
			the_Sta=the_Stage;
			yPos=uint(yP);
			xPos=uint(xP);


			
			the_Sta.addEventListener("dataStartLoading",webAction,true);
			the_Sta.addEventListener("dataLoading",webAction,true);
			the_Sta.addEventListener("dataLoaded",webAction,true);
			//the_Sta.addEventListener("dataNotLoaded",webAction,true);
			webTimer=new Timer(250);
			webTimer.addEventListener( TimerEvent.TIMER, onWebTimer);
			super(img,uint(hei),uint(wid));
			
		}
		
		override public function addImage(bmp:Bitmap){
			myBmp=bmp;
			myBmp.x=xPos;
			myBmp.y=yPos;
			if (addAfterLoaded) add();
		}
		
		public function addToStage(){
			
			if (myBmp){
				add();
				}
			else addAfterLoaded = true;
			
		}
		
		public function webAction(e:Event){
			
			trace(e.target);
			if (onScreen) {
				myBmp.rotation+=5;}
			if (!onScreen){myBmp.rotation=0; add();}
			webTimer.reset();
			webTimer.start();
			
		}
		
		public function add(){
			if (fadeTimer && fadeTimer.running){this.removeChild(myBmp);} 
			myBmp.alpha=1;
			the_Sta.addChildAt(myBmp,1);
			onScreen=true;
		}
		
		public function remove(){
			fade();
		}
		
		private function onWebTimer(e:TimerEvent){
			webTimer.stop(); webTimer.reset(); onScreen=false; 
			remove();
		}
		
		private function fade(){
			if (!fadeTimer)	fadeTimer=new Timer(30,15); 
			if (!fadeTimer.running){
				fadeTimer.addEventListener(TimerEvent.TIMER, fadeTimerAction); 
				myBmp.alpha=1;
				fadeTimer.reset();
				fadeTimer.start();}
		}
		
		private function fadeTimerAction(e:TimerEvent){
			if (!fadeTimer.running){this.removeChild(myBmp);onScreen=false;}
			else myBmp.alpha-=.08;
			myBmp.rotation+=5;
			//trace("here");
		}
		
		

	}
	
}
