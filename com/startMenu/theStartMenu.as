﻿package com.startMenu{
	import flash.desktop.NativeApplication;
	import flash.desktop.SystemIdleMode;
	import flash.display.Stage;
	import flash.display.StageAlign;
	import flash.display.StageOrientation;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.StageOrientationEvent;
	import flash.system.Capabilities;
	import flash.ui.Keyboard;
	import flash.events.MouseEvent;
	import flash.display.Sprite;
	import com.startmenu.touchlist.renderers.TouchListItemRenderer;
	import com.startmenu.touchlist.events.ListItemEvent;
	import com.startmenu.touchlist.controls.TouchList;
	import com.startmenu.touchlist.controls.MainItem;
	import com.startMenu.startMenuItem;
	import com.startMenu.startMenuLoadExptList;
	import runnerANDROID;
	import com.startMenu.localDatabase;
	import importExptScript;
	import flash.display.BitmapData;
	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.display.StageDisplayState;
	import com.startMenu.webConnection;
	import com.startMenu.getImage;
	import importExptScript;


	public class theStartMenu extends Sprite {
		private var the_Sta:Stage;
		private var menuList:Array=new Array  ;
		private var i:uint;
		private var tempData:importExptScript;
		private var menuSpec:XML;
		var storedExptNames:Array=new Array  ;
		var myStartMenu:theStartMenu;
		var localDB:localDatabase;
		var tempImportData:importExptScript;
		var myWebConnectionIndicator:webConnection;
		private var myRetrievedExpt:XML;
		var background:getImage;
		
		public function kill(){
			if(myWebConnectionIndicator)this.removeChild(myWebConnectionIndicator);
			if(background) this.removeChild(background);
			if(menuList){
				for (var i:uint=0;i<menuList.length;i++){
					menuList[i].kill();
					menuList[i]=null;
				}
			}
			trace("theStartMenu.kill(): dead");
		}
		

		function theStartMenu(sta:Stage) {
			the_Sta=sta;
			tempData=new importExptScript("menu.xml");
			tempData.addEventListener("dataLoaded",continueAfterLoadingScripts);
			tempData.addEventListener("dataNotLoaded",continueAfterNotLoadingScripts);
		}
		

		function continueAfterLoadingScripts(e:Event) {
			tempData.removeEventListener("dataLoaded",continueAfterLoadingScripts);
			tempData.removeEventListener("dataNotLoaded",continueAfterNotLoadingScripts);
			menuSpec=tempData.giveMeData();
			
			the_Sta.scaleMode=StageScaleMode.NO_SCALE;
			the_Sta.align=StageAlign.TOP_LEFT;
			the_Sta.displayState=StageDisplayState.FULL_SCREEN_INTERACTIVE;
			sortOutPercentages();

			if (Capabilities.screenResolutionX<800) {
				background=new getImage("background.jpg",flash.system.Capabilities.screenResolutionY,flash.system.Capabilities.screenResolutionX);
			}
			else {
				background=new getImage("background.jpg",the_Sta.stageHeight,the_Sta.stageWidth);
			}

			this.addChild(background);
			background.y=0;

			myWebConnectionIndicator=new webConnection("web.png",menuSpec.internetConnectionPicInfo.width[0],
													   menuSpec.internetConnectionPicInfo.height[0],
													   the_Sta,menuSpec.internetConnectionPicInfo.x[0],menuSpec.internetConnectionPicInfo.y[0]);
			this.addChild(myWebConnectionIndicator);


			the_Sta.addEventListener("actionRequired",optionsMenuButtonPressed);

			the_Sta.addEventListener(KeyboardEvent.KEY_DOWN, handleKeyDown);
			if (Capabilities.cpuArchitecture=="ARM") {
				NativeApplication.nativeApplication.addEventListener(Event.ACTIVATE, handleActivate, false, 0, true);
				NativeApplication.nativeApplication.addEventListener(Event.DEACTIVATE, handleDeactivate, false, 0, true);
			}

			for (i=0; i<menuSpec.menu.length(); i++) {
				this_menu=new startMenuItem(the_Sta,menuSpec.menu[i]);
				sortOutMenuCreation(this_menu);
			}
			for (i=0; i<menuSpec.menuLoadExptList.length(); i++) {
				this_menu=new startMenuLoadExptList(the_Sta,menuSpec.menuLoadExptList[i]);
				sortOutMenuCreation(this_menu);
			}

			localDB = new localDatabase();
			this.addChild(localDB);
			localDB.addEventListener("exptListGen",populateMenu);
			localDB.init();

		}

		function populateMenu(e:Event) {
			localDB.removeEventListener("exptListGen",populateMenu);
			storedExptNames=new Array;
			storedExptNames=e.target.listOfExperimentNamesAndDescriptions;
			storedExptNames=removeDuplicatesAndSort(storedExptNames);
			if (storedExptNames.length!=0) {
				updateMenu("done experiments",storedExptNames);
				menuSpec.menuLoadExptList.exptsInMemory=storedExptNames.join(";");// a bodge to pass on this info to another menu
				menuList[menuSpec.menu.header.(theTitle=="done experiments").childIndex()].resetMenu();
			}
		}
		
		function removeDuplicatesAndSort(arr:Array):Array {
			arr.sort();
			var i:int=0;
			while (i < arr.length) {
				while (i < arr.length+1 && arr[i] == arr[i+1]) {
					arr.splice(i, 1);
				}
				i++;
			}
			return arr;
		}


		var actionArr:Array;

		function optionsMenuButtonPressed(e:Event) {
			e.stopImmediatePropagation();
			e.target.removeOptionsMenu();
			actionArr=e.target.returnButtonInfo();

			switch (actionArr.menu) {
				
				case "done experiments" :   
					switch (actionArr.optionsMenuButtonPressed) {
						case "Delete from phone" :
							localDB.deleteItem(actionArr.menuItem,actionArr.menuButtonPressed);
							e.target.deleteMenuItem(actionArr.menuButtonPressed);
							break;
						case "Run the experiment" :
						myRetrievedExpt=localDB.retrieveScript(actionArr.menuButtonPressed);
						this.dispatchEvent(new Event("here's a script!",true));
							break;
					}
					break;

				case "New experiments" :
					switch (actionArr.optionsMenuButtonPressed) {
						case "Save for later" :
							localDB.action("doNothing");
							saveExpttoDB();
							break;
						case "Run the experiment" :
							e.target.removeList();
							localDB.action("tellMeWhenReady");
							saveExpttoDB();
							break;
					}
					break;
			}

		}
		
		public function myScript():XML{
			var returnStr:String;
			if ("done experiments"==actionArr.menu){
				returnStr =  unescape(myRetrievedExpt);
			}
			else if("New experiments"==actionArr.menu){
				returnStr = unescape(localDB.mostRecentScript("saved"));
			}
			returnStr=returnStr.split('&amp;').join('&');//stupid bodge as unescape cannot revert back some characters
			returnStr=returnStr.split('&#xD;').join(String.fromCharCode(13));
			//trace("fff: "+returnStr);
			
			return XML(returnStr);
		}
		
		
		private function saveExpttoDB() {

			buttonPressedInfo=new Array  ;
			buttonPressedInfo.menu=actionArr.menu;
			buttonPressedInfo.menuItem=actionArr.menuButtonPressed;
			buttonPressedInfo.id=menuSpec.*.header.(theTitle==actionArr.menu).parent().menuItem[actionArr.menuButtonPressed].id[0];
			buttonPressedInfo.url=menuSpec.*.header.(theTitle==actionArr.menu).parent().menuItem[actionArr.menuButtonPressed].url[0];
			buttonPressedInfo.theTitle=menuSpec.*.header.(theTitle==actionArr.menu).parent().menuItem[actionArr.menuButtonPressed].theTitle[0];
			buttonPressedInfo.description=menuSpec.*.header.(theTitle==actionArr.menu).parent().menuItem[actionArr.menuButtonPressed].description[0];


			menuOnScreen=false;
			tempImportData=new importExptScript(buttonPressedInfo.url);
			tempImportData.addEventListener("dataLoaded",addToDatabaseAfterLoaded);
			the_Sta.addChild(tempImportData);
			trace("attempting to add this File to the db:"+buttonPressedInfo.url);
		}

		function addToDatabaseAfterLoaded(e:Event) {
			tempImportData.removeEventListener("dataLoaded",addToDatabaseAfterLoaded);
			delete (menuSpec.menu.header.(theTitle=="done experiments").parent().menuItem);
			the_Sta.removeChild(tempImportData);
			trace("experiment Added");
			localDB.addEventListener("exptListGen",populateMenu);
			var exptScriptToAdd:XML=tempImportData.giveMeData();
			localDB.addExpt(buttonPressedInfo.id, buttonPressedInfo.url, buttonPressedInfo.theTitle, buttonPressedInfo.description,exptScriptToAdd);
			localDB.checkTable();

		}

		public function updateMenu(m:String, arr:Array) {

			for (i=0; i<arr.length; i++) {
				menuSpec.menu.header.(theTitle==m).parent().appendChild(
						<menuItem action="loadResponseMenu"><theTitle>{arr[i]}</theTitle></menuItem>);
			}
		}

		var this_menu:startMenuItem;

		var tempString:String;
		var tempInt:int;



		private function sortOutMenuCreation(me:startMenuItem) {
			this.addChild(me);
			me.init();
			me.header.addEventListener(MouseEvent.CLICK,showMenu);
			menuList.push(me);
		}

		var temp:startMenuItem;
		var menuOnScreen:Boolean=false;

		private function showMenu(ev:MouseEvent) {
			if (String(ev.target.parent.parent).substring(8,17)=="startMenu"&&ev.target.parent.parent!=null) {
				for (var i:uint=0; i<menuList.length; i++) {
					menuList[i].removeList();
					menuOnScreen=false;
				}

				if (ev.target.parent.parent!=temp) {
					temp=ev.target.parent.parent;
					temp.showList();

					menuOnScreen=true;
				}
				else {
					temp=null;
				}
			}
		}


		var buttonPressedInfo:Array;

		public function returnActionInfo():Array {
			return buttonPressedInfo;
		}

		public function returnButtonInfo():Array {
			return buttonPressedInfo;
		}

		private function handleActivate(event:Event):void {
			NativeApplication.nativeApplication.systemIdleMode=SystemIdleMode.KEEP_AWAKE;
		}

		private function handleDeactivate(event:Event):void {
			NativeApplication.nativeApplication.exit();
		}

		/**
		 * Handle keyboard events for menu, back, and seach buttons.
		 * */
		private function handleKeyDown(e:KeyboardEvent):void {
			if (e.keyCode==Keyboard.BACK) {
				e.preventDefault();
				NativeApplication.nativeApplication.exit();
			}
			else if (e.keyCode == Keyboard.MENU) {
				e.preventDefault();
			}
			else if (e.keyCode == Keyboard.SEARCH) {
				e.preventDefault();
			}
		}

		private function sortOutPercentages() {
			//if(menuSpec..*.toString().indexOf("%")!=-1));
			//trace(menuSpec.descendants().(x==50));

			var castNodes:XMLList = menuSpec..*.(y.toString().indexOf("%") != "-1");
			for each (var castNode:XML in castNodes) {
				tempInt=int(castNode.y.toString().slice(0,-1))/100*the_Sta.stageHeight;
				castNode.y=tempInt;
			}

			castNodes = menuSpec..*.(x.toString().indexOf("%") != "-1");
			for each (castNode in castNodes) {
				tempInt=int(castNode.x.toString().slice(0,-1))/100*the_Sta.stageWidth;
				castNode.x=tempInt;
			}

			castNodes = menuSpec..*.(width.toString().indexOf("%") != "-1");
			for each (castNode in castNodes) {
				tempInt=int(castNode.width[0].slice(0,-1))/100*the_Sta.stageWidth;
				castNode.width=tempInt;
			}

			castNodes = menuSpec..*.(height.toString().indexOf("%") != "-1");
			for each (castNode in castNodes) {
				tempInt=int(castNode.height[0].slice(0,-1))/100*the_Sta.stageHeight;
				castNode.height=tempInt;
			}
		}
		
		function continueAfterNotLoadingScripts(e:Event) {
			tempData.removeEventListener("dataLoaded",continueAfterLoadingScripts);
			tempData.removeEventListener("dataNotLoaded",continueAfterNotLoadingScripts);
			trace("you have forgotten to add 'menu.xml' to the experiment file");
		}

	}
}