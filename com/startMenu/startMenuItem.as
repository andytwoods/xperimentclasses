﻿/**
 * @author Michael Ritchie
 * @blog http://www.thanksmister.com
 * @twitter Thanksmister
 * Copyright (c) 2010
 * 
 * This is a Flash application to test the TouchList component.
 * */
package com.startMenu{
	import flash.desktop.NativeApplication;
	import flash.desktop.SystemIdleMode;
	import flash.display.MovieClip;
	import flash.display.Stage;
	import flash.display.StageAlign;
	import flash.display.StageOrientation;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.StageOrientationEvent;
	import flash.system.Capabilities;
	import flash.text.TextField;
	import flash.ui.Keyboard;
	import flash.events.MouseEvent;
	import flash.display.Sprite;
	import com.startmenu.touchlist.renderers.TouchListItemRenderer;
	import com.startmenu.touchlist.events.ListItemEvent;
	import com.startmenu.touchlist.controls.TouchList;
	import com.startmenu.touchlist.controls.MainItem;

	public class startMenuItem extends Sprite {
		private var stageOrientation:String=StageOrientation.DEFAULT;
		public var the_Sta:Stage;
		public var menuInfo:XML;
		public var hitCount=0;
		private var stuffOnScreen:Array=new Array;
		
	
/*
 		function clicksAgain(){
			showList();
		}
*/

		public function kill() {
			actionInfo=null;
			this.removeChild(header);
			if (header){header.kill(); header=null;}
			if (touchList){touchList.kill(); 
			touchList.removeEventListener(ListItemEvent.ITEM_SELECTED, handlelistItemSelected);
			touchList=null;
			}
			
			for (var i:uint=0;i<stuffOnScreen.length;i++){
				stuffOnScreen[i].kill();
			}
			stuffOnScreen=null;
			
			if (myOptionsMenu) {myOptionsMenu.kill(); myOptionsMenu=null;}
			
			try {


			} catch (error:Error) {
				trace(error.message);
			}

		}

		public var data:Array=new Array  ;
		var menuData:Array=new Array  ;

		private var actionInfo:Array = new Array;

		private function optionsMenuButtonPressed(e:Event){
			actionInfo.optionsMenuButtonPressed=e.target.optionSelected(); //optionMenu
			actionInfo.menu=menuInfo.header[0].theTitle.toString();
			actionInfo.menuButtonPressed=buttonPressed;
			actionInfo.menuItem=buttonPressedStr;
			this.dispatchEvent(new Event("actionRequired",true));
		}

		public function returnButtonInfo(){
			return actionInfo;
		}
		

		public function startMenuItem(sta:Stage, m:XML) {
			the_Sta=sta;
			menuInfo=m;
			menuData.MainItemText="";
		}

		public var header:MainItem;
		public var menuName:String;
		public var buttonPressed:uint;
		public var buttonPressedStr:String;

		public function init() {
			menuName=menuInfo.header[0].theTitle.toString();
			header=new MainItem(menuInfo.header[0]);
			header.y=menuInfo.header.y;
			header.x=menuInfo.header.x;
			this.addChild(header);
		}


		public function showList() {
			if (this.numChildren==1) {
				addMenu();
				
			}
			else {
				removeList();
			}
		}
		


		public function removeList() {
			if (this.numChildren==2) {
				this.removeChildAt(1);
			}
		}

		var touchList:TouchList;
		var i:uint=0;

		public function addMenu() {
			if (!touchList) initialTouchList();
			else{
			addChildHeader()
			}
		}
		
		public function initialTouchList(){
			setupList();
			addChildHeader();
		}
		
		public var showMenu:Boolean=true; 
		
		
		private function addChildHeader(){
			if (showMenu) {
			this.addChild(touchList);
			}
			showMenu=true;
		}

		public function setupList(){

			if (menuInfo.menuItem[0] == null) menuInfo.appendChild(<menuItem action="clicksAgain"><theTitle>nothing stored here afraid</theTitle></menuItem>);
			
			touchList=new TouchList(uint(menuInfo.header.y)+uint(menuInfo.header.height),the_Sta.stageWidth,the_Sta.stageHeight,menuInfo.menuItems[0],menuInfo.menuItem[0]);
			touchList.y=uint(menuInfo.header.height[0])+uint(menuInfo.header.y[0])+uint(menuInfo.menuItems.distanceFromHeader[0]);
			touchList.x=0;
			touchList.addEventListener(ListItemEvent.ITEM_SELECTED, handlelistItemSelected);
	
			setupEachMenuItem();
		}
		
		public function setupEachMenuItem(){
			for (var count:uint=0; count<menuInfo.menuItem.length();count++){
				var item:TouchListItemRenderer=new TouchListItemRenderer(menuInfo.menuItems[0]);
				item.colour=menuInfo.menuItems.colour[0];
				item.fontColour=menuInfo.menuItems.fontColour[0];
				item.index=count;
				item.data=menuInfo.menuItem[count].theTitle[0];
				item.itemHeight=menuInfo.menuItems.height[0];
				item.itemWidth=the_Sta.stageWidth;
				touchList.addListItem(item);
				stuffOnScreen.push(item);
			}
		}
		
		
		public function deleteMenuItem(index:uint){
			var searchedForTitle:String=menuInfo.menuItem[index].theTitle[0];
			for (var i:uint=0;i<menuInfo.menuItem.length();i++){		
				if(String(menuInfo.menuItem[i].theTitle[0])==searchedForTitle){
					touchList.removeListItem(i);
				}
			}
		}
		

		private function handlelistItemSelected(e:ListItemEvent):void {
			//trace("List item selected: " + e.renderer.index);
			//trace(e.target.parent
			buttonPressed=uint(e.renderer.index);
			buttonPressedStr=String(e.renderer.data);

			var tempString:String=menuInfo.menuItem[buttonPressed].@action;
			if (tempString.length==0) {
				tempString="blank";
			}
			switch (tempString) {

				case "blank" :
					//this.dispatchEvent(new Event("menuButtonPressed",true));
					break;

				case "nothing" :
					trace("unclickable");
					break;

				default :
					defaultActions(tempString);
			}
		}

		public function defaultActions(str:String) {
			try {
				this[str].call();
			} catch (error:Error) {
				trace("no such function: "+str);
			}
		}
		
		private function back(){
			removeOptionsMenu();
		}

		var myOptionsMenu:optionsMenu;
		
		
		public function resetMenu(){
			trace("resetMenu");
			removeList();
			touchList=null;
			setupList();
						
		}
		
		public function loadResponseMenu() {
			this.dispatchEvent(new Event("menuButtonPressed",true));
			
			var tempXML:XML=menuInfo.menuItems[0].buttonMenus[0];
			//if (!myOptionsMenu)myOptionsMenu.kill();
			myOptionsMenu=new optionsMenu(the_Sta,tempXML);
			myOptionsMenu.addEventListener("optionsMenuButtonPressed",optionsMenuButtonPressed);
			trace("addOptionsMenu");
			the_Sta.addChild(myOptionsMenu);
			//runTimerAction()
			myOptionsMenu.init();
			
		}

		public function removeOptionsMenu() {
			trace("removeOptionsMenu");
			myOptionsMenu.removeEventListener("optionsMenuButtonPressed",optionsMenuButtonPressed);
			myOptionsMenu.removeFromScreen();
			//the_Sta.removeChild(myOptionsMenu);
			//myOptionsMenu=null;
		}
	}
}