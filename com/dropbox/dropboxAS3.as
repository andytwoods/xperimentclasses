﻿package com.dropbox
{
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.LocationChangeEvent;
	import flash.net.FileReference;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.utils.ByteArray;
	import flash.media.StageWebView;
	import com.mobile.saveFilesInternally;
	import com.mobile.deleteFilesInternally;

	import org.hamster.dropbox.DropboxClient;
	import org.hamster.dropbox.DropboxConfig;
	import org.hamster.dropbox.DropboxEvent;
	import org.hamster.dropbox.models.AccountInfo;
	import org.hamster.dropbox.models.DropboxFile;
	import flash.geom.Rectangle;
	import flash.display.Stage;
	import flash.display.Sprite;
	import flash.display.Shape;
	import com.Logger.Logger;
	import com.graphics.pattern.Box;
	import com.mobile.getFilesInternally;

	public class dropboxAS3 extends Sprite{

		private var dropAPI:DropboxClient;
		private var createFolderLabel:String = "";
		private var moveFileLabel:String = "";
		private var deleteFileLabel:String = "";
		private var getFileLabel:String = "";
		private var metadataLabel:String = "";
		private var thumbnailsLabel:String = "";
		private var reqTokenKeyLabel:String = "";
		private var reqTokenSecretLabel:String = "";
		private var eMailLabel:String = "andytwoods@gmail.com";
		private var passwordLabel:String = "drizzt1";
		private var accTokenKeyLabel:String = "";
		private var accTokenSecretLabel:String = "";
		private var uploadFileLabel:String = "";
		private var copyFileLabel:String = "";
		private var requestToken:String = "";
		private var theStage:Stage;
		private var logger:Logger;
		private var getPermissionsFile:getFilesInternally;
		private var appKey:String="mt36f2ipx8b0k38";
		private var appSecret:String="a3attnru1m2vh1g"
		private var webView:StageWebView;
		private var gray:Shape;

		
		//first checks to see if dropbox permissions exist on disk.
		function dropboxAS3(sta:Stage,logger:Logger):void
		{
			theStage=sta;
			this.logger=logger;
			getPermissionsFile = new getFilesInternally("","dropBoxAccessDetails");
			getPermissionsFile.addEventListener("filePing",getDropBoxInfo,false,0,true);	
		}
		
		private function loggedIn(info:String):void{
			//use 'info' to populate info panel
			//dispatch event saying that can do stuff now
			this.dispatchEvent(new Event("loggedIn"));
		}
		
		
		//if permissions exist, try to link to dropbox, else, attempt to be granted permissions.
		private function getDropBoxInfo(e:Event):void{
			getPermissionsFile.removeEventListener("filePing",getDropBoxInfo);
			var str:String=e.target.giveFile();getPermissionsFile=null;
			if(str=="")getRequestToken();
			else {
				var arr:Array=str.split(",");
				accTokenKeyLabel=arr[0];
				accTokenSecretLabel=arr[1];
				tryToLinkToDropBox();
				
			}
		}

		
		public function removeDropBoxCredentials():void{
			logger.log(deleteFilesInternally.deleteFile("dropBoxAccessDetails")+"(dropBoxAccessDetails)");
		}
		
		private function tryToLinkToDropBox():void{
			var config:DropboxConfig = new DropboxConfig(appKey,appSecret);
			config.setAccessToken(accTokenKeyLabel, accTokenSecretLabel);
			dropAPI = new DropboxClient(config);
			accountInfo();
		}
		
		private function onbackground():void{
				var obj:Object=new Object();
				//obj.lineColour; 
				obj.fillColour=0x000001;
				//obj.lineThickness;
				obj.width=theStage.stageWidth;
				obj.height=theStage.stageHeight;
				
				gray=Box.myBox(obj);
				gray.alpha=.8;
				obj=null;
				theStage.addChild(gray)
		}

		private function login():void
		{
			if (StageWebView.isSupported == true)
			{
				onbackground();				
				webView = new StageWebView();
				webView.addEventListener( LocationChangeEvent.LOCATION_CHANGING, onLocationChange );

				webView.stage = theStage;
				webView.assignFocus();
				webView.viewPort = new Rectangle(theStage.stageWidth*.1,theStage.stageHeight*.1,theStage.stageWidth*.8,theStage.stageHeight*.8);
				webView.loadURL(dropAPI.authorizationUrl);
				//webView.loadString( "htmlString" );
				webView.addEventListener( LocationChangeEvent.LOCATION_CHANGE, onLocationChange );

			}
			else
			{
				trace("stageWebView not supported");
			}
		}


		private function onLocationChange( e:LocationChangeEvent ):void
		{

			if (e.target.location.indexOf("dropbox") != -1)
			{
				getAccessToken();

			}
		}





		private function createAccount():void
		{
			dropAPI.createAccount('yourtestemailhere@gmail.com', '123abc', 'a', 'b');
			var handler:Function = function (evt:DropboxEvent):void
			        {
					dropAPI.removeEventListener(DropboxEvent.ACCOUNT_CREATE_RESULT, handler);
			        };
			dropAPI.addEventListener(DropboxEvent.ACCOUNT_CREATE_RESULT, handler);
			if (! dropAPI.hasEventListener(DropboxEvent.ACCOUNT_CREATE_FAULT))
			{
				dropAPI.addEventListener(DropboxEvent.ACCOUNT_CREATE_FAULT, faultHandler);
			}
		}

		private function getRequestToken():void
		{	
			var config:DropboxConfig = new DropboxConfig(appKey,appSecret);
			dropAPI = new DropboxClient(config);
			dropAPI.requestToken();
			var handler:Function = function (evt:DropboxEvent):void
			        {
			                dropAPI.removeEventListener(DropboxEvent.REQUEST_TOKEN_RESULT, handler);
			                var obj:Object = evt.resultObject;
			  
							reqTokenKeyLabel = obj.key;
			                reqTokenSecretLabel = obj.secret;
			                // goto authorization web page to authorize, after that, call get access token 
			                login();
			                
			        }
			dropAPI.addEventListener(DropboxEvent.REQUEST_TOKEN_RESULT, handler);
			if (! dropAPI.hasEventListener(DropboxEvent.REQUEST_TOKEN_FAULT))
			{
				dropAPI.addEventListener(DropboxEvent.REQUEST_TOKEN_FAULT, faultHandler);
			}
		}

		private function emailLogin():void
		{
			dropAPI.token(eMailLabel, passwordLabel);
			var handler:Function = function (evt:DropboxEvent):void
			        {
			                dropAPI.removeEventListener(DropboxEvent.TOKEN_RESULT, handler);
			                var obj:Object = evt.resultObject;
			                accTokenKeyLabel = obj.token;
			                accTokenSecretLabel = obj.secret;
			            trace("loginedAPIContainer.enabled = true");
			        };
			dropAPI.addEventListener(DropboxEvent.TOKEN_RESULT, handler);
			if (! dropAPI.hasEventListener(DropboxEvent.TOKEN_FAULT))
			{
				dropAPI.addEventListener(DropboxEvent.TOKEN_FAULT, faultHandler);
			}
		}

		private function getAccessToken():void
		{
			dropAPI.accessToken();
			var handler:Function = function (evt:DropboxEvent):void
			        {
			                dropAPI.removeEventListener(DropboxEvent.ACCESS_TOKEN_RESULT, handler);
			                var obj:Object = evt.resultObject;
			                accTokenKeyLabel = obj.key;
			                accTokenSecretLabel = obj.secret;
							var saveAccessToken:saveFilesInternally= new saveFilesInternally("",logger);
							saveAccessToken.saveFile("dropBoxAccessDetails", accTokenKeyLabel+","+accTokenSecretLabel);
							logger.log("successfully have been granted permisssions by DropBox(key-"+accTokenKeyLabel+",secret:+"+accTokenSecretLabel+")");
							webView.dispose();
							theStage.removeChild(gray);
							gray=null;
							tryToLinkToDropBox();
			        };
			dropAPI.addEventListener(DropboxEvent.ACCESS_TOKEN_RESULT, handler);
			if (! dropAPI.hasEventListener(DropboxEvent.ACCESS_TOKEN_FAULT))
			{
				dropAPI.addEventListener(DropboxEvent.ACCESS_TOKEN_FAULT, faultHandler);
			}
		}

		private function accountInfo():void
		{
			dropAPI.accountInfo();
			var handler:Function = function (evt:DropboxEvent):void
			        {
			           	dropAPI.removeEventListener(DropboxEvent.ACCESS_TOKEN_RESULT, handler);
			         	var accountInfo:AccountInfo = AccountInfo(evt.resultObject);
						loggedIn(accountInfo.toString());
			        };
			dropAPI.addEventListener(DropboxEvent.ACCOUNT_INFO_RESULT, handler);
			if (! dropAPI.hasEventListener(DropboxEvent.ACCOUNT_INFO_FAULT))
			{
				dropAPI.addEventListener(DropboxEvent.ACCOUNT_INFO_FAULT, faultHandler);
			}
		}

		private var testFolder1:String = new Date().time.toString() + "1";
		private var testFile:String;

		private function uploadFile():void
		{
			var fr:FileReference = new FileReference();
			var loadCompHandler:Function = function (evt:Event):void
			        {
			                fr.removeEventListener(Event.COMPLETE, loadCompHandler);
			                testFile = fr.name;
			                dropAPI.putFile(testFolder1, fr.name, fr.data);
			                var handler:Function = function (evt:DropboxEvent):void
			                {
			                        dropAPI.removeEventListener(DropboxEvent.PUT_FILE_RESULT, handler);
			                        uploadFileLabel = evt.resultObject.toString();
			                };
			                dropAPI.addEventListener(DropboxEvent.PUT_FILE_RESULT, handler);
			                if (!dropAPI.hasEventListener(DropboxEvent.PUT_FILE_FAULT)) {
			                        dropAPI.addEventListener(DropboxEvent.PUT_FILE_FAULT, faultHandler);
			                }
			        };
			var selectHandler:Function = function (evt:Event):void
			        {
			                fr.removeEventListener(Event.SELECT, selectHandler);
			                fr.addEventListener(Event.COMPLETE, loadCompHandler);
			                fr.load();
			        };
			fr.addEventListener(Event.SELECT, selectHandler);
			fr.browse();
		}

		private function copyFile():void
		{
			dropAPI.fileCopy(testFolder1 + '/' + testFile, testFolder1 + '/copied_' + testFile);
			var handler:Function = function (evt:DropboxEvent):void
			        {
			                dropAPI.removeEventListener(DropboxEvent.FILE_COPY_RESULT, handler);
			                var dropboxFile:DropboxFile = DropboxFile(evt.resultObject);
			                copyFileLabel = dropboxFile.toString();
			        };
			dropAPI.addEventListener(DropboxEvent.FILE_COPY_RESULT, handler);
			if (! dropAPI.hasEventListener(DropboxEvent.FILE_COPY_FAULT))
			{
				dropAPI.addEventListener(DropboxEvent.FILE_COPY_FAULT, faultHandler);
			}
		}

		private var testFolder:String = new Date().time.toString();

		private function createFolder():void
		{
			dropAPI.fileCreateFolder(testFolder, 'dropbox');
			var handler:Function = function (evt:DropboxEvent):void
			        {
			                dropAPI.removeEventListener(DropboxEvent.FILE_CREATE_FOLDER_RESULT, handler);
			                var dropboxFile:DropboxFile = DropboxFile(evt.resultObject);
			                createFolderLabel = dropboxFile.toString();
			        };
			dropAPI.addEventListener(DropboxEvent.FILE_CREATE_FOLDER_RESULT, handler);
			if (! dropAPI.hasEventListener(DropboxEvent.FILE_CREATE_FOLDER_FAULT))
			{
				dropAPI.addEventListener(DropboxEvent.FILE_CREATE_FOLDER_FAULT, faultHandler);
			}
		}

		private function moveFile():void
		{
			dropAPI.fileMove(testFolder1 + '/' + testFile, testFolder1 + '/moved_' + testFile);
			var handler:Function = function (evt:DropboxEvent):void
			        {
			                dropAPI.removeEventListener(DropboxEvent.FILE_MOVE_RESULT, handler);
			                var dropboxFile:DropboxFile = DropboxFile(evt.resultObject);
			                moveFileLabel = dropboxFile.toString();
			        };
			dropAPI.addEventListener(DropboxEvent.FILE_MOVE_RESULT, handler);
			if (! dropAPI.hasEventListener(DropboxEvent.FILE_MOVE_FAULT))
			{
				dropAPI.addEventListener(DropboxEvent.FILE_MOVE_FAULT, faultHandler);
			}
		}

		private function deleteFile():void
		{
			dropAPI.fileDelete(testFolder1 + '/moved_' + testFile);
			var handler:Function = function (evt:DropboxEvent):void
			        {
			                dropAPI.removeEventListener(DropboxEvent.FILE_DELETE_RESULT, handler);
			                deleteFileLabel = evt.resultObject.toString();
			        };
			dropAPI.addEventListener(DropboxEvent.FILE_DELETE_RESULT, handler);
			if (! dropAPI.hasEventListener(DropboxEvent.FILE_DELETE_RESULT))
			{
				dropAPI.addEventListener(DropboxEvent.FILE_DELETE_FAULT, faultHandler);
			}
		}

		private function getFile():void
		{
			dropAPI.getFile('VCE_090522_A003.MP3');
			var handler:Function = function (evt:DropboxEvent):void
			        {
			                dropAPI.removeEventListener(DropboxEvent.GET_FILE_RESULT, handler);
			                getFileLabel = ByteArray(evt.resultObject).length.toString();
			        };
			dropAPI.addEventListener(DropboxEvent.GET_FILE_RESULT, handler);
			if (! dropAPI.hasEventListener(DropboxEvent.GET_FILE_FAULT))
			{
				dropAPI.addEventListener(DropboxEvent.GET_FILE_FAULT, faultHandler);
			}
		}

		private function metadata():void
		{
			dropAPI.metadata('12744358208051' + '', 1000, "", true);
			var handler:Function = function (evt:DropboxEvent):void
			        {
			                dropAPI.removeEventListener(DropboxEvent.METADATA_RESULT, handler);
			                metadataLabel = DropboxFile(evt.resultObject).toString();
			        };
			dropAPI.addEventListener(DropboxEvent.METADATA_RESULT, handler);
			if (! dropAPI.hasEventListener(DropboxEvent.METADATA_FAULT))
			{
				dropAPI.addEventListener(DropboxEvent.METADATA_FAULT, faultHandler);
			}
		}

		private function thumbnails():void
		{
			dropAPI.thumbnails('test.jpg', "");
			var handler:Function = function (evt:DropboxEvent):void
			        {
			                dropAPI.removeEventListener(DropboxEvent.THUMBNAILS_RESULT, handler);
			                thumbnailsLabel = ByteArray(evt.resultObject).length.toString();
			        };
			dropAPI.addEventListener(DropboxEvent.THUMBNAILS_RESULT, handler);
			if (! dropAPI.hasEventListener(DropboxEvent.THUMBNAILS_FAULT))
			{
				dropAPI.addEventListener(DropboxEvent.THUMBNAILS_FAULT, faultHandler);
			}
		}

		private function faultHandler(evt:Event):void
		{
			trace((evt as Object).resultObject);
			if(webView)webView.dispose();
			logger.log("There is an issue with logging into your dropbox account.  Most likely Xperiment has had it's DropBox permissions revoked for some reason (Xperiment should be listed here: www.dropbox.com/account#applications).  In which case, click the 'wipe dropbox info' button and try to reestablish these permissions");
		}

	}

}