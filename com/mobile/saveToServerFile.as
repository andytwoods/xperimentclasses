﻿package com.mobile{
	import flash.net.*;
	import flash.display.Sprite;
	import flash.events.EventDispatcher;
    import flash.events.Event;
	import flash.events.ProgressEvent;
	import flash.events.IEventDispatcher;
	import flash.events.SecurityErrorEvent;
	import flash.events.HTTPStatusEvent;
	import flash.events.IOErrorEvent;
	import com.Logger.Logger;
	
	public class saveToServerFile extends Sprite{
		private var logger:Logger;

		public function saveToServerFile(phpLocation:String,results:*,logger:Logger) {
			this.logger=logger;
			var xmlString:String="<?xml version='1.0' encoding='ISO-8859-1'?><root><value>63</value></root>";
			var xmlURLReq:URLRequest=new URLRequest(phpLocation);
			xmlURLReq.data=results;
			xmlURLReq.contentType="text/xml";
			xmlURLReq.method=URLRequestMethod.POST;
			var xmlSendLoad:URLLoader=new URLLoader;

			configureListeners(xmlSendLoad);
			xmlSendLoad.load(xmlURLReq);
		}
		
		private function configureListeners(dispatcher:IEventDispatcher):void {
			dispatcher.addEventListener(Event.COMPLETE, completeHandler);
			dispatcher.addEventListener(Event.OPEN, openHandler);
			dispatcher.addEventListener(ProgressEvent.PROGRESS, progressHandler);
			dispatcher.addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
			dispatcher.addEventListener(HTTPStatusEvent.HTTP_STATUS, httpStatusHandler);
			dispatcher.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
		}
		
		private function completeHandler(e:Event):void {
			removeEventListeners(e);
			var loader:URLLoader=URLLoader(e.target);
			dispatchEvent(new Event("saveXMLtoServerFile_saved"));
			logger.log("      success saving data");
			logger.log("-----------------------------------------------");

		}
		

		private function openHandler(e:Event):void {
			logger.log("-----------------------------------------------");
			logger.log("   ---saving data to web---");
			logger.log("      connection opened");
		}

		private function progressHandler(e:ProgressEvent):void {
			logger.log("      data to send: " + e.bytesLoaded+" bytes");
		}

		private function securityErrorHandler(e:SecurityErrorEvent):void {
			removeEventListeners(e);
			dispatchEvent(new Event("saveXMLtoServerFile_fail"));
			logger.log("     security Error: " + e);
			logger.log("   ---saving data---");
		}

		private function httpStatusHandler(e:HTTPStatusEvent):void {
			// logger.log("httpStatusHandler: " + event);
		}

		private function ioErrorHandler(e:IOErrorEvent):void {
			removeEventListeners(e);
			dispatchEvent(new Event("saveXMLtoServerFile_emergency"));
			dispatchEvent(new Event("saveXMLtoServerFile_fail"));
			logger.log("      problem saving file on server: " + e);
			logger.log("   ---saving data---");
		}
		
		private function removeEventListeners(e:Event):void{
			e.target.removeEventListener(Event.COMPLETE, completeHandler);
			e.target.removeEventListener(Event.OPEN, openHandler);
			e.target.removeEventListener(ProgressEvent.PROGRESS, progressHandler);
			e.target.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
			e.target.removeEventListener(HTTPStatusEvent.HTTP_STATUS, httpStatusHandler);
			e.target.removeEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
		}

	}
	
}
