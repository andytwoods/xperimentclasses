﻿package com.mobile
{
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.filesystem.File;
	import flash.filesystem.FileStream;
	import flash.filesystem.FileMode;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.UncaughtErrorEvent;
	import flash.events.IOErrorEvent;

	public class getFilesInternally extends Sprite
	{

		private var fileLdr:URLLoader;
		private var myData:*;

		//where f=folder to look in
		public function getFilesInternally(type:*, filename:String,f:String=""):void
		{
			myData = type;
						if (f!="")
			{
				filename = f + "/" + filename;
			}
			var folder:File=File.applicationStorageDirectory.resolvePath(f);
			var storageLocation:String = folder.nativePath;

			if (f!="")
			{
				storageLocation = "file://" + storageLocation;
			}
			fileLdr = new URLLoader;
			fileLdr.addEventListener(Event.COMPLETE,LoadFile,false,0,true);
			fileLdr.addEventListener(UncaughtErrorEvent.UNCAUGHT_ERROR,Err,false,0,true);
			fileLdr.addEventListener(IOErrorEvent.IO_ERROR,ioErr,false,0,true);
			fileLdr.load(new URLRequest(storageLocation+"/"+filename));
			//trace("ddddd:"+storageLocation+"/"+filename);
		}

		public function giveFile():*
		{
			
			return myData;
		}

		private function LoadFile(e:Event):void
		{
			removeListeners();
			try
			{
				myData = fileLdr.data;
				//trace("ffffffs:"+ fileLdr.data);
			}
			catch (e:Error)
			{
				myData = "";
				return;
			}
			this.dispatchEvent(new Event("filePing",true));
		}

		private function Err(e:UncaughtErrorEvent):void
		{
			removeListeners();
			myData = "";
			this.dispatchEvent(new Event("filePing",true));
		}
		
		private function ioErr(e:IOErrorEvent):void
		{
			trace("IOerror");
			removeListeners();
			myData = "";
			this.dispatchEvent(new Event("filePing",true));
		}

		private function removeListeners():void
		{
			fileLdr.removeEventListener(Event.COMPLETE,LoadFile);
			fileLdr.removeEventListener(UncaughtErrorEvent.UNCAUGHT_ERROR,Err);
			fileLdr.removeEventListener(IOErrorEvent.IO_ERROR,ioErr);
		}
	
	}
}