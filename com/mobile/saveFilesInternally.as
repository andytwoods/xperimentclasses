﻿package com.mobile{
	import com.Logger.Logger;
	
	import flash.display.Sprite;
	import flash.events.*;
	import flash.events.Event;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.net.URLRequest;
	import flash.text.TextField;
	import flash.utils.ByteArray;
	import flash.utils.getQualifiedClassName;

	public class saveFilesInternally extends Sprite {
		private var folder:File;
		private var file:File;
		private var fileStream:FileStream;
		private var logger:Logger;
		

		public var storageLocation:String;

		public function saveFilesInternally(f:String,masterLoc:String=""):void {
			
			this.logger=Logger.getInstance();
			
			if(masterLoc=="")folder=File.applicationStorageDirectory.resolvePath(f);
			
			else{
				switch(masterLoc.toLowerCase()){
					case "desktop":
						folder=File.desktopDirectory.resolvePath(f);
						break;
					case "my documents":
					case "mydocuments":
						folder=File.documentsDirectory.resolvePath(f);
						break;		
				}
			}
			
			storageLocation=folder.nativePath;
			/*	var message:TextField=new TextField;
			message.text=storageLocation;
			logger.sta.addChild(message);*/

			if (! folder.exists) {
				folder.createDirectory();
				if(logger)logger.log("folder '"+storageLocation+"' did not exist so created it");
			}
			else {
				if(logger)logger.log("'"+storageLocation+"' folder already exists so don't need to make it.");
			}

		}

		public function saveFile(fileName:String, dat:*, subDir:String=""):Boolean {
			var returnVal:Boolean=false;
			try {
				if(subDir!="")subDir=subDir+"/";
				file = folder.resolvePath(storageLocation+"/"+subDir+fileName);
				fileStream = new FileStream();
				fileStream.open(file, FileMode.UPDATE);
				var str:String=getQualifiedClassName(dat);
				if (str.indexOf("ByteArray")!=-1)fileStream.writeBytes(dat,0,dat.length);
				else if(str.indexOf("XML")!=-1)fileStream.writeUTFBytes(dat);

				fileStream.close();
				if(logger)logger.log("saved this file: "+storageLocation+"/"+fileName);

				returnVal=true;
			} catch (error:Error) {
				if(logger)logger.log("PROBLEM: saving was not successful: "+fileName);
			}
			return returnVal;
		}
		
		
		public function deleteDirectory(dir:String):void {
			folder=File.applicationStorageDirectory.resolvePath(dir);
			if (! folder.exists) {
				if(logger)logger.log("cannot delete this folder as it does not exist!");
			}
			else {
				try {
					folder.deleteDirectory(true);
				} catch (error:Error) {
					if(logger)logger.log("PROBLEM: could not delete this directory for some reason: "+folder);
					if(logger)logger.log("here's the error message: "+error);
				}
				if(logger)logger.log("folder "+dir+" deleted");
			}
		}
	}
}