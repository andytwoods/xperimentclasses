﻿package com.mTurk
{
	import com.Logger.Logger;
	
	import flash.display.LoaderInfo;
	import flash.display.Stage;
	import flash.net.LocalConnection;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.net.navigateToURL;
	import flash.net.sendToURL;
	public class mTurk
	{

		private var id:String = "";
		private var dir:String ="";
		private var logger:Logger;
		
		public function mTurk(sta:Stage,logger:Logger){
		this.logger=logger;
		
		
		try{
			var temp:Object=sta.loaderInfo.parameters;
			if (temp && temp.id != null && temp.id!="undefined" && temp.id!="ASSIGNMENT_ID_NOT_AVAILABLE")
				{
					id = temp.id;
					logger.log("received this assignmentId from Mechanical Turk:"+id);
				}
				else logger.log("did not receive a legal assignmentId from Mechanical Turk! Received this:"+temp.id+".");
			
				if (temp && temp.turkPlatform != null && temp.turkPlatform!="undefined")
				{
					dir = temp.turkPlatform;
					logger.log("received this turkPlatform indo from Mechanical Turk:"+dir);
					if(dir=="sandbox")dir="https://workersandbox.mturk.com:443/mturk/externalSubmit"
				}
				else logger.log("did not receive any turkPlatform info from Mechanical Turk! Received this:"+temp.turkPlatform+".");
		}
		catch(error:Error){
			if(logger)logger.log("did not receive any parameters...");
		}
		}

		public function sendBack():void
		{
			var passcode:String = getPasscode();
			var send_lc:LocalConnection = new LocalConnection();
			send_lc.send("_c1","function_in_receiving_swf",passcode);
		}
		
		public function getID():String{
			return id;
		}
		
		public function getDIR():String{
			return dir;
		}

		public function getPasscode():String
		{
			var s1:int = 0;
			var s2:int = 0;
			var tempInt:int;
			for (var i:uint=0; i<id.length; i++)
			{
				tempInt = id.charCodeAt(i);
				s1+=Math.floor(tempInt/10);
				s2+=tempInt-(Math.floor(tempInt*.1)*10);
			}
			return String(s1+s2);
		}
		
		public function submitTurkEnd():void
{
			var loader : URLLoader = new URLLoader();
			loader.dataFormat = URLLoaderDataFormat.TEXT;
			var request:URLRequest = new URLRequest(dir+"?assignmentId="+id+"&dummy=d");
		
			request.method = URLRequestMethod.POST;
			var variables : URLVariables = new URLVariables();
			variables.assignmentId = id;
			variables.dummy = "d";
			request.data = variables;
			try
			{
				sendToURL(request);
			}
			catch (e:Error)
			{
				trace(e);
			}
		}

	}

}