﻿
package com{
	import com.Logger.Logger;
	
	public class trialOrderFunctions {
		
		
		//static var blockSize:uint;
		//static var numBlocks:uint;
		private static var randFirstOrder:String;
		private static var randSecondOrder:String;
		private static var slotInTrials:Array;
		
		
		
		
		
		public static function computeOrder(trialProtocolList:XML,composeTrial:Function):Array{
			
			var trialOrder:Array=[];
			var trialArray:Array =[];
			var trialBlock:Array;
			var blockOrderings:Array=[];
			/*
			.trials=[7,8,9];
			.blocks=[1,2,3];
			.order=random;
			*/
			
			var order:String;
			//take out trial order, block and number of trials info.
			for (var i:uint=0; i<trialProtocolList.TRIAL.length(); i++) { //for each block of trials
				trialBlock=[];
				trialBlock.trials = (trialProtocolList.TRIAL[i].@trials.toString());
				trialBlock.blocks = (trialProtocolList.TRIAL[i].@block.toString() as String).split(",") as Array;
				
				//blockOrderings is used later on to order the blocks.  Note that only the first specification of order is used.  All others ignored.
				order = trialProtocolList.TRIAL[i].@order.toString();
				if(blockOrderings[trialBlock.blocks] == undefined && order!=null || order.length!=0) blockOrderings[trialBlock.blocks]=order;
				
				//verifying that something silly was not put in 'trials' (should be a number) and that trialBlock was specfied
				if(trialBlock.trials.length!=0 && !isNaN(Number(trialBlock.trials)) && trialBlock.blocks.length!=0){
					trialBlock.trials=int(trialBlock.trials);
					if(trialBlock.trials>0)trialArray.push(trialBlock); //ignore trials that say zero trials
				}
			}		
			
			//sort trialOrder according to the block array (awesome this can be done in 1 line of code!)
			trialArray.sortOn('blocks'); 
			var properBlockOrder:Array = [];
			for (i=0;i<trialArray.length;i++){
				if(properBlockOrder.indexOf(String(trialArray[i].blocks))==-1)properBlockOrder.push(String(trialArray[i].blocks));
			}
			
			
			
			var counter:int=0;
			var listOfTrialBlocks:Array=[];
			var rawTrialOrder:Array=[];
			
			//create all trials.  A static order is created wintihn rawTrialOrder.
			var blockNum:int;
			var trialNum:int;
			for (blockNum=0;blockNum<trialArray.length;blockNum++){
				for(trialNum=0;trialNum<trialArray[blockNum].trials;trialNum++){
					//trace(blockNum,trialNum);
					composeTrial(blockNum,i,counter,trialNum);
					rawTrialOrder.push(trialArray[blockNum].blocks);
					
					counter++;
				}
			}
			
			//now to apply the desired randomisation to the trials in each block using rawTrialOrder.
			//First, a list of all the block types and their static trial positions is created:
			var uniqueBlockGroups:Array = [];
			for (i=0;i<rawTrialOrder.length;i++){
				if(uniqueBlockGroups[rawTrialOrder[i]] == undefined)uniqueBlockGroups[rawTrialOrder[i]]=[];
				uniqueBlockGroups[rawTrialOrder[i]].push(i);
			}
			//next go through each uniqueBlockGroup and order as specified here: blockOrderings
			var block:String;
			
			
			for(block in uniqueBlockGroups){
				
				order=blockOrderings[block];
				//trace(order,":",block,":",uniqueBlockGroups[block])
				switch(order.toLowerCase()){
					case "random":
						uniqueBlockGroups[block]=codeRecycleFunctions.arrayShuffle(uniqueBlockGroups[block]);
						
						break;
					case "fixed":
						
						break;
				}
				
				
			}
			
			
			for(i=0;i<properBlockOrder.length;i++){
				block=properBlockOrder[i]
				trialOrder=trialOrder.concat(uniqueBlockGroups[block]);
			}
			//trace(trialOrder)
			
			return trialOrder;
			////////////////////////////////////////////////////////////////////////////////////
		}
		
		
		
		private static function extractVariables(str:String):Array {
			str=str.substring(str.indexOf("[")+1,str.indexOf("]"));
			return (str.split(","));
		}
		
		private static function shuffleArray(a:Array):Array {
			var a2:Array=[];
			while (a.length>0) {
				a2.push(a.splice(Math.round(Math.random()*a.length-1),1)[0]);
			}
			return a2;
		}
		
		
		
		private static function genRepetition(num:uint):Array {
			var repArray:Array=new Array  ;
			for (var i:uint=0; i<num; i++) {
				repArray.push(i+1);
			}
			return repArray;
		}
		
		public static function repeatRandBlocksDifEnds(trials:Array,rawParameters:Array):Array {
			///trace("eee "+trials);
			var parameters:Array=extractparameters(rawParameters);
			var newTrials:Array=new Array  ;
			var blockSize:uint=parameters[0];
			var numBlocks:uint=parameters[2];
			randFirstOrder=parameters[1];
			randSecondOrder=parameters[3];
			
			if (randSecondOrder=="R") {
				newTrials=shuffleWithRepeats(trials,blockSize,numBlocks);
				trials=escalateSecondOrderTrialNumbers(trials,newTrials,blockSize,numBlocks);
			}
			if (randFirstOrder=="S") {
				newTrials=shuffleWithNoRepeats(trials,blockSize,numBlocks);
			} else if (randFirstOrder=="R") {
				newTrials=shuffleWithRepeats(trials,blockSize,numBlocks);
			} else {
				newTrials=noShuffle(trials,blockSize,numBlocks);
			}
			
			return escalateFirstOrderTrialNumbers(trials,newTrials,blockSize,numBlocks);
		}
		
		
		private static function extractparameters(raw:Array):Array {
			var tempString1:String;
			var tempString2:String;
			var returnArray:Array=new Array  ;
			for (var i:uint=0; i<raw.length; i++) {
				tempString1=String(raw[i].substring(0,1));
				tempString2=String(raw[i].substring(1,2));
				if (isANumber(tempString1)) {
					returnArray.push(tempString1);
					if (tempString2.length==0) {
						returnArray.push("F");
					} else {
						returnArray.push(tempString2);
					}
				} else {
					returnArray.push(tempString2);
					if (tempString2.length==0) {
						returnArray.push("F");
					} else {
						returnArray.push(tempString1);
					}
				}
			}
			return returnArray;
		}
		
		private static function isANumber(__str:String):Boolean {
			return ! isNaN(Number(__str));
		}
		
		private static function noShuffle(trials:Array,blockSize:uint,numBlocks:uint):Array {
			var newTrials:Array=new Array  ;
			var tempRep:Array;
			for (var i:uint=0; i<numBlocks; i++) {
				tempRep=genRepetition(blockSize);
				newTrials=newTrials.concat(tempRep);
			}
			return newTrials;
		}
		
		private static function shuffleWithRepeats(trials:Array,blockSize:uint,numBlocks:uint):Array {
			var newTrials:Array=new Array  ;
			var tempRep:Array;
			for (var i:uint=0; i<numBlocks; i++) {
				tempRep=shuffleArray(genRepetition(blockSize));
				newTrials=newTrials.concat(tempRep);
			}
			return newTrials;
		}
		
		private static function shuffleWithNoRepeats(trials:Array,blockSize:uint,numBlocks:uint):Array {
			var newTrials:Array=new Array  ;
			var tempRep:Array;
			newTrials=shuffleArray(genRepetition(blockSize));
			for (var i:uint=1; i<numBlocks; i++) {
				tempRep=shuffleArray(genRepetition(blockSize));
				while (tempRep[blockSize-1]!=newTrials[newTrials.length-1]) {
					tempRep=shuffleArray(genRepetition(blockSize));
				}
				newTrials=newTrials.concat(tempRep);
			}
			return newTrials;
		}
		
		private static function escalateSecondOrderTrialNumbers(trials:Array, newTrials:Array,blockSize:uint,numBlocks:uint):Array {
			
			var splitIntoDataTypes:Array=new Array  ;
			var groupsArray:Array=new Array  ;
			var singleGroup:Array=new Array  ;
			var returnTrials:Array=new Array  ;
			
			for (var i:uint=0; i<numBlocks; i++) {//size of each group / block
				for (var j:uint=0; j<blockSize; j++) {// number of groups / blocks
					singleGroup.push(trials[(i*blockSize)+j]);// e.g. abcd efgh ijkl mnop
					//singleGroup.push((j*blockSize)+i);
				}
				groupsArray.push(singleGroup);
				singleGroup=new Array  ;
			}
			
			
			groupsArray=shuffleArray(groupsArray);
			
			
			for (i=0; i<groupsArray.length; i++) {
				for (j=0; j<groupsArray[i].length;j++){
					returnTrials.push(groupsArray[i][j]);
				}
			}
			return returnTrials;
		}
		
		
		
		
		
		private static function escalateFirstOrderTrialNumbers(trials:Array, newTrials:Array,blockSize:uint,numBlocks:uint):Array {
			
			var splitIntoDataTypes:Array=new Array  ;
			var groupsArray:Array=new Array  ;
			var singleGroup:Array=new Array  ;
			var returnTrials:Array=new Array  ;
			
			
			
			for (var i:uint=0; i<numBlocks; i++) {//size of each group / block
				for (var j:uint=0; j<blockSize; j++) {// number of groups / blocks
					singleGroup.push(trials[(i*blockSize)+j]);// e.g. abcd efgh ijkl mnop
					//singleGroup.push((j*blockSize)+i);
				}
				groupsArray.push(singleGroup);
				singleGroup=new Array  ;
			}
			
			for (i=0; i<newTrials.length; i++) {
				var trialPos:uint=newTrials[i]-1;
				var blockPos:uint=(i-(i%blockSize))/blockSize;
				//trace("groupsArray:"+groupsArray+" trialPos:"+(trialPos)+" blockPos:"+blockPos);
				returnTrials.push(groupsArray[blockPos][trialPos]);
				
			}
			
			trace("newTrials:"+newTrials);
			trace("returnTrials:"+returnTrials);
			
			return returnTrials;
		}
		
		private static function inverseArray(arr:Array):Array {
			var tempArr:Array=new Array  ;
			var returnArray:Array=new Array  ;
			for (var i:uint=0; i<arr[0].length; i++) {
				tempArr=new Array  ;
				for (var j:uint=0; j<arr.length; j++) {
					
					tempArr.push(arr[j][i]);
					
				}
				returnArray.push(tempArr);
			}
			return returnArray;
		}
		
	}
}