﻿package com.xperiment{
	import flash.display.*;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	import flash.text.TextFormat;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormatAlign;
	import flash.events.*;
	import flash.utils.getDefinitionByName;
	import flash.display.StageDisplayState;
	import flash.display.StageScaleMode;
	import com.trialOrderFunctions;
	import com.xperiment.trial.overExperiment;
	import flash.desktop.NativeApplication;
	import flash.utils.Timer;
	import flash.events.TimerEvent;
	import flash.system.Capabilities;
	import com.Logger.Logger;
	import com.xperiment.trial.TrialANDROID;

	public class runnerAPPLE extends runnerANDROID {

		private var _dummyTrialAndroid:TrialANDROID;
		
		override public function initialise(sta:Stage) {
			super.theStage=sta;
			theStage.addEventListener("saveDataEndStudy",endProgram,true,0,true);
			logger.passStage(theStage);
			super.theStage.align=StageAlign.TOP_LEFT;
			super.theStage.scaleMode=StageScaleMode.NO_SCALE;
			super.nerdStuff="APPLE" + getAppInfo();
			//super.theStage.addEventListener(Event.FRAME_CONSTRUCTED, sortOutLocalDataBase);
			super.theStage.addEventListener(Event.FRAME_CONSTRUCTED, sortOutScreen);
			actionBar=0;
		}

		override public function closeProgram(e:Event) {
			dispatchEvent(new Event("endOfStudy"));
			trace("study finished.  Restarting...");
		}
		
		
		

	}
}