﻿
package com.xperiment{
	import com.Logger.Logger;
	
	public class trialOrderFunctions {
		
		
		//static var blockSize:uint;
		//static var numBlocks:uint;
		private static var randFirstOrder:String;
		private static var randSecondOrder:String;
		private static var slotInTrials:Array;
		
		
		
		
		public static function computeOrder(trialProtocolList:XML,composeTrial:Function):Array{
			
			var trialOrder:Array=[];
			var trialArray:Array =[];
			var trialBlock:Array;
			var blockOrderings:Array=[];
			/*
			.trials=[7,8,9];
			.blocks=[1,2,3];
			.order=random;
			*/
			
			var order:String;
			var counter:uint=0;
			//take out trial order, block and number of trials info.
			for (var givenBlock:uint=0; givenBlock<trialProtocolList.TRIAL.length(); givenBlock++) { //for each block of trials
				trialBlock=[];
				trialBlock.trials = (trialProtocolList.TRIAL[givenBlock].@trials.toString());
				trialBlock.blocks = (trialProtocolList.TRIAL[givenBlock].@block.toString() as String).split(",") as Array;
				//trace(trialBlock.trials,trialBlock.blocks);
				//blockOrderings is used later on to order the blocks.  Note that only the first specification of order is used.  All others ignored.
				order = trialProtocolList.TRIAL[givenBlock].@order.toString();
				if(blockOrderings[trialBlock.blocks] == undefined && order!=null || order.length!=0) blockOrderings[trialBlock.blocks]=order;
				
				//verifying that something silly was not put in 'trials' (should be a number) and that trialBlock was specfied
				if(trialBlock.trials.length!=0 && !isNaN(Number(trialBlock.trials)) && trialBlock.blocks.length!=0){
					trialBlock.trials=int(trialBlock.trials);
					trialBlock.actualOrder=counter; //to correctly order trials that have non-random order
					trialBlock.actualBlock=givenBlock; // as 0 number of trial blocks ignored, need some way to reference correct block
					if(trialBlock.trials>0){
						trialArray.push(trialBlock); //ignore trials that say zero trials
						counter++;
					}
				}
			}		
			
			//sort trialOrder according to the block array 
			trialArray = sortAssArrBy(trialArray,'blocks','actualOrder'); 
			var properBlockOrder:Array = [];
			for (givenBlock=0;givenBlock<trialArray.length;givenBlock++){
				//trace(trialArray[i].blocks,"   ",trialArray[i].trials)
				if(properBlockOrder.indexOf(String(trialArray[givenBlock].blocks))==-1)properBlockOrder.push(String(trialArray[givenBlock].blocks));
			}
						
			
			var listOfTrialBlocks:Array=[];
			var rawTrialOrder:Array=[];
			
			//create all trials.  A static order is created within rawTrialOrder.
			var blockNum:int;
			var trialNum:int;
			counter=0;
			for (blockNum=0;blockNum<trialArray.length;blockNum++){
				//trace(blockNum,22);
				for(trialNum=0;trialNum<trialArray[blockNum].trials;trialNum++){
					//trace(trialArray[blockNum].actualBlock,trialNum,trialArray[blockNum].trials,trialArray[blockNum].blocks);
					composeTrial(trialArray[blockNum].actualBlock,trialNum,counter);
					rawTrialOrder.push(trialArray[blockNum].blocks);
					counter++;
				}
			}
			
			//now to apply the desired randomisation to the trials in each block using rawTrialOrder.
			//First, a list of all the block types and their static trial positions is created:
			var uniqueBlockGroups:Array = [];
			for (givenBlock=0;givenBlock<rawTrialOrder.length;givenBlock++){
				if(uniqueBlockGroups[rawTrialOrder[givenBlock]] == undefined)uniqueBlockGroups[rawTrialOrder[givenBlock]]=[];
				uniqueBlockGroups[rawTrialOrder[givenBlock]].push(givenBlock);
			}
			//next go through each uniqueBlockGroup and order as specified here: blockOrderings
			var block:String;
			
			
			for(block in uniqueBlockGroups){
				
				order=blockOrderings[block];
				
				switch(order.toLowerCase()){
					case "random":
						uniqueBlockGroups[block]=codeRecycleFunctions.arrayShuffle(uniqueBlockGroups[block]);
						break;
					case "fixed":
						break;
					case "reversed":
					case "reverse" :
						uniqueBlockGroups[block]=uniqueBlockGroups[block].reverse();
					default		:
						break;
				}
				
				
			}
			
			
			for(givenBlock=0;givenBlock<properBlockOrder.length;givenBlock++){
				block=properBlockOrder[givenBlock]
				trialOrder=trialOrder.concat(uniqueBlockGroups[block]);
			}
			//trace(trialOrder)
			
			return trialOrder;
			////////////////////////////////////////////////////////////////////////////////////
		}
	
		public static function sortAssArrBy(arr:Array,attrib:String,attrib2:String=""):Array{
			
			var maxNumSubBlocks:int=1;
			var sortingArr:Array=[];
			for (var i:uint=0;i<arr.length;i++){
				//what is the maximum number of blocks over the experiment
				if(arr[i][attrib] as Array && arr[i][attrib].length>maxNumSubBlocks)maxNumSubBlocks=arr[i][attrib].length;
				
				//create a clean sortingArr to sort
				if(arr[i][attrib] as Array)arr[i].tempSortVal=arr[i][attrib];
				else arr[i].tempSortVal=[arr[i][attrib]];// makes sure all elements are Arrays.				
				
				//explicitly link the sortingArr and original arr in terms of an index
			}
			
			//standardizes arr length adding 0 where necessary
			for (i=0;i<sortingArr.length;i++){
				while((arr[i].tempSortVal as Array).length<maxNumSubBlocks)(arr[i].tempSortVal as Array).push(0);
			}
			
			//trace(sortLongArrays([1,2,3,6],[1,2,3,5])==1,sortLongArrays([1,2,3,6],[1,2,3,7])==-1,sortLongArrays([1,2,3,6],[1,2,3,6])==0,sortLongArrays([1,2,3,6],[1,2,3])==999);
			//assumes arrays are same length
			var sortLongArrays:Function = function(arr1:Array,arr2:Array):int{
				//trace(arr1.tempSortVal[0],arr2.tempSortVal[0])
				
				//first, sort according to blocks
				if(arr1.tempSortVal.length==arr2.tempSortVal.length){
					for(var i:int=0;i<arr1.tempSortVal.length;i++){
						//tried letting values be alphabetic but it really screwed with the order
						//see: 'Attempt at alphabetic + numeral version' at bottom of class
						if		(int(arr1.tempSortVal[i]) > int(arr2.tempSortVal[i])){
							return 1;
						}
						else if	(int(arr1.tempSortVal[i]) < int(arr2.tempSortVal[i])){
							return -1;
						}
					}
		
					if(arr1.hasOwnProperty(attrib2) && arr2.hasOwnProperty(attrib2)){
						///second, sort according to actual order in the experiment script
						if		(int(arr1[attrib2]) > int(arr2[attrib2])){
							return 1;
						}
						else if	(int(arr1[attrib2]) < int(arr2[attrib2])){
							return -1;
						}
					}
					
					return 0;
				}
				Error("function sortLongArrays(arr1:Array,arr2:Array) was passed Arrays of unequal lengths");
				return 0;
			}
			
			arr.sort(sortLongArrays);
			/*
			//trace(sortingArr.join("		"));
			
			var newSortOrder:Array=[];
			for (i=0;i<sortingArr.length;i++){
				newSortOrder[sortingArr[i].sortingIndex]=i;
			}
			sortingArr=null;
			
			var newArr:Array= new Array(arr.length);
			var posInNewSortOrder:int;
			for (i=0;i<arr.length;i++){
				posInNewSortOrder=newSortOrder[arr[i].sortingIndex]
				newArr[posInNewSortOrder]=arr[i];
				delete newArr[posInNewSortOrder].sortingIndex;
			}
			newSortOrder=null;
			return newArr;*/
			return arr;
		}
		
		
		private static function extractVariables(str:String):Array {
			str=str.substring(str.indexOf("[")+1,str.indexOf("]"));
			return (str.split(","));
		}
		
		private static function shuffleArray(a:Array):Array {
			var a2:Array=[];
			while (a.length>0) {
				a2.push(a.splice(Math.round(Math.random()*a.length-1),1)[0]);
			}
			return a2;
		}
		
		
		
		private static function genRepetition(num:uint):Array {
			var repArray:Array=new Array  ;
			for (var i:uint=0; i<num; i++) {
				repArray.push(i+1);
			}
			return repArray;
		}
		
		public static function repeatRandBlocksDifEnds(trials:Array,rawParameters:Array):Array {
			///trace("eee "+trials);
			var parameters:Array=extractparameters(rawParameters);
			var newTrials:Array=new Array  ;
			var blockSize:uint=parameters[0];
			var numBlocks:uint=parameters[2];
			randFirstOrder=parameters[1];
			randSecondOrder=parameters[3];
			
			if (randSecondOrder=="R") {
				newTrials=shuffleWithRepeats(trials,blockSize,numBlocks);
				trials=escalateSecondOrderTrialNumbers(trials,newTrials,blockSize,numBlocks);
			}
			if (randFirstOrder=="S") {
				newTrials=shuffleWithNoRepeats(trials,blockSize,numBlocks);
			} else if (randFirstOrder=="R") {
				newTrials=shuffleWithRepeats(trials,blockSize,numBlocks);
			} else {
				newTrials=noShuffle(trials,blockSize,numBlocks);
			}
			
			return escalateFirstOrderTrialNumbers(trials,newTrials,blockSize,numBlocks);
		}
		
		
		private static function extractparameters(raw:Array):Array {
			var tempString1:String;
			var tempString2:String;
			var returnArray:Array=new Array  ;
			for (var i:uint=0; i<raw.length; i++) {
				tempString1=String(raw[i].substring(0,1));
				tempString2=String(raw[i].substring(1,2));
				if (isANumber(tempString1)) {
					returnArray.push(tempString1);
					if (tempString2.length==0) {
						returnArray.push("F");
					} else {
						returnArray.push(tempString2);
					}
				} else {
					returnArray.push(tempString2);
					if (tempString2.length==0) {
						returnArray.push("F");
					} else {
						returnArray.push(tempString1);
					}
				}
			}
			return returnArray;
		}
		
		private static function isANumber(__str:String):Boolean {
			return ! isNaN(Number(__str));
		}
		
		private static function noShuffle(trials:Array,blockSize:uint,numBlocks:uint):Array {
			var newTrials:Array=new Array  ;
			var tempRep:Array;
			for (var i:uint=0; i<numBlocks; i++) {
				tempRep=genRepetition(blockSize);
				newTrials=newTrials.concat(tempRep);
			}
			return newTrials;
		}
		
		private static function shuffleWithRepeats(trials:Array,blockSize:uint,numBlocks:uint):Array {
			var newTrials:Array=new Array  ;
			var tempRep:Array;
			for (var i:uint=0; i<numBlocks; i++) {
				tempRep=shuffleArray(genRepetition(blockSize));
				newTrials=newTrials.concat(tempRep);
			}
			return newTrials;
		}
		
		private static function shuffleWithNoRepeats(trials:Array,blockSize:uint,numBlocks:uint):Array {
			var newTrials:Array=new Array  ;
			var tempRep:Array;
			newTrials=shuffleArray(genRepetition(blockSize));
			for (var i:uint=1; i<numBlocks; i++) {
				tempRep=shuffleArray(genRepetition(blockSize));
				while (tempRep[blockSize-1]!=newTrials[newTrials.length-1]) {
					tempRep=shuffleArray(genRepetition(blockSize));
				}
				newTrials=newTrials.concat(tempRep);
			}
			return newTrials;
		}
		
		private static function escalateSecondOrderTrialNumbers(trials:Array, newTrials:Array,blockSize:uint,numBlocks:uint):Array {
			
			var splitIntoDataTypes:Array=new Array  ;
			var groupsArray:Array=new Array  ;
			var singleGroup:Array=new Array  ;
			var returnTrials:Array=new Array  ;
			
			for (var i:uint=0; i<numBlocks; i++) {//size of each group / block
				for (var j:uint=0; j<blockSize; j++) {// number of groups / blocks
					singleGroup.push(trials[(i*blockSize)+j]);// e.g. abcd efgh ijkl mnop
					//singleGroup.push((j*blockSize)+i);
				}
				groupsArray.push(singleGroup);
				singleGroup=new Array  ;
			}
			
			
			groupsArray=shuffleArray(groupsArray);
			
			
			for (i=0; i<groupsArray.length; i++) {
				for (j=0; j<groupsArray[i].length;j++){
					returnTrials.push(groupsArray[i][j]);
				}
			}
			return returnTrials;
		}
		
		
		
		
		
		private static function escalateFirstOrderTrialNumbers(trials:Array, newTrials:Array,blockSize:uint,numBlocks:uint):Array {
			
			var splitIntoDataTypes:Array=new Array  ;
			var groupsArray:Array=new Array  ;
			var singleGroup:Array=new Array  ;
			var returnTrials:Array=new Array  ;
			
			
			
			for (var i:uint=0; i<numBlocks; i++) {//size of each group / block
				for (var j:uint=0; j<blockSize; j++) {// number of groups / blocks
					singleGroup.push(trials[(i*blockSize)+j]);// e.g. abcd efgh ijkl mnop
					//singleGroup.push((j*blockSize)+i);
				}
				groupsArray.push(singleGroup);
				singleGroup=new Array  ;
			}
			
			for (i=0; i<newTrials.length; i++) {
				var trialPos:uint=newTrials[i]-1;
				var blockPos:uint=(i-(i%blockSize))/blockSize;
				//trace("groupsArray:"+groupsArray+" trialPos:"+(trialPos)+" blockPos:"+blockPos);
				returnTrials.push(groupsArray[blockPos][trialPos]);
				
			}
			
			trace("newTrials:"+newTrials);
			trace("returnTrials:"+returnTrials);
			
			return returnTrials;
		}
		
		private static function inverseArray(arr:Array):Array {
			var tempArr:Array=new Array  ;
			var returnArray:Array=new Array  ;
			for (var i:uint=0; i<arr[0].length; i++) {
				tempArr=new Array  ;
				for (var j:uint=0; j<arr.length; j++) {
					
					tempArr.push(arr[j][i]);
					
				}
				returnArray.push(tempArr);
			}
			return returnArray;
		}
		
	}
}

/*
Attempt at alphabetic + numeral version
var sortLongArrays:Function = function(arr1:Array,arr2:Array):int{
	if(arr1.length==arr2.length){
		for(var i:int=0;i<arr1.length;i++){
			if(!isNaN(Number(arr1[i])) && !isNaN(Number(arr2[i]))){
				if		(int(arr1[i]) > int(arr2[i])){
					return 1;
				}
				else if	(int(arr1[i]) < int(arr2[i])){
					return -1;
				}
			}
			else{
				if([arr1[i],arr2[i]].sort().toString()==[arr1[i],arr2[i]].toString()) return -1;
				else return 1;
				
			}
			
		}
		return 0;
	}
	Error("function sortLongArrays(arr1:Array,arr2:Array) was passed Arrays of unequal lengths");
	return 0;
}*/