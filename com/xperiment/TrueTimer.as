﻿//AW taken from http://blog.gritfish.net/?p=76%94

package com.xperiment{
	import flash.display.*;
	import flash.events.*;
	import flash.utils.*;
	public class TrueTimer extends MovieClip {
		public var delay:int;
		public var repeatCount:int;
		public var initTime:Date;
		public var currentTime:Date;
		public var currentCount:int=0;
		public var __offset:int=0;
		public var running:Boolean=false;
		public var timeFromStart:int=0;
		public var timeShouldBe:int=0;
		private var now:Date;
		private var msDiff:Number;
		
		public function TrueTimer(DELAY:int,REPEAT:int):void {
			delay=DELAY;
			repeatCount = REPEAT
			;
			initTime = new Date();
			currentTime=initTime;
		}

		
		public function evaluateTime(e:Event):void {
			if (running) {
				now = new Date();
				msDiff=now.valueOf()-currentTime.valueOf();
				__offset+=msDiff;
				currentTime=now;
				if (__offset > delay) {
					while (__offset > delay) {
						currentCount++;
						__offset -= delay
						;
						if (repeatCount != 0) {
							if (currentCount == repeatCount) {
								timeFromStart=now.valueOf()-initTime.valueOf();
								timeShouldBe=(repeatCount*delay);
								dispatchEvent(new Event(TimerEvent.TIMER));
								dispatchEvent(new Event(TimerEvent.TIMER_COMPLETE));
								__stop();
							}
							else if (currentCount < repeatCount) {
								timeFromStart=now.valueOf()-initTime.valueOf();
								timeShouldBe=(repeatCount*delay);
								dispatchEvent(new Event(TimerEvent.TIMER));
							}
						}
						else {
							timeFromStart=now.valueOf()-initTime.valueOf();
							timeShouldBe=(repeatCount*delay);
							dispatchEvent(new Event(TimerEvent.TIMER));
						}
					}
				}
			}
		}
		
		public function timeFromEnd():Number{
			return delay-timeFromStart;
		}
		
		public function __start():void {
			initTime = new Date();
			currentTime=initTime;
			running=true;
			addEventListener(Event.ENTER_FRAME,evaluateTime);
		}
		public function __stop():void {
			running=false;
			removeEventListener(Event.ENTER_FRAME,evaluateTime);
		}
		public function __reset():void {
			currentCount=0;
			__offset=0;
			running=false;
			removeEventListener(Event.ENTER_FRAME,evaluateTime);
		}

	}
}