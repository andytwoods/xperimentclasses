﻿package com.xperiment {
	import com.Logger.Logger;
	import com.trialOrderFunctions;
	import com.xperiment.runner;
	import com.xperiment.trial.Trial;
	import com.xperiment.trial.TrialANDROID;
	import com.xperiment.trial.Trial_betweenSJsSortOut;
	import com.xperiment.trial.overExperiment;
	
	import flash.desktop.NativeApplication;
	import flash.display.*;
	import flash.display.StageDisplayState;
	import flash.display.StageScaleMode;
	import flash.events.*;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.geom.Rectangle;
	import flash.system.Capabilities;
	import flash.system.System;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.utils.Timer;
	import flash.utils.getDefinitionByName;
	
	

	public class runnerANDROID extends runner {

		private var _dummyTrialAndroid:TrialANDROID;
		public var actionBar:uint=48;
		
		override public function initialise(sta:Stage):void {
			theStage=sta;
			superlogger as Logger.passStage(theStage);
			theStage.addEventListener("saveDataEndStudy",endProgram);
			super.theStage.align=StageAlign.TOP_LEFT;
			super.theStage.scaleMode=StageScaleMode.NO_SCALE;
			super.nerdStuff="Android appID:"+getAppInfo();
			//super.theStage.addEventListener(Event.FRAME_CONSTRUCTED, sortOutLocalDataBase);
			super.theStage.addEventListener(Event.FRAME_CONSTRUCTED, sortOutScreen);
		}
		
		override public function checkForLoadableObjects():void{
			super.runningExptNow();
		}

		override public function getScript():void {
			// a slight hack. Needs to be empty.
		}

		private var _frameCnt:int=0;

		override public function getAppInfo():String {
			return String(NativeApplication.nativeApplication.applicationID + " RunTimeVersion:" + NativeApplication.nativeApplication.runtimeVersion+" OS:" + Capabilities.os + " resX:" + Capabilities.screenResolutionX + " resY:" + Capabilities.screenResolutionY + " DPI:" + Capabilities.screenDPI + " CPUarch:" + Capabilities.cpuArchitecture + " version:"+Capabilities.version);
		}

		public function sortOutScreen(e:Event):void {
			_frameCnt++;
			if (_frameCnt == 3) {

				removeEventListener("dataBaseSorted",sortOutScreen);
				scrWidth=flash.system.Capabilities.screenResolutionX;
				scrHeight=flash.system.Capabilities.screenResolutionY;

				if (super.nerdStuff.indexOf("Windows")!=-1) {
					scrWidth=theStage.stageWidth;
					scrHeight=theStage.stageHeight;
				}
				else if (scrWidth>scrHeight) {
					var swap:uint=scrWidth;
					scrWidth=scrHeight+actionBar;
					scrHeight=swap-actionBar;
				}
				runExpt();
			}
		}

		override public function setBackgroundColour(colour:Number):void {

			super.square.graphics.lineStyle(3,colour);
			super.square.graphics.beginFill(colour);
			super.square.graphics.drawRect(0,0,theStage.stageWidth,theStage.stageHeight);
			super.square.graphics.endFill();
			super.theStage.addChild(square);
		}
		//future job: will need to remove this at some distant future point.;

		public function returnTrialType():Trial {
			return new Trial();
		}
		

		override public function saveData():void {
			
			var finalResults:XML=addInfoToResultsFile();

			var dataSave:saveXML_ANDROID=new saveXML_ANDROID  ;
			dataSave.passLogger(logger);
			dataSave.addEventListener("dataSaved",endProgram);
			dataSave.saveProcedure(finalResults, ExptWideSpecs);
		}

		public function endProgram(event:Event):void {
			currentTrial==trialToSubmitDataOn-1
			var tempTrial:uint=trialOrder[currentTrial];
			//saveData();

			if(TrialBS){trace("removeing TrialBS");TrialBS.generalCleanUp();wipeBetweenSJsTrial();}
			event.target.removeEventListener("dataSaved",endProgram);
			var dur:uint=0;
			if (ExptWideSpecs && ExptWideSpecs.mobile && ExptWideSpecs.mobile.autoClose) dur=ExptWideSpecs.mobile.autoCloseTimer;
				
			var myTimer:Timer;
			myTimer=new Timer(dur,1);
			myTimer.addEventListener(TimerEvent.TIMER_COMPLETE,closeProgram);
			myTimer.start();
			
			if(trialList && trialList.length>0){
				trialList[tempTrial].generalCleanUp();
				trialList[tempTrial].removeEventListener("eventFinishedGoToNextTrial",endTrialGoToNextTrialEvent);
				trialList[tempTrial].removeEventListener("eventFinishedGoToPrevTrial",endTrialGoToPrevTrial);
			
			}
			if(square && theStage.contains(square))theStage.removeChild(square);
			if(theStage.hasEventListener("saveDataEndStudy"))theStage.removeEventListener("saveDataEndStudy",closeProgram);
			if(theStage.hasEventListener("betweenSJsTrialEvent_Runner"))theStage.removeEventListener("betweenSJsTrialEvent_Runner",betweenSJsTrialEvent);
			if(theStage.hasEventListener("betweenSJsTrialEvent_Runner"))theStage.removeEventListener("betweenSJsTrialEvent_Runner",betweenSJsTrialEvent);
			if(theStage.hasEventListener(FullScreenEvent.FULL_SCREEN))theStage.removeEventListener(FullScreenEvent.FULL_SCREEN,changeBGcolourSize);
			if(myScript && myScript.hasEventListener("dataLoaded"))myScript.removeEventListener("dataLoaded",continueStudyAfterLoadingPause);

			System.gc();
			
			
		}

		public function closeProgram(e:Event):void {
			e.target.removeEventListener(TimerEvent.TIMER_COMPLETE,closeProgram);
			NativeApplication.nativeApplication.exit();

		}


	}
}