﻿package com.xperiment{

	import flash.text.*;
	import flash.events.Event;
	import flash.external.ExternalInterface;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.navigateToURL;
	import flash.utils.ByteArray;



	public class saveResults {
		var partialconversion:String;

		public function saveResults(exptResults:XML) {
			var request:URLRequest=new URLRequest("writeXML.php");
			request.contentType="application/octet-stream";
			request.method=URLRequestMethod.POST;
			var binXML: ByteArray = new ByteArray();
			partialconversion=String(exptResults)
			partialconversion=partialconversion.split("\r\n").join("\n");
			var xmlString:String=partialconversion;
			trace(xmlString);
			binXML.writeUTFBytes(xmlString);
			request.data=binXML;

			var loader: URLLoader = new URLLoader();
			loader.addEventListener(Event.COMPLETE, phpDone);
			loader.load(request);
		}

		// once the php is done, this is called. The php echos back the file,
		// so it can confirm the save via that.
		private function phpDone(event: Event):void {
			event.target.removeEventListener(Event.COMPLETE, phpDone);
			var returnString:String=event.target.data.split("\n").join("").split("\r").join("");
			var ourXML:String=partialconversion.split("\n").join("").split("\r").join("");

			if (returnString!=ourXML) {
				trace("Return value from php did not match correctly, your save may have failed.");
			}
		}
	}
}