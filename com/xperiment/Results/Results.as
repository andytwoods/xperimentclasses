﻿package com.xperiment.Results {
	import com.xperiment.ExptWideSpecs;
	import com.xperiment.runner.runner;

	public class Results {
		//this class holds OVERALL experiment data AND intermittant variables which need saving (the latter of which is not saved in the final results of the study).
		
		
		private static var _instance:Results;
		private var _finalResults:XML;
		private var _ongoingExperimentResults:XMLList=new XMLList;
		private var _storedVars:Array = new Array;
		
		public function kill():void{
			_finalResults=null;
			_ongoingExperimentResults=null;
			_storedVars=null;
			Results._instance =null;
			Results._instance=new Results(new PrivateResults());
		}
		
		public function checkDataExists():Boolean{
			return _ongoingExperimentResults.length()>0;
		}
		
		public function giveOngoingResults():String{
			return _ongoingExperimentResults.toString();
		}
		
		public function storeVariable(dat:Object):void{
			//dat.name= variable name
			//dat.data = variable data
			//trace("storing this var and val:",dat.name+"="+dat.data);
			_storedVars[dat.name]=dat.data;
		}
		
		public function getStoredVariable(nam:String):String{
			//trace("asked for this var:",nam,". Returning this:"+_storedVars[nam]);
			if(_storedVars.hasOwnProperty(nam))return _storedVars[nam];
			else return "";
		}

		
		public function replaceWithVariables(str:String):String{
			//extracts variables, defined with the dollar symbol and replaces them with their looked up values back into the String;
			var ALLCAPS:RegExp= /^[A-Z0-9]*$/
			var NOLETTERS:RegExp = /[^a-zA-Z0-9]/
			var currentLetter:String;
			var buildingTxtBlock:String="";
			var newStr:String="";
			
			while (str.length>0){
				currentLetter=str.substr(str.length-1,1);
				str=str.substr(0,str.length-1);
				
				if(ALLCAPS.test(currentLetter+buildingTxtBlock)){
					buildingTxtBlock=currentLetter+buildingTxtBlock;
				}
				else{
					if(NOLETTERS.test(currentLetter)){
						//where lookupval=currentLetter+buildingTxtBlock
						if(NOLETTERS.test(newStr.charAt(0))|| newStr.length==0){
							//currentLetter+buildingTxtBlock
							if(_storedVars.hasOwnProperty(buildingTxtBlock))newStr=_storedVars[buildingTxtBlock]+newStr; //exactly the same as 'getStoredVariables' function except what is returned if not exists
							else newStr=currentLetter+"!!!Does-not-exist$"+buildingTxtBlock+newStr;
							buildingTxtBlock="";
						}
						else{
							newStr = currentLetter+buildingTxtBlock+newStr;
							buildingTxtBlock="";
						}
					}
					else{
						newStr = currentLetter+buildingTxtBlock+newStr;
						buildingTxtBlock="";
					}
				}
			}
			//where lookupval=currentLetter+buildingTxtBlock
			if(ALLCAPS.test(currentLetter+buildingTxtBlock) && NOLETTERS.test(newStr.charAt(0))){
				if(_storedVars.hasOwnProperty(buildingTxtBlock))newStr=_storedVars[buildingTxtBlock]+newStr; //exactly the same as 'getStoredVariables' function except what is returned if not exists
				else newStr=currentLetter+"!!!Does-not-exist$"+buildingTxtBlock+newStr;
			}
			else newStr = buildingTxtBlock+newStr
			
			return newStr; //stringlogic cannot deal with text so this converts variable values that are text to unique numbers
		}
		
		public function convertStringValsToNums(str:String):String{
			//extracts variables, defined with the dollar symbol and replaces them with their looked up values back into the String;
			var textBlock:Array=new Array;
			str="1"+str; //a slight hack to avoid a lot of coding to look for specifically for when str.length=0;
			var regExp:RegExp= /^[a-zA-Z_][a-zA-Z0-9_]*$/
			var currentLetter:String;
			var buildingTxtBlock:String="";
			var newStr:String="";
			
			while (str.length>0){
				currentLetter=str.substr(str.length-1,1);
				str=str.substr(0,str.length-1);
				if(!regExp.test(currentLetter)){
					if(buildingTxtBlock.length!=0 && textBlock.indexOf(buildingTxtBlock)==-1)textBlock.push(buildingTxtBlock);
					if(buildingTxtBlock.length!=0)newStr=currentLetter+((textBlock.indexOf(buildingTxtBlock)+1))+newStr; 
					else newStr=currentLetter+newStr;
					buildingTxtBlock="";
				}
				else{
					buildingTxtBlock=currentLetter+buildingTxtBlock;
				}
			}
			return newStr.substr(1);
		}
		
		public function get finalResults():XML
		{
			return _finalResults;
		}

		public function Results(pvt:PrivateResults){
			PrivateResults.alert();
		}
		
		public function addInfoToResultsFile(txt:String):void{
			
		}
		
		public static function getInstance():Results
		{
			if(Results._instance ==null){
				Results._instance=new Results(new PrivateResults());
			}
			return Results._instance;
		}	
		
		public function setup():void
		{
			//_ongoingExperimentResults=new XMLList;
			//_ongoingExperimentResults=getPracticeData().*;		
		}	
		

		
		public function giveOngoing(instructions:Object):Array{
			//trace(_ongoingExperimentResults);
			return XMLMaths.dataListFromTrialNames(_ongoingExperimentResults,instructions) as Array;
		}
		
		
		public function give(res:XML):void
		{
			
			trace(11,res)
			if(res!=null)_ongoingExperimentResults+=res;
		}
		
		public function sortFinalResults(betweenSJsID:String,nerdStuff:String):void
		{

			var currentDate:Date = new Date();
			var oldDate:Date=ExptWideSpecs.ExptWideSpecs.results.value;
			var duration:Number= currentDate.getSeconds()+(currentDate.getMinutes()*60)+currentDate.getHours()*60*60+(currentDate.getDay()*60*60*24)-oldDate.getSeconds()-(oldDate.getMinutes()*60)-oldDate.getHours()*60*60-(oldDate.getDay()*60*60*24);
			//CPUarch: e cpuArchitecture property can return the following strings: "PowerPC", "x86", "SPARC", and "ARM". The server string is ARCH.
			//OS see bottom of this as file for description
			//version see bottom...
			var tempResults:XML=<Experiment id={ExptWideSpecs.ExptWideSpecs.info.id}>
			 <betweenSJsID>{betweenSJsID}</betweenSJsID>
			 <nerdStuff>{nerdStuff}</nerdStuff>
			<timeStart>{ExptWideSpecs.ExptWideSpecs.results.value} </timeStart>
			 <approxDurationInSeconds>{duration}</approxDurationInSeconds>
			{_ongoingExperimentResults}
			</Experiment>;
			_finalResults=tempResults;

		}		
		private function getPracticeData():XML
		{
			return new XML;
			/*return <Experiment id="fingerStretch">
  <betweenSJsID/>
  <nerdStuff>Web appID: OS:Windows 7 resX:1920 resY:1080 DPI:72 CPUarch:x86 version:WIN 11,1,102,55</nerdStuff>
  <deviceLanguage>Capabilities.language</deviceLanguage>
  <timeStart>Tue Jul 24 12:07:41 GMT+0200 2012</timeStart>
  <timeEnd>Tue Jul 24 12:07:51 GMT+0200 2012</timeEnd>
  <approxDurationInSeconds>10</approxDurationInSeconds>
  <trialData name="ArrowRightShadeTop" trialReplication="0">
    <order>0</order>
    <rt>1221</rt>
    <answer>false</answer>
    <buttonPressed_Count1>r</buttonPressed_Count1>
  </trialData>
  <trialData name="ArrowRightShadeTop" trialReplication="0">
    <order>1</order>
    <rt>1782</rt>
    <answer>correct</answer>
    <buttonPressed_Count1>l</buttonPressed_Count1>
  </trialData>
<trialData name="ArrowLeftShadeTop" trialReplication="0">
    <order>0</order>
    <rt>1221</rt>
    <answer>false</answer>
    <buttonPressed_Count1>r</buttonPressed_Count1>
  </trialData>
  <trialData name="ArrowLeftShadeTop" trialReplication="0">
    <order>1</order>
    <rt>1782</rt>
    <answer>false</answer>
    <buttonPressed_Count1>l</buttonPressed_Count1>
  </trialData>
  <trialData name="ArrowRightShadeRight" trialReplication="0">
    <order>0</order>
    <rt>3221</rt>
    <answer>false</answer>
    <buttonPressed_Count1>r</buttonPressed_Count1>
  </trialData>
  <trialData name="ArrowRightShadeRight" trialReplication="0">
    <order>1</order>
    <rt>1782</rt>
    <answer>correct</answer>
    <buttonPressed_Count1>l</buttonPressed_Count1>
  </trialData>
<trialData name="ArrowLeftShadeRight" trialReplication="0">
    <order>0</order>
    <rt>221</rt>
    <answer>false</answer>
    <buttonPressed_Count1>r</buttonPressed_Count1>
  </trialData>
  <trialData name="ArrowLeftShadeRight" trialReplication="0">
    <order>1</order>
    <rt>882</rt>
    <answer>correct</answer>
    <buttonPressed_Count1>l</buttonPressed_Count1>
  </trialData>
<trialData name="ArrowRightShadeBottom" trialReplication="0">
    <order>0</order>
    <rt>1221</rt>
    <answer>false</answer>
    <buttonPressed_Count1>r</buttonPressed_Count1>
  </trialData>
  <trialData name="ArrowRightShadeBottom" trialReplication="0">
    <order>1</order>
    <rt>1782</rt>
    <answer>correct</answer>
    <buttonPressed_Count1>l</buttonPressed_Count1>
  </trialData>
<trialData name="ArrowLeftShadeBottom" trialReplication="0">
    <order>0</order>
    <rt>1221</rt>
    <answer>false</answer>
    <buttonPressed_Count1>r</buttonPressed_Count1>
  </trialData>
  <trialData name="ArrowLeftShadeBottom" trialReplication="0">
    <order>1</order>
    <rt>1782</rt>
    <answer>false</answer>
    <buttonPressed_Count1>l</buttonPressed_Count1>
  </trialData>
  <trialData name="ArrowRightShadeLeft" trialReplication="0">
    <order>0</order>
    <rt>3221</rt>
    <answer>false</answer>
    <buttonPressed_Count1>l</buttonPressed_Count1>
  </trialData>
  <trialData name="ArrowRightShadeLeft" trialReplication="0">
    <order>1</order>
    <rt>1782</rt>
    <answer>correct</answer>
    <buttonPressed_Count1>r</buttonPressed_Count1>
  </trialData>
<trialData name="ArrowLeftShadeLeft" trialReplication="0">
    <order>0</order>
    <rt>221</rt>
    <answer>false</answer>
    <buttonPressed_Count1>r</buttonPressed_Count1>
  </trialData>
  <trialData name="ArrowLeftShadeLeft" trialReplication="0">
    <order>1</order>
    <rt>882</rt>
    <answer>correct</answer>
    <buttonPressed_Count1>r</buttonPressed_Count1>
  </trialData>
</Experiment>;*/
		}
	}
	
}
