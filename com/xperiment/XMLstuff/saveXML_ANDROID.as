﻿package com.xperiment.XMLstuff{

	import com.Logger.Logger;
	import com.mobile.saveFilesInternally;
	import com.xperiment.ExptWideSpecs;
	import com.xperiment.XMLstuff.saveXML;
	
	import flash.display.*;
	import flash.events.*;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.filesystem.File;
	import flash.net.*;
	
	/*
	works like this:
	var filename:String = "saveXML.php";
	var dummy:saveXMLtoServerFile = new saveXMLtoServerFile(myData, filename);
	
	ps myData is XML
	*/


	public class saveXML_ANDROID extends saveXML{
	

		override public function sendEmergencyEmail(errInfo:String=""):void {
			
			var connector:String = "&";
			if (results.nerdStuff[0].indexOf("Windows")==-1) connector="?";

			if (emergencyEmailSentAlready==false) {
				logger.log("   ---sending emergency data in email---");
				logger.log("			to:		 " + ExptWideSpecs.ExptWideSpecs.fileInformation.emergencyEmailForMobile)
				logger.log("			subject:	 " + ExptWideSpecs.ExptWideSpecs.fileInformation.emergencyEmailForMobileSubject);
				logger.log("   ---sending emergency data in email---");
				emergencyEmailSentAlready=true
				var urlString:String = "mailto:";
				urlString += ExptWideSpecs.ExptWideSpecs.email.toWhom;
				urlString += connector+"subject=";
				urlString +="emergencyEmail "+ExptWideSpecs.ExptWideSpecs.email.subject;
				urlString += "&body=";
				urlString +="There was a problem when trying to save your results.  We hope you don't mind, but could you send this email to us. It contains a backup copy of your results. Thanks!\n\n"  
				urlString += escape(String(results));
				//trace(urlString);
				if(errInfo!="")urlString += "\n\n error Info:"+errInfo;
				successQueryF(urlString);				
				navigateToURL(new URLRequest(urlString));
				listOfRequiredSaves.FORCEemergencyEmailForMobile=String(true);
				everythingSaved();
			}
		}
		
		override public function saveXMLtoLocalFile():void{
			
			var masterLoc:String=ExptWideSpecs.ExptWideSpecs.computer.XperimentDataFolderLocation as String;
			
			var locat:String=ExptWideSpecs.ExptWideSpecs.computer.saveLocallySubFolder as String;

			
			var theDate:Date = new Date();
			var saveName:String=theDate.getFullYear()+"_M"+theDate.getMonth()+"_D"+theDate.getDate()+"_H"+theDate.getHours()+"_M"+theDate.getMinutes()+"_S"+theDate.getSeconds()+"_R"+Math.floor(Math.random()*1000)+".xml";
			
			var saveLocally:saveFilesInternally=new saveFilesInternally(locat,masterLoc);
			
			listOfRequiredSaves.saveToPortableDevice = String(saveLocally.saveFile(saveName,super.results,""));
			
			if(listOfRequiredSaves.saveToPortableDevice){
				logger.log("have successfully saved your data to your portable device:"+locat);
				logger.log("-----------------------------------------------");
				} else{
                logger.log('Unable to save data to internal device');
				logger.log("-----------------------------------------------");
				
			}
			super.everythingSaved();
		}
	}

}