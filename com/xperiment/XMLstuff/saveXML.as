﻿package com.xperiment.XMLstuff{

	import com.Logger.Logger;
	import com.mobile.saveToServerFile;
	import com.xperiment.ExptWideSpecs;
	import com.xperiment.emailResults;
	
	import flash.display.*;
	import flash.events.*;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.net.*;

	//import com.mobile.saveFilesInternally;
	/*
	works like this:
	var filename:String = "saveXML.php";
	var dummy:saveXMLtoServerFile = new saveXMLtoServerFile(myData, filename);
	
	ps myData is XML
	*/

	public class saveXML extends Sprite{

		public var results:XML;
		public var emergencyEmailSentAlready:Boolean=false;
		public var listOfRequiredSaves:Array = new Array;
		public var logger:Logger;
		public var successQueryF:Function;
	
		public function saveXML(){
			logger=Logger.getInstance();
		}


		public function saveProcedure(r:XML,successQueryF:Function):void {
			this.successQueryF=successQueryF;
			results=r;
			
			logger.log("-----------------------------------------------");
			logger.log("------------Saving your data-------------------");

			logger.log("saveToServerFile="+ExptWideSpecs.ExptWideSpecs.fileInformation.saveToServerFile);
			logger.log("send data via email="+ExptWideSpecs.ExptWideSpecs.email.toWhom); 
			logger.log("Only if on mobile: saveToPortableDevice="+(ExptWideSpecs.ExptWideSpecs.fileInformation.saveToPortableDevice!=""));
			logger.log("Only if on mobile: FORCEemergencyEmailForMobile="+ExptWideSpecs.ExptWideSpecs.fileInformation.FORCEemergencyEmailForMobile);
			logger.log("-----------------------------------------------");
		
			if (ExptWideSpecs.ExptWideSpecs.fileInformation.saveToServerFile && ExptWideSpecs.ExptWideSpecs.fileInformation.saveToServerFile==true){
				listOfRequiredSaves.saveToServerFile = new String();
				saveXMLtoServerFile();
			}
			
			if (ExptWideSpecs.ExptWideSpecs.computer.saveLocallySubFolder!=""){
				listOfRequiredSaves.saveLocally = new String();
				saveXMLtoLocalFile();
			}
			

			//if (ExptWideSpecs.fileInformation.FORCEemergencyEmailForMobile && ExptWideSpecs.fileInformation.FORCEemergencyEmailForMobile!=""){
				//sendEmergencyEmail();
			//}
			
			if (ExptWideSpecs.ExptWideSpecs.email.toWhom && ExptWideSpecs.ExptWideSpecs.email.toWhom!="false" ){
				listOfRequiredSaves.email = new String();
				
				var myEmailAddress:String=ExptWideSpecs.ExptWideSpecs.email.myAddress;
				var toWhomAddress:String=ExptWideSpecs.ExptWideSpecs.email.toWhom;
				var subject:String=ExptWideSpecs.ExptWideSpecs.email.subject;
				var tempResults:String = String(results);

				if (Boolean(ExptWideSpecs.ExptWideSpecs.email.simpleOutput)==true) tempResults=simpleResults(results);
				if(ExptWideSpecs.ExptWideSpecs.email.emergencyEmail!="false"){
					var emailXML:emailResults=new emailResults(sendEmergencyEmail,tempResults);
				}
				listOfRequiredSaves.email=String(true);
				everythingSaved();
			}

		}

		public function sendEmergencyEmail(errInfo:String=""):void {
			var connector:String = "&";
			if (results.nerdStuff[0].indexOf("Windows")==-1) connector="?";

			if (emergencyEmailSentAlready==false) {

				logger.log("   ---sending emergency data in email---");
				logger.log("			to:		 " + ExptWideSpecs.ExptWideSpecs.fileInformation.emergencyEmailForMobile)
				logger.log("			subject:	 " + ExptWideSpecs.ExptWideSpecs.fileInformation.emergencyEmailForMobileSubject);
				logger.log("   ---sending emergency data in email---");
				emergencyEmailSentAlready=true
				var urlString:String = "";
				urlString +="<font size= '20'><b>There was a problem when trying to save your results.</b></font>\n\n<font size= '20'>We hope you don't mind, but could you send the text below to "+ExptWideSpecs.ExptWideSpecs.email.toWhom+". Below is a backup copy of your results. Thanks!\n\nFor your convenience, this text has been copied to your clipboard.\n\n"  
				successQueryF(urlString,(results.toString()) + "\n\n error Info:"+errInfo);
				listOfRequiredSaves.FORCEemergencyEmailForMobile=String(true);
				everythingSaved();
				
			}
		}
		
		public function saveXMLtoLocalFile():void{
			logger.log("saveXMLtoLocalFile is not possible for an 'on web' experiment.  Best to set saveXMLtoLocalFile='false' or remove altogether");
			listOfRequiredSaves.saveLocally = String(false);
			everythingSaved();
		}

		public function saveXMLtoServerFile():void {
			var phpLocation:String=ExptWideSpecs.ExptWideSpecs.fileInformation.saveDataURL+"/saveXML.php";
			var saveToServerF:saveToServerFile = new saveToServerFile(phpLocation,results,logger);
			saveToServerF.addEventListener("saveXMLtoServerFile_saved",saveXMLtoServerFile_saved);
			saveToServerF.addEventListener("saveXMLtoServerFile_fail",saveXMLtoServerFile_fail);
			if(ExptWideSpecs.ExptWideSpecs.email.emergencyEmail!="false")saveToServerF.addEventListener("saveXMLtoServerFile_emergency",saveXMLtoServerFile_emergency);

		}
		
		private function saveXMLtoServerFile_saved(e:Event):void{saveXMLtoServerFile_combined(e,false);}
		private function saveXMLtoServerFile_fail(e:Event):void{saveXMLtoServerFile_combined(e,false);}
		private function saveXMLtoServerFile_emergency(e:Event):void{sendEmergencyEmail();saveXMLtoServerFile_combined(e,false);}
		
		private function saveXMLtoServerFile_combined(e:Event, success:Boolean):void{
			e.target.removeEventListener("saveXMLtoServerFile_saved",saveXMLtoServerFile_saved);
			e.target.removeEventListener("saveXMLtoServerFile_fail",saveXMLtoServerFile_fail);	
			e.target.removeEventListener("saveXMLtoServerFile_emergency",saveXMLtoServerFile_fail);	
			listOfRequiredSaves.saveToServerFile=String(success);
			everythingSaved();
		}

		public function everythingSaved():void{
			var count:uint=0;
			var numberOfRequired:uint=0;
			
			for each (var myBool:Boolean in listOfRequiredSaves){
				if (myBool !="") count++;
				numberOfRequired++;
			}
			
			if (count==numberOfRequired) {
				logger.log("told xperiment to stop everything");
				this.dispatchEvent(new Event("dataSaved"))
				};
		}

		public function simpleResults(str:String):String {
			var removeStr:RegExp=/<(\/).*?>/g;
			str=str.replace(removeStr,"");

			removeStr=/<.*?trialData.*>/g;
			str=str.replace(removeStr,"");

			removeStr=/<buttonPressed_Count1>Continue/g;
			str=str.replace(removeStr,"");

			removeStr=/<Experiment id="study1">/g;
			str=str.replace(removeStr,"");

			removeStr=/(\n)/gm;
			str=str.replace(removeStr,"");

			removeStr=/(\<)/g;
			str=str.replace(removeStr,"\n\n");
			removeStr=/(\/)/g;
			str=str.replace(removeStr,"");
			removeStr=/(\>)/g;
			str=str.replace(removeStr,"\t");
			return str;
		}
	}
}