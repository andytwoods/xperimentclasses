﻿package com.xperiment{
	import flash.display.*;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	import flash.text.TextFormat;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormatAlign;
	import flash.events.*;
	import flash.utils.getDefinitionByName;
	import flash.display.StageDisplayState;
	import flash.display.StageScaleMode;
	import com.trialOrderFunctions;
	import com.xperiment.trial.Trial;
	import com.xperiment.XMLstuff.saveXML;
	import com.xperiment.XMLstuff.saveXMLtoServerFile;
	import com.xperiment.XMLstuff.importExptScript;

	public class TrialListGenerator extends Sprite {

		//- PUBLIC & INTERNAL VARIABLES ---------------------------------------------------------------------------
		var trialList:Array=new Array  ;
		var trialListUnitiatedKeptInMemory:Array=new Array  ;
		var trialOrder:Array=new Array  ;
		var trialOrderVariableDirection:Array=new Array  ;//this might be dead,,, remove?
		var haveAfterWhichTrial:Array=new Array  ;
		var ongoingExperimentResults:XMLList=new XMLList  ;
		var currentTrial:Number=0;//negative values = instructions etc.
		var trialProtocolList:XML=<exptData />;
		var trialToSubmitDataOn:uint=10000;
		private var theStage:Stage;
		private var arrayOfSplices:Array=new Array  ;


		var trialListOrder:Array=new Array  ;
		var tempData:importExptScript;
		private var ExptWideSpecs:Array=new Array  ;
		private var trialIJvalues:Array=new Array  ;

		//private var _dummyVarToAddBoxToAppCompilation1:Trial_imageCollage_Blocks2;
		private var _dummyVarToAddBoxToAppCompilation1:Trial;
		//private var _dummyVarToAddBoxToAppCompilation4:Trial_instructions;
		//private var _dummyVarToAddBoxToAppCompilation5:specialTrial_login;

		//below where: trial name, number trials, 
		//////////////ADDED
		public var storedVariables:Array=new Array  ;
		//////////////ADDED


		//- CONSTRUCTOR -------------------------------------------------------------------------------------------

		public function TrialListGenerator(value:Stage) {
			theStage=value;
			theStage.scaleMode=StageScaleMode.SHOW_ALL;
			tempData=new importExptScript  ;
			tempData.addEventListener("dataLoaded",continueStudyAfterLoadingPause);
			//setBackgroundColour();
		}


		//- background colour -------------------------------------------------------------------------------------------

		var square:Sprite=new Sprite  ;

		function changeBGcolourSize(event:FullScreenEvent) {
			theStage.removeChild(square);
			var colour:Number=ExptWideSpecs.screen.BGcolour;
			square.graphics.lineStyle(3,colour);
			square.graphics.beginFill(colour);
			square.graphics.drawRect(0,0,theStage.fullScreenWidth,theStage.fullScreenHeight);
			square.graphics.endFill();
			square.x=theStage.stageWidth-theStage.fullScreenWidth/2;
			square.y=theStage.stageHeight-theStage.fullScreenHeight/2;
			theStage.addChildAt(square,0);//future job: will need to remove this at some distant future point.
		}

		private function setBackgroundColour(colour:Number) {
			square.graphics.lineStyle(3,colour);
			square.graphics.beginFill(colour);
			square.graphics.drawRect(0,0,theStage.stageWidth,theStage.stageHeight);
			square.graphics.endFill();
			square.x=0;
			square.y=0;
			theStage.addChild(square);//future job: will need to remove this at some distant future point.
		}

		//-------------------------------------------------------------------------------------------


		private function composeTrial(i,j,tNum:uint,adhoc:Boolean,blockGroup:String,blockType:String) {

			var trialNumTag:uint=tNum;
			var tempGenAttribs:XMLList=trialProtocolList.TRIAL[i].generalAttributes;
			////////////////////////////////////////changes code within generalAttributes, given specificAttributes
			var tempSpecAttribs:XMLList=trialProtocolList.TRIAL[i].specificAttributes;
			tempGenAttribs.trialName=trialProtocolList.TRIAL[i].@trialName;
			//tempGenAttribs.order=tNum;
			tempGenAttribs.duplicateTrialNumber=j;
			tempGenAttribs.loadOnTheFly=trialProtocolList.TRIAL[i].@loadOnTheFly;
			var tempClassReference:Class=getDefinitionByName(trialProtocolList.TRIAL[i].@TYPE) as Class;
			var tempTrial:Trial=new tempClassReference  ;
			//var tempTrial:Trial = new tempClassReference() as String;
			tempTrial.setup(theStage,tempGenAttribs,tempSpecAttribs,tNum,storedVariables);

			storedVariables=combineAssociativeArraysDestructively(storedVariables,tempTrial.getTempStoredVars());//run at the end of each trial AND when trial is created


			if (blockType=="undefined") {
				blockType="fixed";
			}

			if (blockType=="") {
				blockType="fixed";
			}


			var tempIJ:Array=new Array  ;
			tempIJ.i=blockGroup;
			tempIJ.j=j;
			tempIJ.blockGroup=blockGroup;
			tempIJ.blockType=blockType;
			trialIJvalues.push(tempIJ);

			trialList.push(tempTrial);

			if (adhoc==false) {
				trialOrder.push(trialList.length-1);
				trialListOrder.push(trialProtocolList.TRIAL[i].blockGroup.text());
				var tempString:String=trialProtocolList.TRIAL[i].blockGroup.attribute("trialNumber");
				var tempArray:Array=new Array  ;
				tempArray.afterTrial=tempString==""?"-":tempString;
				tempArray.trialNumber=i;
				haveAfterWhichTrial.push(tempArray);
			}

			if (trialProtocolList.TRIAL[i].@submitData=="true") {
				trialToSubmitDataOn=trialList.length;
			}


		}

		private function continueStudyAfterLoadingPause(event:Event) {

			//setBackgroundColour(trialProtocolList.SETUP[0].BGcolour.text()); //remove this 

			trialProtocolList=tempData.giveMeData();
			var ensuresFinishedRunning:Boolean=specifyExperimentWideVariableDefaults();
			var trialOrderSpecifier:Array=new Array  ;
			tempData=null;
			var newTrialOrder:Array=new Array  ;

			//for (var u:uint=0; u<trialProtocolList.length; u++){
			//  trialProtocolList[u].specificAttributes = new Array;
			//}

			//below sets up ALL the trials before runtime.
			var counter:uint=0;
			var adhoc:Boolean=false;//ensures early stuff set up.
			for (var i:uint=0; i<trialProtocolList.TRIAL.length(); i++) {
				trace(trialProtocolList.TRIAL[i].@TYPE+" x "+trialProtocolList.TRIAL[i].numberTrials.text());

				var tempType:String=trialProtocolList.TRIAL[i].blockGroup.@trialOrder;
				//trace("111 "+tempType);
				var tempBlockGroup:String=trialProtocolList.TRIAL[i].blockGroup.text();

				if (tempBlockGroup=="0") {
					trialOrderSpecifier.push("fixed");
				} else if (tempType=="") {
					trialOrderSpecifier.push("random");
				} else {
					trialOrderSpecifier.push(tempType);
				}

				var NumTrials:uint=trialProtocolList.TRIAL[i].numberTrials.text();

				for (var j:uint=0; j<NumTrials; j++) {
					composeTrial(i,j,counter,adhoc,tempBlockGroup,tempType);
					counter++;
				}
			}

			var blockArray:Array=new Array  ;
			for (var pp:uint=0; pp<trialIJvalues.length; pp++) {
				//trace("trialIJvalues["+pp+"]: "+trialIJvalues[pp].i+" "+trialIJvalues[pp].j+"  bb: "+blockArray);
				var tempStr:String=trialIJvalues[pp].blockGroup;
				var tempNam:String=trialIJvalues[pp].blockType;
				//trace("t: "+tempNum);

				if (blockArray[tempStr]==undefined) {
					blockArray[tempStr]=new Array  ;
					blockArray[tempStr].push(tempNam);
				}
				//trace("tempNam: "+tempNam);

				blockArray[tempStr].push(pp);
			}

			for (pp=0; pp<blockArray.length; pp++) {
				//trace("blockArray "+blockArray[pp]+"  l: "+blockArray.length);
			}

			trace("blockArray: "+blockArray);
			//trace("newTrialOrder: "+newTrialOrder);
			////////////////////////this bit creates a randomly ordered script for the trials)
			/////future job: include, in XML input, whether a trial is 'fixed' in order or not, and sort out shuffle.

			var tempTrialOrder=new Array(trialList.length);
			trialOrder=new Array  ;
			//var trialOrderSpecifierZeroRemoved:Array=new Array  ;

			for (i=0; i<tempTrialOrder.length; i++) {
				tempTrialOrder[i]=i;
			}

			//trailOrder contains a simple list of numbers that correspond to trial number.  
			//This used to determine which trial to run stored in trialList.

			var numberRandomTrialTypes:uint=blockArray.length;
			var ExptWideSpecs:Array=new Array  ;
			var shuffledTrials:Array=new Array  ;//might be best to call this 'nonShuffled Trials'.
			var arrayRandomTrials:Array=new Array  ;

			trace("initial trialOrder: "+trialOrder);
			for (i=0; i<blockArray.length; i++) {//zzzz
				if (blockArray[i]!=undefined) {
					var newTrials:Array=new Array  ;
					var tempBlockArr:Array=blockArray[i];
					var tempTrialType:String=tempBlockArr[0];
					tempBlockArr.shift();
					if (tempTrialType=="random") {
						newTrials=shuffleArray(tempBlockArr);
					} else if (tempTrialType=="fixed") {
						newTrials=tempBlockArr;
					} else if (tempTrialType.substring(0,23)=="repeatRandBlocksDifEnds") {
						newTrials=trialOrderFunctions.repeatRandBlocksDifEnds(tempBlockArr,extractVariables(tempTrialType));


					}
					trace("newTrials: "+newTrials);
					trialOrder=trialOrder.concat(newTrials);
				}
			}
			trace("trialOrder after shuffle: "+trialOrder);
			////////////////////////////////////////////////////////////////////////////////////

			////////////////////////////////////////// this splices all non ordered trials into trialorder.
			for (i=0; i<trialListOrder.length; i++) {
				if (trialListOrder[i]==0) {
					if (haveAfterWhichTrial[i].afterTrial=="-") {
						trialOrder.splice(i,0,tempTrialOrder[i]);
					} else {
						trialOrder.splice(haveAfterWhichTrial[i].afterTrial,0,tempTrialOrder[i]);
					}
				}
			}


			for (i=0; i<arrayOfSplices.length; i++) {
				var SplicVars:Array=arrayOfSplices[i];
				var funct:Array=splitIntoArray(SplicVars[0]," ");
				if (funct[0]=="rand") {
					var randNum:Number=Math.random()*100;
					if (randNum>Number(funct[1])) {
						var tempArr:Array=trialOrder.splice(SplicVars[1],SplicVars[2]);
						trialOrder.splice(SplicVars[3],0,tempArr);
						//trace("splice [" + SplicVars+ "] was done as random number ("+(Math.round(randNum))+") exceeded your threshold ("+funct[1]+")");
					} else {
						//trace("splice [" + SplicVars+ "] was not done as random number ("+(Math.round(randNum))+") did not exceed your threshold ("+funct[1]+")");
					}

				}


				if (trialToSubmitDataOn==10000) {
					trialToSubmitDataOn=trialList.length-1;
				}

				//trace("trialOrder after splice: "+trialOrder);
			}

			////////////////////////////////////////////////////////////////////////////////////

			nextTrial();//starts the trial sequence
		}

		private function addOne(arr:Array):Array {
			for (var i:uint=0; i<arr.length; i++) {
				arr[i]=arr[i]+1;
			}
			return arr;
		}


		private function extractVariables(str:String):Array {
			str=str.substring(str.indexOf("[")+1,str.indexOf("]"));
			return (str.split(","));
		}

		private function specifyExperimentWideVariableDefaults():Boolean {

			//identification
			ExptWideSpecs.info=new Array  ;
			ExptWideSpecs.info.id="default ID";

			//screen properties
			ExptWideSpecs.screen=new Array  ;
			ExptWideSpecs.screen.BGcolour=0x000000 as Number;

			//save to local file variables
			ExptWideSpecs.localFile=new Array  ;
			ExptWideSpecs.localFile.save=true as Boolean;
			ExptWideSpecs.localFile.filename="needs implementing!" as String;

			//send email variables
			ExptWideSpecs.email=new Array  ;
			ExptWideSpecs.email.send=false as Boolean;
			ExptWideSpecs.email.myAddress="test@sausage.com" as String;
			ExptWideSpecs.email.toWhomAddress="test@sausage.com" as String;
			ExptWideSpecs.email.subject="ExperimentData" as String;

			ExptWideSpecs.spliceTrials=new Array  ;
			ExptWideSpecs.spliceTrials.splices=new String  ;


			/////////////////////////////////////////////////////////////////////////////////////////////////////////
			//////////////////////AW make below elegant (search over children and auto run the property assigner...//
			for (var i:uint=0; i<trialProtocolList.SETUP.children().length(); i++) {
				try {
					var tempClassReference:String=trialProtocolList.SETUP.children()[i].name();
					XMLListObjectPropertyAssigner(tempClassReference,trialProtocolList.SETUP[tempClassReference]);
				} catch (error:Error) {
					trace("you've tried to set a global experiment property that unfortunately does not exist");
				}
			}
			//////////////////////instigate immediately necessary experiment specifications//////////////////////////
			setBackgroundColour(ExptWideSpecs.screen.BGcolour);
			theStage.addEventListener(FullScreenEvent.FULL_SCREEN,changeBGcolourSize);
			var tempArr:Array=splitIntoArray(ExptWideSpecs.spliceTrials.splices,";");

			for (i=0; i<tempArr.length; i++) {
				arrayOfSplices.push(splitIntoArray(tempArr[i],","));
			}

			return true;
		}

		private function splitIntoArray(arr:String,char:String):Array {
			return arr.split(char);
		}

		private function nextTrial() {
			if (currentTrial==trialToSubmitDataOn-1) {
				saveData();
			}

			if (currentTrial<trialList.length) {
				var tempTrial:uint=trialOrder[currentTrial];
				trialList[tempTrial].setOrder(currentTrial);
				trialList[tempTrial].run();
				trace("tempTrial: "+tempTrial);
				trialList[tempTrial].addEventListener("eventFinishedGoToNextTrial",endTrialGoToNextTrial);
				trialList[tempTrial].addEventListener("eventFinishedGoToPrevTrial",endTrialGoToPrevTrial);
			}
		}

		private function saveData() {
			extractStudyData();
			var finalResults:XML=addInfoToResultsFile();
			trace("experiment ended, here's the data:");
			trace(finalResults);
			/////save to file on server
			//if (ExptWideSpecs.localFile.save==true){
			var filename:String="saveXML.php";
			var XMLtoFILE:saveXMLtoServerFile=new saveXMLtoServerFile(String(finalResults),filename);
			//}
			/////send via email
			if (ExptWideSpecs.email.send==true) {
				var myEmailAddress:String=ExptWideSpecs.email.myAddress;
				var toWhomAddress:String=ExptWideSpecs.email.toWhomAddress;
				var subject:String=ExptWideSpecs.email.subject;
				var emailXML:emailResults=new emailResults(String(finalResults),myEmailAddress,toWhomAddress,subject);
			}
		}

		private function extractStudyData() {
			for (var i:uint=0; i<trialList.length; i++) {
				var tempTrial:uint=trialOrder[i];
				trace("end of trial "+i);
				//trace( ":: " + tempTrial + " " + trialList[0].returnTrialData());
				ongoingExperimentResults+=trialList[i].trialData;
			}

		}

		private function endTrialGoToNextTrial(event:Event) {
			storedVariables=combineAssociativeArraysDestructively(storedVariables,event.target.getTempStoredVarsEachTrial());//run at the end of each trial AND when trial is created
			trace("vars: "+storedVariables);
			cleanUpListeners();
			currentTrial++;
			nextTrial();
		}

		private function endTrialGoToPrevTrial(event:Event) {
			cleanUpListeners();
			var tempTrial:uint=trialOrder[currentTrial];

			for (var q:uint=1; q<=2; q++) {//change q max to change the number of trials to go 'into the past'
				trialToSubmitDataOn++;
				//trialOrderVariableDirection stores the original trial numbers as the expt is updated.
				trialOrderVariableDirection.splice(currentTrial,0,trialOrder[currentTrial+q-1]);
				trialOrder.splice(currentTrial+q,0,trialOrder.length);
				composeTrial(trialIJvalues[tempTrial-2+q].i,trialIJvalues[tempTrial-2+q].j,currentTrial+q+1,true,"special","special");
			}

			currentTrial++;
			nextTrial();
		}



		private function combineAssociativeArraysDestructively(arr1:Array,arr2:Array):Array {

			

			for (var nam:String in arr1) {
				trace("arrr:"+nam);
				if (arr2[nam]!=undefined) {
					arr1[nam]=arr2[nam];
				}
			}
			for (nam in arr2) {
				if (arr1[nam]==undefined) {
					arr1[nam]=arr2[nam];
				}
			}

			return arr1;
		}



		private function cleanUpListeners() {
			var tempTrial:uint=trialOrder[currentTrial];
			trialList[tempTrial].removeEventListener("eventFinishedGoToNextTrial",endTrialGoToNextTrial);
			trialList[tempTrial].removeEventListener("endTrialGoToPrevTrial",endTrialGoToPrevTrial);
			//theStage.removeChild(trialList[tempTrial]);
			//trialList[tempTrial]=null;
		}


		private function addInfoToResultsFile():XML {
			var tempResults:XML=<Experiment id={ExptWideSpecs.info.id}>
			{ongoingExperimentResults}
			</Experiment>;;
			return tempResults;
		}

		////////////////////fundamental hidden functions//////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////////

		private function BlockRandomise(x:Array):Array {
			var CutinHalfA:Array=new Array(x.length/2);
			var CutinHalfB:Array=new Array(x.length/2);
			for (var i:int; i<CutinHalfA.length; i++) {
				CutinHalfA[i]=x[i];
				CutinHalfB[i]=x[i+CutinHalfA.length];
			}

			CutinHalfA=shuffleArray(CutinHalfA);
			CutinHalfB=shuffleArray(CutinHalfB);

			if (randRange(0,1)<500) {
				x=CutinHalfA.concat(CutinHalfB);
			} else {
				x=CutinHalfB.concat(CutinHalfA);
			}
			return x;
		}

		private function shuffleArray(a:Array):Array {
			var a2:Array=[];
			while (a.length>0) {
				a2.push(a.splice(Math.round(Math.random()*a.length-1),1)[0]);
			}
			return a2;
		}


		private function randRange(start:Number,end:Number):Number {
			return Math.floor(start+Math.random()*end-start);
		}

		public function setVar(type:String,nam:String,val:*) {
			ExptWideSpecs[nam]=returnType(type,val);
		}


		public function XMLListObjectPropertyAssigner(subType:String,textinfo:XMLList) {

			var attNamesList:XMLList=textinfo.@*;

			var VarName:String=attNamesList.parent().name();//this is the problem one

			//trace("varName: "+VarName);
			//trace (attNamesList+ "---"+attNamesList[i].name());


			for (var i:int=0; i<attNamesList.length(); i++) {
				var tag:String=attNamesList[i].name();// id and color
				var tagValue:String=attNamesList[i];
				//trace("VARIABLES VarName -"+VarName+"-; tag -"+tag+"-; value -"+tagValue+"-");
				if (typeof ExptWideSpecs[subType][String(attNamesList[i].name())]!="undefined") {
					try {
						trace("Global experiment property: "+VarName+"."+tag+" = "+tagValue);

						var a:Array=new Array  ;
						var tempString:String=tag;
						a=tempString.split('.');

						switch (a.length) {
							case 0 :
								trace("PROBLEM----------no variables given to set properties with-----------------!!!!!!!");
								break;
							case 1 :
								ExptWideSpecs[subType][a[0]]=returnType(typeof ExptWideSpecs[subType][a[0]],tagValue);
								break;
						}

						//ExptWideSpecs[VarName][tag]=returnType(typeof ExptWideSpecs[VarName][tag],tagValue);
					} catch (error:Error) {
						trace("PROBLEM----------'"+tag+"' is not a known property of '"+VarName+"'-----------------!!!!!!!");
					}
				} else {
					trace("PROBLEM----------'"+tag+"' property does not exist unfortunately-----------------!!!!!!!");
				}
			}


			//ExptWideSpecs.textInput.setTextFormat(ExptWideSpecs.myTextFormat);

		}

		private function returnType(type:String,value:*):* {
			var returnType;
			switch (type) {
				case "string" :
					returnType=new String  ;
					returnType=String(value);
					break;
				case "int" :
					returnType=new int  ;
					returnType=int(value);
					break;
				case "number" :
					returnType=new Number  ;
					returnType=Number(value);
					break;
				case "boolean" :
					returnType=new Boolean  ;
					returnType=Boolean(value);
					break;
				case "uint" :
					returnType=new uint  ;
					returnType=uint(value);
					break;
				case "array" :
					returnType=new Array  ;
					returnType=value as Array;
					break;
				default :
					trace("incorrect variable type-----------------!!!!!!!");
			}
			return returnType;
		}

		/*
		private function onAllItemsLoaded(e:BulkProgressEvent){
		trace("Loaded");
		}
		
		        public function onAllItemsProgress(evt : BulkProgressEvent) : void{
		            trace(evt.loadingStatus());
		        }*/

	}
}