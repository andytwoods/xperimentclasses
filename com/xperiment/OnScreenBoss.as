﻿package com.xperiment{
	/**
	 Class Name :  onScreenBoss 
	 Author :  Samit Basak
	 
	 Note :  To run the application accurately all the sprites must be loaded before starting the operation.
	 */
	import com.Logger.Logger;
	import com.xperiment.TrueTimer;
	
	import flash.display.Sprite;
	import flash.events.*;
	
	public class OnScreenBoss extends Sprite implements IOnScreenBoss {
		// FOREVER   is the constant that used to run a sprite for ever
		
		public static const FOREVER:Number=99999999999999;
		// Sprites that are to display in stage.
		private var _uSprites:Array;
		// An array of the sprites sorted on the start time.
		public var _startTimeSorted:Array;
		// An array of the sprites sorted on the end time.
		public var _endTimeSorted:Array;
		// The referance of the stage.
		private var pic:Sprite;
		
		
		private var pic_Sprite:uberSprite;
		// The master timer
		public var _mainTimer:TrueTimer;
		private var objsOnScreen:Array=new Array  ;
		
		private var logger:Logger;
		private var running:Boolean;
		
		public function getObjTimes(obj:uberSprite):Array{
			for(var i:uint=0;i<_startTimeSorted.length;i++){
				if(_startTimeSorted[i].sprite==obj){
					return [_startTimeSorted[i].startTime,_startTimeSorted[i].endTime];
					break;
				}
			}
			return null;
		}
		
		public function getPegTimes(peg:String):Array{
			for(var i:uint=0;i<_startTimeSorted.length;i++){
				if(_startTimeSorted[i].peg==peg){
					return [_startTimeSorted[i].startTime,_startTimeSorted[i].endTime];
					break;
				}
			}
			return null;
		}
		
		//this function needs testing.
		public function setTimes(obj:uberSprite, startTime:Number, endTime:Number,duration:Number):Boolean{
			//trace(times)
			var success:Boolean=false;
			for(var i:uint=0;i<_startTimeSorted.length;i++){
				if(_startTimeSorted[i].sprite==obj){
					if(startTime!=-1)	_startTimeSorted[i].startTime=startTime;
					if(endTime!=-1)		_startTimeSorted[i].endTime=endTime;
					if(duration!=-1)	_startTimeSorted[i].endTime=_uSprites[i].startTime+duration;
					
					//if these changes mean that the object is now NOT on screen, stop it.
					if(_startTimeSorted[i].startTime<_mainTimer.currentCount || _startTimeSorted[i].endTime >_mainTimer.currentCount){
						stopObj(obj.peg);
					}
					//if object NOW on screen, add it
					else if (!pic_Sprite.contains(_startTimeSorted[i].uSprite)){
						pic_Sprite.addChild(_startTimeSorted[i].uSprite);
						if(objsOnScreen)objsOnScreen.push(_startTimeSorted[i]);	
					}
					
					success=true;
					break;
					//stopObj
				}
			}
			
			//note, have not done the above for _endTimeSorted as assume it's residents are updated when
			//above is updated. 
			
			//Note that _uSprites and (_startTimeSorted || _endTimeSorted) not necessarily have same content,
			//thus have to do below too.
			
			for(i=0;i<_uSprites.length;i++){
				if(_uSprites[i].sprite==obj){
					if(startTime!=-1)	_uSprites[i].startTime=startTime;
					if(endTime!=-1)		_uSprites[i].endTime=endTime;
					if(duration!=-1)	_uSprites[i].endTime=_uSprites[i].startTime+duration;
					success=success && true;
					break;
				}
			}
			if(success)sortSpritesTIME();
			return success;
		} 
		
		public function sortSpritesTIME():void{
			sortSprites(_startTimeSorted,"startTime");
			sortSprites(_endTimeSorted,"endTime");
		}
		
		/*
		onScreenBoss : The constructor of the class
		@paramvalue The reference of the stage
		*/
		
		public function OnScreenBoss(pic:Sprite,logger:Logger):void {
			this.logger=logger;
			running=true;
			pic=pic;
			pic_Sprite=new uberSprite;
			if(pic)pic.addChild(pic_Sprite);
			_uSprites=new Array  ;
			_startTimeSorted=new Array  ;
			_endTimeSorted=new Array  ;
			
			_mainTimer=new TrueTimer(1,FOREVER);
			_mainTimer.addEventListener(TimerEvent.TIMER,checkForEvent);
			_mainTimer.stop();
		}
		/**
		 elappsedTime - this method returns the elapsed time of the master timer
		 */
		public function elapsedTime():String {
			return String(_mainTimer.currentCount);
		}
		/**
		 checkForEvent - this method checks for the event on every clock ticks
		 @param   evet  Event
		 */
		
		public function checkForEvent(evt:Event):void {
			if(running){
				evt=new Event("ElapsedTime");
				dispatchEvent(evt);
				if (_startTimeSorted.length!=0&&_startTimeSorted[0].startTime==_mainTimer.currentCount) {
					do {
						var v:Object=_startTimeSorted.shift();
						//pic_Sprite.onBefore();
		
						pic_Sprite.addChild(v.uSprite);
						if(objsOnScreen)objsOnScreen.push(v);
							
					} while (_startTimeSorted.length != 0 && _startTimeSorted[0].startTime == _mainTimer.currentCount);
					
				}
				if (_endTimeSorted.length!=0&&_endTimeSorted[0].endTime==_mainTimer.currentCount) {
					do {
						v=_endTimeSorted.shift();
						if(pic_Sprite.contains(v.uSprite)){
							pic_Sprite.removeChild(v.uSprite);
							//trace("OFFScreen:"+v.peg);
						}//problem here
						objsOnScreen.splice(objsOnScreen.indexOf(v)-1,1);

					} while (_endTimeSorted.length != 0 && _endTimeSorted[0].endTime == _mainTimer.currentCount);
				}
				
				if(running==false)removeVars();
			}
			
			
		}
		/**
		 commenceDisplay - this function is used to start the master timmer
		 and all the sprites that should start automatically will play on 
		 exact time.
		 @param  no parameter
		 @return no return
		 */
		public function commenceDisplay():void {
			sortSprites(_startTimeSorted,"startTime");
			//displaySpriteArray(_startTimeSorted);
			sortSprites(_endTimeSorted,"endTime");
			//displaySpriteArray(_endTimeSorted);
			_mainTimer.__start();
		}
		
		/**
		 cleanUpScreen -- this function is used to cleanup the screen 
		 Resets the master timer. Clears the sorted arrays
		 @param    no parameter
		 @return no return value
		 */
		public function cleanUpScreen():void {
			running=false;
			_mainTimer.removeEventListener(TimerEvent.TIMER,checkForEvent);
			_mainTimer.__stop();
			_mainTimer.__reset();
			
			if(logger)logger.log(" CLEAN UP SCREEN");
			for (var i:int=0; i < _endTimeSorted.length; i++) {
				if (_endTimeSorted[i].uSprite.parent!=null) {
					if(pic_Sprite.contains(_endTimeSorted[i].uSprite))pic_Sprite.removeChild(_endTimeSorted[i].uSprite);
				}
			}
			_startTimeSorted.splice(0,_startTimeSorted.length);
			_endTimeSorted.splice(0,_endTimeSorted.length);
			objsOnScreen.splice(0,objsOnScreen.length);
			_uSprites.splice(0,_uSprites.length);
			
			
		}
		
		public function removeVars():void{
			_uSprites=null;
			_startTimeSorted=null;
			_endTimeSorted=null;
			pic=null;
			pic_Sprite=null;
			objsOnScreen=null;
			_mainTimer=null;
		}
		
		/**
		 addDrivenElement - this method used to add dynamic sprites in the array.
		 @param   element:sprite   this is the sprite that is to add as dynamic sprite
		 @param id:String  The id of the sprite
		 @param      startTime:Number The starting time which is minning less as the sprite
		 is to run dynamically
		 @param endTime:Number endTime is used to denote how long the sprite will play on screen.
		 Better to state the duration of the sprite on stage after getting 
		 the instruction to play.
		 @return  no return value
		 */

		
		/**
		 addtoTimedEventList- This is a private method. Method is used to enlist the sprites to the 
		 timed event list.
		 @param element Sprite The sprite to insert into the timed list
		 @param starTime Number The start time  of the sprite 
		 @param  endTime Number The end time of the sprite
		 */
		public function addtoTimeLine(hasBehavs:Boolean,element:uberSprite,startTime:Number,endTime:Number):void {
			if (startTime==0) startTime=1;
			
			var obj:Object= {	peg:		element.peg,
								uSprite:	element,
								startTime:	startTime,
								endTime:	endTime		}	
		
			if(hasBehavs)	_uSprites.push(obj);	
			
			else{			_startTimeSorted.push(obj);
							_endTimeSorted.push(obj);
														}	
		}

		/**
		 sortSprites - This method is used to sort the sprites based on the passed attribute 
		 This method is mainly used to sort the sprites based on start and end time
		 @param sprites Array   the arry of the sprite
		 @param attribute  String  the attribute based on which the sprites need to be sorted
		 @return  no return value
		 */
		public function sortSprites(uSprites:Array,attribute:String):void {
			var tmpObj:Object;
			//displaySpriteArray(sprites);
			for (var j:int=0; j < uSprites.length - 1; j++) {
				for (var i:int=j + 1; i < uSprites.length; i++) {
					if (uSprites[j][attribute]>uSprites[i][attribute]) {		
						tmpObj=uSprites[j];
						uSprites[j]=uSprites[i];
						uSprites[i]=tmpObj;
					}
				}
			}
		}
		
		public function killObj(peg:String):void {
			if(stopObj(peg)){
				var obj:Object;
				for (var i:int=0; i < _uSprites.length; i++) {
					if (_uSprites[i].peg==peg) {
						_uSprites.slice(i,1);
						break;
					}
				}
			}
		}
		
		
		public function stopObj(peg:String):Boolean {
			var obj:Object;
			var index:int;
			var stopped:Boolean=false;
			for (var i:uint=0; i < objsOnScreen.length; i++) {
				
				if (objsOnScreen[i].peg==peg) {
					
					obj=objsOnScreen[i];
					
					if(pic_Sprite.contains(obj.uSprite)){
						
						pic_Sprite.removeChild(obj.uSprite);
			
						index=_endTimeSorted.indexOf(obj);		if (index!=-1) _endTimeSorted.splice([index-1],1);
						
						index=_startTimeSorted.indexOf(obj);	if (index!=-1) _startTimeSorted.splice([index-1],1);
						
						index=objsOnScreen.indexOf(obj);		if (index!=-1) objsOnScreen.splice(index,1);

						stopped=true;
					}
						
					else if(logger)logger.log("you asked to remove a screen element ("+peg+") that is not on screen");
				}
			}
			
			if(stopped)sortSpritesTIME();
			
			return stopped; //only used by killObj function above
		}
		
		/**
		 runDrivenEvent  - Method to commence a particular sprite to start 
		 @param id String   the id of the sprite to start play
		 @return   no return value
		 */
		
		
		public function runDrivenEvent(peg:String,delay:String="",dur:String=""):void {
			var obj:Object;
			for (var i:int=0; i < _uSprites.length; i++) {
				if (_uSprites[i].peg==peg) {
					obj=_uSprites[i];
					break;
				}
			}

			if (obj!=null && objsOnScreen.indexOf(obj)==-1) {
			
				obj.endTime+=_mainTimer.currentCount;
				if(dur!="" && !isNaN(Number(dur))){
					//trace("herere",dur);
					obj.endTime=_mainTimer.currentCount+Number(dur);
					if(!isNaN(Number(delay)))obj.peg+=Number(delay);
				}
				
				var index:int=-1;
				for (i=0; i < _endTimeSorted.length; i++) {
					if (_endTimeSorted[i].peg>obj.peg) {
						index=i;
						break;
					}
				}
				if (index!=-1) {
					var tobj:Object=_endTimeSorted[index];
					_endTimeSorted[index]=obj;
					_endTimeSorted.push(tobj);
				}
				else {
					_endTimeSorted.push(obj);
				}
				
				
				if (delay=="") {
					//pic_Sprite.onBefore();
					//trace("erere",peg)
					pic_Sprite.addChild(obj.uSprite);
					if(objsOnScreen)objsOnScreen.push(obj);
				}
				else {
					obj.startTime+=_mainTimer.currentCount;
					
					if(delay!="" && !isNaN(Number(delay))){
						//trace("22333");
						obj.startTime=_mainTimer.currentCount+Number(delay);
					}
					index=-1;
					for (i=0; i<_startTimeSorted.length; i++) {
						if (obj.startTime<_startTimeSorted[i]) {
							_startTimeSorted.splice(i,0,obj);
							break;
						}
					}
					if (index==-1) {
						_startTimeSorted.push(obj);
					}
					
				}
			}
			else if (obj==null) err(" You asked to start an non-existing object with peg: " + peg);
			
  
		}
		
		private function err(txt:String):void{
			throw new Error("Stimulus presentation Error: "+txt); 
		}
	
	}
}