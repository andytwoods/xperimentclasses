package com.xperiment.BehavLogicAction.Action
{
	import com.xperiment.BehavLogicAction.Action.ActionTypes.ActionCommand;
	import com.xperiment.BehavLogicAction.Action.ActionTypes.ActionMaths;
	import com.xperiment.BehavLogicAction.Action.ActionTypes.ActionSimpleMaths;
	import com.xperiment.BehavLogicAction.PropValDict;
	import com.xperiment.BehavLogicAction.common.SolveEquation;
	

	public class ActionFactory
	{
		public static function Action(action:String,propValDict:PropValDict):IAction
		{
									//includes the equals sign
			if(action.indexOf("()")==-1 && action.indexOf("=")!=-1){ //if has mathematical notation..
				
				if(SolveEquation.test(action,[])==false) 	return new ActionSimpleMaths(action,propValDict.bind,propValDict.updateVal);
				
				else 										return new ActionMaths(action,propValDict.bind,propValDict.updateVal); 
				
			}
			
			else if(action.substr(action.length-2)=="()")	return new ActionCommand(action,propValDict.addCommand,propValDict.incrementPerTrial);
			
			else throw new Error("Wrongly formatted action:"+action);
			return null;
		}
	}
}