package com.xperiment.BehavLogicAction.Action.ActionTypes
{
	import com.xperiment.BehavLogicAction.Action.IAction;
	import com.xperiment.BehavLogicAction.common.IReturnEquation;

	public class ActionCommand implements IAction
	{
		public var action:String;
		private var incrementalUpdate:Function;
		
		public function kill():void{
			action=null;
		}
		
		
		public function ActionCommand(action:String,addCommand:Function,incrementalUpdate:Function)
		{
			this.action=action;
			this.incrementalUpdate=incrementalUpdate;
			addCommand(action);
			
		}
	
		
		public function doAction():Function{			
			return function():void{			
				incrementalUpdate(action)
			};
		}
		
		public function equationNow(orig:Boolean=true):*{

		}	
		
		/*		private function behav_setVariable(action:String,origObj:object_baseClass):void{
		
		//the below is used to replace this variable (prefixed by _) with a local variable (no prefix). 
		//Useful if you have a long bevaviours section and want to swap out varaibles when you have howMany>1.
		var str:String;
		function isLocalVar(obj:object_baseClass, attrib:String):String{
		
		if(attrib.substr(0,1)=="_" && (str=obj.getVar(attrib.substr(1)))!=""){
		//trace("attrib:",str,attrib);
		return str;
		}
		else return attrib;
		}
		
		//trace(333,action,origObj);
		var setVariables:Array=split_quotationless(action,",");
		var lhsPeg:String;
		var rhsPeg:Array = [];
		var lhsVar:String;
		var rhsVar:Array =[];
		var arr:Array;
		var pos:int;
		var lhsObj:object_baseClass;
		var rhsObj:Array =[];
		var tempObj:object_baseClass;
		var count:uint;
		var contin:Boolean;
		//	var suffix:String="";
		
		for(var codeLine:uint=0;codeLine<setVariables.length;codeLine++){
		contin=false;
		arr=split_quotationless(setVariables[codeLine],"=");
		lhsVar=arr[0];
		rhsVar=arr[1].split("+");
		count=rhsVar.length;
		
		pos=lhsVar.indexOf(".");
		if(pos!=-1){
		lhsPeg=lhsVar.substr(0,pos);
		lhsVar=lhsVar.substr(pos+1);
		//trace(3,action,lhsPeg,lhsVar);
		}
		else {
		lhsPeg=origObj.peg;
		lhsObj=origObj;
		}
		
		if(lhsPeg=="this")lhsObj=origObj;
		
		/////////////////////////////////// RHS stuff.  As there can be multiple concatenations (using + symbol), must go through each
		for (var i:uint=0;i<rhsVar.length;i++){
		pos=rhsVar[i].indexOf(".");
		if(pos!=-1){
		rhsPeg[i]=rhsVar[i].substr(0,pos);
		rhsVar[i]=rhsVar[i].substr(pos+1);
		}
		else {
		rhsPeg[i]=origObj.peg;
		rhsObj[i]=origObj;
		//trace(action,lhsPeg,rhsPeg,2);
		count--;
		};
		}
		//////////////////////////////////
		
		
		//trace("heree",action);
		for(i=0;i<drivenStoppableObjs.length;i++){
		tempObj=object_baseClass(drivenStoppableObjs[i]);
		//trace(55,tempObj.peg);
		
		if(!lhsObj && lhsPeg==tempObj.peg)lhsObj=tempObj;
		
		for(var p:uint=0;p<rhsPeg.length;p++){
		
		if(!rhsObj[p] && rhsPeg[p]==tempObj.peg){
		rhsObj[p]=tempObj;
		count--;
		}
		}
		if(lhsObj && count==0){
		contin=true;
		break;
		}
		}
		//trace("in herererer",rhsVar,lhsVar,rhsPeg,contin);
		if(contin){
		
		lhsVar=isLocalVar(lhsObj,lhsVar);//////testing if lhsPEG is a local variable and replacing as appropriate
		
		var combinedRHS:String="";
		var temp:String;
		for(i=0;i<rhsObj.length;i++){
		if(rhsVar[i].charAt(0)=="'" || rhsVar[i].charAt(rhsVar.length-1)=="'"){
		//trace(55,rhsVar,4,action);
		combinedRHS=combinedRHS+rhsVar[i].replace(new RegExp("'","g"),"");
		//trace(combinedRHS,2,action)
		}
		else {
		rhsVar[i]=isLocalVar(rhsObj[i],rhsVar[i]);//////testing if rhsPEG is a local variable and replacing as appropriate
		//trace(rhsVar[i],rhsObj[i],rhsVar[i],55)
		//////////////////////////////////////////////
		////////////If the variable is all capital letters, this means get an experimental wide variable.
		if(rhsVar[i].toUpperCase()==rhsVar[i]){
		if(!exptResults)exptResults = Results.getInstance();
		temp=exptResults.getStoredVariable(rhsVar[i]);
		}
		//////////////////////////////////////////////
		else temp=rhsObj[i].behav_getVar(rhsVar[i]);
		//trace(rhsObj[i].peg,11,temp,action,rhsObj[i].peg);
		if(temp!=null && temp!="") combinedRHS=combinedRHS+temp; //perhaps more logic needs to be added in the future	
		}
		}
		//trace(combinedRHS,33);
		
		//////////////////////////////////////////////
		////////////If the variable is all capital letters, this means save the data experiment wide.
		
		if(lhsVar.toUpperCase()==lhsVar){
		//trace("hererere");
		if(!exptResults)exptResults = Results.getInstance();
		//trace(545,lhsVar,":",combinedRHS);
		exptResults.storeVariable({name:lhsVar,data:combinedRHS})
		
		}
		//////////////////////////////////////////////
		else {
		//trace("44",action,lhsVar,combinedRHS,lhsObj)
		lhsObj.behav_setVar(lhsVar,combinedRHS);	
		}
		}
		else{
		if(lhsVar.toUpperCase()==lhsVar){
		
		var combined:String=rhsVar.join();
		//trace("hererere",combinedRHS);
		if(combined.charAt(0)=="'" && combined.charAt(combined.length-1)=="'"){
		if(!exptResults)exptResults = Results.getInstance();
		//trace(545,lhsVar,":",combined);
		combined=combined.substr(1,combined.length-2);
		exptResults.storeVariable({name:lhsVar,data:combined})
		//trace(22,exptResults.getStoredVariable(lhsVar));
		}
		
		
		}
		log("! You asked to perform this action: "+action+" but there was a problem: object["+lhsPeg+"].variable["+lhsVar+"] = object["+rhsPeg+"].variable["+rhsVar+"].");	
		}
		//}
		//else if(logger)logger.log("!There is a problem with your setting a value in a behaviour with this parameter line:"+action+". Not all the objects / values could be found:"+lhsVar +" "+ lhsPeg +" " + rhsPeg +" " + rhsVar+".");
		}
		}*/
	}
}