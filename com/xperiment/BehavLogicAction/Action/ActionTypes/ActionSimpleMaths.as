package com.xperiment.BehavLogicAction.Action.ActionTypes
{
	import com.xperiment.BehavLogicAction.Action.IAction;
	import com.xperiment.BehavLogicAction.Logic.LogicEquation.Equation.IEval;
	import com.xperiment.BehavLogicAction.common.OneValueEquation;
	import com.xperiment.BehavLogicAction.common.IReturnEquation;

	public class ActionSimpleMaths  implements IAction, IReturnEquation
	{
		public var LHS:OneValueEquation;
		public var RHS:OneValueEquation;
		private var _updateProperty:Function;
		private var action:String;
		
		
		public function ActionSimpleMaths(action:String,bindProperty:Function,updateProperty:Function)
		{
			this.action=action;
			//HERE: Need to split the equation by =, store LHS and calc RHS (but using updateDicts also on the LHS)
			_updateProperty=updateProperty;
			var actArr:Array=action.split("=");
			if(actArr.length==2){
				LHS=new OneValueEquation;
				RHS=new OneValueEquation;
				LHS.equationOrigStr=actArr[0];
				RHS.equationOrigStr=actArr[1]; //rhs formula removed here
				LHS.requestUpdates(bindProperty);
				RHS.requestUpdates(bindProperty);
				actArr=null;
			}
			else throw Error("an ActionSimpleMaths was passed a (simple!) maths problem with more than one equals sign");
			
		}
		
		public function doAction():Function{
			return function():void{_updateProperty(LHS.equationOrigStr,RHS.equationNow(false))};
		}
		
		public function equationNow(orig:Boolean=true):*{
			return LHS.equationNow(orig) + "="+ RHS.equationNow(orig);
		}	
		

	}
}