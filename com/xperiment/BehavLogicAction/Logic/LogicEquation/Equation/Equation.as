package com.xperiment.BehavLogicAction.Logic.LogicEquation.Equation
{
	import com.xperiment.BehavLogicAction.common.SolveEquation;
	
	import flash.utils.getQualifiedClassName;
	
	/**
	 * @flowerModelElementId _ApmHcDfEEeKLpqAb5JQQNA
	 */
	public class Equation implements IEval
	{
		public var left:IEquationSide;
		public var comparison:String;
		public var right:IEquationSide;
		private var logicOutcome:Function;
		
		public function kill():void{
			left.kill();
			left=null;
			if(right){
				right.kill();
				right=null;
			}
		}
		
		
		public function Equation(logicOutcome:Function){
			this.logicOutcome=logicOutcome;
		}
	
		public function update(what:String, toWhat:*):void{
			left.update(what,toWhat);
			if(right)right.update(what,toWhat);//potentially both could be the same...
		}
		
		public function reconstruct(orig:Boolean=true):String{			
			return left.equationNow(orig)+comparison+right.equationNow(orig);
		}

		
		public function eval():Boolean{
			var lhs:*= left.equation;
			
			if(right)var rhs:*= right.equation;
			else{
				if(left.equationNegated)return !Boolean(lhs);
				else					return  Boolean(lhs)
			}

			if(left.equationNegated)lhs=!lhs; //only used to see if object exists / or object is boolean
			if(right.equationNegated)rhs=!rhs; 
			
			if(left.equation  && right.equation){
				
			//test for sameness in Types
				if(getQualifiedClassName(lhs) != getQualifiedClassName(rhs)){
					//trace("you are comparing "+getQualifiedClassName(lhs)+"("+lhs+") with "+getQualifiedClassName(rhs)+"("+rhs+") which is not recommended, and indeed forced to return FALSE." )
					return false;
				}
			
				//trace("eval:",_leftequationSide || _rightequationSide);
				if		(comparison=="==")		return 	lhs ==	rhs;
				else if	(comparison=="!=")		return 	lhs	!= 	rhs;
				else if	(comparison==">") 		return 	lhs	>	rhs;
				else if	(comparison=="<")		return  lhs <	rhs;
				else if	(comparison==">=")		return  lhs >=	rhs;
				else if	(comparison=="<=")		return  lhs <=	rhs;
				else if	(comparison=="===")	return  lhs ===	rhs;
			}
			
			else if(left.equation && !right.equationOrigStr){
				
				return lhs;
			}
			
			else if(left.equationOrigStr && right.equation==null){
				if(left.equationNegated)return true;
				else 					return false;
			}	

			return false;
		}
		
		public function updateDicts(updateDi:Function):void
		{
			for each(var side:String in ["left","right"]){
				if(this[side]!=null)this[side].requestUpdates(updateDi);
			}
		}
				

		
		public function passParameters(lhsStr:String, comparison:String, rhsStr:String):void
		{
			
			this.comparison=comparison;
			if(SolveEquation.test(lhsStr,null)) left = new ManyVarSide(logicOutcome);
			else left=new OneVarSide(logicOutcome);
			left.equationOrigStr=lhsStr;
			
			if(rhsStr!=""){
				if(SolveEquation.test(rhsStr,null)) right = new ManyVarSide(logicOutcome);
				else right=new OneVarSide(logicOutcome);
				right.equationOrigStr=rhsStr;
			}
		}
		
		//used only for testing purposes
		public function setupSides():void{
			left= new OneVarSide(logicOutcome);
			right=new OneVarSide(logicOutcome);
		}
	}
}