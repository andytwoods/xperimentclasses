package com.xperiment.BehavLogicAction.Logic.LogicEquation.Equation
{
	import com.xperiment.BehavLogicAction.common.OneValueEquation;

	internal class OneVarSide extends OneValueEquation implements IEquationSide
	{
		private var logicOutcome:Function = function():void{};
		
		public function OneVarSide(logicOutcome:Function){
			
			this.logicOutcome=logicOutcome;
		}
		
		
		///PROB, being called Multiple times, once for 123 which should not happen
		//NOT a problem actually as this funciton is called when the equationOrigStr is set.
		override public function update(what:String, toWhat:*):void{

			super.update(what,toWhat);
			if(what!=toWhat)logicOutcome();
			
		}
	}
}