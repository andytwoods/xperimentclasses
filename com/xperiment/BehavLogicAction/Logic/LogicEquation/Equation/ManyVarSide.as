package com.xperiment.BehavLogicAction.Logic.LogicEquation.Equation
{
	import com.xperiment.BehavLogicAction.PropValDict;
	import com.xperiment.BehavLogicAction.common.SolveEquation;
	import com.xperiment.parsers.CompiledObject;
	import com.xperiment.parsers.MathParser;

	/**
	 * @flowerModelElementId _9kOqQDfJEeKW7cM3ixcl0w
	 */
	public class ManyVarSide extends SolveEquation implements IEquationSide
	{		
		private var logicOutcome:Function = function():void{};
		
		public function ManyVarSide(bubble:Function){
			this.logicOutcome=logicOutcome;
		}
		
		override public function updateRequests(prop:String,pos:uint):void{
			requestUpdatesArr.push({what:prop,funct:function(what:String, to:*):void{
				_equation[pos]=to;
				if(logicOutcome!=null && what != to) logicOutcome();
			} as Function})
		}
		
	}
}