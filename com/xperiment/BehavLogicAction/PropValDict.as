package com.xperiment.BehavLogicAction
{
	import flash.utils.Dictionary;

	public final class PropValDict
	{
	
		public var updateFunctsDict:Dictionary;
		public var propDict:Dictionary;
		
		public var perTrialProps:Vector.<String>;
		public var perTrialCommands:Vector.<String>;
		
		private static var _instance:PropValDict;
		private var actions:Object = new Object;
		
		public function PropValDict(pvt:PrivateSingletonDict){
			PrivateSingletonDict.alert();
		}
		
		public function propVal(prop:String):String{
			return propDict[prop];
		}
		
		public static function getInstance():PropValDict
		{
			if(PropValDict._instance ==null){
				PropValDict._instance=new PropValDict(new PrivateSingletonDict());
				PropValDict._instance.setup();
			}
			if(PropValDict._instance.updateFunctsDict==null)PropValDict._instance.setup();
			return PropValDict._instance;
		}
		
		
		////////////////////////////////////////
		////////////////////////////////////////
		//Per Trial Functions
		
				////////////property functions
		public function killPerTrial():void{
			if(perTrialProps){
				while(perTrialProps.length>0){
					removeProp(perTrialProps.shift());
				}
				perTrialProps = null;
			}
			
			if(perTrialCommands){
				while (perTrialCommands.length>0){
					removeAction(perTrialCommands.shift());
				}
				perTrialCommands=null;
			}
		}
		
		public function addTrialProps(prop:String):void{
			if(!perTrialProps)perTrialProps = new Vector.<String>;
			if(!perTrialProps.hasOwnProperty(prop)){
				if(prop.indexOf(".")!=-1)perTrialProps.push(prop); //if a trial only variable...
				else if(propDict[prop] == undefined) propDict[prop]=prop; // else, add to Experiment wide variables.
			}
		}
			
		//sets up perTrialProp if necessary else increments it by 1
		public function incrementPerTrial(action:String):void{
			if(propDict[action] !=undefined){
	
				if(propDict[action]==action){
					updateVal(action,"1");
				}
				else{
					updateVal(action,int(propDict[action])+1);
				}
				
			}
			else throw Error("An action occured that was not specified in your script: "+action);
			
			//trace(action,":"+propDict[action]);
		}
		
		public function updateDictsTrialVars(rawProp:String,funct:Function):void{
			if(updateFunctsDict[rawProp] == undefined){
				perTrialProps.push(rawProp);
			}
			bind(rawProp,funct);
		}
		
		
				////////////command functions
		public function addCommand(command:String):void{
			if(!perTrialProps)updateFunctsDict = new Dictionary;
			if(!perTrialProps.hasOwnProperty(command))perTrialProps.push(command);
			bind(command,null);
		}
		
		
		//
		////////////////////////////////////////
		////////////////////////////////////////
			
		public function removeProp(prop:String):void{
			//deleted wrapper 4.1.2013 as uncalled actions threw an error 	if(propDict.hasOwnProperty(prop)){
			//or called actions threw an error								if(propDict[prop]==null || propDict[prop] as String){
				delete propDict[prop];
				delete updateFunctsDict[prop];		
			//}
			//else throw Error("tried to delete a property ("+prop+") but this property is not in propDict!"+propDict[prop]);
		}
		
		public function removeAction(action:String):void{
			throw new Error("unknown error");
		}

		public function setup():void{
			updateFunctsDict=	new Dictionary(true);
			propDict=			new Dictionary(true);
		}
		
		public function setDicts(where:*,what:*):void{
			if(updateFunctsDict[what] == undefined){
				updateFunctsDict[what] = new Array;
				propDict[what]=what;
			}
			updateFunctsDict[what].push(where);
		}
			
		
		//thos function is passed around other classes and used to collect functions
		public function bind(rawProp:String,funct:Function):void{
			if(updateFunctsDict[rawProp] == undefined) updateFunctsDict[rawProp]=new Array;
			if(funct!=null)updateFunctsDict[rawProp].push(funct);
			addTrialProps(rawProp);
			if(propDict[rawProp] == undefined) propDict[rawProp]=rawProp;
		}
		
		public function bindAction(action:String, funct:Function):void
		{
			if(updateFunctsDict[action] != undefined)bind(action, funct);
			else throw new Error ("An response action has been provided for an action that has been specified no where (must be a bug)")
			
		}

		
		private function updatePropEveryWhere(prop:String):void
		{
			if(updateFunctsDict[prop]!=undefined){
				for(var i:uint=0;i<(updateFunctsDict[prop] as Array).length;i++){
					//note have to use logDict.propDict[prop] NOT val as ony the former has been typecasted.
					if(		//if returns false, remove from array.
							//Note, CANNOT assign to Bool first as Bool=undefined is false (killing this logic);
							(updateFunctsDict[prop][i] as Function)(prop,propDict[prop]) //run the update function		
						== false)	updateFunctsDict[prop].splice(i,1);
				}
			}
			else throw Error("Error trying to update a property ("+prop+"): it was not set before.");
		}
		

		
		public function updateVal(prop:String, val:*):void
		{
			if(propDict[prop]!=undefined){
				if(!isNaN(Number(val)))propDict[prop]=Number(val);
					//only do the second comparison if there is indeed evidence for true or false
				else if(["true","false"].indexOf(val.toLowerCase().split("'").join())!=-1){
					if("true"==val.toLowerCase().split("'").join())	propDict[prop]=true;
					else propDict[prop]=false;
				}
				else propDict[prop]=val;
				updatePropEveryWhere(prop);
			}
			
		}
		
		
		public function kill():void{
			if(updateFunctsDict){
				for each (var arr:Array in updateFunctsDict){				
					while(arr.length>0){
						arr[0]=null;
						arr.shift();
					}
				}
				updateFunctsDict=null;
			}
			
			for (var val:String in propDict){
				propDict=null;
			}
			
			if(perTrialProps){
				while(perTrialProps.length>0){
					perTrialProps.shift();
				}
				perTrialProps = null;
			}
			
			if(perTrialCommands){
				while (perTrialCommands.length>0){
					perTrialCommands.shift();
				}
				perTrialCommands=null;
			}
		}
	}
}