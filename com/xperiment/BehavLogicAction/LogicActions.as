package com.xperiment.BehavLogicAction
{
	import com.tools.StrTools;
	import com.xperiment.BehavLogicAction.Action.Actions;


	public class LogicActions
	{

		public var logicActionsVect:Vector.<LogicAction> = new Vector.<LogicAction>; 
		private var propValDict:PropValDict;


		public function LogicActions(propValDict:PropValDict)
		{	
			this.propValDict=propValDict;
		}
		
		public function passLogicAction(logicActionArr:Array):void{
			for each(var logicAction:String in logicActionArr){
				logicActionsVect.push(new LogicAction(logicAction,propValDict));
			}
			
		}
		public function kill():void{
			for each(var v:LogicAction in logicActionsVect){
				v.kill();
			}
			logicActionsVect=null;
		}
		
	

	/*					
		private function giveUpperLevelSplit(str:String,split:String):Array{

			//if there are not an even number of quotes
			if((str.split("'").length-1)%2!=0){
				throw Error("wrongly formed formula (odd number of quotation marks):"+str);
			}
			
			var inBraces:Boolean=false;
			var inBracesOld:Boolean=false;
			var inBracesStr:String="";
			var newStr:String="";
			var inBracesArr:Array = [];
			var index:int=-1;
			
			for(var i:uint=0;i<str.length;i++){
				if(str.charAt(i)=="'"){
					inBraces=!inBraces; //toggles InBraces depending on whether in braces.
					if(!inBraces){
						index++;
						inBracesArr[index]=inBracesStr.substr(1);
						newStr+="'<~"+index+"~>";
						inBracesStr="";
					}
				}
				if(inBraces)inBracesStr+=str.charAt(i);
				else newStr+=str.charAt(i);
			}
			newStr=newStr.replace(new RegExp(split,"g"),"<-~split~->");
			while(index>=0){
				newStr=newStr.replace("<~"+index+"~>",inBracesArr[index]);
				index--;
			}
			return newStr.split("<-~split~->");
		}*/
		
		
		
	
	}
}