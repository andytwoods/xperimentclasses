package com.xperiment.BehavLogicAction
{
	import com.xperiment.BehavLogicAction.Action.Actions;
	import com.xperiment.BehavLogicAction.Logic.LogicEquation.Equation.EquationsLevel;
	import com.xperiment.BehavLogicAction.Logic.LogicEquation.TopLevelLogic;

	public class LogicAction
	{
		public var logic:TopLevelLogic;
		public var actions:Actions;
		public var elseActions:Actions;

		
		//input: banana+123>3?doStuff:elseDoSuff
		
		public function LogicAction(logicAction:String,propValDict:PropValDict)
		{
			var actionsArr:Array;
			var logicActionArr:Array=logicAction.split("?");
			if(logicActionArr.length>1){
				
				
				logic=	new	 TopLevelLogic(logicActionArr.shift(),logicOutcome,propValDict);
				
				actionsArr=logicActionArr.join().split(":");
				actions=	new	 Actions(actionsArr[0],propValDict);
				if(actionsArr.length>1)elseActions=	new	 Actions(actionsArr[1],propValDict);
									
			}
			else throw Error("improperly formatted behaviourLogic: "+logicAction);
		}
		

		
		public function logicOutcome():void{
			if(logic.eval()){
				actions.run();
			}
			else if(elseActions){
				actions.run();
			}
		}
		
		public function kill():void{
			
			if(actions){
				for each(var a:Actions in actions){
					a.kill();
				}
			}
			actions=null;
			if(elseActions){
				for each(a in elseActions){
					a.kill();
				}
			}
			actions=null;
			
			logic.kill();
			
		}
	}
}