﻿package com.xperiment{
 import flash.events.Event;
 import org.papervision3d.materials.ColorMaterial;
 import org.papervision3d.materials.MovieAssetMaterial;
 import org.papervision3d.objects.DisplayObject3D;
 import org.papervision3d.objects.primitives.PaperPlane;
 import org.papervision3d.objects.primitives.Sphere;
 import org.papervision3d.view.BasicView;
 
 public class Hello3DWorld extends BasicView {
 
  // move the definitions out here so the whole class can get them
  private var myFirstSphere:Sphere;
  private var myFirstPlane:PaperPlane;
  private var pivotContainer:DisplayObject3D;
 
  public function Hello3DWorld() {
   // create a movie material for the sphere from the library
   var sphereMaterial:MovieAssetMaterial = new MovieAssetMaterial("sphereMaterial");
   // create a color material for the plane
   var planeMaterial:ColorMaterial = new ColorMaterial(0xFFFF11);
   planeMaterial.doubleSided = true;
   // create a sphere and a paper airplane, increase the triangles on the sphere
   myFirstSphere = new Sphere(sphereMaterial, 100, 12, 12);
   myFirstPlane = new PaperPlane(planeMaterial, 3);
   // move the plane in 3d space
   myFirstPlane.x = 150;
   myFirstPlane.y = 150;
   myFirstPlane.z = -50;
   // set an initial rotation for the plane
   myFirstPlane.rotationY = -135;
   // add the sphere to the scene
   scene.addChild(myFirstSphere);
   // add a container to use for pivoting the plane rotation
   pivotContainer = new DisplayObject3D();
   // add the plane to the container
   pivotContainer.addChild(myFirstPlane);
   // add the container to the scene
   scene.addChild(pivotContainer);
   // this is an inherited method (onRenderTick) that starts an
   // enterframe event to render the scene
   startRendering();
  }
 
  override protected function onRenderTick(event:Event = null):void {
   // rotate the pivot container (will offset the plane rotation)
   pivotContainer.rotationY += 1;
   // rotate the sphere
   myFirstSphere.rotationY -= 1;
   // make sure that it is calling the renderer
   super.onRenderTick();
  }
 }
}