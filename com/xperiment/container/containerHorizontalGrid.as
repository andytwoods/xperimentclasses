﻿package com.xperiment.container{
	import flash.display.*;
	import flash.geom.*;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import com.xperiment.codeRecycleFunctions;
	import com.xperiment.trial.overExperiment;
	import flash.utils.getQualifiedClassName;
	import flash.system.System;
	import com.xperiment.uberSprite;
	import flash.display.Shape;


	public class containerHorizontalGrid extends containerVerticalGrid {


		override public function getA(i:uint):uint {
			return ((i-(i%getVar("columns")))/getVar("columns"));
		}

		override public function getB(i:uint):uint {
			
			return i%getVar("columns");
		}
		
		override public function splitVCorrection():int{
			return -1;
		}
		
		override public function splitHCorrection():int{
			return 0;
		}
		
		

	}
}