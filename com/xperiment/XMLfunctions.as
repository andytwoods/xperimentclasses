﻿package com.xperiment {


	class XMLfunctions  {




/*-----------------------------------
	Recursively replace nodeNames.
-------------------------------------*/
      private function findAndReplaceTagNames
      (xml:XML,find:String,replace:String):XML
      {
          if(xml.localName() == find){
              xml.setLocalName(replace);
          }
          var n:int=0;
          var c:XML;
          while(n <xml.children().length()){
              c=xml.children()[n];
              findAndReplaceTagNames(c,find,replace);
              n++;
          }
          return xml;
      }
 
/*-----------------------------------------
	Recursively replace attributes.
-------------------------------------------*/
      private function findAndReplaceAttributeNames
      (xml:XML,find:String,replace:String,in_tags_named:String=""):XML
      {
          var ok:Boolean=true;
          if(in_tags_named != "" && xml.localName() != in_tags_named){
              ok=false;
          }
          if(xml.@[find] != null && ok){
              xml.@[replace]=xml.@[find];
              delete xml.@[find];
          }
          var n:int=0;
          var c:XML;
          while(n <xml.children().length()){
              c=xml.children()[n];
              findAndReplaceAttributeNames(c,find,replace);
              n++;
          }
          return xml;
      }
 
/*---------------------------------------------
	Recursively find and remove attributes.
-----------------------------------------------*/
      private function findAndRemoveAttributeNames
      (xml:XML,find:String,in_tags_named:String=""):XML
      {
          var ok:Boolean=true;
          if(in_tags_named != "" && xml.localName() != in_tags_named){
              ok=false;
          }
          if(xml.@[find] != null && ok){
              delete xml.@[find];
          }
          var n:int=0;
          var c:XML;
          while(n <xml.children().length()){
              c=xml.children()[n];
              findAndRemoveAttributeNames(c,find);
              n++;
          }
          return xml;
      }

		
	}
}