﻿
package com.xperiment{

	import fl.controls.Button;
	import fl.controls.Slider;
	import fl.controls.SliderDirection;
	import flash.text.TextFieldAutoSize;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.display.*;
	import flash.events.*;
	import flash.text.*;
	import OnScreenBoss;
	import flash.display.MovieClip;
	import flash.display.Loader;
	import flash.net.URLRequest;
	import flash.geom.*;




	public class XMLObjectPropertyAssigner {
		var OnScreenElements: Object = new Object();

		public function XMLObjectPropertyAssigner(textinfo:XML,tempOnScreenElements:Object) {
			OnScreenElements=tempOnScreenElements;

			for each (var item:XML in textinfo.elements()) {
				var attNamesList:XMLList=item.@*;
				var VarName:String=attNamesList.parent().name();

				trace("-----------------'"+VarName.child()+"' object-----------------");
				for (var i:int=0; i<attNamesList.length(); i++) {
					var tag:String=attNamesList[i].name();// id and color
					var tagValue:String=attNamesList[i];
					//trace("VARIABLES VarName -"+VarName+"-; tag -"+tag+"-; value -"+tagValue+"-");
					if (typeof OnScreenElements[VarName]!="undefined") {
						try {
							trace(VarName+"."+tag+" = "+tagValue);





							var a:Array=new Array  ;
							var tempString:String=tag;
							a=tempString.split('.');

							switch (a.length) {
								case 0 :
									trace("Problem: no variables given to set properties with");
									break;
								case 1 :
									OnScreenElements[VarName][a[0]]=returnType(typeof(OnScreenElements[VarName][a[0]]),tagValue);
									break;
								case 2 :
									trace("-------here---"+[a[0]]+"---"+[a[1]]);
									trace("-------here---"+VarName+"---");
									OnScreenElements[VarName][a[0]][a[1]]=returnType(typeof(OnScreenElements[VarName][a[0]][a[1]]),tagValue);
									break;
								case 3 :
									OnScreenElements[VarName][a[0]][a[1]][a[2]]=returnType(typeof(OnScreenElements[VarName][a[0]][a[1]][a[2]]),tagValue);
									break;
								case 4 :
									OnScreenElements[VarName][a[0]][a[1]][a[2]][a[3]]=returnType(typeof(OnScreenElements[VarName][a[0]][a[1]][a[2]][a[3]]),tagValue);
									break;
								case 5 :
									OnScreenElements[VarName][a[0]][a[1]][a[2]][a[3]][a[4]]=returnType(typeof(OnScreenElements[VarName][a[0]][a[1]][a[2]][a[3]][a[4]]),tagValue);
									break;
								case 6 :
									OnScreenElements[VarName][a[0]][a[1]][a[2]][a[3]][a[4]][a[5]]=returnType(typeof(OnScreenElements[VarName][a[0]][a[1]][a[2]][a[3]][a[4]][a[5]]),tagValue);
									break;
							}

							//OnScreenElements[VarName][tag]=returnType(typeof OnScreenElements[VarName][tag],tagValue);
						} catch (error:Error) {
							trace("'"+tag+"' is not a known property of '"+VarName+"'");
						}
					} else {
						trace("'"+VarName+"' object does not exist unfortunately");
					}
				}

				OnScreenElements.textInput.setTextFormat(OnScreenElements.myTextFormat);
			}
			trace("-----------------");
		}

		public function giveMeData():Object {
			return OnScreenElements;
		}



		private function returnType(type:String,value:*):* {
			var returnType;
			switch (type) {
				case "string" :
					returnType=new String  ;
					returnType=String(value);
					break;
				case "int" :
					returnType=new int  ;
					returnType=int(value);
					break;
				case "number" :
					returnType=new Number  ;
					returnType=Number(value);
					break;
				case "boolean" :
					returnType=new Boolean  ;
					returnType=Boolean(value);
					break;
				case "uint" :
					returnType=new uint  ;
					returnType=uint(value);
					break;
				default :
					trace("incorrect variable type");
			}
			return returnType;
		}

	}
}