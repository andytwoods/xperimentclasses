package com.xperiment
{
	import air.update.logging.Logger;
	
	import flash.display.Sprite;
	import flash.events.Event;

	public interface IOnScreenBoss
	{
		
		function getObjTimes(obj:uberSprite):Array
		function getPegTimes(peg:String):Array;
		function setTimes(obj:uberSprite, startTime:Number, endTime:Number,duration:Number):Boolean		
		function sortSpritesTIME():void					
		function checkForEvent(evt:Event):void 			 
		function commenceDisplay():void 			 
		function cleanUpScreen():void 
		function removeVars():void
		function addtoTimeLine(hasBehavs:Boolean,element:uberSprite,startTime:Number,endTime:Number):void 
		function sortSprites(sprites:Array,attribute:String):void			
		function killObj(peg:String):void 
		//function stopEventWithObj(objec:uberSprite):void
		function stopObj(peg:String):Boolean 
		function runDrivenEvent(peg:String,delay:String="",dur:String=""):void

	}
}