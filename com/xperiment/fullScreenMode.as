﻿
package com.xperiment{

	import flash.display.StageDisplayState;
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	import fl.controls.Button;
	import flash.display.*;
	import flash.display.Shape;
	import flash.events.Event;


	public class fullScreenMode extends addButton {

		//public function Trial_imageCollage(genAttrib:XMLList, specAttrib:XMLList) {
		override public function setVariables(list:XMLList) {
			super.setVariables(list);
			super.button1.label="fullScreen button";

			
		}


		override public function buttonClick(e:MouseEvent):void {
			if (getVar("eventDriver")== true){
			dispatchEvent(new Event("runDrivenEvent"));
			}
			if (super.theStage.displayState==StageDisplayState.NORMAL) {
				try {
					super.theStage.displayState=StageDisplayState.FULL_SCREEN;
				} catch (e:SecurityError) {
					//if you don't complete STEP TWO below, you will get this SecurityError
					trace("an error has occured. please modify the html file to allow fullscreen mode");
				}
			}
						//super.button1.addChild(myShape);
			trace("button pressed: "+e.target.name);
		}




		override public function RunMe():uberSprite {
			super.pic.addChild(button1);
			super.button1.addEventListener(MouseEvent.CLICK,buttonClick);
			super.button1.width=getVar("width");
			super.button1.height=getVar("height");
			super.button1.name=getVar("resultFileName");
			super.button1.label=getVar("text");
			super.button1.emphasized=getVar("emphasized");
			super.button1.version=getVar("IdriveWhichEvent"); //AW: a total BODGE which I've used to get past the annoying issue of not passing variables via Event.
			super.setUniversalVariables();
			return super.pic;
		}

	}
}