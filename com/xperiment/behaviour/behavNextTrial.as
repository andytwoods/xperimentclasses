﻿package com.xperiment.behaviour{
	import com.xperiment.uberSprite;
	import com.xperiment.events.GotoTrialEvent;
	import com.xperiment.trial.Trial;
	
	import flash.events.Event;

	public class behavNextTrial extends behav_baseClass {

		
		override public function nextStep(id:String=""):void {
			//trace(2222222,peg,id);
			if(parentPic)parentPic.dispatchEvent(new GotoTrialEvent(GotoTrialEvent.TRIAL_PING_FROM_OBJECT,"nextTrial"));
		}
		
	}

}