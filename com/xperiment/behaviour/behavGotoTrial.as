﻿package com.xperiment.behaviour{
	
	import com.xperiment.uberSprite;
	import com.xperiment.events.GotoTrialEvent;
	import com.xperiment.stimuli.addButton;
	import com.xperiment.stimuli.object_baseClass;
	import com.xperiment.trial.Trial;
	import com.xperiment.trial.overExperiment;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;

	public class behavGotoTrial extends behav_baseClass {

		override public function setVariables(list:XMLList):void {
			setVar("String","trial","");
			super.setVariables(list);
			
		}
		
		override public function myUniqueActions(action:String):Function{

			switch(action){
				case "goto": return function():void{nextStep(getVar("goto"));};	break;
			}

			return null;
		}


		override public function nextStep(id:String=""):void{
			if(id!="") parentPic.dispatchEvent(new GotoTrialEvent(GotoTrialEvent.TRIAL_PING_FROM_OBJECT,id));
			else parentPic.dispatchEvent(new GotoTrialEvent(GotoTrialEvent.TRIAL_PING_FROM_OBJECT,getVar("trial")));

			
		}
		
		
		override public function givenObjects(obj:uberSprite):void{	
			var str:String=(obj as object_baseClass).getVar(this.getVar("var"));
			if(str!="")setVar("String","trial",str);
			
			super.givenObjects(obj);
		}
		


		override public function kill():void {

			super.kill();
		}
	}
}