﻿package com.xperiment.behaviour{
	import com.greensock.TweenMax;
	import com.greensock.plugins.TransformAroundPointPlugin;
	import com.greensock.plugins.TweenPlugin;
	import com.xperiment.uberSprite;
	import com.xperiment.events.BehaviourDataEvent;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public class behavDrag extends behav_baseClass{
		public var movingBlock:uberSprite;
		public var scores:Array = new Array;
		private var point:Point;
		private var objPoint:Point;
		private var angle:Number;
		
		public static const RAD_TO_DEG:Number = 57.295779513082325225835265587527; // 180.0 / PI;
		
		override public function setVariables(list:XMLList):void {
			setVar("string","orientateToPointX","");
			setVar("string","orientateToPointY","");
			setVar("string", "horizontalPivotPoint","");
			setVar("string", "verticalPivotPoint","");
			
			super.setVariables(list);
			sortOrientateToPoint();
		}	
		
		private function sortOrientateToPoint():void
		{
			if(getVar("orientateToPointX")!="" && getVar("orientateToPointY")!=""){

				point=new Point((int(getVar("orientateToPointX")))*super.theStage.stageWidth*.01,int(getVar("orientateToPointY"))*super.theStage.stageHeight*.01);
				angle=0;
			}
		}
		
		override public function givenObjects(obj:uberSprite):void{
			scores.push(-1);
			obj.addEventListener(MouseEvent.MOUSE_DOWN, startBlockMove,false,0,true);
			
			if(point){
				TweenPlugin.activate([TransformAroundPointPlugin]);
			}

			super.givenObjects(obj);
		}
		
		public function startBlockMove(e:MouseEvent):void {
			super.theStage.addEventListener(MouseEvent.MOUSE_UP, stopMotion,false,0,true);
			if(point)super.theStage.addEventListener(MouseEvent.MOUSE_MOVE, move,false,0,true);
			movingBlock=uberSprite(e.currentTarget.pic);
			movingBlock.parent.setChildIndex(movingBlock,movingBlock.parent.numChildren-1);
			objPoint=new Point(int(getVar("horizontalPivotPoint"))*.01*movingBlock.myWidth,int(getVar("verticalPivotPoint"))*.01*movingBlock.myHeight);
			movingBlock.startDrag();
		}
		
		
		protected function move(event:MouseEvent):void
		{
			if(movingBlock){
				var angle:Number = Math.atan2( movingBlock.y - point.y, movingBlock.x - point.x ) * RAD_TO_DEG -110;
				TweenMax.to(movingBlock,0,{transformAroundPoint:{point:new Point(movingBlock.myWidth*.5, movingBlock.myHeight*.5),pointIsLocal:true,rotation:angle}});		
				sendData("",new Point(movingBlock.x,movingBlock.y))
			}
		}

		public function stopMotion(evt:MouseEvent):void {
			if(point) theStage.removeEventListener(MouseEvent.MOUSE_MOVE,move);
			if(movingBlock)movingBlock.stopDrag();
			movingBlock = null;
		}
		
		override public function kill():void{
			super.theStage.removeEventListener(MouseEvent.MOUSE_UP, stopMotion);
			if(theStage.hasEventListener(MouseEvent.MOUSE_MOVE)) theStage.removeEventListener(MouseEvent.MOUSE_MOVE,move);
			for (var i:uint=0;i<behavObjects.length;i++){
				super.behavObjects[i].pic.removeEventListener(MouseEvent.MOUSE_DOWN, startBlockMove);
			}
			super.kill();
		}

	}

}