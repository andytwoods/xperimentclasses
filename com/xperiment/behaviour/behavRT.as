package com.xperiment.behaviour
{
	import com.xperiment.uberSprite;

	public class behavRT extends behav_baseClass
	{
		private var startTime:Number;

		override public function setVariables(list:XMLList):void {
			setVar("string","event","rt");
			setVar("boolean","hideResults",false);
			super.setVariables(list);
			
		}

		override public function stopBehaviour(obj:uberSprite):void {//remove this?
			//trace("stopRT",String((new Date).time-startTime));
			calcReturnData();
			behaviourFinished();
		}
		
		private function calcReturnData():void
		{
			var tempData:Array
			tempData = new Array();
			tempData.event=getVar("event"); 
			tempData.data=String((new Date).time-startTime);
			super.objectData.push(tempData);
		}
		
		
		override public function returnsDataQuery():Boolean {
			if (getVar("hideResults")) return false;	
			else return true;
		}
		
		override public function nextStep(id:String=""):void {
			//trace("startRT");
			startTime=(new Date).time;	
		}
		
	}
}