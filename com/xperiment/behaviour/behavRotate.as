package com.xperiment.behaviour{
	import com.greensock.TweenMax;
	import com.greensock.plugins.TransformAroundCenterPlugin;
	import com.greensock.plugins.TweenPlugin;
	import com.xperiment.uberSprite;
	import com.xperiment.events.BehaviourDataEvent;
	import com.xperiment.trial.overExperiment;
	
	import flash.display.GradientType;
	import flash.display.SpreadMethod;
	import flash.geom.Matrix;
	import flash.geom.Point;
	
	
	public class behavRotate extends behav_baseClass {
		
		public static const RAD_TO_DEG:Number = 57.295779513082325225835265587527; // 180.0 / PI;
		private var angle:Number;
		private var point:Point;
		private var distance:Number;
		private var gradientColors:Object;
		private var gradientMatrix:Matrix;
		
		override public function setVariables(list:XMLList):void {
			
			setVar("int","rotation",0);
			setVar("number","duration",0);
			super.setVariables(list);
			TweenPlugin.activate([TransformAroundCenterPlugin]);
			
		}	
		
		override public function nextStep(id:String=""):void{
			applyAngle(getVar("rotation"),id);
		}
	
		
		override public function receivedData(e:BehaviourDataEvent):void{
			point=e.data
			angle = Math.atan2( theStage.stageWidth*.5 - point.y, theStage.stageHeight*.5- point.x ) * RAD_TO_DEG-180;
			applyAngle(angle);	
		}		
		
		private function applyAngle(angle:Number,id:String=""):void{
			
			for each(var us:uberSprite in behavObjects){
				
				if(getVar("duration")==0)TweenMax.set(us,{transformAroundCenter:{rotation:angle}});
				else {
				
					
					if(us.id==id && !TweenMax.isTweening(us)){			
						TweenMax.to(us,getVar("duration") as Number,{transformAroundCenter:{rotation:String(getVar("rotation"))}});
					}
					
				}
			}
		}
		
	}
}

