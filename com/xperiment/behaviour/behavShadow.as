﻿package com.xperiment.behaviour{
	import com.greensock.TweenMax;
	import com.xperiment.uberSprite;
	import com.xperiment.events.BehaviourDataEvent;
	import com.xperiment.trial.overExperiment;
	
	import flash.geom.Point;
	
	
	public class behavShadow extends behav_baseClass {
		
		public static const RAD_TO_DEG:Number = 57.295779513082325225835265587527; // 180.0 / PI;
		private var angle:Number;
		private var point:Point;
		private var distance:Number;
		override public function setVariables(list:XMLList):void {

			setVar("int","startOppacity",1);	
			super.setVariables(list);
		}		
		
		
		override public function receivedData(e:BehaviourDataEvent):void{
			shadow(e.data);
			point=e.data as Point;
			
		}
		
	
		override public function nextStep(id:String=""):void
		{
			shadow(new Point(theStage.width*.25,theStage.height*.25));	
			super.nextStep();
		}
		
		private function shadow(point:Point):void{
			for each(var us:uberSprite in behavObjects){
				distance = Math.sqrt((us.x - point.x) * (us.x - point.x) + (us.y - point.y) * (us.y - point.y));
				angle = Math.atan2( us.y+us.myHeight*.5 - point.y, us.x +us.width*.5- point.x ) * RAD_TO_DEG-180;
				TweenMax.to(us,0,{dropShadowFilter:{inner:true, blurX:25, angle:angle, blurY:5, distance:distance/5, alpha:0.6}});
			}
		}

	}
}

/*		distance : Number [0] 
angle : Number [45] 
color : uint [0x000000] 
alpha :Number [0] 
blurX : Number [0] 
blurY : Number [0] 
strength : Number [1] 
quality : uint [2] 
inner : Boolean [false] 
knockout : Boolean [false] 
hideObject : Boolean [false] 
index : uint 
addFilter : Boolean [false] 
remove : Boolean [false] 
Set remove to true if you want the filter to be removed when the tween completes. 


override public function kill():void {

super.kill();
}
*/