package com.xperiment.behaviour
{
	import com.uberSprite;
	import com.xperiment.stimuli.addText;
	import com.xperiment.stimuli.object_baseClass;
	
	import flash.utils.getDefinitionByName;

	public class behavText extends behav_baseClass
	{
		
		private var textObjs:Array=new Array;
		
		override public function setVariables(list:XMLList):void {
			setVar("string","words",""); //words seperated by a comma
			setVar("string","colours","0x11830e");//green here
			setVar("string","suffixes","");
			setVar("string","prefixes","");
			setVar("boolean","tally",false);
			setVar("boolean","onWordListToo",false);
			super.setVariables(list);
		}
				
		override public function givenObjects(obj:uberSprite):void{	
			if(String(obj).indexOf("Text")){
				//trace(getValFromAction((obj as object_baseClass).getVar("behaviours")),22);
				switch(getValFromAction((obj as object_baseClass).getVar("behaviours"))){
					case "giveText":
						textObjs.push(obj  as object_baseClass);
						break;
					
					default:
						super.givenObjects(obj); //only accept Text stimuli
						break;
				}		
			}
			else if(logger)logger.log("! you have passed behavTextHighlight (peg="+getVar("peg")+") a non Text object... (of type "+String(obj).replace("object ","").replace("[","").replace("]","")+").");
		}
		
		override public function nextStep(id:String=""):void {			
			var txt:String;
			var wordList:Array=(getVar("words") as String).split(",");
			for each(var objBC:addText in textObjs){
				txt=objBC.myText.htmlText;
				txt = txt.replace(new RegExp("<[^<]+?>", "gi"), ""); //removes all HTML stuff
				if(txt!="" && wordList.indexOf(txt)==-1)wordList.push(txt);
			}
			
			var colArr:Array=(getVar("colours")as String).split(",");
			var suffixes:Array=(getVar("suffixes")as String).split(",");
			var prefixes:Array=(getVar("prefixes")as String).split(",");
			var regexp:RegExp;
			var tally:uint=0;
			
			for each(var obj:addText in behavObjects){					
				for(var i:uint=0;i<wordList.length;i++){
					txt=obj.myText.htmlText;
	
					regexp=new RegExp(wordList[i],"g");
					obj.myText.htmlText=txt.replace(regexp,"<font color='"+colArr[i%colArr.length].replace("0x","#")+"'>"+prefixes[i%colArr.length]+wordList[i]+suffixes[i%colArr.length]+"</font>");
					if(obj.myText.htmlText!=txt)tally++;
				}
			}
			if(getVar("tally") as Boolean)putInResults(getVar("peg"),tally as String);	
		}
	}
}