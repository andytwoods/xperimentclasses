﻿package com.xperiment.behaviour{
	import com.xperiment.uberSprite;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.geom.Point;
	import flash.utils.Timer;

	public class behavDragToShiftingArea extends behav_baseClass {
		private var startingPt:Point=new Point  ;
		private var DropZones:Array = new Array;
		private var DropZonesOccupied:Array = new Array;
		private var currentMovingBlockNumber:uint=9999;
		private var movingBlock:uberSprite;
		private var objectsSelected:Array;
		private var inDZ:Boolean=false;
		private var myDelayTimer:Timer;
		private const timerWait:uint=500;
		private var indicatorArr:Array;
		private var dragableObjects:Array = new Array;

		override public function setVariables(list:XMLList):void {
			setVar("string","areaNamePrefix","");
			setVar("boolean","lockInPlaceOncePlaced",false);
			setVar("boolean","snapBackToOriginalLocation",false);
			setVar("boolean","showDropZones",false);
			setVar("string","snapToDropZones","middle");
			setVar("string","whichImageHideAfter",""); //either 'selected', 'unselected', ''.
			setVar("uint","actionsAfterHowManyFilled",1);
			setVar("string","indicator","");
			super.setVariables(list);
			if(list.@hideResults.toString()!="true")OnScreenElements.hideResults=false;
		}

		override public function returnsDataQuery():Boolean {
			if (getVar("hideResults")) return false;	
			else return true;
		}

		public function filled():void {

			calcReturnData();
			behaviourFinished();

			for (var i:uint=0; i<behavObjects.length; i++) {
				if (super.behavObjects[i].pic&&super.behavObjects[i].pic.hasEventListener(MouseEvent.MOUSE_DOWN)) {
					super.behavObjects[i].pic.removeEventListener(MouseEvent.MOUSE_DOWN, startBlockMove);
				}
			}			
			if (getVar("whichImageHideAfter")!="") whichImageHideAfter();
		}
		
		private function calcReturnData():void
		{
			
			var tempData:Array;
			
			var getnam:Function = function():String{
				if(getVar("id")!="")return tempData.event=getVar("id");
				else return tempData.event="DragToShiftingArea-";
			}
			
			
			for (var i:uint=0;i<DropZonesOccupied.length;i++){
				if(DropZonesOccupied[i]!=null){
					if(dragableObjects.length>1){
						tempData = new Array();
						tempData.event=getnam()+(DropZones[i].name as String).replace(getVar("areaNamePrefix"),""); 
						tempData.data=DropZonesOccupied[i].name;
					}
					else if(dragableObjects.length==1){
						tempData = new Array();
						tempData.event=getnam();
						tempData.data=(DropZones[i].name as String).replace(getVar("areaNamePrefix"),"");
					}	
					super.objectData.push(tempData);
				}
			}
			
		}


		private function whichImageHideAfter():void{
			var selected:Array=new Array();
				for (var i:uint=0; i<behavObjects.length;i++){
					for (var j:uint=0;j<objectsSelected.length;j++){
						if(behavObjects[i].name==objectsSelected[j])selected.push(i);
					}
				}
			
			switch(getVar("whichImageHideAfter")){
				case "selected":
					for (i=0; i<selected.length;i++)behavObjects[selected[i]].pic.visible=false;
					break;
				case "unselected":
					for (i=0; i<behavObjects.length;i++){					
						if(selected.indexOf(i)==-1)behavObjects[i].pic.visible=false;
					}
					break;
				}
		}	


		public function startBlockMove(e:MouseEvent):void {
			movingBlock=uberSprite(e.currentTarget.pic);
			movingBlock.parent.setChildIndex(movingBlock,movingBlock.parent.numChildren-1);
			startingPt.x=movingBlock.x;
			startingPt.y=movingBlock.y;
			movingBlock.startDrag();
			for (var i:uint=0; i<behavObjects.length; i++) {
				if (behavObjects[i].pic==movingBlock) {
					currentMovingBlockNumber=i;
					break;
				}
			}
			
			if(DropZonesOccupied.indexOf(movingBlock)!=-1){
				DropZonesOccupied[DropZonesOccupied.indexOf(movingBlock)]=null;
				myDelayTimer=new Timer(timerWait,1);
				myDelayTimer.addEventListener(TimerEvent.TIMER_COMPLETE,stopTimer,false,0,true);
				
				if(indicatorArr){
					for(i=0;i<indicatorArr.length;i++){
						if(indicatorArr[i].name==movingBlock.name && movingBlock.parent.contains(indicatorArr[i] as Sprite)){
							movingBlock.parent.removeChild(indicatorArr[i]);
							indicatorArr[i]=null;
							indicatorArr.splice(i,1);
						}
					}
				}				
				
				myDelayTimer.start();
			}
			
			theStage.addEventListener(MouseEvent.MOUSE_UP, stopMotion,false,0,true);
			theStage.addEventListener(MouseEvent.MOUSE_MOVE, snapInPlace,false,0,true);
		}
		
		protected function stopTimer(e:TimerEvent):void{myDelayTimer=null;e.target.removeEventListener(TimerEvent.TIMER_COMPLETE,stopTimer);}
		
		override public function givenObjects(obj:uberSprite):void {
			if (alive==true) {
				if ((obj.name as String).indexOf(getVar("areaNamePrefix"))==-1) {//if obj is of type dropZone (specified via 'areaNamePrefix') then do dropzone stuff.
					dragableObjects.push(obj);
					obj.addEventListener(MouseEvent.MOUSE_DOWN, startBlockMove,false,0,true);
				}
				else {
					DropZones.push(obj);
					DropZonesOccupied.push(null);
				}
			}
			super.givenObjects(obj);
		}

	

		public function stopMotion(evt:MouseEvent):void {

			theStage.removeEventListener(MouseEvent.MOUSE_MOVE, snapInPlace);
			movingBlock.stopDrag();
			if (inDZ==false && getVar("snapBackToOriginalLocation")) {
				movingBlock.x=startingPt.x;
				movingBlock.y=startingPt.y;
			}
			movingBlock=null;
			
			//below, a test to see if the dropZones are full enough to emit a reponse (if required ;)
			if (getVar("actionsAfterHowManyFilled")!=0 && countBooleanContents(DropZonesOccupied)==getVar("actionsAfterHowManyFilled")) {
				filled();
			}
			
			theStage.removeEventListener(MouseEvent.MOUSE_UP, stopMotion);
			theStage.removeEventListener(MouseEvent.MOUSE_MOVE, snapInPlace);
		}

		private function snapInPlace(e:MouseEvent):void {
			if(myDelayTimer==null){
				for (var i:uint=0; i<DropZones.length; i++) {
					
					if (!DropZonesOccupied[i] && DropZones[i].hitTestPoint(movingBlock.x+(movingBlock.width*.5),movingBlock.y+(movingBlock.height*.5))) {
						inDZ=true;
						
						movingBlock.x=DropZones[i].x+(DropZones[i].width-movingBlock.width)/2;
						movingBlock.y=DropZones[i].y+(DropZones[i].height-movingBlock.height)/2;
						
						
						if(getVar("indicator")!=""){
							var indicator:Array=(getVar("indicator") as String).split(",");
							if(indicator.length==2){
								var spr:Sprite = new Sprite;
								spr.graphics.beginFill(indicator[1] as Number,1);
								spr.graphics.drawCircle(0,0,indicator[0]);
								spr.graphics.endFill();
								spr.x=DropZones[i].x+DropZones[i].width*.5;
								spr.y=DropZones[i].y+movingBlock.height*2;
								movingBlock.parent.addChild(spr);
								spr.name=movingBlock.name;
								if(!indicatorArr)indicatorArr=new Array;	
								indicatorArr.push(spr);
							}
						}
						
						DropZonesOccupied[i]=movingBlock;
	
						if (getVar("lockInPlaceOncePlaced")) {
							movingBlock.removeEventListener(MouseEvent.MOUSE_DOWN, startBlockMove);
						}
						theStage.removeEventListener(MouseEvent.MOUSE_UP, stopMotion);
						stopMotion(e);
						break;
					}
				}
			}
		}

		private function countBooleanContents(arr:Array):uint {
			var count:uint=0;
			for (var i:uint=0; i< arr.length; i++) {
				if (arr[i]!=null) {
					count++;
				}
			}
			return count;
		}



		override public function kill():void {
			if(myDelayTimer){
				myDelayTimer.stop(); 
				myDelayTimer=null;
			}
			
			if(indicatorArr){
				while (indicatorArr.length>0){
					if(behavObjects[0].pic.parent.contains(indicatorArr[0]))behavObjects[0].pic.parent.removeChild(indicatorArr[0]);
					indicatorArr[0]=null;
					indicatorArr.splice(0,1);
				}
				indicatorArr=null;
			}
			
			startingPt=null;
			movingBlock=null;
			if(theStage)super.theStage.removeEventListener(MouseEvent.MOUSE_UP, stopMotion);
			if(theStage && theStage.hasEventListener(MouseEvent.MOUSE_MOVE))theStage.removeEventListener(MouseEvent.MOUSE_MOVE, snapInPlace);
			
			dragableObjects=null;
			
			for (var i:uint=0; i<behavObjects.length; i++) {
				if (super.behavObjects[i].pic&&super.behavObjects[i].pic.hasEventListener(MouseEvent.MOUSE_DOWN)) {
					super.behavObjects[i].pic.removeEventListener(MouseEvent.MOUSE_DOWN, startBlockMove);
				}
			}

			for (i=0; i<DropZones.length-1; i++) {DropZones[i]=null;}
			DropZones=null;
			
			for (i=0; i<DropZonesOccupied.length-1; i++) {DropZonesOccupied[i]=null;}
			DropZonesOccupied=null;
			
			
			super.kill();
		}
	}
}