﻿package com.xperiment.behaviour{

	import com.xperiment.codeRecycleFunctions;
	import com.xperiment.uberSprite;
	import com.xperiment.StringStuff.StringLogic;
	import com.xperiment.events.BehaviourDataEvent;
	import com.xperiment.stimuli.object_baseClass;
	import com.xperiment.trial.overExperiment;
	
	import flash.display.*;
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.events.*;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	import flash.geom.*;
	import flash.net.URLRequest;
	import flash.text.*;
	import flash.text.TextFieldAutoSize;
		
	public class behav_baseClass extends object_baseClass {
		public var behavObjects:Array=new Array  ;
		private var listenForChangeOnce:Boolean;
		private var logicObj:Object;
		private var notYetRun:Boolean=true;

		private function nextStepONCE():void{
			if(notYetRun || OnScreenElements.doMany){
				notYetRun=false;
				//trace("heree23")
				//first check there are no doBefore behaviours
				//trace("here",this.behav ,this.behav.hasOwnProperty("doBefore"));
				var occured:Boolean=true
				if(getVar("random")!=-1){
					occured=false;
					var prob:int=Math.random()*100;
					if(logger)logger.log("behaviour "+getVar("peg") +" was given a "+getVar("random")+"% chance of occurring.  The generated random % was "+prob+"%.");
					
					if (prob>=getVar("random")){
						
						if(logger)logger.log("...the behaviour was thus run.");
						occured=true;
					}
					else if(logger)logger.log("...the behaviour was thus NOT run.");
	
					
					var tempData:Array = new Array();
					tempData.event=String("behaviourRND_"+getVar("peg"));
					tempData.data=occured;
					super.objectData.push(tempData);
				}
				
				//AW JAN FIX
				//if(occured && this.actions && this.actions.hasOwnProperty("doBefore"))if(behavObjects)manageBehaviours.doBehaviourFirst(this);
				
				if(occured)nextStep();
				if(occured && pic && !theStage.contains(pic))manageBehaviours.actionHappened(this,"onShow"); //some behaviours have nothing added to stage.  This fakes that :-)
			}
		}
		
		override public function givenObjects(obj:uberSprite):void{	
			behavObjects.push(obj);
			if(getVar("listenForChange")!="")listenForChange(obj);
		}
		
		override public function wipeObjects():void{
			behavObjects=new Array;
		}
		
		public function behaviourFinished():void{
			//AW JAN FIX
			//if(behavObjects)manageBehaviours.behaviourFinished(this);
		}

		override public function setVariables(list:XMLList):void {
			setVar("int","random",-1);
			setVar("string","result","");
			setVar("string","listenForChange","");
			setVar("boolean","listenForChangeOnce",false);
			setVar("string","useTheseObjects","");
			setVar("string","logic","");
			setVar("boolean","receiveData",false);
			//setVar("string","timeStart",""); //used to be set at -1, pre logic-update
			setVar("boolean","sendDataTo","");
			setVar("string","sendWhatData","");
			setVar("boolean", "doMany",false);
			super.setVariables(list);
			if(getVar("usePegs")!="")usePegs=getVar("usePegs");
			if(getVar("logic")!="")sortLogicObj();
			if(getVar("receiveData"))dataListener(true);
			
		}
		
		private function dataListener(setup:Boolean):void
		{
			if(setup){
				super.theStage.addEventListener(BehaviourDataEvent.PASSDATA,receivedData,false,0,true);
			}
			else super.theStage.removeEventListener(BehaviourDataEvent.PASSDATA,receivedData);
		}		
		
		public function sendData(calleeBehavID:String,data:*):void
		{
			super.theStage.dispatchEvent(new BehaviourDataEvent(BehaviourDataEvent.PASSDATA,this.peg,calleeBehavID,data));
	
		}	
		
		public function receivedData(event:BehaviourDataEvent):void
		{
			trace("!!! Internal problem.  You asked for a behaviour with this peg ["+getVar("peg")+"] to listen for data but no function has been set up to listen for such data.");
			// TODO Auto-generated method stub
			
		}
		
		private function sortLogicObj():void{
			logicObj=new Object;
			var e:String=getVar("logic");
			const PAREN_PATTERN:RegExp = /^[^a-zA-Z_]+|[^a-zA-Z_0-9]+/g
			e=e.replace(PAREN_PATTERN, ",");
			for each(var s:String in e.split(",")){
				logicObj[s]=0;			
			}
		}
		
		override public function returnsDataQuery():Boolean {
			var re:Boolean = true;
			if (getVar("hideResults")) re=re&&false;	
			return re;
		}		
		
	
		final override public function runBehav(id:String):void {//DO NOT OVERRIDE THIS.  Use 'nextStep' instead.
			if(logicObj){
				if(logicObj.hasOwnProperty(id)){
					logicObj[id]=1;
				}
				else{
					logger.log("PROBLEM! There is a problem with your logic.  You need to reference this object's ID in your expression for it to have any effect.  The missing ID is: "+id+".");
				}
			}
			
			if(!logicObj || StringLogic.replaceVarsWithNumbers(getVar("logic"),logicObj))nextStepONCE();
		
			else{
				if(logger)logger.log("behaviour has been asked to run but has not done so as your logic terms to run the behaviour have not yet been met");
			}
		}
		

		
		public function nextStep(id:String=""):void{
				if(pic)pic.addEventListener(Event.REMOVED_FROM_STAGE,removedFromScreen,false,0,true);
		}

		override public function setUniversalVariables():void {
		}

		override public function RunMe():uberSprite {
			if(pic!=null && peg!=null && ((getVar("peg") as String)==""  || (getVar("timeStart") as Number)!=-1)){
				pic.addEventListener(Event.ADDED_TO_STAGE, behavAddedToStageNO_peg);
			}
			return pic;//note empty as nothing on stage :)
		}
		
		protected function behavAddedToStageNO_peg(e:Event):void
		{	
			nextStepONCE();
		}
		
		public function errorMessage():void{
			if(logger)logger.log("Problem: afraid you have not given your behaviour a peg (e.g. peg='myBehav1') so there is no way it can identify objects to which to apply itself too.");
		}

		public function removedFromScreen(e:Event):void{
			stopBehav(new String);
		}
		
		override public function stopBehav(id:String):void{
			pic.removeEventListener(Event.REMOVED_FROM_STAGE,removedFromScreen);
			stopBehaviourFromID("ALL");
		}

		public function stopBehaviour(obj:uberSprite):void {//remove this?
			if(logger)logger.log("requested to stop behaviour but no stopping function in place for behaviour with this name: "+obj.name);
		}

		public function stopBehaviourFromID(str:String):void {
			if (str.length==0) {
				if(logger)logger.log("the ID provided was, er, empty");
			}
			if (str=="ALL"){
				var removedObjs:String="You stopped a behaviour in turn stopped effects on objects with these names:";
				stopBehaviour(behavObjects[i]);
				for (var i:uint=0; i<behavObjects.length; i++) {
					removedObjs=removedObjs+","+behavObjects[i].name;
				}
				if(logger)logger.log(removedObjs)
			}
			else{
				for (i=0; i<behavObjects.length; i++) {
					if (String(behavObjects[i].name)==str) {
						stopBehaviour(behavObjects[i]);
						break;
					}
				}
			}
		}
		
		public function listenForChange(spr:uberSprite):void{
			var listeners:Array = (getVar("listenForChange") as String).split(",");
			for each(var str:String in listeners){
				switch(str){
					case "onCk":
						spr.addEventListener(MouseEvent.MOUSE_UP,listen_ck,false,0,true);
						break;
					case "onClick":
						spr.addEventListener(MouseEvent.CLICK,listen_ck,false,0,true);
						break;
					case "onCli":
						spr.addEventListener(MouseEvent.MOUSE_DOWN,listen_ck,false,0,true);
						break;
				}
			}
		}
		
		public function listen_ck(e:MouseEvent):void{
			if(getVar("listenForChangeOnce"))e.target.removeEventListener(e,listen_ck);
			//do something!
		}

		override public function kill():void {
			if(pic.hasEventListener(Event.REMOVED_FROM_STAGE)) pic.removeEventListener(Event.REMOVED_FROM_STAGE,removedFromScreen);
			if(getVar("receiveData"))dataListener(false);
			for (var i:uint=0; i<behavObjects.length;i++) {
			if(behavObjects[i] && behavObjects[i].pic){
					if(behavObjects[i].pic.hasEventListener(MouseEvent.MOUSE_UP))behavObjects[i].pic.removeEventListener(MouseEvent.MOUSE_UP,listen_ck)
					if(behavObjects[i].pic.hasEventListener(MouseEvent.CLICK))behavObjects[i].pic.removeEventListener(MouseEvent.CLICK,listen_ck)
					if(behavObjects[i].pic.hasEventListener(MouseEvent.MOUSE_DOWN))behavObjects[i].pic.removeEventListener(MouseEvent.MOUSE_UP,listen_ck)
					behavObjects[i]=null;
				}
			}		
			if(pic.hasEventListener(Event.ADDED_TO_STAGE))pic.removeEventListener(Event.ADDED_TO_STAGE, behavAddedToStageNO_peg);
			
			driveEvent=null;
			behavObjects=null;
			super.kill();
		}
	}
}