﻿package com.xperiment.behaviour{
	
	//note: each object MUST have unique peg
	
	import com.Logger.Logger;
	import com.xperiment.IOnScreenBoss;
	import com.xperiment.stimuli.object_baseClass;
	import com.xperiment.uberSprite;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	
	
	public class BehaviourBoss extends abstractBehaviourBoss {
		
		private var currentDisplay:IOnScreenBoss;
		private var logger:Logger;
		public var bossSprite:Sprite; //note, only ONE other class accesses this: behavShake. 
		private var anonymousPegs:uint=0;
		
		override public function kill():void{
			actionListeners(false);
			currentDisplay = null
			logger = null;
			bossSprite = null;
			super.kill();
		}
		
		public function BehaviourBoss(bossSprite:Sprite,currentDisplay:IOnScreenBoss,logger:Logger){
			this.currentDisplay=currentDisplay;
			this.logger=logger;
			this.bossSprite=bossSprite;
			actionListeners(true);
			super();
		}
		
		override public function log(what:String):void{
			logger.log(what);
		}
		
		override public function actionWrapper(stim:object_baseClass, action:String):Function
		{
			switch(action){
				case "start"	:		return function():void{currentDisplay.runDrivenEvent(stim.peg);}; 	break;
				
				case "stop"	:			return function():void{currentDisplay.stopObj(stim.peg);}; 			break;
				
				case "kill"	:			return function():void{currentDisplay.killObj(stim.peg);}; 			break;
				
				case "hide":
				case "invisible":		return function():void{stim.pic.visible==false;};					break;
				
				case "visible":
				case "reveal":
				case "unhide":
				case "show":			return function():void{stim.pic.visible==true;};					break;
				
				case "togglevisible": 	return function():void{stim.pic.visible=!stim.pic.visible;};		break;
			}
			
			return null;
		}
		
		override public function propWrapper(stim:object_baseClass, prop:String):Function
		{
			//trace("33333:",prop,stim.OnScreenElements.hasOwnProperty(prop),["timeStart","timeEnd","duration"].indexOf(prop)!=-1);
			
			if(["timeStart","timeEnd","duration"].indexOf(prop)!=-1)return timeSetter(stim,prop);
				
			else if(stim.OnScreenElements.hasOwnProperty(prop)){
				
				var functional:Function=setValue(stim,prop)
				return function(what:String =null,to:* =null):String{
					if(what!=null) functional(what,to);
					return stim.OnScreenElements[prop];
				};				
			}
			super.propWrapper(stim,prop); //if this happens, Error
			return null;
		}
		
		private function setValue(stim:object_baseClass, prop:String):Function{
			
			switch(typeof(stim.OnScreenElements[prop])){
				
				case "boolean":
				case "string": 	return function(what:String,to:String):void{
					
					stim.OnScreenElements[what]=to;
				}
					break;
				
				case "uint":
				case "int":
				case "number": 	return function(what:String=null,to:* =null):void{
					if(!isNaN(Number(to)))stim.OnScreenElements[what]=to;
					else throw new Error("Problem setting a value ('"+what+"' to '"+to+"' as it must be a number.");
				}
					break;
				case "object": return function(what:String =null,to:* =null):void{
					if(to is Object)stim.OnScreenElements[what] = to.concat();
					else if(to is String){
						if(to.charAt(0)=="[" && to.charAt(to.length-1)=="]") to= to.substr(1,to.length-2)
						stim.OnScreenElements[what]=to.split(",");
					}
					else throw new Error("Problem setting a value ('"+what+"' to '"+to+"' as it must be an array or a string as such: '['a','b']'.");
				}
					break;
			}		
			return null;
		}
		
		private function timeSetter(stim:object_baseClass, prop:String):Function
		{	
			var funct:Function = function(prop:*):Function{
				switch(prop){
					case "timeStart": 	return function(what:String, to:Number):void{currentDisplay.setTimes(stim,to,-1,-1);}; break;
					case "timeEnd":		return function(what:String, to:Number):void{currentDisplay.setTimes(stim,-1,to,-1);}; break;
					case "duration":	return function(what:String, to:Number):void{
						currentDisplay.setTimes(stim,-1,-1,to);
					}; break;			
				}
				return null	;	
			}
			
			var functional:Function = funct(prop);
			
			return function(what:* =null,to:* =null):Number{
				if(what){
					if(!isNaN(Number(to)))functional(what,to);
					else throw new Error("Problem in changing timing value '"+what+"': you must provide a number, not this: '"+to+"'"); 
				}
				return to;
			}
		}
		
		
		
		
		
		//from Trial.  Critical interface
		public function passObject(tempObj:object_baseClass,params:Object):void{
			
			if(tempObj.peg=="")tempObj.peg="noPeg"+anonymousPegs++;
			
			var hasBehavs:Boolean = params.start==-1;
			currentDisplay.addtoTimeLine(hasBehavs,tempObj, params.start, params.end);
			
			objectAdded(tempObj);
		}
		
		/*public function behaviourLogic(tempObj:object_baseClass,params:Object):void{
		
		var iteration:uint= 		params.iteration;
		var start:Number= 			params.start;		
		var end:Number=				params.end;		
		var logicStart:String=		params.logicStart;
		var logicEnd:String=		params.logicEnd;
		var myPeg:String= 			params.myPeg;
		var tempObject:uberSprite=	tempObj.RunMe();
		
		var notAddedToCurrentDis:Boolean=true;
		
		if((logicStart!="" && logicEnd!="") ||
		myPeg=="" || 
		(myPeg!="" && logicStart!="")) //may have a behaviour that is passed objects but NEEDS to run at a specific time
		{		
		currentDisplay.addtoTimedEventList(tempObj, start, end);
		
		if(myPeg=="")tempObj.peg="noPeg"+anonymousPegs++;
		
		addtoTimedEventList(tempObj);
		notAddedToCurrentDis=false;
		}
		
		if(myPeg!=""){
		trace(myPeg,5555555555555555555)
		//trace(objName,2222,tempTimeStart,tempTimeEnd,start,end);	
		currentDisplay.addDrivenStoppableElement(tempObject, myPeg, start, end);
		
		}*/
		
		/*override public function startStim(peg:String,delay:String="",duration:String=""):void{
		currentDisplay.runDrivenEvent(peg,delay,duration)
		
		}
		
		override public function stopStim(peg:String,delay:String="",duration:String=""):void{
		currentDisplay.stopObj(peg);
		}
		
		
		override public function killStim(peg:String,delay:String="",duration:String=""):void{
		currentDisplay.killObj(peg);
		}*/
		
		//unclear if followING Fs still used
		
		/*	public function stimulusFinished(obj:object_baseClass):void{
		if(!obj.peg)currentDisplay.stopObj(obj.peg);
		else currentDisplay.stopObj(obj.peg);
		behaviourFinished(obj);
		}
		
		public function behaviourFinished(obj:object_baseClass):void{			
		if (obj.actions && obj.actions["onFinish"]){
		doEvents(obj as object_baseClass,obj.actions["onFinish"],"onFinish");
		
		//wipe this behaviour as it is permanent
		obj.actions["onFinish"]=null;
		}
		}
		
		public function doBehaviourFirst(obj:object_baseClass):void{
		doEvents(obj,obj.actions["doBefore"],"doBefore");
		}
		
		public function doBehaviourAfter(obj:object_baseClass):void{
		doEvents(obj,obj.actions["doAfter"],"doAfter");
		}*/
		
		/*private function addDrivenStoppableElement(obj:object_baseClass):void {
		objectAdded(obj);
		//addDrivenStoppableObjs(obj);
		}
		private function addtoTimedEventList(obj:object_baseClass):void {
		objectAdded(obj);
		}		*/
		
		
		private function actionListeners(isOn:Boolean):void
		{
			if(isOn){
				this.bossSprite.addEventListener(Event.REMOVED_FROM_STAGE,screenActions,true);
				this.bossSprite.addEventListener(Event.ADDED_TO_STAGE,screenActions,true);
				this.bossSprite.addEventListener(MouseEvent.CLICK,mouseActions,true);
				this.bossSprite.addEventListener(MouseEvent.MOUSE_DOWN,mouseActions,true);
				this.bossSprite.addEventListener(MouseEvent.MOUSE_UP,mouseActions,true);
			}
			else{
				this.bossSprite.removeEventListener(Event.REMOVED_FROM_STAGE,screenActions);
				this.bossSprite.removeEventListener(Event.ADDED_TO_STAGE,screenActions);
				this.bossSprite.removeEventListener(MouseEvent.CLICK,mouseActions);
				this.bossSprite.removeEventListener(MouseEvent.MOUSE_DOWN,mouseActions);
				this.bossSprite.removeEventListener(MouseEvent.MOUSE_UP,mouseActions);
			}
		}
		
		////////////////////////////////////////////////
		////////////////////////////////////////////////
		//responds to the above eventListeners
		////////////////////////////////////////////////
		private function screenActions(e:Event):void {
			if(e.target is uberSprite && e.target.actions)actionHappened(e.target as uberSprite,e.type);
		}
		
		private function mouseActions(e:MouseEvent):void {	
			/*if(e.target is uberSprite && e.target.behav){
			actionHappened(e.target as uberSprite,e.type);
			}
			else if(e.target is uberSprite){
			//findBehavs(e.target.parent as Sprite,e.type,0)
			}
			else if(e.target.name=="buttonAction"|| e.target.parent=="[object addText]"){
			//this is an annoying hack to get past an issue in the custom addButton class where if one clicks the button name it registers here at a TextField, not a Button. 
			actionHappened(e.target.parent as uberSprite,e.type);}
			else if(e.target.parent.parent=="[object addMultipleChoice]"){
			actionHappened(e.target.parent.parent as uberSprite,e.type);
			}*/
		}
	}
}