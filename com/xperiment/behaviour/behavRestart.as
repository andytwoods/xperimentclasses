package com.xperiment.behaviour
{
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	public class behavRestart extends behav_baseClass
	{
		override public function setVariables(list:XMLList):void {
			setVar("number","timeToRestart",1000);
			super.setVariables(list);
			
		}
		
		override public function nextStep(id:String=""):void {
			//trace(getVar("timeToRestart"));
			var timer:Timer = new Timer(Number(getVar("timeToRestart")),1);
			timer.addEventListener(TimerEvent.TIMER_COMPLETE,function(e:TimerEvent):void{
				e.currentTarget.removeEventListener(e.type,arguments.callee);
				theStage.dispatchEvent(new Event("endOfStudy",true));
			});
			timer.start();
		}
	}
}