﻿package com.xperiment.behaviour {
	import com.xperiment.uberSprite;
	import com.xperiment.trial.overExperiment;
	
	import flash.display.Shape;
	import flash.geom.Point;

	public class behavSwap extends behav_baseClass{
		
		
		override public function setVariables(list:XMLList):void {
		super.setVariables(list);
		var test:Shape = new Shape();
		test.graphics.drawRect(0,0,1,1);
		pic.addChild(test);
		}
		
		
		override public function storedData():Array {
			var tempData:Array = new Array();
			tempData.event=String(getVar("peg"));
			tempData.data="true";
			super.objectData.push(tempData);
			return objectData;
		}


		private function flip():void{
			var tempPoint:Point = new Point(behavObjects[0].x,behavObjects[0].y);
			var tempScaleX:Number = behavObjects[0].pic.scaleX;
			var tempScaleY:Number = behavObjects[0].pic.scaleY;
			behavObjects[0].x=behavObjects[1].x;
			behavObjects[0].y=behavObjects[1].y;
			behavObjects[1].x=tempPoint.x;
			behavObjects[1].y=tempPoint.y;		
		}
		
		override public function nextStep(id:String=""):void{
			//trace("flip");
			flip();
		}
		
		override public function stopBehaviour(obj:uberSprite):void{
			flip();
		}
			

		override public function kill():void {
		super.kill();
		}
	}
}