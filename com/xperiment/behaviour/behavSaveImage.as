﻿package com.xperiment.behaviour {
	import com.adobe.images.PNGEncoder;
	import com.dgrigg.minimalcomps.graphics.Rect;
	import com.xperiment.uberSprite;
	import com.xperiment.stimuli.addText;
	import com.xperiment.trial.overExperiment;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.net.FileReference;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.utils.ByteArray;
	
	import flash.filters.DropShadowFilter;
	
	
	
	public class behavSaveImage extends behav_baseClass{
		private var bg:Sprite;
		private var tagArr:Array=new Array;
		private var counter:uint=1;
		
		override public function setVariables(list:XMLList):void {
			setVar("string","imageSize","11");
			setVar("boolean","hideFrame",false);
			setVar("boolean","useAsFrame",false);
			super.setVariables(list);
		}
		
		override public function nextStep(id:String=""):void{
			for each(var obj:uberSprite in behavObjects){
				var contin:Boolean=true;
				for each(var spr:Sprite in tagArr){
					if(obj.contains(spr))contin=false;
				}
				if(contin){
					var x:int=0;
					var y:int=0;
					var width:uint=0;
					var height:uint=0;
					var myTag:Sprite;
					myTag=createTag();
					myTag.x=(obj.width-myTag.width)/obj.scaleX;
					//myTag.y=(myObj.height-myTag.height)/myObj.scaleY;
					myTag.visible=true;
					obj.addChild(myTag);
					myTag.scaleX=1/obj.scaleX;
					myTag.scaleY=1/obj.scaleY;
					myTag.addEventListener(MouseEvent.CLICK,cli);
					tagArr.push(myTag);
				}
			}
		}
		
		override public function stopBehaviour(obj:uberSprite):void {
			for each(var spr:Sprite in tagArr){
				if(obj.contains(spr))spr.removeChild(spr);
			}
			
			super.stopBehav(id);
		}
		
		
		
		protected function cli(e:MouseEvent):void
		{
			(e.target as Sprite).visible=false;
			var spr:Sprite=e.target.parent;
			var byteArray:ByteArray;
			
			if(getVar("useAsFrame") as Boolean)
			{
				if(getVar("hideFrame") as Boolean)spr.visible=false;
				var bitmapdata:BitmapData = new BitmapData(theStage.stageWidth, theStage.stageHeight);
				bitmapdata.draw(theStage);
				var bitmapDataA: BitmapData = new BitmapData(spr.width, spr.height);
				bitmapDataA.copyPixels(bitmapdata, new Rectangle(spr.x, spr.y, spr.width, spr.height), new Point(0, 0));
				byteArray = PNGEncoder.encode(bitmapDataA);
				spr.visible=true;
			}
			else {
				var bitmapData:BitmapData=new BitmapData(spr.width/spr.scaleX, spr.height/spr.scaleY);
				bitmapData.draw(spr);  
				byteArray = PNGEncoder.encode(bitmapData);		
			}
			
			trace("cli");
			var fileReference:FileReference=new FileReference();
			fileReference.save(byteArray, "image"+(counter)+".png");
			counter++;
			(e.target as Sprite).visible=true;
		}
		
		private function createTag():Sprite{
			var spr:Sprite = new Sprite;
			spr.graphics.beginFill(0x3892ee,1);
			spr.graphics.drawRoundRect(0,0, 10,10,1.5,1.5);
			spr.graphics.beginFill(0xffffff,1);
			spr.graphics.drawRect(2,7,6,2.8);
			spr.graphics.beginFill(0x353535,1); //black bit
			spr.graphics.drawRect(2,7,2.2,2.8);
			spr.graphics.beginFill(0xffffff,1);
			spr.graphics.drawRect(1,.5,8,4.5);
			var dropShadow:DropShadowFilter = new DropShadowFilter();
			dropShadow.alpha = .5;
			dropShadow.blurX = 5;
			dropShadow.blurY = 5;
			spr.filters = new Array(dropShadow);
			spr.buttonMode=true;
			spr.mouseEnabled=true;
			
			return spr;
		}
		
		
		override public function kill():void {
			for (var i:uint=0;i<tagArr.length;i++){
				if(tagArr[i].hasEventListener(MouseEvent.CLICK))tagArr[i].removeEventListener(MouseEvent.CLICK,cli);
				
			}
			super.kill();
		}
	}
}