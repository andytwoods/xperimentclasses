package com.xperiment.behaviour
{
	import com.Logger.Logger;
	import com.xperiment.uberSprite;
	import com.xperiment.ProcessScript;
	import com.xperiment.events.runGlobalFunctionsEvent;
	import com.xperiment.interfaces.IGiveScript;
	
	import flash.display.Sprite;
	
	public class behavLanguage extends behav_baseClass
	{
		private var exptScript:XML;

		
		
		override public function setVariables(list:XMLList):void {
			
			setVar("string","language","");
			super.setVariables(list);

		}	
		
		public function giveExptScript(exptScript:XML):void{
			this.exptScript=exptScript;
		}
		
		override public function nextStep(id:String=""):void
		{
			//trace("22",getVar("language"));
			theStage.dispatchEvent(new runGlobalFunctionsEvent(runGlobalFunctionsEvent.COMMAND,"language",getVar("language")));
			super.nextStep();
		}
	}
}