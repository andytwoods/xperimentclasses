package com.xperiment.behaviour
{
	import com.xperiment.Results.Results;
	import com.xperiment.events.runGlobalFunctionsEvent;

	public class behavSaveData extends behav_baseClass
	{
		
		override public function setVariables(list:XMLList):void {
			setVar("boolean","checkDataExists",true)
			super.setVariables(list);
			
		}	

		
		override public function nextStep(id:String=""):void
		{
			var results:Results = Results.getInstance();
			if((getVar("checkDataExists") as Boolean == false) || results.checkDataExists()){
			
				theStage.dispatchEvent(new runGlobalFunctionsEvent(runGlobalFunctionsEvent.COMMAND,"savedata"));
				super.nextStep();
			}
		}
	}
}