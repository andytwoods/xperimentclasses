﻿package com.xperiment.behaviour{
	import flash.events.MouseEvent;
	import flash.display.*;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import com.xperiment.trial.overExperiment;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	import flash.filters.BlurFilter;
	import flash.filters.BitmapFilter;
	import flash.filters.BitmapFilterQuality;
	import com.xperiment.uberSprite;

	public class behavPause extends behav_baseClass {//note without 'public' nothing happens!!

		private var myTimer:Timer;
		private var myTimers:Array=new Array  ;
		private var alreadyStarted:Boolean=false;

		override public function setVariables(list:XMLList):void {
			setVar("uint","duration",1000);
			super.setVariables(list);
		}


		override public function nextStep(id:String=""):void{
			if (alreadyStarted==false) {
				myTimer=new Timer(getVar("duration"),1);
				myTimer.addEventListener( TimerEvent.TIMER_COMPLETE, onListTimer,false,0,true);
				myTimer.start();
				alreadyStarted=true;
			}
		}

		private function onListTimer(e:Event):void {
			behaviourFinished();
		}

	

		override public function kill():void {
			if(myTimer && myTimer.hasEventListener(TimerEvent.TIMER))myTimer.removeEventListener( TimerEvent.TIMER, onListTimer);
			super.kill();
		}
	}
}