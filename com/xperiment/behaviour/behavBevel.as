﻿package com.xperiment.behaviour{
	import com.greensock.TweenMax;
	import com.xperiment.uberSprite;
	import com.xperiment.events.BehaviourDataEvent;
	import com.xperiment.trial.overExperiment;
	
	import flash.geom.Point;
	

	public class behavBevel extends behav_baseClass {
		
		public static const RAD_TO_DEG:Number = 57.295779513082325225835265587527; // 180.0 / PI;
		private var angle:Number;
		private var point:Point;
		private var distance:Number;
		private var tweens:Object = new Object;
		private var numberSelectable:uint;
		override public function setVariables(list:XMLList):void {

			setVar("int","startOppacity",1);
			setVar("int","angle",45);
			setVar("int","distance",10);
			setVar("int","blur",5);
			setVar("int","numberSelectable",1);
			setVar("boolean","forceDeselect",true);
			setVar("boolean","saveWhichSelected",false);
			super.setVariables(list);
			numberSelectable=getVar("numberSelectable") as uint;
		}	
		
		
		override public function returnsDataQuery():Boolean {
			return getVar("saveWhichSelected") as Boolean;
		}
		
		override public function storedData():Array {
			var tempData:Array = [];	
		
			if(peg && peg!="")tempData.event=peg;
			else tempData.event="numSelected"
			
			var selected:Array=[];
			for each(var us:uberSprite in behavObjects){
				if(tweens[us.peg]!=undefined)selected.push(us.peg)
			}
			
			tempData.data=selected.join(",");
			
			super.objectData.push(tempData);
			return objectData;
		}
		
		
		override public function receivedData(e:BehaviourDataEvent):void{
			point=e.data as Point;
			shadow();
			
			
		}
		
		override public function myUniqueActions(action:String):Function{
/*
			switch(action){
				case "this": return function():void{if(origObj!=null)thisShadow(origObj);}; break;
		
				case "thistoggle":
					if(tweens[origObj.peg]==undefined){
						if(numberSelectable==0)thisShadow(origObj);
						else{
							var count:uint=0;
							if(numberSelectable==1 && getVar("forceDeselect") as Boolean){
								for each(var us:uberSprite in behavObjects){
									if(tweens[us.peg]!=undefined)thisShadowRemove(us);
								}
								thisShadow(origObj);
							}
							
							else{
								for each(us in behavObjects){
									if(tweens[us.peg]!=undefined)count++;
								}
								if(count<numberSelectable)	thisShadow(origObj);
							}
						}
					}
					else {
						thisShadowRemove(origObj);
					}

					break;
				
				default:
					super.doAction(str);
					break;
			}*/
			return null;
		}
		
		private function thisShadowRemove(origObj:uberSprite):void
		{
			var tween:TweenMax=tweens[origObj.peg];
			tween.reverse()
			tweens[origObj.peg]=undefined;
		}		
	
/*		override public function nextStep(id:String=""):void
		{
			
			super.nextStep();
		}*/
		
		private function shadow():void{
			for each(var us:uberSprite in behavObjects){
				thisShadow(us);
			}
		}
		
		private function thisShadow(us:uberSprite):void{
			if(point){
				point=new Point(theStage.width*.25,theStage.height*.25);	
				distance = Math.sqrt((us.x - point.x) * (us.x - point.x) + (us.y - point.y) * (us.y - point.y));
				angle = Math.atan2( us.y+us.myHeight*.5 - point.y, us.x +us.width*.5- point.x ) * RAD_TO_DEG-180;
				var myTween:TweenMax=TweenMax.to(us, 1, {bevelFilter:{blurX:50, blurY:50, distance:distance, angle:angle, strength:10}});
				tweens[us]=myTween;
			}
			else{
				myTween=TweenMax.to(us, 0.1, {glowFilter:{alpha:1, blurX:30, blurY:30,strength:5},dropShadowFilter:{color:0x000000, alpha:1, blurX:10, blurY:10, angle:90, distance:10, inner:true}});
				tweens[us.peg]=myTween;
				//trace(3,tweens[us.peg]);
			}
		}

	}
}
/*distance : Number [0]
angle : Number [0]
highlightColor : uint [0xFFFFFF]
highlightAlpha : Number [0.5]
shadowColor : uint [0x000000]
shadowAlpha :Number [0.5]
blurX : Number [2]
blurY : Number [2]
strength : Number [0]
quality : uint [2]
index : uint
addFilter : Boolean [false]
remove : Boolean [false]*/