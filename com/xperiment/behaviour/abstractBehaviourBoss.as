package com.xperiment.behaviour
{
	
	import com.xperiment.uberSprite;
	import com.xperiment.BehavLogicAction.LogicActions;
	import com.xperiment.BehavLogicAction.PropValDict;
	import com.xperiment.stimuli.object_baseClass;
	
	import flash.display.Sprite;
	import flash.utils.Dictionary;
	import flash.utils.getQualifiedClassName;
	
	public class abstractBehaviourBoss extends Sprite
	{
		private const availEvents:Vector.<String> = Vector.<String>(["mouseDown", "mouseClick",
			"doNow",  //happens immediately after setup.
			"doBefore", //happens immediately before object appears
			"doAfter", //happens immediately after object appears
			"rightClick","rightDown","rightUp","middleClick","middleDown","middleUp","mouseUp","mouseMove","mouseWheel","mouseOver","mouseOut","doubleClick","rollOut","rollOver"]);
		
		private var behavObjectsDict:Dictionary=new Dictionary; //[peg] = [obj1,obj2]
		//private var drivenStoppableObjs:Array=[];
		public var propValDict:PropValDict; //public for testing purposes only
		private var logicActions:LogicActions;
		private var listenerKillList:Vector.<Listener>;
		private var doNowBehavs:Vector.<String>;
		
		public function abstractBehaviourBoss(){
			propValDict=PropValDict.getInstance();
			logicActions=new LogicActions(propValDict); //created afresh each Trial
		}
		
		public function kill():void{
			
			for each(var l:Listener in listenerKillList){
				l.killF();
				l.kill();
				l=null;
			}
			listenerKillList=null;
			
			for (var i:uint=0;i<behavObjectsDict.length;i++)behavObjectsDict[i]=null;
			behavObjectsDict=null;
			
			propValDict.killPerTrial();
			
			//for (i=0;i<drivenStoppableObjs.length;i++)drivenStoppableObjs[i]=null;
			//drivenStoppableObjs=null;
		}	
		
		public function init():void{
			//TOPS=trialOnlyProperties
			//getExistingTOPS(propValDict.perTrialProps);
			linkTOPS();
			if(doNowBehavs)doNowBehavsF();
		}
		
		private function doNowBehavsF():void{
			
			for(var i:int=0;i<doNowBehavs.length;i++){
				propValDict.incrementPerTrial(doNowBehavs[i]);
				doNowBehavs[i]=null;
			}
			doNowBehavs=null;
		}
		
		/*	private function getExistingTOPS(props:Vector.<String>):void
		{
		var obj:String;
		var index:int;
		var prop:String;
		var objsArr:Array
		var stim:object_baseClass;
		
		for each(var objProp:String in props){
		if(objProp.indexOf("()")==-1){
		index=objProp.indexOf(".");
		obj=objProp.substr(0,index);
		prop=objProp.substr(index+1);
		
		if(behavObjectsDict.indexOf(prop)!=-1){
		stim = behavObjectsDict[prop][0];
		
		
		
		}
		}
		}
		}
		
		private function setPropInDict(obj:object_baseClass):void
		{
		// TODO Auto Generated method stub
		
		}*/
		
		/*		
		
		
		private function linkACTIONSfunct(stim:object_baseClass, action:String, subscribe:Function):void
		{
		subscribe(stim,function():void{
		trace("running behaviour:", action, "in this object",stim);
		});
		}
		*/
		//prop_i: goes throguh propDict.propDict and isolates the object in question.
		//		object_i: iterates through the objects until matches with above object.
		
		private function linkTOPS():void
		{
			//IMPORTANT. Goes through propDict 
			////TOPS=trialOnlyProperties
			var prop:String;
			var objsArr:Array;
			var i:int;
			
			for each(var pegProp:String in propValDict.propDict){
				
				/*possible values:
				E.action 	- 	a listener
				k 			-	across experiment var so IGNORE
				'sausage' 	-	throw error
				*/
				var len:int=pegProp.split(".").length;
				
				if(len==2){
					
					objsArr=behavObjectsDict[pegProp.split(".")[0]]; //returns the list of objects with this peg.
					prop = pegProp.split(".")[1]
					if(objsArr) 
						for(i=0;i<objsArr.length;i++){	
							{			
								if(pegProp.indexOf("()")==-1 && pegProp.split(".").length==2){
									
									if(		linkTOPSListeners(objsArr[i],prop,propValDict.incrementPerTrial,pegProp)
										==false){
										if(		linkTOPSProps(objsArr[i],propValDict.bind,propValDict.updateVal,pegProp,prop)
											==false){											
											throw 	new Error("Problem, '"+pegProp+"' never ever can happen!  You've specified an unknown event or unsettable object property.");}}}
									
								else if(pegProp.indexOf("()")!=-1)						linkTOPSActions(objsArr[i],prop,propValDict.bind,pegProp);
								else throw new Error("badly formed action [should end in () ] or listener [should have ONE . ]:"+pegProp);
							}		
						}
					else throw new Error("You have not given any object this peg: "+pegProp.split(".")[0]+".  Nothing can be done with this action then: "+pegProp+".");
					propValDict.addTrialProps(pegProp); 
				}
					
				else if(len==1){
					//ignore this.  It is an 'across experiment' variable.
				}
					
				else if(len>2){
					throw new Error("Action specified with 2 dots.. can only have 1 dot.  e.g. banana.cli.  Was given: "+pegProp);
				}
					
				else if(pegProp.split("'").length==3 || !isNaN(Number(pegProp))){
					throw new Error("Found a bug: Should not be possible to receive strings or numbers here.  Was given: "+pegProp);
				}
					
				else{
					throw new Error("Found a bug: wrongly formatted action: "+pegProp);
				}	
			}
		}
		
		public function propWrapper(stim:object_baseClass, prop:String):Function{
			throw new Error("Problem trying to set a value '"+prop+"' on this object '"+stim.peg+"': propery does not exist."); 
			return null;
		}
		
		//only public for testing purposes
		//final public function linkTOPSProps(stim:object_baseClass, what:String, bind:Function,updateVal:Function, prop:String):Boolean
		final public function linkTOPSProps(stim:object_baseClass, bind:Function,updateVal:Function,pegProp:String, prop:String):Boolean
		{
			/*
			unversal & special: timeStart, end, position, transparency, 
			think about: shadow?? Perhaps goes in linkTOPSActions 
			*/
			if(!(stim.disallowedSettings && stim.disallowedSettings.indexOf(prop)==-1)){
				
				////If BehaviourBoss finds the approp named function, passes it a function to call when the given behaviour occurs.
				var settingF:Function = stim.myUniqueProps(prop);
				if(settingF==null) 	settingF = 		 propWrapper(stim,prop);
				
				if(settingF!=null){
					updateVal(pegProp,settingF());	
					bind(pegProp, settingF);}
				else 				throw new Error("Unknown property trying to be set for peg "+stim.peg+"': "+prop);		
				
			}
			else throw 	new Error("Forbidden setting'"+prop+"()"+"'  asked for on '"+stim.peg+"'!");
			
			return true;
			
		}
		
		private function linkTOPSActions(stim:object_baseClass, action:String, bind:Function,prop:String):Boolean
		{
			
			if(!(stim.disallowedActions && stim.disallowedActions.indexOf(action)==-1)){
				
				
				////If BehaviourBoss finds the approp named function, passes it a function to call when the given behaviour occurs.
				var actionF:Function;
				action=action.replace("()","");
				
				actionF = 	stim.myUniqueActions(action);
				if(actionF==null) 	actionF = 	actionWrapper(stim, action);
				
				if(actionF!=null) 	bind(prop, actionF);
				//else 				throw new Error("Unknown action' for peg "+stim.peg+"': "+action);						
				
			}
			else throw 	new Error("Forbidden action '"+action+"()"+"'  asked for on '"+stim.peg+"'!");
			
			return true;
			
		}
		
		public function actionWrapper(stim:object_baseClass, action:String):Function{return null;}
		
		//gets called once for each discovered action
		private function linkTOPSListeners(stim:object_baseClass, listenFor:String, incrementPerTrial:Function,prop:String):Boolean
		{
			//need to provide a custom event ability for individual stimuli, as done for actions (myUniqueactions)
			
			if(listenFor=="click")listenFor="mouseClick";
			
			
			if(availEvents.indexOf(listenFor)!=-1){
				
				if(!(stim.disallowedEvents && stim.disallowedEvents.indexOf(listenFor)==-1)){
					
					var listener:Listener = new Listener;
					
					listener.anonyF= function():void{incrementPerTrial(prop);}//this function is called when the action happens
					
					////If BehaviourBoss finds the approp named function, passes it a function to call when the given behaviour occurs.
					if(stim.hasOwnProperty(listenFor) && stim[listenFor] is Function)(stim[listenFor] as Function)(listener.anonyF)
					else {
						//else, create the listener here
						stim.pic.addEventListener(listenFor,listener.anonyF);
					}
					
					//this function returning a function allows variables from above to be passed 'in the future'
					//thus allowing the listener to be safely removed.
					listener.killF	= function():Function{return function():void{
						stim.pic.removeEventListener(listenFor,listener.anonyF);
						trace("removed listener:",listener,"from",stim.peg);
					}};
					if(!listenerKillList)listenerKillList = new Vector.<Listener>;
					listenerKillList.push(listener);
					
					if(listenFor=="doNow"){
						doNowBehavs ||= new Vector.<String>;; 
						doNowBehavs.push(stim.peg+"."+listenFor);
					}
					
				}
				else throw 	new Error("Problem: "+listenFor+"' on '"+stim.peg+"' never happens!");
			}
			else return false; 
			
			return true;
			//if requires mouseEvent
			//see what listener is required
			//create eventListener and listen function.
			//pass listenfunction the updateVal Function and Prop Str, and update the prop to 'true'. 
			
			//if requires onFinish / onEnd
			//first see if the stim has a function called onFinish
			
			//trace("listeners:"+prop);
			
		}		
		
		public function log(str:String):void{
			throw Error("log function should not be accessed from abstract class");
		}
		
		
		//should not need this now
		/*		public function addDrivenStoppableObjs(pic:uberSprite):void{
		drivenStoppableObjs.push(pic);
		}*/
		
		
		
		public function actionHappened(obj:uberSprite,type:String):void {
			//trace("22",obj,type)
			switch (type) {
				//case "onStart": does not work
				case "addedToStage" :
					type="onShow";
					break;
				case "removedFromStage" :
					type="onFinish";
					break;
				case "click" :
					type="onClick";
					break;
				case "mouseDown" :
					type="onCli";
					break;
				case "mouseUp" :
					type="onCk";
					break;
				case "behaviourFinished":
					type="onFinish";
					//trace(22223)
					if(obj.behavJustStopped){//allows actions originating from behaviours to be processed differently
						var tempObj:uberSprite=obj.behavJustStopped;
						obj.behavJustStopped=null;
						obj=tempObj;
					}
				case "stop":
				case "kill":
					type="stop"; 
					break;
			}
			
			//doEvents(obj as object_baseClass, obj.behav[type],type);
			
		}
		
		/*	
		public function doEvents(origObj:object_baseClass,str:String,whatHappened:String):void{
		
		
		}*/
		
		/*public function startStim(peg:String,delay:String="",duration:String=""):void{
		throw Error("startStim function should not be accessed from abstract class");
		}
		
		public function stopStim(peg:String,delay:String="",duration:String=""):void{
		throw Error("stopStim function should not be accessed from abstract class");
		}
		
		public function killStim(peg:String,delay:String="",duration:String=""):void{
		throw Error("killStim function should not be accessed from abstract class");
		}*/
		
		
		/*			
		public function proceedWithEvents(origObj:object_baseClass,action:String,tag:String,whatHappened:String):void{
		
		}*/
		
		
		public function objectAdded(obj:object_baseClass):void {
			if(behavObjectsDict[obj.peg]==undefined)behavObjectsDict[obj.peg] = [];
			
			if(behavObjectsDict[obj.peg].indexOf(obj)==-1){//each object can only be added once :-)  
				behavObjectsDict[obj.peg].push(obj);
				
				var cmbndObjLogActs:String="";
				var str:String;
				
				str=obj.getVar("behaviours");
				
				
				if(str!="")cmbndObjLogActs+=behavToIf(obj.peg,str);
				str=obj.getVar("if");
				
				if(str!=""){
					if(cmbndObjLogActs!="")cmbndObjLogActs+=",";
					cmbndObjLogActs+=str;
				}
				
				if(cmbndObjLogActs!=""){
					logicActions.passLogicAction(BehaviourBossHelper.fixBehavCommaIssue(cmbndObjLogActs));
					
				}	
			}
			//sortOutBehaviours(pic.rawBehav);
			//behavObjects.push(pic); 
		}
		
		
		private function behavToIf(behavPeg:String,behav:String):String{
			var behavArr:Array=behav.split(":");
			if(behavArr.length!=2)throw Error("problem with this logic:"+behav);
			return behavPeg+"."+behavArr[0]+"?"+behavArr[1];
		}
		
		public function testFunct(nam:String):Function{
			if(this[nam]!=undefined)return this[nam];
			else return null
		}
		
	}
}