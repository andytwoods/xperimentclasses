﻿package com.xperiment.behaviour{
	import com.xperiment.Results.Results;
	import com.xperiment.behaviour.interfaces.IbehavSaveVariable;
	
	
	
	public class behavRand extends behav_baseClass implements IbehavSaveVariable{
		private var what:Array;
		private var likelihood:Array;
		private var ran:String="";
		private var exptResults:Results;
		
		override public function setVariables(list:XMLList):void {
			setVar("boolean","saveVariables",true); //add these to setVariables at top
			setVar("string","saveVariableID","");
			setVar("string","what","");
			setVar("string","likelihood","");
			setVar("string","action","onShow");
			setVar("string","doWhat","start");
			super.setVariables(list);
			
			what=(getVar("what") as String).split(",");
			likelihood=(getVar("likelihood") as String).split(",");

			var sumLikelihood:Number=0;
			
			if(likelihood.length==0)for(var i:uint=0;i<what.length;i++){likelihood.push(1/what.length);} //if no likelihood scores, create even distribution
			for(i=0;i<likelihood.length;i++){sumLikelihood+=Number(likelihood[i]);} 
			for(i=0;i<likelihood.length;i++){likelihood[i]=likelihood[i]/sumLikelihood;} //if scores add up over 1, fix this
			for(i=1;i<likelihood.length;i++){likelihood[i]+=likelihood[i-1];} //make scores accumulative
			
			addSetVars();
		}

		public function addSetVars():void{
			if(getVar("saveVariables") && getVar("saveVariableID")==""){
				if(getVar("peg")!="") setVar("string","saveVariableID",getVar("peg"));		
				else setVar("boolean","saveVariables",false);
			}
		}
		
		public function storeVariable(dat:String):void{
			if(!exptResults)exptResults = Results.getInstance();
			if(getVar("saveVariables")){
				var savNam:String="";
				if(getVar("saveVariableID")!="")savNam=getVar("saveVariableID");
				else if(getVar("peg")!="")savNam=getVar("peg");
				if(savNam!="")exptResults.storeVariable({name:savNam,data:dat});
				else logger.log("!Could not save outcome of decision in BehavIf as you have not provided a saveVariableID and there is no peg to use.  BTW data="+dat);
			}
		}

		override public function nextStep(id:String=""):void{
			var randNumber:Number = Math.random();
			for(var i:uint=0;i<likelihood.length;i++){
				if(randNumber<likelihood[i])break;
			}
			
			ran=what[uint(i%what.length)]; //need the ran variable for the results
			 //n.b. % circular number stuff.  If run out of positions in an array, restart at position 0
			storeVariable(ran);
			
			var doWhat:String=getVar("doWhat");

			//AW JAN FIX
			//manageBehaviours.doEvents(this, ran+"["+doWhat+"]","onFinish");
		}
		
		private function sortResults():void
		{
			var tempData:Array = new Array;
			tempData.event=getVar("peg");
			tempData.data=ran;						
			objectData.push(tempData);
		}
		
		override public function returnsDataQuery():Boolean {
			var res:Boolean=super.returnsDataQuery();
			if(getVar("result")) return true;
			else if(res) return true;
			else return false;
		}
	}
}