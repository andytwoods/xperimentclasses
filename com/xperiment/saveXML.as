﻿package com.xperiment{

	import flash.events.*;
	import flash.net.*;
	import flash.events.EventDispatcher;
    import flash.events.Event;
	import flash.display.*;
	import com.mobile.saveToServerFile;
	import com.Logger.Logger;
	//import com.mobile.saveFilesInternally;
	/*
	works like this:
	var filename:String = "saveXML.php";
	var dummy:saveXMLtoServerFile = new saveXMLtoServerFile(myData, filename);
	
	ps myData is XML
	*/

	public class saveXML extends Sprite{

		public var results:XML;
		public var ExptWideSpecs:Array;
		public var emergencyEmailSentAlready:Boolean=false;
		public var listOfRequiredSaves:Array = new Array;
		public var logger:Logger;
		
		public function passLogger(logger:Logger):void{
			this.logger=logger;
		}

		public function saveProcedure(r:XML, e:Array):void {
			
			results=r;
			ExptWideSpecs=e;
			
			logger.log("-----------------------------------------------");
			logger.log("------------Saving your data-------------------");

			logger.log("saveToServerFile="+ExptWideSpecs.fileInformation.saveToServerFile);
			logger.log("send data via email="+ExptWideSpecs.email.toWhom); 
			logger.log("Only if on mobile: saveToPortableDevice="+(ExptWideSpecs.fileInformation.saveToPortableDevice!=""));
			logger.log("Only if on mobile: FORCEemergencyEmailForMobile="+ExptWideSpecs.fileInformation.FORCEemergencyEmailForMobile);
			logger.log("-----------------------------------------------");
		
			if (ExptWideSpecs.fileInformation.saveToServerFile && ExptWideSpecs.fileInformation.saveToServerFile==true){
				listOfRequiredSaves.saveToServerFile = new String();
				saveXMLtoServerFile();
			}

			if (ExptWideSpecs.fileInformation.saveToPortableDevice && ExptWideSpecs.fileInformation.saveToPortableDevice!=""){
				listOfRequiredSaves.saveToPortableDevice = new String();
				saveXMLtoLocalFile(ExptWideSpecs.fileInformation.saveToPortableDevice);
			}
			if (ExptWideSpecs.fileInformation.FORCEemergencyEmailForMobile && ExptWideSpecs.fileInformation.FORCEemergencyEmailForMobile!=""){
				sendEmergencyEmail();
			}
			
			if (ExptWideSpecs.email.toWhom && ExptWideSpecs.email.toWhom!="false" ){
				listOfRequiredSaves.email = new String();
				var myEmailAddress:String=ExptWideSpecs.email.myAddress;
				var toWhomAddress:String=ExptWideSpecs.email.toWhom;
				var subject:String=ExptWideSpecs.email.subject;
				var tempResults:String = String(results);
				if (Boolean(ExptWideSpecs.email.simpleOutput)==true) tempResults=simpleResults(results);
				var emailXML:emailResults=new emailResults(logger,tempResults,myEmailAddress,toWhomAddress,subject);
				listOfRequiredSaves.email=String(true);
				everythingSaved();
			}

		}

		public function sendEmergencyEmail():void {
			logger.log("in here");
			logger.log("sendEmergencyEmail is not possible for an 'on web' experiment.  Best to set FORCEemergencyEmailForMobile='false' or remove altogether");
			//listOfRequiredSaves.FORCEemergencyEmailForMobile=String(false);
			everythingSaved();
		}
		
		public function saveXMLtoLocalFile(locat:String):void{
			logger.log("saveXMLtoLocalFile is not possible for an 'on web' experiment.  Best to set saveXMLtoLocalFile='false' or remove altogether");
			listOfRequiredSaves.saveToPortableDevice = String(false);
			everythingSaved();
		}

		public function saveXMLtoServerFile():void {
			var phpLocation:String=ExptWideSpecs.fileInformation.saveDataURL+"/saveXML.php";
			var saveToServerF:saveToServerFile = new saveToServerFile(phpLocation,results,logger);
			saveToServerF.addEventListener("saveXMLtoServerFile_saved",saveXMLtoServerFile_saved);
			saveToServerF.addEventListener("saveXMLtoServerFile_fail",saveXMLtoServerFile_fail);
			saveToServerF.addEventListener("saveXMLtoServerFile_emergency",saveXMLtoServerFile_emergency);

		}
		
		private function saveXMLtoServerFile_saved(e:Event):void{saveXMLtoServerFile_combined(e,false);}
		private function saveXMLtoServerFile_fail(e:Event):void{saveXMLtoServerFile_combined(e,false);}
		private function saveXMLtoServerFile_emergency(e:Event):void{sendEmergencyEmail();saveXMLtoServerFile_combined(e,false);}
		
		private function saveXMLtoServerFile_combined(e:Event, success:Boolean):void{
			e.target.removeEventListener("saveXMLtoServerFile_saved",saveXMLtoServerFile_saved);
			e.target.removeEventListener("saveXMLtoServerFile_fail",saveXMLtoServerFile_fail);	
			e.target.removeEventListener("saveXMLtoServerFile_emergency",saveXMLtoServerFile_fail);	
			listOfRequiredSaves.saveToServerFile=String(success);
			everythingSaved();
		}

		public function everythingSaved():void{
			var count:uint=0;
			var numberOfRequired:uint=0;
			
			for each (var myBool:Boolean in listOfRequiredSaves){
				if (myBool !="") count++;
				numberOfRequired++;
			}
			
			if (count==numberOfRequired) {
				logger.log("told xperiment to stop everything");
				this.dispatchEvent(new Event("dataSaved"))
				};
		}

		public function simpleResults(str:String):String {
			var removeStr:RegExp=/<(\/).*?>/g;
			str=str.replace(removeStr,"");

			removeStr=/<.*?trialData.*>/g;
			str=str.replace(removeStr,"");

			removeStr=/<buttonPressed_Count1>Continue/g;
			str=str.replace(removeStr,"");

			removeStr=/<Experiment id="study1">/g;
			str=str.replace(removeStr,"");

			removeStr=/(\n)/gm;
			str=str.replace(removeStr,"");

			removeStr=/(\<)/g;
			str=str.replace(removeStr,"\n\n");
			removeStr=/(\/)/g;
			str=str.replace(removeStr,"");
			removeStr=/(\>)/g;
			str=str.replace(removeStr,"\t");
			return str;
		}
	}
}