﻿package com.xperiment{
	import com.Logger.Logger;
	
	import flash.display.*;
	import flash.events.*;
	import flash.net.*;
	import flash.system.ApplicationDomain;
	import flash.system.LoaderContext;

	/*
	works like this:
	
	var myEmailAddress:String = "123@123.com";
	var toWhomAddress:String = "andytwoods@gmail.com";
	var subject:String = "123";
	var dummy:emailResults = new emailResults(myData, myEmailAddress, toWhomAddress, subject);
	
	ps myData is XML
	*/


	public class emailResults {

		public var loader:URLLoader;
		public var logger:Logger;
		private var emailSuccessful:Boolean=false;
		private var emergencyEmailF:Function;
		
		public function success():Boolean{
			return emailSuccessful;
		}
		

		//public function err:Error(e:Event);
		
		public function emailResults(emergencyEmailF:Function, myResults:String) {
			this.emergencyEmailF=emergencyEmailF;
			this.logger=Logger.getInstance()
			var variables:URLVariables=new URLVariables();
			for(var str:String in ExptWideSpecs.ExptWideSpecs.email){
				if(["myEmail","toWhom"].indexOf(str)==-1)variables[str]=ExptWideSpecs.ExptWideSpecs.email[str];
			}
			
			variables.from=ExptWideSpecs.ExptWideSpecs.email.myAddress;
			variables.email=ExptWideSpecs.ExptWideSpecs.email.toWhom;
			
			variables.data=myResults;

			var request:URLRequest=new URLRequest(); 
			//request.requestHeaders.push(new URLRequestHeader('Content-type', 'multipart/form-data'));
			request.url='http://www.opensourcesci.com/experiments/email/emailXMLasAttachment.php';
			request.method=URLRequestMethod.POST;
			request.data=variables;
			
			
			
			loader=new URLLoader();
			loader.addEventListener(Event.COMPLETE,messageSent);
			loader.addEventListener(IOErrorEvent.IO_ERROR, catchIOError,false,0,true);
			loader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler,false,0,true);
			loader.addEventListener(UncaughtErrorEvent.UNCAUGHT_ERROR,uncaughtError);
		
			//loader.dataFormat=URLLoaderDataFormat.TEXT;
			loader.dataFormat=URLLoaderDataFormat.VARIABLES;
			

			
			try{
				loader.load(request);
			}
			catch(e:Error){
				variables=null;
				//trace("Unable to send email as the reciprocating php file does not exist","Error message: "+e);
				logger.log("Unable to send email as the reciprocating php file does not exist");
				logger.log("Error message: "+e);
				emergencyEmailF(e);
				
			}

			
			logger.log("-----------------------------------------------");
			logger.log("attempting to send you your data via email:");
			logger.log("toWhom:"+variables.email);
			logger.log("subject:"+variables.subject);
			logger.log("from:"+variables.from);

		}
		
		protected function uncaughtError(e:Event):void
		{
			if(emergencyEmailF!=null)emergencyEmailF(e);
		}		
		
		private function removeListeners():void{
			loader.removeEventListener(Event.COMPLETE,messageSent);
			loader.removeEventListener(IOErrorEvent.IO_ERROR, catchIOError);
			loader.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
			loader.removeEventListener(HTTPStatusEvent.HTTP_STATUS,httpStatus);
		}
		
		protected function httpStatus(e:HTTPStatusEvent):void
		{
			
			switch(String(e.status).substr(0,1)){ //for status codes see http://en.wikipedia.org/wiki/List_of_HTTP_status_codes
				
				// 1xx Informational Request received, continuing proces
				case "2": // 2xx Success
					//all is good :)
					break;
				default:
					if(emergencyEmailF!=null)emergencyEmailF(e.status);
					break;
				//3xx Redirection
				//4xx Client Error
				//5xx Server Error
				
				
			}
		}		
		

		private function catchIOError(event:IOErrorEvent):void {

			event.target.removeEventListener(Event.COMPLETE,messageSent);
			event.target.removeEventListener(IOErrorEvent.IO_ERROR, catchIOError);
			event.target.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
			logger.log("Unable to send email as there is no net connection");
			logger.log("Error message: "+event.type);
			if(emergencyEmailF!=null)emergencyEmailF(event);
			removeListeners();
		}
		private function securityErrorHandler(event:SecurityErrorEvent):void {
			event.target.removeEventListener(Event.COMPLETE,messageSent);
			event.target.removeEventListener(IOErrorEvent.IO_ERROR, catchIOError);
			event.target.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
			logger.log("Unable to send email as you have not specified a good URL address");
			logger.log("Error message: "+event.type);
			if(emergencyEmailF!=null)emergencyEmailF(event);
			removeListeners();
		}
		private function messageSent(event:Event):void {
			removeListeners();
			event.target.removeEventListener(Event.COMPLETE,messageSent);
			event.target.removeEventListener(IOErrorEvent.IO_ERROR, catchIOError);
			event.target.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
			var loader:URLLoader=URLLoader(event.target);
			var vars:URLVariables=new URLVariables(loader.data);
			var str:String=unescape(vars.toString())
			trace("sent email. response from server:",str);
			if(str.indexOf("Mailer Error")==-1){
				logger.log("-----------------------------------------------");
				logger.log("message from email server: "+vars.answer);
				logger.log("-----------------------------------------------");
			}
			else emergencyEmailF(str);
			
		}
	}
}