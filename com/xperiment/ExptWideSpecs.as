package com.xperiment
{

	
	import com.Logger.Logger;

	public class ExptWideSpecs
	{
		
		static public var ExptWideSpecs:Array = new Array;
		static private var logger:Logger;
		
		static public function setup(xml:*):void{ 
			
			logger=Logger.getInstance();
		
			
			ExptWideSpecs.info=new Array  ;
			ExptWideSpecs.info.id="default ID";
			
			ExptWideSpecs.computer = new Array;
			ExptWideSpecs.computer.autoClose = true as Boolean;
			ExptWideSpecs.computer.autoCloseTimer = 2000 as int;
			ExptWideSpecs.computer.alwaysInFront = true;
			ExptWideSpecs.computer.fullScreen = false as Boolean;
			ExptWideSpecs.computer.align = "center";
			ExptWideSpecs.computer.fullScreenMessage = true as Boolean;
			ExptWideSpecs.computer.saveLocallySubFolder = "Xperiment/Expt" as String;
			ExptWideSpecs.computer.XperimentDataFolderLocation = "My Documents" as String;
			ExptWideSpecs.computer.XperimentDataFolder = "Xperiment" as String;
			//screen properties
			ExptWideSpecs.screen=new Array  ;
			ExptWideSpecs.screen.BGcolour=0x000000 as Number;
			ExptWideSpecs.screen.orientation="default" as String;
			ExptWideSpecs.screen.percentageFromWidthOrHeightOrBoth="both";
			
			//save to local file variables
			ExptWideSpecs.fileInformation=new Array  ;
			ExptWideSpecs.fileInformation.save=true as Boolean;
			ExptWideSpecs.fileInformation.saveDataURL = "" as String;
			ExptWideSpecs.fileInformation.saveToServerFile = true as Boolean;
			ExptWideSpecs.fileInformation.saveToPortableDevice="myExpt";
			ExptWideSpecs.fileInformation.stimuliFolder="stimuli"
			
			//send email variables
			ExptWideSpecs.email=new Array  ;
			ExptWideSpecs.email.simpleOutput=false as Boolean;
			ExptWideSpecs.email.myAddress="andy.woods@xperiment.mobi" as String;
			ExptWideSpecs.email.toWhom="false" as String;
			ExptWideSpecs.email.subject="ExperimentData" as String;
			ExptWideSpecs.email.emergencyEmail = "true" as String;//true or false
			ExptWideSpecs.email.forcedEmergencyEmail = false as Boolean;
			ExptWideSpecs.email.Host= "" as String;
			ExptWideSpecs.email.Port= "" as String;
			ExptWideSpecs.email.Username= "" as String;
			ExptWideSpecs.email.Password= "" as String;
			ExptWideSpecs.email.SMTPAuth= "" as String;		
			ExptWideSpecs.email.message="Here is some participant data. Best wishes, Xperiment.mobi";
			ExptWideSpecs.email.filename="sjData.sav";
			
			ExptWideSpecs.spliceTrials=new Array  ;
			ExptWideSpecs.spliceTrials.splices=new String  ;
			
			ExptWideSpecs.results=new Array  ;
			ExptWideSpecs.results.diagnostics=true as Boolean;
			
			ExptWideSpecs.defaults = new Array;
			ExptWideSpecs.defaults.ITI=500 as uint;
			
			for (var i:uint=0; i<xml.children().length(); i++) {
				try {
					var tempClassReference:String=xml.children()[i].name();
					XMLListObjectPropertyAssigner(tempClassReference,xml[tempClassReference]);
				} catch (error:Error) {
					logger.log("you've tried to set a global experiment property that unfortunately does not exist");
				}
				
				
				if (ExptWideSpecs.results.diagnostics) {
					var currentDate:Date = new Date();
					ExptWideSpecs.results.value=currentDate as Date;
				}
				
			}
			
		}
		
		static public function XMLListObjectPropertyAssigner(subType:String,textinfo:XMLList):void {
			
			var attNamesList:XMLList=textinfo.@*;
			
			var VarName:String=attNamesList.parent().name();//this is the problem one
			
			//logger.log("varName: "+VarName);
			//logger.log (attNamesList+ "---"+attNamesList[i].name());
			
			
			for (var i:int=0; i<attNamesList.length(); i++) {
				var tag:String=attNamesList[i].name();// id and color
				var tagValue:String=attNamesList[i];
				//logger.log("VARIABLES VarName -"+VarName+"-; tag -"+tag+"-; value -"+tagValue+"-");
				if (typeof ExptWideSpecs[subType][String(attNamesList[i].name())]!="undefined") {
					try {
						logger.log("Global experiment property: "+VarName+"."+tag+" = "+tagValue);
						
						var a:Array=new Array  ;
						var tempString:String=tag;
						a=tempString.split('.');
						
						switch (a.length) {
							case 0 :
								logger.log("PROBLEM----------no variables given to set properties with-----------------!!!!!!!");
								break;
							case 1 :
								ExptWideSpecs[subType][a[0]]=returnType(typeof ExptWideSpecs[subType][a[0]],tagValue);
								break;
						}
						
						//ExptWideSpecs[VarName][tag]=returnType(typeof ExptWideSpecs[VarName][tag],tagValue);
					} catch (error:Error) {
						logger.log("PROBLEM----------'"+tag+"' is not a known property of '"+VarName+"'-----------------!!!!!!!");
					}
				} else {
					logger.log("PROBLEM----------'"+tag+"' property does not exist unfortunately-----------------!!!!!!!");
				}
			}
			
			
			//ExptWideSpecs.textInput.setTextFormat(ExptWideSpecs.myTextFormat);
			
		}
		
		static private function returnType(type:String,value:*):* {
			var returnType:*;
			switch (type) {
				case "string" :
					returnType=new String  ;
					returnType=String(value);
					break;
				case "int" :
					returnType=new int  ;
					returnType=int(value);
					break;
				case "number" :
					returnType=new Number  ;
					returnType=Number(value);
					break;
				case "boolean" :
					returnType=new Boolean  ;
					if (value=="true"){
						returnType=true;
					}
					else {value=false;}
					
					break;
				case "uint" :
					returnType=new uint  ;
					returnType=uint(value);
					break;
				case "array" :
					returnType=new Array  ;
					returnType=value as Array;
					break;
				default :
					logger.log("incorrect variable type-----------------!!!!!!!");
			}
			return returnType;
		}
		
		
		
	}
}