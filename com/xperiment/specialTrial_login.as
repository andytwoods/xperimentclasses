﻿package com.xperiment{
	import com.adventuresInActionscript.utils.SuperTimer;
	import com.adventuresInActionscript.graphics.SuperStage;
	import fl.controls.Button;
	import fl.controls.Slider;
	import fl.controls.SliderDirection;
	import flash.text.TextFieldAutoSize;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.display.*;
	import flash.events.*;
	import flash.text.*;
	import com.xperiment.OnScreenBoss;
	import flash.display.MovieClip;
	import flash.display.Loader;
	import flash.net.URLRequest;
	import flash.geom.*;
	import com.xperiment.db_dataGetter;
	import com.xperiment.trial.Trial;


	public class specialTrial_login extends Trial {
		
	var deleteThisVariable:String;
    var database:db_dataGetter;

		override public function setup(theSta:Stage,genAttrib:XMLList,specAttrib:XMLList) {
		super.setup(theSta,genAttrib,specAttrib);

			//database code
			var tempDBname:String;
			var tempDBColumns:Array = new Array();
			if (genAttrib.database.@databaseName!=null) {
				tempDBname=genAttrib.database.@databaseName;
			}

			for (var i:uint = 0; i < genAttrib.database.ColumnVariable.length(); i++) {
				tempDBColumns.push(genAttrib.database.ColumnVariable[i].text());
			}
			//database=new db_dataGetter(tempDBname,tempDBColumns);
			//database.specificObjectToReturn("ExptID","=","AW");
			
			//database.doDBjobs();
				

			var tempArray:Array=database.giveData();
			if (tempArray[0] != null){
			deleteThisVariable=tempArray[0];
			}
			else{
			deleteThisVariable="no value..";
			}
			trace ("......gfdd.."+deleteThisVariable);
			

		}
		////////////////////////////////////////////////////////////////////////
		//TRIAL TYPE SPECIFIC VARIABLES AND FUNCTIONS
		////////////////////////////////////////////////////////////////////////


		/////////////////////////////
		//BOSS VARIABLES
		/////////////////////////////

		var Stim:SuperStage=new SuperStage  ;
		var exptButtons:Array=new Array  ;

		/////////////////////////////
		//BOSS FUNCTIONS
		/////////////////////////////
		override function trialEvents():void {//where trial type specific stuff is specified.
			super.OnScreenElements.exptButtons.push(super.makeButton("continue",super.generalAttributes.returnStageWidth/2,super.generalAttributes.returnStageHeight/1.5));

			//super.CurrentDisplay.addElement(addText(instructions, 0x111111, 12, "CENTER",super.returnStageWidth/2,(1)*super.returnStageHeight/5));


			for (var i:uint=0; i<super.generalAttributes.inputBox.length(); i++) {
				super.generalAttributes.CurrentDisplay.addElement(addTextField(super.generalAttributes.inputBox[i],super.generalAttributes.OnScreenElements.returnStageWidth/2,1*super.OnScreenElements.returnStageHeight/5));
			}


			super.OnScreenElements.CurrentDisplay.addElement(exptButtons[0]);
			super.OnScreenElements.CurrentDisplay.runTimedEvents();
		}



		override function endOfTrial():void {
			trace(super.trialData.Slider1+" "+super.trialData.Slider1);
			Stim=null;
			super.endOfTrial();
		}


		function addTextField(textinfo:XML,locX:int,locY:int):TextField {

			// arr_cue_points[cueObj.cueTitle] = cueObj;

		
			super.OnScreenElements.myTextFormat=new TextFormat  ;
			super.OnScreenElements.textInput=new TextField  ;
			super.OnScreenElements.textInfo=new TextField  ;


			super.OnScreenElements.myTextFormat.color=0x000000;
			super.OnScreenElements.textInput.borderColor=0xFFFFFF;
			super.OnScreenElements.textInput.background=true;

			super.OnScreenElements.myTextFormat.size=12;
			//textInput.type = TextFieldType.DYNAMIC;
			//textInput.width = textInputSize;
			super.OnScreenElements.textInput.type=TextFieldType.INPUT;
			super.OnScreenElements.textInput.border=true;
			super.OnScreenElements.textInput.x=locX;
			super.OnScreenElements.textInput.y=locY;
			super.OnScreenElements.textInput.text=deleteThisVariable;
			super.OnScreenElements.textInput.selectable=true;


			//var tempXMLObjectPropertyAssigner:XMLObjectPropertyAssigner=new XMLObjectPropertyAssigner(textinfo,super.OnScreenElements);
			//super.OnScreenElements=tempXMLObjectPropertyAssigner.giveMeData();
			//tempXMLObjectPropertyAssigner=null;

			super.OnScreenElements.myTextFormat.align=TextFormatAlign.RIGHT;
			super.OnScreenElements.textInput.autoSize=TextFieldAutoSize.RIGHT;

			return super.OnScreenElements.textInput;
		}

		/////////////////////////////
		//SUB FUNCTIONS
		/////////////////////////////

	}


}