package com.xperiment.runner {
	import com.Logger.Logger;
	import com.greensock.TimelineLite;
	import com.greensock.TimelineMax;
	import com.greensock.TweenAlign;
	import com.greensock.TweenMax;
	import com.greensock.easing.Bounce;
	import com.xperiment.trialOrderFunctions;
	import com.xperiment.ExptWideSpecs;
	import com.xperiment.stimuli.primitives.myArduino;
	import com.xperiment.trial.TrialPC;
	import com.xperiment.trial.Trial_betweenSJsSortOut;
	import com.xperiment.trial.overExperiment;
	
	import flash.desktop.NativeApplication;
	import flash.display.*;
	import flash.display.StageDisplayState;
	import flash.display.StageScaleMode;
	import flash.events.*;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.geom.Rectangle;
	import flash.system.Capabilities;
	import flash.system.System;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.utils.Timer;
	import flash.utils.getDefinitionByName;
	
	
	
	public class runnerPC extends runnerANDROID {
		
		private var _dummyTrialAndroid:TrialPC;
		private var message:TextField;
				
	
		override public function resizeListeners():void{
			/*
			The below is quite messy.
			As this is for big computers and as its important that the WHOLE screen is filled with no border or menu, 'fullScreenInteractive' is used.
			Firstly, changes from fullscreen are listened for.
			*/
			
			//if runningTrial does not exist, dont add the resize message (means it is the last trial);
			if(!theStage.hasEventListener(FullScreenEvent.FULL_SCREEN))theStage.addEventListener(FullScreenEvent.FULL_SCREEN, fullscreenF, false, 0, true);
			/*
			Upon change, if the change leads to the loss of full screen, a timer is started.  When the timer stops, its listeners are removed and then 'escape' is listened for.
			NB that a timer has to be used here as otherwise the 'up-press' of the escape key causes havoc (the 'FullScreenEvent.FULL_SCREEN' listener stupidly activates to this..., this
			timer stops that happening...
			NBB I tried using preventDefault() for the Escape key but couldn't get it to work.
			*/
			function keyboardF(k:KeyboardEvent):void
			{
				theStage.removeEventListener(k.type,keyboardF);
				if(k.keyCode==27)fullScreen()
			}
			/*
			Once the timer finishes an 'escape key' listener is started. Upon activation, the screen is made full size.
			*/	
			function fullscreenF(e:FullScreenEvent):void{

				if(theStage.stage.displayState!="fullScreenInteractive"){
					if(ExptWideSpecs.ExptWideSpecs.computer.fullScreenMessage)smallScreenMessage();
					var timer:Timer = new Timer(200,1);
					timer.addEventListener(TimerEvent.TIMER_COMPLETE,	
						function (t:TimerEvent):void{
							t.currentTarget.removeEventListener(t.type,arguments.callee);
							theStage.addEventListener(KeyboardEvent.KEY_UP,keyboardF,false,0,true);
						}	
						,false,0,true);	
					timer.start();
				}
			}
					
			/*
			It is possible that Xperiment looses focus for some reason (perhaps the person has 2 monitors...).  The below sorts out a focusing issue upon refocus.
			*/
			if(!theStage.stage.nativeWindow.hasEventListener(Event.ACTIVATE))theStage.stage.nativeWindow.addEventListener(Event.ACTIVATE,
				function(e:Event):void{
					theStage.focus = theStage; 
				}
				,false,0,false);
				
			function smallScreenMessage():void{
				
				message = new TextField;
				message.background=true;
				message.backgroundColor=0;
				message.textColor=0xffffff;
				message.text="Press 'Escape' to go back into fullscreen mode.\nThis window for the Experimenter.";
				var format:TextFormat = new TextFormat();
				format.size = 27;
				message.setTextFormat(format);
				message.autoSize=TextFieldAutoSize.CENTER;
				message.alpha=0;
				message.multiline=true;
				message.x=-message.width;
				message.y=theStage.stage.nativeWindow.height*.5-message.height*.5;
				theStage.addChild(message);
				TweenMax.to(message, .8, {alpha:1,x:theStage.stage.nativeWindow.width-message.width*1.1, ease:Bounce.easeOut});
				TweenMax.to(message, 1.5, {delay:4, alpha:0,y:-4*message.height,ease:Bounce.easeIn, onComplete:function():void{message=null;format=null;}});
		}
		}
		
		override public function setFullScreen():void
		{
		
			switch((ExptWideSpecs.ExptWideSpecs.computer.align as String).toLowerCase()){
				case "top":stage.align=StageAlign.TOP;break;
				case "bottom":stage.align=StageAlign.BOTTOM;break;
				case "left":stage.align=StageAlign.LEFT;break;
				case "right":stage.align=StageAlign.RIGHT;break;
				case "topRight":stage.align=StageAlign.TOP_RIGHT;break;
				case "topLeft":stage.align=StageAlign.TOP_LEFT;break;
				case "bottomRight":stage.align=StageAlign.BOTTOM_RIGHT;break;
				case "bottomLeft":stage.align=StageAlign.BOTTOM_LEFT;break;
				case "middle":
				case "centre":
				case "center":
				default: stage.align="C"; break;
			}			

			if(ExptWideSpecs.ExptWideSpecs.computer.alwaysInFront as Boolean)theStage.stage.nativeWindow.alwaysInFront=true;
			fullScreen()
		}
		

		private function fullScreen():void{
			if(stage)stage.displayState = StageDisplayState.FULL_SCREEN_INTERACTIVE;
		}
		
		override public function kill():void{
			fullScreen();
			if(myArduino.instanceExists()){
				var arduino:myArduino = myArduino.getInstance();
				arduino.kill();
				arduino=null;
			}
			super.kill();
		}
	}
}