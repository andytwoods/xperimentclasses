﻿package com.xperiment.runner {
	import com.Logger.Logger;
	import com.bit101.components.Style;
	import com.bulkloader.preloadFilesFromWeb;
	import com.greensock.TweenMax;
	import com.mTurk.mTurk;
	import com.xperiment.BehavLogicAction.PropValDict;
	import com.xperiment.ExptWideSpecs;
	import com.xperiment.ProcessScript;
	import com.xperiment.Results.Results;
	import com.xperiment.XMLstuff.importExptScript;
	import com.xperiment.XMLstuff.saveXML;
	import com.xperiment.events.GotoTrialEvent;
	import com.xperiment.events.runGlobalFunctionsEvent;
	import com.xperiment.stimuli.primitives.WindowPrimitive;
	import com.xperiment.trial.Trial;
	import com.xperiment.trial.Trial_betweenSJsSortOut;
	import com.xperiment.trial.overExperiment;
	import com.xperiment.trialOrderFunctions;
	
	import flash.display.*;
	import flash.display.StageScaleMode;
	import flash.events.*;
	import flash.events.TimerEvent;
	import flash.system.Capabilities;
	import flash.text.TextField;
	import flash.utils.Timer;
	
	
	public class runner extends Sprite {
		
		//- PUBLIC & INTERNAL VARIABLES ---------------------------------------------------------------------------
		public var trialList:Array=new Array  ;
		private var trialListUnitiatedKeptInMemory:Array=new Array  ;
		public var trialOrder:Array=new Array  ;
		private var trialOrderVariableDirection:Array=new Array  ;//this might be dead,,, remove?
		private var haveAfterWhichTrial:Array=new Array  ;
		public var currentTrial:Number=0;//negative values = instructions etc.
		public var trialProtocolList:XML;
		public var theStage:Stage;
		private var arrayOfSplices:Array=new Array  ;
		public var scrWidth:uint;
		public var scrHeight:uint;
		public var exptResults:Results;
		public var betweenSJsID:String="";
		private var betweenSJsTrial:Trial;
		private var propValDict:PropValDict;
		
		private var trialListOrder:Array=new Array  ;
		public var myScript:importExptScript;
		private var trialIJvalues:Array=new Array  ;
		private var _dummyVarToAddBoxToAppCompilation1:Trial;
		
		public var nerdStuff:String = "web-browser";
		public var acrossExperimentTrial:overExperiment=new overExperiment  ;///future: consider making this optional via the setup parameters (prob eats some memory)
		public var preloader:preloadFilesFromWeb;
		public var logger:Logger=Logger.getInstance(); 
		
		public var runningTrial:Trial;
		
		private 	var mechTurk:mTurk;
		//- CONSTRUCTOR -------------------------------------------------------------------------------------------
		
		public function initialise(sta:Stage,myXMLScript:XML=null):void {
			theStage=sta;
			
			//switch on after upgrading to flashbuilder >4.6
			//if(getQualifiedClassName(this) =="com.xperiment.runner::runner") sta.addEventListener(MouseEvent.RIGHT_CLICK,rightClickMenuF);
			
			logger.start(false,theStage);
			exptResults = Results.getInstance();
			exptResults.setup();
			Style.embedFonts=false;
			
			if(!Trial.theStage){//needed in case the experiment is restarted and is in fullscreen mode (else goes bananas);
				Trial.returnStageHeight=theStage.stageHeight;
				Trial.returnStageWidth=theStage.stageWidth;
			}
			
			
			Trial.overEntireExperiment=acrossExperimentTrial;
			Trial.theStage=theStage;
			trialProtocolList=myXMLScript	
			initDeviceSpecificStuff();
		}	
		
		
		
		//private function rightClickMenuF(e:MouseEvent):void{
		
		//}
		
		public function initDeviceSpecificStuff():void{
			theStage.scaleMode=StageScaleMode.EXACT_FIT;
			nerdStuff="Web appID:" + " OS:" + Capabilities.os + " resX:" + Capabilities.screenResolutionX + " resY:" + Capabilities.screenResolutionY + " DPI:" + Capabilities.screenDPI + " CPUarch:" + Capabilities.cpuArchitecture + " version:"+Capabilities.version;
		}
		
		/*public function initStarling():void
		{
		this.myStarling = MyStarling.getInstance();
		myStarling.setup(theStage);
		}*/
		
		public function scriptListeners(load:Boolean):void{
			if(load){
				myScript.addEventListener("dataLoaded",continueStudyAfterLoadingPause,false,0,true);
				myScript.addEventListener("dataNotLoaded",dataNotLoaded,false,0,true);
			}
			else{
				myScript.removeEventListener("dataLoaded",continueStudyAfterLoadingPause);
				myScript.removeEventListener("dataNotLoaded",dataNotLoaded);
			}
		}
		
		protected function dataNotLoaded(e:Event):void
		{
			scriptListeners(false);
			var txt:TextField=new TextField;
			txt.text="Afraid your Xperiment Script cannot be loaded.  Please check it for errors.";
			txt.wordWrap=true;
			txt.scaleX=3;
			txt.scaleY=3;
			theStage.addChild(txt);
		}
		
		public function giveScript(scr:XML):void {
			
			trialProtocolList=scr;
			runExpt();
		}
		
				
		public function getScript():void{
			trialProtocolList=myScript.giveMeData();
		}
		
		public function continueStudyAfterLoadingPause(e:Event):void {
			scriptListeners(false);
			runExpt();
		}
		
		public function runExpt():void{
			theStage.addEventListener(GotoTrialEvent.RUNNER_PING_FROM_TRIAL,nextTrial,false,0,false);
			theStage.addEventListener(runGlobalFunctionsEvent.COMMAND,runCommand);			
			if(!trialProtocolList)getScript();
			trialProtocolList=ProcessScript.process(trialProtocolList,logger);
			switch(trialProtocolList.name().toString().substr(0,4).toLowerCase()){
				
				case("mult"):
					sortOutMultiExperiment();
					break;
				case("expt"):
				default:
					checkForLoadableObjects();
					
					
			}
		}
		
		protected function runCommand(e:runGlobalFunctionsEvent):void
		{
			switch(e.theFunction.toLowerCase()){
				case "language":
					trialProtocolList=ProcessScript.replaceAllInstancesOfAttribWithSuffixedAttrib(e.values as String,trialProtocolList,logger);
					break;
				case "savedata":
					saveDataProcedure();
					break;
			}
			
		}
		
		public var TrialBS:Trial_betweenSJsSortOut;
		private var composeTrialArr:Array;
		public function sortOutMultiExperiment():void{
			
			var info:XMLList=trialProtocolList.info;
			delete trialProtocolList.Info;
			
			if(trialProtocolList.@random.toString()=="true"){
				trialProtocolList=ProcessScript.BetweenSJsprocessRND(trialProtocolList,logger);
			}
				
			else if(info.toString().length>0){//afraid no imported graphics allowed for this 
				TrialBS = new Trial_betweenSJsSortOut();
				theStage.addEventListener("betweenSJsTrialEvent_Runner", betweenSJsTrialEvent);
				var tempObjects:XMLList=info;
				tempObjects.perSize=ExptWideSpecs.ExptWideSpecs.screen.percentageFromWidthOrHeightOrBoth as String;
				TrialBS.introSetup(tempObjects,trialProtocolList);
				TrialBS.prepare(0,new XML);
				
			}
		}
		
		public function wipeBetweenSJsTrial():void{
			theStage.removeEventListener("betweenSJsTrialEvent_Runner", betweenSJsTrialEvent);
			//TrialBS.generalCleanUp()
			TrialBS=null;
		}
		
		public function betweenSJsTrialEvent(e:Event):void{
			var response:String=TrialBS.introInfo();
			wipeBetweenSJsTrial();
			var parameter:String=response.split("=")[1];
			
			switch(response.split("=")[0]){
				case "runCondition":
					//betweenSJsTrial GETDATA
					trialProtocolList=ProcessScript.BetweenSJsprocess(trialProtocolList,parameter,logger);
					if(trialProtocolList!=new XML)checkForLoadableObjects();
					break;
			}
			
		}
		
		public function checkForLoadableObjects():void{
			if(trialProtocolList.SETUP.loadFilesInAdvance.toString()=="false"){runningExptNow()}
			else{	
				
				//have not yet specified ExptWideSpecs so necessary to manually check if value first exists, else use defaults from ExptWideSpecs
				var stimFolder:String=trialProtocolList.SETUP.fileInformation.@stimuliFolder;
				if(stimFolder=="")stimFolder=ExptWideSpecs.ExptWideSpecs.fileInformation.stimuliFolder;
				
		
				preloader = new preloadFilesFromWeb(trialProtocolList,stimFolder+"/");
				Trial.preloader=preloader;
				runningExptNow();
			}
			
			if("mTurk" in trialProtocolList.SETUP)mechTurk=new mTurk(theStage,logger);
		}
		
		
		public function runningExptNow():void{				
			var ensuresFinishedRunning:Boolean=specifyExperimentWideVariableDefaults();
			ListTools.extract(trialProtocolList,exptResults);
			
			myScript=null;
			
			
			
			/////////////////////////////OVER EXPERIMENT STUFF//////////////////////
			////////////////////////////////////////////////////////////////////////
			if(trialProtocolList.hasOwnProperty("overExperiment")){
				var tempObjects:XML=trialProtocolList.overExperiment[0];
				acrossExperimentTrial=new overExperiment();
				theStage.addChild(acrossExperimentTrial);
				acrossExperimentTrial.prepare(0,trialProtocolList.SETUP.fileInformation.@stimuliFolder);
				acrossExperimentTrial.setupForOverExperiment(theStage,tempObjects);
				acrossExperimentTrial.run();
			}
			////////////////////////////////////////////////////////////////////////
			////////////////////////////////////////////////////////////////////////

			trialOrder = trialOrderFunctions.computeOrder(trialProtocolList,composeTrial);	
			
			logger.log("------------------commence experiment-------------------------");
			
			runningTrial=trialList[trialOrder[currentTrial]];
			commenceWithTrial();//starts the trial sequence 
		}
		
		private function composeTrial(blockNum:int,duplicateTrialNumber:int,tNumber:uint):void {
			var info:Object = new Object;
			info.order=						tNumber;
			info.TRIAL_ID=					blockNum;
			info.trialBlockPositionStart=	duplicateTrialNumber;	
			var tempTrial:Trial=new Trial();
			tempTrial.setup(new XMLList,info);
			trialList.push(tempTrial);
		}
		
		public function specifyExperimentWideVariableDefaults():Boolean {	
			ExptWideSpecs.setup(trialProtocolList.SETUP);
			if(ExptWideSpecs.ExptWideSpecs.computer.fullScreen as Boolean)setFullScreen();
			setBackgroundColour();
			setOrientation(ExptWideSpecs.ExptWideSpecs.screen.orientation);
			resizeListeners();
			
			var tempArr:Array=(ExptWideSpecs.ExptWideSpecs.spliceTrials.splices as String).split(";");
			
			for (var i:uint=0; i<tempArr.length; i++) {
				arrayOfSplices.push((tempArr[i] as String).split(","));
			}
			
			return true;
		}
		
		public function setFullScreen():void
		{
			//empty here, but not so in runnerPC;
		}
		
		public function resizeListeners():void{
			theStage.addEventListener(Event.RESIZE,
				function(e:Event):void{
					setBackgroundColour();
				},false,0,true);
		}
		
		
		public function commenceWithTrial():void {
			if(!runningTrial){
				logger.stageErrorMessage("!End of the study. You should provide an 'end of study' screen and not rely on this message!");
			}
			else{
				
				if (runningTrial.submitData) {
					if(acrossExperimentTrial)acrossExperimentTrial.generalCleanUp();
					if(String(this)=="[object runner]"){
						//trace("kill");
						kill();
					}
					else endProgram();
				}
				
				if (currentTrial<trialList.length-1) {
					logger.log("--------------------------------------------------------------");
					logger.log("running trial: "+currentTrial);
				}
				
				//trace("TrialNum:"+currentTrial,"currentTrial:"+trialOrder[currentTrial]);
				runningTrial.prepare(currentTrial,trialProtocolList.TRIAL[runningTrial.TRIAL_ID]);	
			}
		}
		
		
		public function nextTrial(e:GotoTrialEvent):void {
			
			
			if(runningTrial.ignoreData==false)exptResults.give(runningTrial.giveTrialData())
			var doContinue:Boolean=false;
			
			if(e.action=="nextTrial"){
				//trace("nextTrial",currentTrial);
				currentTrial++;
				doContinue=true;
			}	 
				
			else if(e.action=="prevTrial"){
				currentTrial--;
				doContinue=true;
			}
				
			else if(e.action!=""){
				for(var t:uint=0;t<trialList.length;t++){
					if((trialList[t] as Trial).trialLabel==e.action){
						currentTrial=t;
						doContinue=true;
						break;
					}
				}				
			}
			
			if(doContinue){
				//trace(trialOrder[currentTrial],222);
				//trace(trialOrder);
				runningTrial=trialList[trialOrder[currentTrial]];
				//trace("currentTrial:",currentTrial,trialOrder,runningTrial.duplicateTrialNumber);
				commenceWithTrial();
			}
			else{
				var problemTimer:Timer=new Timer(10000,1);
				problemTimer.addEventListener(TimerEvent.TIMER,function(e:TimerEvent):void{
					e.currentTarget.removeEventListener(e.type,arguments.callee);
					currentTrial=0;
					runningTrial=trialList[trialOrder[currentTrial]];
					commenceWithTrial();
					logger.stageErrorMessage("");
					setBackgroundColour();
				}
					,false,0,true);
			}
		}
		
		
		
		
		public function saveDataProcedure():void{
			extractStudyData();
			exptResults.sortFinalResults(betweenSJsID,nerdStuff);
			var dataSave:saveXML = new saveXML();
			dataSave.saveProcedure(exptResults.finalResults,successQueryF);
		}
		
		public function successQueryF(header:String,str:String):void{
			var grayBG:Sprite = new Sprite;
			grayBG.graphics.beginFill(0,.7);
			grayBG.graphics.drawRect(0,0,theStage.stageWidth,theStage.stageHeight);
			theStage.addChild(grayBG);
			
			var window:WindowPrimitive = new WindowPrimitive({title:"",width:theStage.stageWidth*.9,height:theStage.stageHeight*.9})
			window.addMessage(header,str);	
			//theStage.addChild(window);
			
			window.myWindow.addEventListener(Event.CLOSE, function(e:Event):void{
				e.currentTarget.removeEventListener(Event.CLOSE,arguments.callee);
				theStage.removeChild(grayBG);
				grayBG=null;
				theStage.removeChild(window);
				window.kill();
				window=null;
			});
			
			TweenMax.fromTo(window,1,{onStart:function():void{theStage.addChild(window)},x:-theStage.stageWidth, y:-theStage.stageHeight, alpha:0},{x:theStage.stageWidth*.05,y:theStage.stageHeight*.05,alpha:1});
		}
		
		public function ssuccessQueryF(header:String,str:String):void{
			
			var window:WindowPrimitive;
			window = new WindowPrimitive({title:"",width:theStage.stageWidth-5,height:theStage.stageHeight-5});
			window.addMessage(header,str);				
			theStage.addChildAt(window,theStage.numChildren-1);
			//TweenMax.fromTo(window,1,{onStart:function():void{theStage.addChild(window)},x:-window.width, y:-window.height, alpha:0},{x:theStage.stageWidth*.5-window.width*.5,y:theStage.stageHeight*.5-window.height*.5,alpha:1});
		}
		
		public function destroy():void{
			runningTrial.generalCleanUp(true);
			
		}
		
		public function endProgram():void {}
		
		public function kill():void
		{
			
			//exptResults.kill();
			//exptResults=null;
			theStage.removeEventListener(GotoTrialEvent.RUNNER_PING_FROM_TRIAL,nextTrial);
			theStage.removeEventListener(runGlobalFunctionsEvent.COMMAND,runCommand);
			if(theStage.contains(acrossExperimentTrial))theStage.removeChild(acrossExperimentTrial);
		}
		
		public function extractStudyData():void {
			if(acrossExperimentTrial && acrossExperimentTrial.ignoreData==false)exptResults.give(acrossExperimentTrial.trialData);		
		}	
		
		
		private function combineAssociativeArraysDestructively(arr1:Array,arr2:Array):Array
		{
			for (var nam:String in arr1) {
				//logger.log("arrr:"+nam);
				if (arr2[nam]!=undefined) {
					arr1[nam]=arr2[nam];
				}
			}
			for (nam in arr2) {
				if (arr1[nam]==undefined) {
					arr1[nam]=arr2[nam];
				}
			}
			
			return arr1;
		}
		
		
		
		////////////////////fundamental hidden functions//////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////////
		
		private function BlockRandomise(x:Array):Array {
			var CutinHalfA:Array=new Array(x.length/2);
			var CutinHalfB:Array=new Array(x.length/2);
			for (var i:int; i<CutinHalfA.length; i++) {
				CutinHalfA[i]=x[i];
				CutinHalfB[i]=x[i+CutinHalfA.length];
			}
			
			CutinHalfA=shuffleArray(CutinHalfA);
			CutinHalfB=shuffleArray(CutinHalfB);
			
			if (randRange(0,1)<500) {
				x=CutinHalfA.concat(CutinHalfB);
			} else {
				x=CutinHalfB.concat(CutinHalfA);
			}
			return x;
		}
		
		private function shuffleArray(a:Array):Array {
			var a2:Array=[];
			while (a.length>0) {
				a2.push(a.splice(Math.round(Math.random()*a.length-1),1)[0]);
			}
			return a2;
		}
		
		
		private function randRange(start:Number,end:Number):Number {
			return Math.floor(start+Math.random()*end-start);
		}
		
		
		
		
		
		//- background colour -------------------------------------------------------------------------------------------
		
		//private var myStarling:Object;
		
		public function setBackgroundColour():void {		
			stage.color=ExptWideSpecs.ExptWideSpecs.screen.BGcolour as Number;
		}
		
		public function setOrientation(orien:String):void {
			switch(orien){
				case "vertical":
					//theStage.align = StageAlign.TOP_LEFT;
					break;
				case "horizontal":
					//theStage.align = StageAlign.LEFT;
					break;
				
			}
		}
	}
}

/*OS Operating system	Value
Windows 7	"Windows 7"
Windows Vista	"Windows Vista"
Windows Server 2008 R2	"Windows Server 2008 R2"
Windows Server 2008	"Windows Server 2008"
Windows Home Server	"Windows Home Server"
Windows Server 2003 R2	"Windows Server 2003 R2"
Windows Server 2003	"Windows Server 2003"
Windows XP 64	"Windows Server XP 64"
Windows XP	"Windows XP"
Windows 98	"Windows 98"
Windows 95	"Windows 95"
Windows NT	"Windows NT"
Windows 2000	"Windows 2000"
Windows ME	"Windows ME"
Windows CE	"Windows CE"
Windows SmartPhone	"Windows SmartPhone"
Windows PocketPC	"Windows PocketPC"
Windows CEPC	"Windows CEPC"
Windows Mobile	"Windows Mobile"
Mac OS	"Mac OS X.Y.Z" (where X.Y.Z is the version number, for example: "Mac OS 10.5.2")
Linux	"Linux" (Flash Player attaches the Linux version, such as "Linux 2.6.15-1.2054_FC5smp"
iPhone OS 4.1	"iPhone3,1"

version
WIN 9,0,0,0  // Flash Player 9 for Windows
MAC 7,0,25,0   // Flash Player 7 for Macintosh
LNX 9,0,115,0  // Flash Player 9 for Linux
AND 10,2,150,0 // Flash Player 10 for Android
*/

//Czech cs; Danish da; Dutch nl; English en; Finnish fi; French fr; German de; Hungarian hu; Italian it; Japanese ja; Korean ko; Norwegian no; Other/unknown xu; Polish pl; Portuguese pt; Russian ru; Simplified Chinese zh-CN; Spanish es; Swedish sv; Traditional Chinese zh-TW; Turkish tr

