﻿package com.xperiment{
	import flash.display.*;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	import flash.text.TextFormat;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormatAlign;
	import flash.events.*;
	import flash.utils.getDefinitionByName;
	import flash.display.StageDisplayState;
	import flash.display.StageScaleMode;
	import com.trialOrderFunctions;
	import com.xperiment.trial.overExperiment;
	import flash.utils.Timer;
	import flash.events.TimerEvent;
	import flash.system.Capabilities;
	import com.xperiment.XMLstuff.importExptScript;
	import com.codeRecycleFunctions;
	import com.Logger.Logger;
	import com.xperiment.trial.Trial;
	import com.xperiment.trial.overExperiment;
	import com.xperiment.XMLstuff.saveXML;
	import com.xperiment.XMLstuff.importExptScript;
	
	
	public class runner extends Sprite {

		//- PUBLIC & INTERNAL VARIABLES ---------------------------------------------------------------------------
		var trialList:Array=new Array  ;
		var trialListUnitiatedKeptInMemory:Array=new Array  ;
		var trialOrder:Array=new Array  ;
		var trialOrderVariableDirection:Array=new Array  ;//this might be dead,,, remove?
		var haveAfterWhichTrial:Array=new Array  ;
		var ongoingExperimentResults:XMLList=new XMLList  ;
		var currentTrial:Number=0;//negative values = instructions etc.
		var trialProtocolList:XML=<exptData />;
		var trialToSubmitDataOn:uint=10000;
		public var theStage:Stage;
		private var arrayOfSplices:Array=new Array  ;
		public var scrWidth:uint;
		public var scrHeight:uint;
		private var blockListForAllTrials:Array=new Array;


		var trialListOrder:Array=new Array  ;
		var tempData:importExptScript;
		public var ExptWideSpecs:Array=new Array  ;
		private var trialIJvalues:Array=new Array  ;
		private var _dummyVarToAddBoxToAppCompilation1:Trial;
		
		public var nerdStuff:String = "web-browser";

		
		public var storedVariables:Array=new Array  ;


		//- CONSTRUCTOR -------------------------------------------------------------------------------------------

		public function initialise(sta:Stage) {
			var logger = new Logger(true); 
			theStage=sta;
			theStage.scaleMode=StageScaleMode.SHOW_ALL;
			
			scrWidth=theStage.stageWidth;
			scrHeight=theStage.stageHeight;
			
			tempData=new importExptScript("myScript.xml");
			tempData.addEventListener("dataLoaded",continueStudyAfterLoadingPause,false,0,true);
		}


		//- background colour -------------------------------------------------------------------------------------------

		var square:Sprite=new Sprite  ;

		public function changeBGcolourSize(event:FullScreenEvent) {
			theStage.removeChild(square);
			var colour:Number=ExptWideSpecs.screen.BGcolour;
			square.graphics.lineStyle(3,colour);
			square.graphics.beginFill(colour);
			square.graphics.drawRect(0,0,theStage.stageWidth,theStage.stageHeight);
			square.graphics.endFill();
			square.x=theStage.stageWidth-theStage.fullScreenWidth/2;
			square.y=theStage.stageHeight-theStage.fullScreenHeight/2;
			theStage.addChildAt(square,0);//future job: will need to remove this at some distant future point.
		}

		public function setBackgroundColour(colour:Number) {
			square.graphics.lineStyle(3,colour);
			square.graphics.beginFill(colour);
			square.graphics.drawRect(0,0,theStage.stageWidth,theStage.stageHeight);
			square.graphics.endFill();
			square.x=0;
			square.y=0;
			theStage.addChild(square);//future job: will need to remove this at some distant future point.
		}
		
		public function setOrientation(orien:String) {
            
			switch(orien){
				case "vertical":
					//theStage.align = StageAlign.TOP_LEFT;
					break;
				case "horizontal":
					//theStage.align = StageAlign.LEFT;
					break;
			
			}
			
			
		}
		//-------------------------------------------------------------------------------------------
		public function returnTrialType():Trial{
			return new Trial();
		}
		

		private function composeTrial(i,j,tNum:uint,adhoc:Boolean,blockGroup:String,blockType:String,tWBP:uint) {

			var trialNumTag:uint=tNum;
			var tempObjects:XMLList=trialProtocolList.TRIAL[i].objects;
			////////////////////////////////////////changes code within generalAttributes, given specificAttributes
			
			tempObjects.trialName=trialProtocolList.TRIAL[i].@trialName;
			
			///for onPhone experiments. Where the files are stored;
			
			tempObjects.fileLocation="";
			if (String(trialProtocolList.storage).length!=0) {
			tempObjects.fileLocation=String(trialProtocolList.storage.explicitLocalDir);
			//tempObjects.fileLocation="//localDir_delete/";
			} 
			
			tempObjects.ITI=trialProtocolList.TRIAL[i].@ITI;
			tempObjects.order=tNum;
			tempObjects.trialBlockPositionStart=tWBP;
			tempObjects.duplicateTrialNumber=j;
			tempObjects.loadOnTheFly=trialProtocolList.TRIAL[i].@loadOnTheFly;
			//var tempClassReference:Class=getDefinitionByName(trialProtocolList.TRIAL[i].@TYPE) as Class;
			//var tempTrial:Trial=new tempClassReference  ;
			var tempTrial:Trial=returnTrialType();
			//var tempTrial:Trial = new tempClassReference() as String;
			tempTrial.setup(theStage,tempObjects,tNum,storedVariables,acrossExperimentTrial,scrWidth,scrHeight);

			storedVariables=combineAssociativeArraysDestructively(storedVariables,tempTrial.getTempStoredVars());//run at the end of each trial AND when trial is created


			if (blockType=="undefined"||blockType=="") {
				blockType="fixed";
			}

/*			if (blockGroup=="undefined"||blockGroup=="") {
				blockGroup="999";
				tempTrial.fixedLocation=uint(trialProtocolList.TRIAL[i].putAfterWhichTrial.text());
			}*/

			var tempIJ:Array=new Array  ;
			tempIJ.i=blockGroup;
			tempIJ.j=j;
			tempIJ.blockGroup=blockGroup;
			tempIJ.blockType=blockType;
			trialIJvalues.push(tempIJ);

			trialList.push(tempTrial);

			if (adhoc==false) {
				

				var tempString:String=trialProtocolList.TRIAL[i].blockGroup.attribute("afterWhichTrial");
					
					var tempArray:Array=new Array  ;
					tempArray.trialNumber=i;
					tempArray.afterTrial="";
					
					if (tempString!=""){
						
						trialOrder.push(trialList.length-1);
						trialListOrder.push(blockGroup);
						
						if( trialProtocolList.TRIAL[i].blockGroup.attribute("trialOrder")=="random"){
							tempString=shuffleArray(tempString.split(";")).join(";");
						}
						tempArray.afterTrial=tempString;
						haveAfterWhichTrial.push(tempArray);
					}
					
				
					
			}

			if (trialProtocolList.TRIAL[i].@submitData=="true") {
				trialToSubmitDataOn=trialList.length;
			}


		}
		

		var acrossExperimentTrial:overExperiment=new overExperiment  ;///future: consider making this optional via the setup parameters (prob eats some memory)

		public function getScript(){
			trialProtocolList=tempData.giveMeData();
		}

		public function continueStudyAfterLoadingPause(event:Event) {
			runExpt();
		}

		
		public function runExpt(){
			theStage.addEventListener("saveDataEndStudy",saveDataEndStudy,true,0,true);
			getScript();
			trialProtocolList=ProcessScript.process(trialProtocolList);
			
			var ensuresFinishedRunning:Boolean=specifyExperimentWideVariableDefaults();
			var trialOrderSpecifier:Array=new Array  ;
			tempData=null;
			var newTrialOrder:Array=new Array  ;

			for (var i:uint=0; i<trialProtocolList.overExperiment.length(); i++) {
				var tempObjects:XMLList=trialProtocolList.overExperiment[i].generalAttributes;
				acrossExperimentTrial=new overExperiment();
				acrossExperimentTrial.setupForOverExperiment(theStage,tempObjects,storedVariables);
				acrossExperimentTrial.run(counter);
				storedVariables=combineAssociativeArraysDestructively(storedVariables,acrossExperimentTrial.getTempStoredVars());//run at the end of each trial AND when trial is created
				theStage.addChild(acrossExperimentTrial);
			}



			var counter:uint=0;
			var blockCounter:uint=0;
			var currentBlock:uint = 0;
			var blockStartVal:uint = 0;
			var NumTrials:uint= 0;
			
			var adhoc:Boolean=false;
			for (i=0; i<trialProtocolList.TRIAL.length(); i++) {
				NumTrials=uint(trialProtocolList.TRIAL[i].numberTrials.text());
				
				if (currentBlock<i){
					currentBlock = i;
					blockStartVal = counter;
					blockCounter++;
				}
				
				trace(trialProtocolList.TRIAL[i].@TYPE+" x "+NumTrials);
				var tempType:String=trialProtocolList.TRIAL[i].blockGroup.@trialOrder;
				var tempBlockGroup:String=trialProtocolList.TRIAL[i].blockGroup.text();

				if (tempBlockGroup=="fixed") {
					trialOrderSpecifier.push("fixed");
				} else if (tempType=="") {
					trialOrderSpecifier.push("random");
				} else {
					trialOrderSpecifier.push(tempType);
				}

				for (var j:uint=0; j<NumTrials; j++) {
					
					//trace("2222: "+trialOrder.indexOf(counter)+" "+trialOrder);
					//trace("hhhh: "+ " i: "+i+" j:"+j+ " counter: "+counter+ " blockStartVal: "+blockStartVal))
					//trace("hhhh: "+(blockStartVal+j));
					composeTrial(i,j,counter,adhoc,tempBlockGroup,tempType,blockStartVal);
					counter++;
				}
			}

			var blockArray:Array=new Array  ;
			for (var pp:uint=0; pp<trialIJvalues.length; pp++) {
				var tempStr:String=trialIJvalues[pp].blockGroup;
				var tempNam:String=trialIJvalues[pp].blockType;

				if (blockArray[tempStr]==undefined) {
					blockArray[tempStr]=new Array  ;
					blockArray[tempStr].push(tempNam);
				}
				blockArray[tempStr].push(pp);
				if (tempStr.substr(0,6)!="slotIn"){
				blockListForAllTrials.push(tempStr);
				}
			}

			trace("blockArray: "+blockArray);
			//trace("newTrialOrder: "+newTrialOrder);
			////////////////////////this bit creates a randomly ordered script for the trials)
			/////future job: include, in XML input, whether a trial is 'fixed' in order or not, and sort out shuffle.

			var tempTrialOrder=new Array(trialList.length);
			trialOrder=new Array  ;
			//var trialOrderSpecifierZeroRemoved:Array=new Array  ;

			for (i=0; i<tempTrialOrder.length; i++) {
				tempTrialOrder[i]=i;
			}

			//trailOrder contains a simple list of numbers that correspond to trial number.  
			//This used to determine which trial to run stored in trialList.

			var numberRandomTrialTypes:uint=blockArray.length;
			var ExptWideSpecs:Array=new Array  ;
			var shuffledTrials:Array=new Array  ;//might be best to call this 'nonShuffled Trials'.
			var arrayRandomTrials:Array=new Array  ;

			trace("initial trialOrder: "+trialOrder);
			for (i=0; i<blockArray.length; i++) {//
				if (blockArray[i]!=undefined) {
					var newTrials:Array=new Array  ;
					var tempBlockArr:Array=blockArray[i];
					var tempTrialType:String=tempBlockArr[0];
					tempBlockArr.shift();
					if (tempTrialType=="random") {
						newTrials=shuffleArray(tempBlockArr);
					} else if (tempTrialType=="fixed") {
						newTrials=tempBlockArr;
					} else if (tempTrialType.substring(0,23)=="repeatRandBlocksDifEnds") {
						newTrials=trialOrderFunctions.repeatRandBlocksDifEnds(tempBlockArr,extractVariables(tempTrialType));
					}
					trace("newTrials: "+newTrials);
					trialOrder=trialOrder.concat(newTrials);//ddd
					
				} 
			}
			trace("trialOrder after shuffle: "+trialOrder);
			////////////////////////////////////////////////////////////////////////////////////

			////////////////////////////////////////// this splices all non ordered trials into trialorder.
			var slotInWhere:Array=new Array;
			var positions:Array = new Array;
			var blockStartPosition:int = -1;
			for (i=0; i<trialListOrder.length; i++) {
				var myBlock:uint;
				if (trialListOrder[i].substring(0,6)=="slotIn") {
					
					myBlock=uint(trialListOrder[i].substring(7).split(")")[0]);
					if(!slotInWhere[myBlock]){
						positions=haveAfterWhichTrial[i].afterTrial.split(";");
						slotInWhere[myBlock]=positions;
						}
					for (j=0;j<blockListForAllTrials.length;j++){
						if (myBlock==blockListForAllTrials[j]){
							blockStartPosition=j;
							break;
						}
					}
					
					if(slotInWhere[myBlock].length==0) trace("PROBLEM: You have not provided enough trial positions for your 'fixed position in block' trials (in block "+myBlock+")");
					if(blockStartPosition==-1) trace("PROBLEM: You have specified a non-existing block to slot in your 'fixed position trials' into (you specified block "+myBlock+" but only these blocks exist ["+ codeRecycleFunctions.getUniqueValues(blockListForAllTrials)+"].");
					if(slotInWhere[myBlock].length!=0&& blockStartPosition!=-1){
						//trace("hhhhhhhhhhhhhh:"+slotInWhere[myBlock].shift());
						trialOrder.splice(uint(slotInWhere[myBlock].shift()+blockStartPosition),0,tempTrialOrder[i]);
					}

				}
			}


			for (i=0; i<arrayOfSplices.length; i++) {
				var SplicVars:Array=arrayOfSplices[i];
				var funct:Array=splitIntoArray(SplicVars[0]," ");
				if (funct[0]=="rand") {
					var randNum:Number=Math.random()*100;
					if (randNum>Number(funct[1])) {
						var tempArr:Array=trialOrder.splice(SplicVars[1],SplicVars[2]);
						trialOrder.splice(SplicVars[3],0,tempArr);
						//trace("splice [" + SplicVars+ "] was done as random number ("+(Math.round(randNum))+") exceeded your threshold ("+funct[1]+")");
					} else {
						//trace("splice [" + SplicVars+ "] was not done as random number ("+(Math.round(randNum))+") did not exceed your threshold ("+funct[1]+")");
					}

				}


				if (trialToSubmitDataOn==10000) {
					trialToSubmitDataOn=trialList.length-1;
				}

				//trace("trialOrder after splice: "+trialOrder);
			}

/*			var tempLoc:uint;
			for (i=0; i<trialList.length; i++) {
				tempLoc=trialList[i].fixedLocation;
				if (tempLoc!=999) {
					for (j=0; j<trialOrder.length; j++) {
						if (trialOrder[j]==i) {
							trialOrder.splice(j,1);
							trialOrder.splice(tempLoc,0,i);
						}
					}
				}
			}
*/
			////////////////////////////////////////////////////////////////////////////////////

			nextTrial();//starts the trial sequence
		}
		

		private function addOne(arr:Array):Array {
			for (var i:uint=0; i<arr.length; i++) {
				arr[i]=arr[i]+1;
			}
			return arr;
		}


		private function extractVariables(str:String):Array {
			str=str.substring(str.indexOf("[")+1,str.indexOf("]"));
			return (str.split(","));
		}

		public function specifyExperimentWideVariableDefaults():Boolean {

			//identification
			ExptWideSpecs.info=new Array  ;
			ExptWideSpecs.info.id="default ID";

			//screen properties
			ExptWideSpecs.screen=new Array  ;
			ExptWideSpecs.screen.BGcolour=0x000000 as Number;
			ExptWideSpecs.screen.orientation="default" as String;

			//save to local file variables
			ExptWideSpecs.fileInformation=new Array  ;
			ExptWideSpecs.fileInformation.save=true as Boolean;
			ExptWideSpecs.fileInformation.filename="needs implementing!" as String;
			ExptWideSpecs.fileInformation.saveDataURL = "" as String;
			ExptWideSpecs.fileInformation.emergencyEmailForMobile = "" as String;
			ExptWideSpecs.fileInformation.emergencyEmailForMobileSubject = "" as String;
			ExptWideSpecs.fileInformation.saveToServerFile = true as Boolean;
			ExptWideSpecs.fileInformation.FORCEemergencyEmailForMobile = false as Boolean;
			
			ExptWideSpecs.mobile = new Array;
			ExptWideSpecs.mobile.autoClose = false as Boolean;
			ExptWideSpecs.mobile.autoCloseTimer = 2000 as int;
			
			//send email variables
			ExptWideSpecs.email=new Array  ;
			ExptWideSpecs.email.simpleOutput=false as Boolean;
			ExptWideSpecs.email.send=false as Boolean;
			ExptWideSpecs.email.myAddress="test@sausage.com" as String;
			ExptWideSpecs.email.toWhomAddress="test@sausage.com" as String;
			ExptWideSpecs.email.subject="ExperimentData" as String;

			ExptWideSpecs.spliceTrials=new Array  ;
			ExptWideSpecs.spliceTrials.splices=new String  ;

			ExptWideSpecs.results=new Array  ;
			ExptWideSpecs.results.diagnostics=true as Boolean;
			
			
			

			/////////////////////////////////////////////////////////////////////////////////////////////////////////
			//////////////////////AW make below elegant (search over children and auto run the property assigner...//
			for (var i:uint=0; i<trialProtocolList.SETUP.children().length(); i++) {
				try {
					var tempClassReference:String=trialProtocolList.SETUP.children()[i].name();
					XMLListObjectPropertyAssigner(tempClassReference,trialProtocolList.SETUP[tempClassReference]);
				} catch (error:Error) {
					trace("you've tried to set a global experiment property that unfortunately does not exist");
				}
				

				if (ExptWideSpecs.results.diagnostics) {
					var currentDate:Date = new Date();
					ExptWideSpecs.results.value=currentDate as Date;
				}

			}
			
			
			//////////////////////instigate immediately necessary experiment specifications//////////////////////////
			setBackgroundColour(ExptWideSpecs.screen.BGcolour);
			setOrientation(ExptWideSpecs.screen.orientation);
			theStage.addEventListener(FullScreenEvent.FULL_SCREEN,changeBGcolourSize,false,0,true);
			var tempArr:Array=splitIntoArray(ExptWideSpecs.spliceTrials.splices,";");

			for (i=0; i<tempArr.length; i++) {
				arrayOfSplices.push(splitIntoArray(tempArr[i],","));
			}

			return true;
		}

		private function splitIntoArray(arr:String,char:String):Array {
			return arr.split(char);
		}

		private function nextTrial() {
			if (currentTrial==trialToSubmitDataOn-1) {
				acrossExperimentTrial.generalCleanUp();
				saveData();
			}

			if (currentTrial<trialList.length) {
				var tempTrial:uint=trialOrder[currentTrial];
				trialList[tempTrial].run(currentTrial);
				trace("tempTrial: "+tempTrial);
				trialList[tempTrial].addEventListener("eventFinishedGoToNextTrial",endTrialGoToNextTrial,false,0,true);
				trialList[tempTrial].addEventListener("eventFinishedGoToPrevTrial",endTrialGoToPrevTrial,false,0,true);
			
			}
		}
		
		private function saveDataEndStudy(event:Event){
				currentTrial=trialToSubmitDataOn-1;
				nextTrial();
		}

		public function saveData() {
			extractStudyData();
			var finalResults:XML=addInfoToResultsFile();
			trace("experiment ended, here's the data:");
			trace(finalResults);
			
			var dataSave:saveXML = new saveXML;
			dataSave.saveProcedure(finalResults, ExptWideSpecs);
			
		}



		public function extractStudyData() {
			for (var i:uint=0; i<trialList.length; i++) {
				//var tempTrial:uint=trialOrder[i];
				trace("end of trial "+i);
				//trace( ":: " + tempTrial + " " + trialList[0].returnTrialData());
				ongoingExperimentResults+=trialList[i].trialData;
			}
			ongoingExperimentResults+=acrossExperimentTrial.trialData;


		}

		private function endTrialGoToNextTrial(event:Event) {
			trialList[tempTrial].removeEventListener("eventFinishedGoToNextTrial",endTrialGoToNextTrial);
			trialList[tempTrial].removeEventListener("eventFinishedGoToPrevTrial",endTrialGoToPrevTrial);
			storedVariables=combineAssociativeArraysDestructively(storedVariables,trialList[currentTrial].					getTempStoredVarsEachTrial());//run at the end of each trial AND when trial is created
			
			
		
			trace("vars: "+storedVariables);
			var tempTrial:uint=trialOrder[currentTrial];
			currentTrial++;
			nextTrial();
		}

		private function endTrialGoToPrevTrial(event:Event) {
			trialList[tempTrial].removeEventListener("eventFinishedGoToNextTrial",endTrialGoToNextTrial);
			trialList[tempTrial].removeEventListener("eventFinishedGoToPrevTrial",endTrialGoToPrevTrial);
			var tempTrial:uint=trialOrder[currentTrial];

			for (var q:uint=1; q<=2; q++) {//change q max to change the number of trials to go 'into the past'
				trialToSubmitDataOn++;
				//trialOrderVariableDirection stores the original trial numbers as the expt is updated.
				trialOrderVariableDirection.splice(currentTrial,0,trialOrder[currentTrial+q-1]);
				trialOrder.splice(currentTrial+q,0,trialOrder.length);
				composeTrial(trialIJvalues[tempTrial-2+q].i,trialIJvalues[tempTrial-2+q].j,currentTrial+q+1,true,"special","special",99999);
			}

			currentTrial++;
			nextTrial();
		}



		private function combineAssociativeArraysDestructively(arr1:Array,arr2:Array):Array {



			for (var nam:String in arr1) {
				trace("arrr:"+nam);
				if (arr2[nam]!=undefined) {
					arr1[nam]=arr2[nam];
				}
			}
			for (nam in arr2) {
				if (arr1[nam]==undefined) {
					arr1[nam]=arr2[nam];
				}
			}

			return arr1;
		}






		public function addInfoToResultsFile():XML {

			var currentDate:Date = new Date();
			var oldDate:Date=ExptWideSpecs.results.value;
			var duration:Number= currentDate.getSeconds()+(currentDate.getMinutes()*60)+currentDate.getHours()*60*60+(currentDate.getDay()*60*60*24)-oldDate.getSeconds()-(oldDate.getMinutes()*60)-oldDate.getHours()*60*60-(oldDate.getDay()*60*60*24);
			//CPUarch: e cpuArchitecture property can return the following strings: "PowerPC", "x86", "SPARC", and "ARM". The server string is ARCH.
			//OS see bottom of this as file for description
			//version see bottom...
			var nerdStuff= "ranOnWhat: "+nerdStuff+" OS:" + Capabilities.os + " resX:" + Capabilities.screenResolutionX + " resY:" + Capabilities.screenResolutionY + " DPI:" + Capabilities.screenDPI + " CPUarch:" + Capabilities.cpuArchitecture + " version:"+Capabilities.version;
			var tempResults:XML=<Experiment id={ExptWideSpecs.info.id}>
			 <nerdStuff>{nerdStuff}</nerdStuff>
			 <deviceLanguage>Capabilities.language</deviceLanguage>
			<timeStart>{ExptWideSpecs.results.value} </timeStart>
			 <timeEnd>{currentDate}</timeEnd> 
			 <approxDurationInSeconds>{duration}</approxDurationInSeconds>
			{ongoingExperimentResults}
			</Experiment>;
			return tempResults;
		}

		////////////////////fundamental hidden functions//////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////////

		private function BlockRandomise(x:Array):Array {
			var CutinHalfA:Array=new Array(x.length/2);
			var CutinHalfB:Array=new Array(x.length/2);
			for (var i:int; i<CutinHalfA.length; i++) {
				CutinHalfA[i]=x[i];
				CutinHalfB[i]=x[i+CutinHalfA.length];
			}

			CutinHalfA=shuffleArray(CutinHalfA);
			CutinHalfB=shuffleArray(CutinHalfB);

			if (randRange(0,1)<500) {
				x=CutinHalfA.concat(CutinHalfB);
			} else {
				x=CutinHalfB.concat(CutinHalfA);
			}
			return x;
		}

		private function shuffleArray(a:Array):Array {
			var a2:Array=[];
			while (a.length>0) {
				a2.push(a.splice(Math.round(Math.random()*a.length-1),1)[0]);
			}
			return a2;
		}


		private function randRange(start:Number,end:Number):Number {
			return Math.floor(start+Math.random()*end-start);
		}

		public function setVar(type:String,nam:String,val:*) {
			ExptWideSpecs[nam]=returnType(type,val);
		}


		public function XMLListObjectPropertyAssigner(subType:String,textinfo:XMLList) {

			var attNamesList:XMLList=textinfo.@*;

			var VarName:String=attNamesList.parent().name();//this is the problem one

			//trace("varName: "+VarName);
			//trace (attNamesList+ "---"+attNamesList[i].name());


			for (var i:int=0; i<attNamesList.length(); i++) {
				var tag:String=attNamesList[i].name();// id and color
				var tagValue:String=attNamesList[i];
				//trace("VARIABLES VarName -"+VarName+"-; tag -"+tag+"-; value -"+tagValue+"-");
				if (typeof ExptWideSpecs[subType][String(attNamesList[i].name())]!="undefined") {
					try {
						trace("Global experiment property: "+VarName+"."+tag+" = "+tagValue);

						var a:Array=new Array  ;
						var tempString:String=tag;
						a=tempString.split('.');

						switch (a.length) {
							case 0 :
								trace("PROBLEM----------no variables given to set properties with-----------------!!!!!!!");
								break;
							case 1 :
								ExptWideSpecs[subType][a[0]]=returnType(typeof ExptWideSpecs[subType][a[0]],tagValue);
								break;
						}

						//ExptWideSpecs[VarName][tag]=returnType(typeof ExptWideSpecs[VarName][tag],tagValue);
					} catch (error:Error) {
						trace("PROBLEM----------'"+tag+"' is not a known property of '"+VarName+"'-----------------!!!!!!!");
					}
				} else {
					trace("PROBLEM----------'"+tag+"' property does not exist unfortunately-----------------!!!!!!!");
				}
			}


			//ExptWideSpecs.textInput.setTextFormat(ExptWideSpecs.myTextFormat);

		}

		private function returnType(type:String,value:*):* {
			var returnType;
			switch (type) {
				case "string" :
					returnType=new String  ;
					returnType=String(value);
					break;
				case "int" :
					returnType=new int  ;
					returnType=int(value);
					break;
				case "number" :
					returnType=new Number  ;
					returnType=Number(value);
					break;
				case "boolean" :
					returnType=new Boolean  ;
					if (value=="true"){
						returnType=true;
					}
					else {value=false;}

					break;
				case "uint" :
					returnType=new uint  ;
					returnType=uint(value);
					break;
				case "array" :
					returnType=new Array  ;
					returnType=value as Array;
					break;
				default :
					trace("incorrect variable type-----------------!!!!!!!");
			}
			return returnType;
		}

	}
}

/*OS Operating system	Value
Windows 7	"Windows 7"
Windows Vista	"Windows Vista"
Windows Server 2008 R2	"Windows Server 2008 R2"
Windows Server 2008	"Windows Server 2008"
Windows Home Server	"Windows Home Server"
Windows Server 2003 R2	"Windows Server 2003 R2"
Windows Server 2003	"Windows Server 2003"
Windows XP 64	"Windows Server XP 64"
Windows XP	"Windows XP"
Windows 98	"Windows 98"
Windows 95	"Windows 95"
Windows NT	"Windows NT"
Windows 2000	"Windows 2000"
Windows ME	"Windows ME"
Windows CE	"Windows CE"
Windows SmartPhone	"Windows SmartPhone"
Windows PocketPC	"Windows PocketPC"
Windows CEPC	"Windows CEPC"
Windows Mobile	"Windows Mobile"
Mac OS	"Mac OS X.Y.Z" (where X.Y.Z is the version number, for example: "Mac OS 10.5.2")
Linux	"Linux" (Flash Player attaches the Linux version, such as "Linux 2.6.15-1.2054_FC5smp"
iPhone OS 4.1	"iPhone3,1"

version
 WIN 9,0,0,0  // Flash Player 9 for Windows
	 MAC 7,0,25,0   // Flash Player 7 for Macintosh
	 LNX 9,0,115,0  // Flash Player 9 for Linux
	 AND 10,2,150,0 // Flash Player 10 for Android
	 */
	 
	 //Czech cs; Danish da; Dutch nl; English en; Finnish fi; French fr; German de; Hungarian hu; Italian it; Japanese ja; Korean ko; Norwegian no; Other/unknown xu; Polish pl; Portuguese pt; Russian ru; Simplified Chinese zh-CN; Spanish es; Swedish sv; Traditional Chinese zh-TW; Turkish tr
			 
