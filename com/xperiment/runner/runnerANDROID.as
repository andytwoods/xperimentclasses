﻿package com.xperiment.runner {
	import com.Logger.Logger;
	import com.bit101.components.Style;
	import com.xperiment.trialOrderFunctions;
	import com.xperiment.ExptWideSpecs;
	import com.xperiment.Results.Results;
	import com.xperiment.XMLstuff.importExptScript;
	import com.xperiment.XMLstuff.saveXML_ANDROID;
	import com.xperiment.runner.runner;
	import com.xperiment.trial.Trial;
	import com.xperiment.trial.TrialANDROID;
	import com.xperiment.trial.Trial_betweenSJsSortOut;
	import com.xperiment.trial.overExperiment;
	
	import flash.desktop.NativeApplication;
	import flash.display.*;
	import flash.display.StageDisplayState;
	import flash.display.StageScaleMode;
	import flash.events.*;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.geom.Rectangle;
	import flash.system.Capabilities;
	import flash.system.System;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.utils.Timer;
	import flash.utils.getDefinitionByName;
	
	

	public class runnerANDROID extends runner {

		private var _dummyTrialAndroid:TrialANDROID;
		public var actionBar:uint=48;
		
		
		override public function initDeviceSpecificStuff():void{
			theStage.scaleMode=StageScaleMode.EXACT_FIT;
			super.theStage.align=StageAlign.TOP_LEFT;
			super.theStage.scaleMode=StageScaleMode.NO_SCALE;
			super.nerdStuff="Android appID:"+NativeApplication.nativeApplication.applicationID + " RunTimeVersion:" + NativeApplication.nativeApplication.runtimeVersion+" OS:" + Capabilities.os + " resX:" + Capabilities.screenResolutionX + " resY:" + Capabilities.screenResolutionY + " DPI:" + Capabilities.screenDPI + " CPUarch:" + Capabilities.cpuArchitecture + " version:"+Capabilities.version;
			super.theStage.addEventListener(Event.FRAME_CONSTRUCTED, sortOutScreen);
		}


		private var _frameCnt:int=0;


		public function sortOutScreen(e:Event):void {
			_frameCnt++;
			if (_frameCnt == 3) {

				removeEventListener("dataBaseSorted",sortOutScreen);
				scrWidth=flash.system.Capabilities.screenResolutionX;
				scrHeight=flash.system.Capabilities.screenResolutionY;

				if (super.nerdStuff.indexOf("Windows")!=-1) {
					scrWidth=theStage.stageWidth;
					scrHeight=theStage.stageHeight;
				}
				else if (scrWidth>scrHeight) {
					var swap:uint=scrWidth;
					scrWidth=scrHeight+actionBar;
					scrHeight=swap-actionBar;
				}
				//runExpt();
			}
		}

/*		override public function setBackgroundColour(colour:Number=-1):void {

			this.graphics.lineStyle(3,colour);
			this.graphics.beginFill(colour);
			this.graphics.drawRect(0,0,theStage.stageWidth,theStage.stageHeight);
			this.graphics.endFill();
		}*/
		//future job: will need to remove this at some distant future point.;

/*		public function returnTrialType():Trial {
			return new Trial();
		}
	*/

		override public function endProgram():void {
			var tempTrial:uint=trialOrder[currentTrial];
			//saveData();

			if(TrialBS){trace("removing TrialBS");TrialBS.generalCleanUp();wipeBetweenSJsTrial();}

			if(ExptWideSpecs.ExptWideSpecs.computer.autoClose as Boolean){
				var myTimer:Timer;
				myTimer=new Timer(ExptWideSpecs.ExptWideSpecs.computer.autoCloseTimer as Number,1);
				myTimer.addEventListener(TimerEvent.TIMER_COMPLETE,function(e:Timer):void{
					myTimer.removeEventListener(TimerEvent.TIMER_COMPLETE,arguments.callee);
					NativeApplication.nativeApplication.exit();
				});
				myTimer.start();
			}
			
			if(trialList && trialList.length>0){
				trialList[tempTrial].generalCleanUp();
			}
			
			if(theStage.hasEventListener("betweenSJsTrialEvent_Runner"))theStage.removeEventListener("betweenSJsTrialEvent_Runner",betweenSJsTrialEvent);
			if(myScript && myScript.hasEventListener("dataLoaded"))myScript.removeEventListener("dataLoaded",continueStudyAfterLoadingPause);

			System.gc();
			this.kill();
		}
		
		override public function saveDataProcedure():void{
			extractStudyData();
			exptResults.sortFinalResults(this);
			var dataSave:saveXML_ANDROID = new saveXML_ANDROID();
			dataSave.saveProcedure(exptResults.finalResults,successQueryF);
		}
	



	}
}