﻿package com.xperiment {
	import com.bulkloader.preloadFilesFromWeb;
	import com.codeRecycleFunctions;
	import com.mTurk.mTurk;
	import com.trialOrderFunctions;
	import com.xperiment.trial.Trial_betweenSJsSortOut;
	import com.xperiment.importExptScript;
	import com.xperiment.trial.overExperiment;
	import com.xperiment.saveXML;
	
	import flash.display.*;
	import flash.display.StageDisplayState;
	import flash.display.StageScaleMode;
	import flash.events.*;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.geom.Rectangle;
	import flash.system.Capabilities;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.utils.Timer;
	import flash.utils.getDefinitionByName;
	import com.Logger.Logger;
	import com.xperiment.trial.Trial;
	import com.xperiment.trial.overExperiment;
	
	
	public class runner extends Sprite {
		
		//- PUBLIC & INTERNAL VARIABLES ---------------------------------------------------------------------------
		public var trialList:Array=new Array  ;
		private var trialListUnitiatedKeptInMemory:Array=new Array  ;
		public var trialOrder:Array=new Array  ;
		private var trialOrderVariableDirection:Array=new Array  ;//this might be dead,,, remove?
		private var haveAfterWhichTrial:Array=new Array  ;
		private var ongoingExperimentResults:XMLList=new XMLList  ;
		public var currentTrial:Number=0;//negative values = instructions etc.
		public var trialProtocolList:XML;
		public var trialToSubmitDataOn:uint=10000;
		public var theStage:Stage;
		private var arrayOfSplices:Array=new Array  ;
		public var scrWidth:uint;
		public var scrHeight:uint;
		private var slotInTrials:Array;
		private var trialOrderBlocks:Array;
		private var finalResults:XML;
		private var betweenSJsID:String="";
		private var betweenSJsTrial:Trial;
		public var defaultITI:uint =500;
		
		private var trialListOrder:Array=new Array  ;
		public var myScript:importExptScript;
		public var ExptWideSpecs:Array=new Array  ;
		private var trialIJvalues:Array=new Array  ;
		private var _dummyVarToAddBoxToAppCompilation1:Trial;
		
		public var nerdStuff:String = "web-browser";
		private var acrossExperimentTrial:overExperiment=new overExperiment  ;///future: consider making this optional via the setup parameters (prob eats some memory)
		public var preloader:preloadFilesFromWeb;
		public var logger:Logger=Logger.getInstance(); 
		
		private 	var mechTurk:mTurk;
		//- CONSTRUCTOR -------------------------------------------------------------------------------------------
		
		public function initialise(sta:Stage):void {
			theStage=sta;
			logger.start(false,theStage);
			theStage.scaleMode=StageScaleMode.SHOW_ALL;
			nerdStuff="Web appID:" + getAppInfo();
			
			scrWidth=theStage.stageWidth;
			scrHeight=theStage.stageHeight;
			
			if(!trialProtocolList){
				myScript=new importExptScript("myScriptPetter2.xml");
				myScript.addEventListener("dataLoaded",continueStudyAfterLoadingPause,false,0,true);
			}
			else runExpt();
		}
		
		
		
		public function giveScript(scr:XML):void {
			
			trialProtocolList=scr;
		}
		
		public function getAppInfo():String{
			return String(" OS:" + Capabilities.os + " resX:" + Capabilities.screenResolutionX + " resY:" + Capabilities.screenResolutionY + " DPI:" + Capabilities.screenDPI + " CPUarch:" + Capabilities.cpuArchitecture + " version:"+Capabilities.version);
		}
		
		private function composeTrial(i:int,j:int,tNum:uint,adhoc:Boolean,blockGroup:String,blockType:String,tWBP:uint):void {
			var trialNumTag:uint=tNum;
			var tempObjects:XMLList=trialProtocolList.TRIAL[i].objects;
			//trace("---"+tempObjects+"---");
			////////////////////////////////////////changes code within generalAttributes, given specificAttributes
			tempObjects.trialName=trialProtocolList.TRIAL[i].@trialName;
			
			
			/////////////////////////////////////////////////
/*			tempObjects.fileLocation=trialProtocolList.fileLocation;
			if (String(trialProtocolList.SETUP).length!=0 && trialProtocolList.fileLocation=="") {
				tempObjects.fileLocation=String(trialProtocolList.SETUP.explicitLocalDir);
			} */
			//above, note may have killed things.  Standardised it to the below. 
			
			tempObjects.fileLocation=trialProtocolList.SETUP.fileInformation.@stimuliFolder;
			tempObjects.storageFolder=String(trialProtocolList.SETUP.fileInformation.@saveToPortableDevice);
			tempObjects.urlSave=String(trialProtocolList.SETUP.fileInformation.@saveDataURL);
			
			tempObjects.email_toWhom=String(trialProtocolList.SETUP.email.@toWhom);
			tempObjects.email_from=String(trialProtocolList.SETUP.email.@from);
			tempObjects.email_subject=String(trialProtocolList.SETUP.email.@subject);
			
			tempObjects.perSize=String(trialProtocolList.SETUP.percentageFromWidthOrHeightOrBoth);
			/////////////////////////////////////////////////
			
			if(trialProtocolList.TRIAL[i].@ITI && trialProtocolList.TRIAL[i].@ITI.toString().length!=0)tempObjects.ITI=trialProtocolList.TRIAL[i].@ITI;
			else tempObjects.ITI=defaultITI;
			tempObjects.order=tNum;
			tempObjects.trialBlockPositionStart=tWBP;
			tempObjects.duplicateTrialNumber=j;
			if(trialProtocolList.TRIAL[i].hasOwnProperty('@submitData'))tempObjects.submitData="submitData";
			var tempTrial:Trial=new Trial();
			tempTrial.passPreloader(preloader);
			tempTrial.setup(theStage,tempObjects,tNum,acrossExperimentTrial,scrWidth,scrHeight);
			
			
			if (blockType=="undefined"||blockType=="") {
				blockType="fixed";
			}
			
			var tempIJ:Array=new Array  ;
			tempIJ.i=blockGroup;
			tempIJ.j=j;
			tempIJ.blockGroup=blockGroup;
			tempIJ.blockType=blockType;
			trialIJvalues.push(tempIJ);
			
			trialList.push(tempTrial);
			
			
			if (trialProtocolList.TRIAL[i].@submitData=="true") {
				trialToSubmitDataOn=trialList.length;
			}
		}
		
		public function getScript():void{
			trialProtocolList=myScript.giveMeData();
		}
		
		public function continueStudyAfterLoadingPause(event:Event):void {
			runExpt();
		}
		
		public function runExpt():void{
			if(!trialProtocolList)getScript();
			switch(trialProtocolList.name().toString().substr(0,4).toLowerCase()){
				case("expt"):
					checkForLoadableObjects();
					break;
				case("mult"):
					sortOutMultiExperiment();
					break;
				default:
					checkForLoadableObjects();
					
					
			}
		}
		
		public var TrialBS:Trial_betweenSJsSortOut;
		public function sortOutMultiExperiment():void{
			
			var info:XMLList=trialProtocolList.info;
			delete trialProtocolList.Info;
			
			if(trialProtocolList.@random.toString()=="true"){
				trialProtocolList=ProcessScript.BetweenSJsprocessRND(trialProtocolList,logger);
			}
				
			else if(info.objects.toString().length>0){//afraid no imported graphics allowed for this 
				TrialBS = new Trial_betweenSJsSortOut();
				theStage.addEventListener("betweenSJsTrialEvent_Runner", betweenSJsTrialEvent);
				var tempObjects:XMLList=info.objects;
				tempObjects.perSize=String(trialProtocolList.SETUP.percentageFromWidthOrHeightOrBoth);
				TrialBS.introSetup(theStage,tempObjects,trialProtocolList,scrWidth,scrHeight);
				TrialBS.prepare(0);
				
			}
		}
		
		public function wipeBetweenSJsTrial():void{
			theStage.removeEventListener("betweenSJsTrialEvent_Runner", betweenSJsTrialEvent);
			//TrialBS.generalCleanUp()
			TrialBS=null;
		}
		
		public function betweenSJsTrialEvent(e:Event):void{
			var response:String=TrialBS.introInfo();
			wipeBetweenSJsTrial();
			var parameter:String=response.split("=")[1];
			
			switch(response.split("=")[0]){
				case "runCondition":
					//betweenSJsTrial GETDATA
					trialProtocolList=ProcessScript.BetweenSJsprocess(trialProtocolList,parameter,logger);
					if(trialProtocolList!=new XML)checkForLoadableObjects();
					break;
			}
			
		}
		
		public function checkForLoadableObjects():void{
			if(trialProtocolList.SETUP.loadFilesInAdvance.@run.toString()=="false"){runningExptNow()}
			else{				
				preloader = new preloadFilesFromWeb(trialProtocolList,trialProtocolList.SETUP.fileInformation.@stimuliFolder+"\\");
				
				runningExptNow();
			}
			if("mTurk" in trialProtocolList.SETUP)mechTurk=new mTurk(theStage,logger);
		}
		
		
		public function runningExptNow():void{	
			trialProtocolList=ProcessScript.process(trialProtocolList,logger);
			
			var ensuresFinishedRunning:Boolean=specifyExperimentWideVariableDefaults();
			var trialOrderSpecifier:Array=new Array  ;
			myScript=null;
			var newTrialOrder:Array=new Array  ;
			
			
			/////////////////////////////OVER EXPERIMENT STUFF//////////////////////
			////////////////////////////////////////////////////////////////////////
			if(trialProtocolList.hasOwnProperty("overExperiment")){
				var tempObjects:XML=trialProtocolList.overExperiment[0];
				acrossExperimentTrial=new overExperiment();
				theStage.addChild(acrossExperimentTrial);
				acrossExperimentTrial.prepare(counter);
				acrossExperimentTrial.setupForOverExperiment(theStage,tempObjects);
				acrossExperimentTrial.run();
			}
			////////////////////////////////////////////////////////////////////////
			////////////////////////////////////////////////////////////////////////
			
			
			var counter:uint=0;
			var blockCounter:uint=0;
			var currentBlock:uint = 0;
			var blockStartVal:uint = 0;
			var NumTrials:uint= 0;
			
			var adhoc:Boolean=true;
			for (var i:uint=0; i<trialProtocolList.TRIAL.length(); i++) { //for each block of trials
				NumTrials=uint(trialProtocolList.TRIAL[i].numberTrials.text());
				
				if (currentBlock<i){
					currentBlock = i;
					blockStartVal = counter;
					blockCounter++;
				}
				
				logger.log(trialProtocolList.TRIAL[i].@TYPE+" x "+NumTrials);
				var tempType:String=trialProtocolList.TRIAL[i].blockGroup.@trialOrder;
				var tempBlockGroup:String=trialProtocolList.TRIAL[i].blockGroup.text();
				
				
				if(codeRecycleFunctions.IsNumeric(tempBlockGroup)==false){
					if(!slotInTrials) slotInTrials= new Array();
					
					var arr:Array = new Array;
					arr.addOn=Math.pow(10,String(codeRecycleFunctions.biggestInArray(trialProtocolList.TRIAL.blockGroup.*.toXMLString().split("\n"))).length);
					arr.blockToSlotInto=int(tempBlockGroup.split("(")[1].split(")")[0]);
					arr.newBlockGroup=arr.addOn+arr.blockToSlotInto;
					arr.slotInWhere=trialProtocolList.TRIAL[i].blockGroup.attribute("afterWhichTrial");
					slotInTrials.push(arr);
					tempBlockGroup=arr.newBlockGroup;
				}
				
				if (tempBlockGroup=="fixed") {
					trialOrderSpecifier.push("fixed");
				} else if (tempType=="") {
					trialOrderSpecifier.push("random");
				} else {
					trialOrderSpecifier.push(tempType);
				}
				
				for (var j:uint=0; j<NumTrials; j++) {///////////////////////////////////////////////////here is the problem
					composeTrial(i,j,counter,adhoc,tempBlockGroup,tempType,blockStartVal);
					counter++;
				}
			}
			
			var blockArray:Array=new Array  ;
			for (var pp:uint=0; pp<trialIJvalues.length; pp++) {
				var tempStr:String=trialIJvalues[pp].blockGroup;
				var tempNam:String=trialIJvalues[pp].blockType;
				
				if (blockArray[tempStr]==undefined) {
					blockArray[tempStr]=new Array  ;
					blockArray[tempStr].push(tempNam);
					
				}
				blockArray[tempStr].push(pp); //bit crap here as the Array autofills all the missing values inbetween.
			}
			
			logger.log("blockArray: "+blockArray);
			//logger.log("newTrialOrder: "+newTrialOrder);
			////////////////////////this bit creates a randomly ordered script for the trials)
			/////future job: include, in XML input, whether a trial is 'fixed' in order or not, and sort out shuffle.
			
			var tempTrialOrder:Array=new Array(trialList.length);
			trialOrder=new Array  ;
			//var trialOrderSpecifierZeroRemoved:Array=new Array  ;
			
			for (i=0; i<tempTrialOrder.length; i++) {
				tempTrialOrder[i]=i;
			}
			
			//trailOrder contains a simple list of numbers that correspond to trial number.  
			//This used to determine which trial to run stored in trialList.
			
			var numberRandomTrialTypes:uint=blockArray.length;
			var ExptWideSpecs:Array=new Array  ;
			var shuffledTrials:Array=new Array  ;//might be best to call this 'nonShuffled Trials'.
			var arrayRandomTrials:Array=new Array  ;
			
			logger.log("---initial trialOrder--- ");
			for (i=0; i<blockArray.length; i++) {//
				if (blockArray[i]!=undefined) {
					var newTrials:Array=new Array  ;
					var tempBlockArr:Array=blockArray[i];
					var tempTrialType:String=tempBlockArr[0];
					tempBlockArr.shift();
					if (tempTrialType=="random") {
						newTrials=shuffleArray(tempBlockArr);
					} else if (tempTrialType=="fixed") {
						newTrials=tempBlockArr;
					} else if (tempTrialType.substring(0,23)=="repeatRandBlocksDifEnds") {
						newTrials=trialOrderFunctions.repeatRandBlocksDifEnds(tempBlockArr,extractVariables(tempTrialType));
					}
					
					if(!slotInTrials || i<slotInTrials[0].addOn){//if the blockArray is not a special 'slot into' block array...
						logger.log("trials: "+newTrials);
						if (!trialOrderBlocks) trialOrderBlocks=new Array;
						for (var num:uint=0;num<newTrials.length;num++){
							trialOrderBlocks.push(i);
						}
						trialOrder=trialOrder.concat(newTrials);
					}
						
						////////////////////////////////////////////////////////////////////////////////////
						//////////////////////////////////sorts out the 'slot into' stuff
					else{
						for (var bg:uint=0; bg<slotInTrials.length; bg++){
							if(slotInTrials[bg].newBlockGroup==i){
								var newPosition:uint = codeRecycleFunctions.findStart(trialOrderBlocks,slotInTrials[bg].blockToSlotInto);
								var endPosition:uint=codeRecycleFunctions.findEnd(trialOrderBlocks,slotInTrials[bg].blockToSlotInto);
								var addOn:Array=(slotInTrials[bg].slotInWhere.split(";")as Array).sort();
								addOn=removeItemsTooBig(addOn,trialOrder);
								logger.log("slot in trials: '"+newTrials+"' into Block '"+slotInTrials[bg].blockToSlotInto+"' after these locations: '"+addOn+"'.");
								for (var jj:uint=0; jj<addOn.length; jj++){
									if(newPosition+uint(addOn[jj])<=endPosition){	
										
										trialOrder.splice(uint(newPosition+uint(addOn[jj])),0,newTrials.shift());
										trialOrderBlocks.splice(uint(newPosition+uint(addOn[jj])),0,slotInTrials[bg].blockToSlotInto);
									}
									else trace("could not slot in this trial as the exceeded block limit:"+newPosition+uint(addOn[jj]));
								}
								break;
							}
						}
					}
					
				} 
			}
			
			for (i=0;i<trialOrder.length;i++){
				var zeros:uint=String(trialOrder[i]).length-String(trialOrderBlocks[i]).length;
				switch(zeros)
				{
					case 1:
						trialOrderBlocks[i]=" "+trialOrderBlocks[i];
						break;
					
					case 2:
						trialOrderBlocks[i]=" "+trialOrderBlocks[i];
						break;
					
				}
			}
			logger.log("trial block order       : "+trialOrderBlocks);
			logger.log("trialOrder after shuffle: "+trialOrder);
			////////////////////////////////////////////////////////////////////////////////////
			
			
			
			for (i=0; i<arrayOfSplices.length; i++) {
				var SplicVars:Array=arrayOfSplices[i];
				var funct:Array=splitIntoArray(SplicVars[0]," ");
				if (funct[0]=="rand") {
					var randNum:Number=Math.random()*100;
					if (randNum>Number(funct[1])) {
						var tempArr:Array=trialOrder.splice(SplicVars[1],SplicVars[2]);
						trialOrder.splice(SplicVars[3],0,tempArr);
						//logger.log("splice [" + SplicVars+ "] was done as random number ("+(Math.round(randNum))+") exceeded your threshold ("+funct[1]+")");
					} else {
						//logger.log("splice [" + SplicVars+ "] was not done as random number ("+(Math.round(randNum))+") did not exceed your threshold ("+funct[1]+")");
					}
					
				}
				
				
				if (trialToSubmitDataOn==10000) {
					trialToSubmitDataOn=trialList.length-1;
				}
				
				//logger.log("trialOrder after splice: "+trialOrder);
			}
			
			
			////////////////////////////////////////////////////////////////////////////////////
			
			logger.log("------------------commence experiment-------------------------");
			
			if (trialProtocolList.SETUP.experiment && trialProtocolList.SETUP.experiment.defaultITI)defaultITI=int(trialProtocolList.SETUP.experiment.@defaultITI);
			
			nextTrial();//starts the trial sequence 
		}
		
		private function removeItemsTooBig(arr:Array,tOrder:Array):Array{
			return arr;
		}
		
		
		private function addOne(arr:Array):Array {
			for (var i:uint=0; i<arr.length; i++) {
				arr[i]=arr[i]+1;
			}
			return arr;
		}
		
		
		private function extractVariables(str:String):Array {
			str=str.substring(str.indexOf("[")+1,str.indexOf("]"));
			return (str.split(","));
		}
		
		public function sortDefaultVariables():void{
			//identification
			ExptWideSpecs.info=new Array  ;
			ExptWideSpecs.info.id="default ID";
			
			ExptWideSpecs.mobile = new Array;
			ExptWideSpecs.mobile.autoClose = false as Boolean;
			ExptWideSpecs.mobile.autoCloseTimer = 2000 as int;
			
			//screen properties
			ExptWideSpecs.screen=new Array  ;
			ExptWideSpecs.screen.BGcolour=0x000000 as Number;
			ExptWideSpecs.screen.orientation="default" as String;
			
			//save to local file variables
			ExptWideSpecs.fileInformation=new Array  ;
			ExptWideSpecs.fileInformation.save=true as Boolean;
			ExptWideSpecs.fileInformation.filename="needs implementing!" as String;
			ExptWideSpecs.fileInformation.saveDataURL = "" as String;
			ExptWideSpecs.fileInformation.emergencyEmailForMobile = "" as String;
			ExptWideSpecs.fileInformation.emergencyEmailForMobileSubject = "" as String;
			ExptWideSpecs.fileInformation.saveToServerFile = true as Boolean;
			ExptWideSpecs.fileInformation.FORCEemergencyEmailForMobile = false as Boolean;
			ExptWideSpecs.fileInformation.saveToPortableDevice = "" as String;
			
			//send email variables
			ExptWideSpecs.email=new Array  ;
			ExptWideSpecs.email.simpleOutput=false as Boolean;
			ExptWideSpecs.email.myAddress="test@xperiment.mobi" as String;
			ExptWideSpecs.email.toWhom="false" as String;
			ExptWideSpecs.email.subject="ExperimentData" as String;
			
			ExptWideSpecs.spliceTrials=new Array  ;
			ExptWideSpecs.spliceTrials.splices=new String  ;
			
			ExptWideSpecs.results=new Array  ;
			ExptWideSpecs.results.diagnostics=true as Boolean;
			
		}
		
		public function specifyExperimentWideVariableDefaults():Boolean {
			
			sortDefaultVariables();	
			
			
			/////////////////////////////////////////////////////////////////////////////////////////////////////////
			//////////////////////AW make below elegant (search over children and auto run the property assigner...//
			for (var i:uint=0; i<trialProtocolList.SETUP.children().length(); i++) {
				try {
					var tempClassReference:String=trialProtocolList.SETUP.children()[i].name();
					XMLListObjectPropertyAssigner(tempClassReference,trialProtocolList.SETUP[tempClassReference]);
				} catch (error:Error) {
					logger.log("you've tried to set a global experiment property that unfortunately does not exist");
				}
				
				
				if (ExptWideSpecs.results.diagnostics) {
					var currentDate:Date = new Date();
					ExptWideSpecs.results.value=currentDate as Date;
				}
				
			}
			
			
			//////////////////////instigate immediately necessary experiment specifications//////////////////////////
			setBackgroundColour(ExptWideSpecs.screen.BGcolour);
			setOrientation(ExptWideSpecs.screen.orientation);
			theStage.addEventListener(FullScreenEvent.FULL_SCREEN,changeBGcolourSize,false,0,true);
			var tempArr:Array=splitIntoArray(ExptWideSpecs.spliceTrials.splices,";");
			
			for (i=0; i<tempArr.length; i++) {
				arrayOfSplices.push(splitIntoArray(tempArr[i],","));
			}
			
			return true;
		}
		
		private function splitIntoArray(arr:String,char:String):Array {
			return arr.split(char);
		}
		
		public function nextTrial():void {
			if (trialList[trialOrder[currentTrial]].submitData) {
				acrossExperimentTrial.generalCleanUp();
				saveData();
				beginLastTrial();
			}
			
			if (currentTrial<trialList.length-1) {
				logger.log("--------------------------------------------------------------");
				logger.log("running trial: "+currentTrial);
				var tempTrial:uint=trialOrder[currentTrial];
				beginNextTrial();
			}
			
		}
		
		private function beginLastTrial():void{
			var tempTrial:uint=trialOrder[currentTrial];
			trialList[tempTrial].prepare(currentTrial);			
		}
		
		private function beginNextTrial():void{
			if(trialOrder.length-1<currentTrial)trace(12345);
			var tempTrial:uint=trialOrder[currentTrial];
			trialList[tempTrial].prepare(currentTrial);				
			trialList[tempTrial].addEventListener("eventFinishedGoToNextTrial",endTrialGoToNextTrialEvent,false,0,true);
			trialList[tempTrial].addEventListener("eventFinishedGoToPrevTrial",endTrialGoToPrevTrial,false,0,true);
		}
		
		public function saveData():void {
			extractStudyData();
			finalResults=addInfoToResultsFile();
			
			var dataSave:saveXML = new saveXML();
			dataSave.passLogger(logger);
			dataSave.saveProcedure(finalResults, ExptWideSpecs);
		}
		
		
		
		public function extractStudyData():void {
			//for (var i:uint=0; i<trialList.length; i++) {
			//var tempTrial:uint=trialOrder[i];
			//logger.log("end of trial "+i);
			//logger.log( ":: " + tempTrial + " " + trialList[0].returnTrialData());
			
			//}
			ongoingExperimentResults+=acrossExperimentTrial.trialData;
			
			
		}
		
		
		
		private function endTrialGoToNextTrial():void{
			var tempTrial:uint=trialOrder[currentTrial];
			trialList[tempTrial].removeEventListener("eventFinishedGoToNextTrial",endTrialGoToNextTrialEvent);
			trialList[tempTrial].removeEventListener("eventFinishedGoToPrevTrial",endTrialGoToPrevTrial);
			ongoingExperimentResults+=trialList[tempTrial].giveTrialData();
			trialList[tempTrial]=null;
			currentTrial++;
			nextTrial();
		}
		
		public function endTrialGoToNextTrialEvent(event:Event):void {endTrialGoToNextTrial();}
		
		public function endTrialGoToPrevTrial(event:Event):void {
			var tempTrial:uint=trialOrder[currentTrial];
			for (var q:uint=1; q<=2; q++) {//change q max to change the number of trials to go 'into the past'
				trialToSubmitDataOn++;
				//trialOrderVariableDirection stores the original trial numbers as the expt is updated.
				trialOrderVariableDirection.splice(currentTrial,0,trialOrder[currentTrial+q-1]);
				trialOrder.splice(currentTrial+q,0,trialOrder.length);
				composeTrial(trialIJvalues[tempTrial-2+q].i,trialIJvalues[tempTrial-2+q].j,currentTrial+q+1,true,"special","special",99999);
			}
			endTrialGoToNextTrial();
		}
		
		
		
		private function combineAssociativeArraysDestructively(arr1:Array,arr2:Array):Array {
			
			
			
			for (var nam:String in arr1) {
				//logger.log("arrr:"+nam);
				if (arr2[nam]!=undefined) {
					arr1[nam]=arr2[nam];
				}
			}
			for (nam in arr2) {
				if (arr1[nam]==undefined) {
					arr1[nam]=arr2[nam];
				}
			}
			
			return arr1;
		}
		

		public function addInfoToResultsFile():XML {
			
			var currentDate:Date = new Date();
			var oldDate:Date=ExptWideSpecs.results.value;
			var duration:Number= currentDate.getSeconds()+(currentDate.getMinutes()*60)+currentDate.getHours()*60*60+(currentDate.getDay()*60*60*24)-oldDate.getSeconds()-(oldDate.getMinutes()*60)-oldDate.getHours()*60*60-(oldDate.getDay()*60*60*24);
			//CPUarch: e cpuArchitecture property can return the following strings: "PowerPC", "x86", "SPARC", and "ARM". The server string is ARCH.
			//OS see bottom of this as file for description
			//version see bottom...
			var tempResults:XML=<Experiment id={ExptWideSpecs.info.id}>
			 <betweenSJsID>{betweenSJsID}</betweenSJsID>
			 <nerdStuff>{nerdStuff}</nerdStuff>
			 <deviceLanguage>Capabilities.language</deviceLanguage>
			<timeStart>{ExptWideSpecs.results.value} </timeStart>
			 <timeEnd>{currentDate}</timeEnd> 
			 <approxDurationInSeconds>{duration}</approxDurationInSeconds>
			{ongoingExperimentResults}
			</Experiment>;
			return tempResults;
		}
		
		////////////////////fundamental hidden functions//////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////////
		
		private function BlockRandomise(x:Array):Array {
			var CutinHalfA:Array=new Array(x.length/2);
			var CutinHalfB:Array=new Array(x.length/2);
			for (var i:int; i<CutinHalfA.length; i++) {
				CutinHalfA[i]=x[i];
				CutinHalfB[i]=x[i+CutinHalfA.length];
			}
			
			CutinHalfA=shuffleArray(CutinHalfA);
			CutinHalfB=shuffleArray(CutinHalfB);
			
			if (randRange(0,1)<500) {
				x=CutinHalfA.concat(CutinHalfB);
			} else {
				x=CutinHalfB.concat(CutinHalfA);
			}
			return x;
		}
		
		private function shuffleArray(a:Array):Array {
			var a2:Array=[];
			while (a.length>0) {
				a2.push(a.splice(Math.round(Math.random()*a.length-1),1)[0]);
			}
			return a2;
		}
		
		
		private function randRange(start:Number,end:Number):Number {
			return Math.floor(start+Math.random()*end-start);
		}
		
		public function setVar(type:String,nam:String,val:*):void {
			ExptWideSpecs[nam]=returnType(type,val);
		}
		
		
		public function XMLListObjectPropertyAssigner(subType:String,textinfo:XMLList):void {
			
			var attNamesList:XMLList=textinfo.@*;
			
			var VarName:String=attNamesList.parent().name();//this is the problem one
			
			//logger.log("varName: "+VarName);
			//logger.log (attNamesList+ "---"+attNamesList[i].name());
			
			
			for (var i:int=0; i<attNamesList.length(); i++) {
				var tag:String=attNamesList[i].name();// id and color
				var tagValue:String=attNamesList[i];
				//logger.log("VARIABLES VarName -"+VarName+"-; tag -"+tag+"-; value -"+tagValue+"-");
				if (typeof ExptWideSpecs[subType][String(attNamesList[i].name())]!="undefined") {
					try {
						logger.log("Global experiment property: "+VarName+"."+tag+" = "+tagValue);
						
						var a:Array=new Array  ;
						var tempString:String=tag;
						a=tempString.split('.');
						
						switch (a.length) {
							case 0 :
								logger.log("PROBLEM----------no variables given to set properties with-----------------!!!!!!!");
								break;
							case 1 :
								ExptWideSpecs[subType][a[0]]=returnType(typeof ExptWideSpecs[subType][a[0]],tagValue);
								break;
						}
						
						//ExptWideSpecs[VarName][tag]=returnType(typeof ExptWideSpecs[VarName][tag],tagValue);
					} catch (error:Error) {
						logger.log("PROBLEM----------'"+tag+"' is not a known property of '"+VarName+"'-----------------!!!!!!!");
					}
				} else {
					logger.log("PROBLEM----------'"+tag+"' property does not exist unfortunately-----------------!!!!!!!");
				}
			}
			
			
			//ExptWideSpecs.textInput.setTextFormat(ExptWideSpecs.myTextFormat);
			
		}
		
		private function returnType(type:String,value:*):* {
			var returnType:*;
			switch (type) {
				case "string" :
					returnType=new String  ;
					returnType=String(value);
					break;
				case "int" :
					returnType=new int  ;
					returnType=int(value);
					break;
				case "number" :
					returnType=new Number  ;
					returnType=Number(value);
					break;
				case "boolean" :
					returnType=new Boolean  ;
					if (value=="true"){
						returnType=true;
					}
					else {value=false;}
					
					break;
				case "uint" :
					returnType=new uint  ;
					returnType=uint(value);
					break;
				case "array" :
					returnType=new Array  ;
					returnType=value as Array;
					break;
				default :
					logger.log("incorrect variable type-----------------!!!!!!!");
			}
			return returnType;
		}
		
		//- background colour -------------------------------------------------------------------------------------------
		
		public var square:Sprite=new Sprite;
		
		public function changeBGcolourSize(event:FullScreenEvent):void {
			theStage.removeChild(square);
			var colour:Number=ExptWideSpecs.screen.BGcolour;
			square.graphics.lineStyle(3,colour);
			square.graphics.beginFill(colour);
			square.graphics.drawRect(0,0,theStage.stageWidth,theStage.stageHeight);
			square.graphics.endFill();
			square.x=theStage.stageWidth-theStage.fullScreenWidth/2;
			square.y=theStage.stageHeight-theStage.fullScreenHeight/2;
			theStage.addChildAt(square,0);//future job: will need to remove this at some distant future point.
		}
		
		public function setBackgroundColour(colour:Number):void {
			square.graphics.lineStyle(3,colour);
			square.graphics.beginFill(colour);
			square.graphics.drawRect(0,0,theStage.stageWidth+10,theStage.stageHeight+10);
			square.graphics.endFill();
			square.x=-5;
			square.y=-5;
			theStage.addChild(square);//future job: will need to remove this at some distant future point.
		}
		
		public function setOrientation(orien:String):void {
			
			switch(orien){
				case "vertical":
					//theStage.align = StageAlign.TOP_LEFT;
					break;
				case "horizontal":
					//theStage.align = StageAlign.LEFT;
					break;
				
			}
			
			
		}
	}
}

/*OS Operating system	Value
Windows 7	"Windows 7"
Windows Vista	"Windows Vista"
Windows Server 2008 R2	"Windows Server 2008 R2"
Windows Server 2008	"Windows Server 2008"
Windows Home Server	"Windows Home Server"
Windows Server 2003 R2	"Windows Server 2003 R2"
Windows Server 2003	"Windows Server 2003"
Windows XP 64	"Windows Server XP 64"
Windows XP	"Windows XP"
Windows 98	"Windows 98"
Windows 95	"Windows 95"
Windows NT	"Windows NT"
Windows 2000	"Windows 2000"
Windows ME	"Windows ME"
Windows CE	"Windows CE"
Windows SmartPhone	"Windows SmartPhone"
Windows PocketPC	"Windows PocketPC"
Windows CEPC	"Windows CEPC"
Windows Mobile	"Windows Mobile"
Mac OS	"Mac OS X.Y.Z" (where X.Y.Z is the version number, for example: "Mac OS 10.5.2")
Linux	"Linux" (Flash Player attaches the Linux version, such as "Linux 2.6.15-1.2054_FC5smp"
iPhone OS 4.1	"iPhone3,1"

version
WIN 9,0,0,0  // Flash Player 9 for Windows
MAC 7,0,25,0   // Flash Player 7 for Macintosh
LNX 9,0,115,0  // Flash Player 9 for Linux
AND 10,2,150,0 // Flash Player 10 for Android
*/

//Czech cs; Danish da; Dutch nl; English en; Finnish fi; French fr; German de; Hungarian hu; Italian it; Japanese ja; Korean ko; Norwegian no; Other/unknown xu; Polish pl; Portuguese pt; Russian ru; Simplified Chinese zh-CN; Spanish es; Swedish sv; Traditional Chinese zh-TW; Turkish tr

