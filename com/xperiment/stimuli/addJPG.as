package  com.xperiment.stimuli{
	
	import com.bulkloader.preloadFilesFromWeb;
	import com.graphics.pattern.Box;
	import com.xperiment.ExptWideSpecs;
	import com.xperiment.uberSprite;
	
	import flash.display.*;
	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.events.*;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	
	public class addJPG extends object_baseClass implements IgivePreloader  {
		
		private var pictURLReq:URLRequest;
		private var pictLdr:Loader;
		private var preloader:preloadFilesFromWeb;
		private var bmp:Bitmap;
		
		override public function setVariables(list:XMLList):void {
			
			//If width and height not specified, just use the graphics file's dimensions
			if(list.@width.toString().length==0)OnScreenElements.width="0";
			if(list.@height.toString().length==0)OnScreenElements.height="0";	
			////////////////////////////////////////////////////////////////////////////
			
			setVar("string","filename","");
			setVar("string","directory","","","specify without the ending backslash");
			setVar("boolean","showOutlineBeforeLoaded",false);
			super.setVariables(list);
			if(theStage){
				if (preloader && preloader.isFinished()) {
					bmp=preloader.give(getVar("filename"));
					if(bmp)pic.addChild(new Bitmap(bmp.bitmapData)); //if don't make a copy, have problems if you have multiple instances on stage
					if(getVar("width")=="0")setVar("string","width", String(bmp.width));
					if(getVar("height")=="0")setVar("string","height", String(bmp.height));
					//trace(getVar("width"),getVar("height"));
					setUniversalVariables();
				}
				else {
					//if (localStorageLocation!="" && localStorageLocation!="assets/")localStorageLocation="file://"+localStorageLocation+"/";
					var localStorageLocation:String= ExptWideSpecs.ExptWideSpecs.fileInformation.stimuliFolder;
					if (localStorageLocation!="" && localStorageLocation!="assets/")localStorageLocation=localStorageLocation+"/";
					//note that the above 'assets/' logic is for the DESKTOP version 
					pictLdr=new Loader  ;
					pictLdr.contentLoaderInfo.addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
					pictLdr.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
					pictLdr.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
					pictLdr.contentLoaderInfo.addEventListener(Event.INIT,loadedEvent,false,0,true);
					pictLdr.load(new URLRequest((getVar("directory")+localStorageLocation+getVar("filename"))));	
					
				}
			}
			
		}
		
		
		
		public function securityErrorHandler(event:SecurityErrorEvent):void {
			trace("addJPG problem: securityErrorHandler: " + event);
		}
		

		public function ioErrorHandler(event:IOErrorEvent):void {
			trace("addJPG problem: ioErrorHandler: " + event);
		}
		
		public function loadedEvent(event:Event):void {
			pictLdr.contentLoaderInfo.removeEventListener(Event.INIT,loadedEvent);
			var bmp:Bitmap = new Bitmap();
			bmp = pictLdr.content as Bitmap;
			pic.addChild(bmp);
			setUniversalVariables();			
			if (getVar("showBox"))showBox();
		}
		
		public function passPreloader(preloader:preloadFilesFromWeb):void {
			this.preloader=preloader;
		}
		
		
		override public function kill():void {
			pictURLReq=null;
			pictLdr=null;
			super.kill();
		}
	}
}