package com.xperiment.stimuli
{

	import com.xperiment.uberSprite;
	import com.xperiment.Results.Results;
	import com.xperiment.stimuli.primitives.graphs.BarChart;
	
	import flash.events.Event;
	
	
	public class addBarGraph extends object_baseClass
	{
		private var results:Results;
		private var barGraph:BarChart;

		override public function setVariables(list:XMLList):void {	
			setVar("string","trialNames","270right,270left");
			setVar("string","xAxisLabels1","correct,false");
			setVar("string","xAxisLabels2","");
			setVar("string","xAxisTitle","xAxisTitle");
			setVar("string","yAxisTitle","yAxisTitle");
			setVar("string","title","");
			setVar("string","type", "count");//count or mean
			setVar("string","cond1TrialNames","");
			setVar("string","cond2TrialNames","");
			setVar("string","cond3TrialNames","");
			setVar("string","cond4TrialNames","");
			setVar("string","cond1DataObjects","");
			setVar("string","cond2DataObjects","");
			setVar("string","cond3DataObjects","");
			setVar("string","cond4DataObjects","");
			setVar("string","dataVars1","");
			setVar("string","dataVars2","");
			setVar("string","dataVars3","");
			setVar("string","dataVars4","");
			setVar("Number","yAxisMax",0);
			setVar("int","dp",1);
			super.setVariables(list);
		}
		
		//IMPORTANT, note that DV MUST BE numerical
		
		//override public function sortOutWidthHeight():void{}
		
		private function addedToStage(e:Event):void{
			pic.removeEventListener(Event.ADDED_TO_STAGE,addedToStage);
			if(barGraph)barGraph.init();
		}

		private function pushLastValIfExistsElseBlank(arr:Array):Array{
			if(arr.length>0)arr.push(arr[arr.length-1]);
			return arr;
		}
		
		override public function RunMe():uberSprite {
			pic.addEventListener(Event.ADDED_TO_STAGE,addedToStage);
			
			//trace(getVar("condition1"),333)
			
			var i:uint=1;
			//trace(getVar("condition"+(i)),55);
			
			var conditionals:Array=new Array;
			var dataObjects:Array=new Array;
			var DVs:Array=new Array;
			//var subCondition:Array;  for later!
			var str:String;
			var conditionalsContinue:Boolean=true;
			var datObjectsContinue:Boolean=true;
			var DVcontinue:Boolean=true;
		
			var widHei:Array=getWidthHeight();
			
			
			while(conditionalsContinue || datObjectsContinue || DVcontinue){
				
				/////////////////conditionalsContinue
				str=getVar("cond"+String(i)+"TrialNames");
				if(str=="")conditionalsContinue=false;
				else{
					conditionals.push(str);
					conditionalsContinue=true;
				}
				//////////////////datObjectsContinue
				str=getVar("cond"+String(i)+"DataObjects");
				if(str==""){
					datObjectsContinue=false;
					if(conditionalsContinue)pushLastValIfExistsElseBlank(dataObjects);
				}
				else{
					dataObjects.push(str);
					datObjectsContinue=true;
					if(!conditionalsContinue)pushLastValIfExistsElseBlank(conditionals);
				}
				//////////////////dataVars
				str=getVar("DV"+String(i));
				if(str==""){
					DVcontinue=false;
					if(conditionalsContinue || datObjectsContinue)pushLastValIfExistsElseBlank(DVs);
				}
				else{
					DVs.push(str);
					DVcontinue=true;
					if(!conditionalsContinue)pushLastValIfExistsElseBlank(conditionals);
					if(!datObjectsContinue)pushLastValIfExistsElseBlank(dataObjects);
				}
				i++;
			}

			/*trace(conditionals,conditionals.length,333);
			trace(dataObjects,dataObjects.length,333);
			trace(DVs,DVs.length,333);*/
		
			if((conditionals.length!=0 || dataObjects.length!=0) && DVs.length!=0){
				this.results=Results.getInstance();
				var myResults:Array=results.giveOngoing({DVs:DVs,dataObjects:dataObjects,conditionals:conditionals, what:getVar("type"),trialNames:(getVar("trialNames") as String).split(",")});
					var xAxisLabels1:Array = (getVar("xAxisLabels1") as String).split(",");
					var xAxisLabels2:Array = (getVar("xAxisLabels2") as String).split(",");
					
					//trace(myResults);
					barGraph = new BarChart(myResults,{title:getVar("title"),xAxisLabels1:xAxisLabels1,xAxisLabels2:xAxisLabels2, x:getVar("xAxisTitle"),y:getVar("yAxisTitle"),dp:getVar("dp") as int,width:widHei[0],height:widHei[1],yAxisMax:getVar("yAxisMax") as int});
					pic.addChild(barGraph);
					}
			else(logger.log("!!You have asked for a barGraph but have not told me what object to collect data on: dataObject=..."));
	
			//pic.graphics.beginFill(0x00ff00,.5);
			//pic.graphics.drawRect(0,0,pic.width,pic.height);
			
			super.setUniversalVariables();
			return (pic);
		}
		
		
		override public function kill():void {
			if(pic.hasEventListener(Event.ADDED_TO_STAGE))pic.removeEventListener(Event.ADDED_TO_STAGE,addedToStage);
			super.kill();
			
		}
	}
}