﻿
package  com.xperiment.stimuli{

	import com.xperiment.uberSprite;
	import com.xperiment.stimuli.addText;
	
	import flash.display.*;
	import flash.events.Event;
	import flash.events.FocusEvent;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.events.TextEvent;
	import flash.text.*;
	import flash.text.TextField;
	import flash.text.TextFormat;

	//import fl.controls.myText;


	public class addInputTextBox extends addText {

		private var replaceOld:Array;
		private var replaceNew:Array;
		
		override public function storedData():Array {
			var tempData:Array = new Array();
			if(getVar("peg")=="")tempData.event="InputTextBox";
			else tempData.event=getVar("peg");
			tempData.data=myText.text;
			
			//trace(tempData,22);
			super.objectData.push(tempData);
			return objectData;
		}

		override public function returnsDataQuery():Boolean {
			if(getVar("hideResults"))return false;
			return true;
		}

		//public function Trial_imageCollage(genAttrib:XMLList, specAttrib:XMLList) {
		override public function setVariables(list:XMLList):void {
			setVar("uint","inputBoxNumCharacters", 1000);//maxChars
			setVar("boolean","password", false);
			setVar("string","restrict","","","any list of characters with no space, e.g. abc");
			setVar("uint","textBoxWidth",200);
			setVar("uint","textBoxHeight",100);
			setVar("string","text","demo text");
			setVar("boolean","hideResults",false);
			setVar("boolean","emptyWhenClicked",false);
			setVar("number","background", 0xFFFFFF);
			setVar("number","border",0xFFF0FF);
			setVar("string","replace", "");//e.g. 32-13 [space with enter] (charcodes); use codes from here: http://www.asciitable.com/
			setVar("boolean","wordWrap",true);
			super.setVariables(list);
			
			if(getVar("replace")!=""){
				replaceOld=new Array;
				replaceNew=new Array;
				var arr:Array=(getVar("replace") as String).split(",");
				var sub:Array;
				for each(var str:String in arr){
					sub=str.split("-");
					replaceOld.push(uint(sub[0]));
					replaceNew.push(uint(sub[1]));
				}			
			}		
		}




		override public function setupText():void {
			super.setupText();
			myText.selectable=true;
			myText.autoSize=TextFieldAutoSize.NONE;
			myText.type=TextFieldType.INPUT;
			myText.width=getVar("textBoxWidth");
			myText.height=getVar("textBoxHeight");
			myText.htmlText=sortOutHTML(getVar("text"));
			myText.setSelection(myText.length,myText.length);
			myText.maxChars=getVar("inputBoxNumCharacters");
			myText.displayAsPassword=getVar("password");
			
			if(getVar("emptyWhenClicked"))myText.addEventListener(MouseEvent.CLICK, wipe,false,0,true);
			
			if (getVar("restrict")!="") {
				myText.restrict=getVar("restrict");
			}
			if(getVar("replace")!=""){
				myText.addEventListener(TextEvent.TEXT_INPUT,replace,false,0,true);
			}	
			pic.addEventListener(FocusEvent.FOCUS_IN,blinkerOn,false,0,true);
			
			pic.addEventListener(Event.ADDED_TO_STAGE,function(e:Event):void{
				e.currentTarget.removeEventListener(e.type,arguments.callee);
				
				myText.selectable = true;
				myText.stage.focus = myText;
				myText.setSelection(myText.text.length, myText.text.length);
			},false,0,true);

		}
		
		private function blinkerOn(e:FocusEvent):void
		{
			myText.selectable = true;
			myText.stage.focus = myText;
			myText.setSelection(myText.text.length, myText.text.length);
		}		
		
		private function wipe(e:MouseEvent):void{
			e.target.removeEventListener(MouseEvent.CLICK, wipe)
			myText.text="";
		}
		
		private function  replace(e:TextEvent):void{
			//trace(e.text.charCodeAt(0));
			var num:int=replaceOld.indexOf(e.text.charCodeAt(0));
			if(num!=-1){
				myText.selectable=false;
				myText.text=(myText.text+String.fromCharCode(replaceNew[num]));
				myText.setSelection(myText.length,myText.length);
				
				theStage.addEventListener(KeyboardEvent.KEY_UP,function(ee:KeyboardEvent):void{
					e.currentTarget.removeEventListener(ee.type,arguments.callee);
					for(var i:uint=0;i<replaceOld.length;i++){
	
						myText.text=myText.text.replace(new RegExp(e.text,"g"),"")
					}

					myText.selectable=true;
																},false,0,true);
				
			}
		}
		
		override public function kill():void{
			pic.removeEventListener(FocusEvent.FOCUS_IN,blinkerOn);
			if(getVar("expand") as Boolean){
				theStage.removeEventListener(TextEvent.TEXT_INPUT,replace,false,0,true);
				
			}
			if(getVar("emptyWhenClicked") && myText.hasEventListener(MouseEvent.CLICK))myText.removeEventListener(MouseEvent.CLICK, wipe);
			super.kill();
		}

	}






}