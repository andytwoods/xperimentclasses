package com.xperiment.stimuli
{
	import com.bit101.components.ComboBox;
	import com.bit101.components.Style;
	import com.xperiment.uberSprite;
	
	public class addComboBox extends object_baseClass
	{
			
		private var comboBox:ComboBox;
		
		override public function kill():void{
			comboBox=null;
			super.kill();
		}
		
		override public function storedData():Array {
			var tempData:Array = new Array();
			tempData.event=String("multipleChoice"+getVar("id"));
			tempData.data=comboBox.selectedItem as String;
			super.objectData.push(tempData);
			return objectData;
		}
		
		override public function returnsDataQuery():Boolean {return true;}
		
		override public function setVariables(list:XMLList):void 
		{
			setVar("string","items","United States<sep>United Kingdom<sep>Ireland<sep>France<sep>Afghanistan<sep>Albania<sep>Algeria<sep>American Samoa<sep>Andorra<sep>Angola<sep>Anguilla<sep>Antarctica<sep>Antigua and Barbuda<sep>Argentina<sep>Armenia<sep>Aruba<sep>Australia<sep>Austria<sep>Azerbaijan<sep>Bahamas<sep>Bahrain<sep>Bangladesh<sep>Barbados<sep>Belarus<sep>Belgium<sep>Belize<sep>Benin<sep>Bermuda<sep>Bhutan<sep>Bolivia<sep>Bosnia and Herzegovina<sep>Botswana<sep>Bouvet Island<sep>Brazil<sep>British Indian Ocean Territory<sep>Brunei Darussalam<sep>Bulgaria<sep>Burkina Faso<sep>Burundi<sep>Cambodia<sep>Cameroon<sep>Canada<sep>Cape Verde<sep>Cayman Islands<sep>Central African Republic<sep>Chad<sep>Chile<sep>China<sep>Christmas Island<sep>Cocos (Keeling) Islands<sep>Colombia<sep>Comoros<sep>Congo<sep>Congo, The Democratic Republic of The<sep>Cook Islands<sep>Costa Rica<sep>Cote D'ivoire<sep>Croatia<sep>Cuba<sep>Cyprus<sep>Czech Republic<sep>Denmark<sep>Djibouti<sep>Dominica<sep>Dominican Republic<sep>Ecuador<sep>Egypt<sep>El Salvador<sep>Equatorial Guinea<sep>Eritrea<sep>Estonia<sep>Ethiopia<sep>Falkland Islands (Malvinas)<sep>Faroe Islands<sep>Fiji<sep>Finland<sep>France<sep>French Guiana<sep>French Polynesia<sep>French Southern Territories<sep>Gabon<sep>Gambia<sep>Georgia<sep>Germany<sep>Ghana<sep>Gibraltar<sep>Greece<sep>Greenland<sep>Grenada<sep>Guadeloupe<sep>Guam<sep>Guatemala<sep>Guinea<sep>Guinea-bissau<sep>Guyana<sep>Haiti<sep>Heard Island and Mcdonald Islands<sep>Holy See (Vatican City State)<sep>Honduras<sep>Hong Kong<sep>Hungary<sep>Iceland<sep>India<sep>Indonesia<sep>Iran, Islamic Republic of<sep>Iraq<sep>Ireland<sep>Israel<sep>Italy<sep>Jamaica<sep>Japan<sep>Jordan<sep>Kazakhstan<sep>Kenya<sep>Kiribati<sep>Korea, Democratic People's Republic of<sep>Korea, Republic of<sep>Kuwait<sep>Kyrgyzstan<sep>Lao People's Democratic Republic<sep>Latvia<sep>Lebanon<sep>Lesotho<sep>Liberia<sep>Libyan Arab Jamahiriya<sep>Liechtenstein<sep>Lithuania<sep>Luxembourg<sep>Macao<sep>Macedonia, The Former Yugoslav Republic of<sep>Madagascar<sep>Malawi<sep>Malaysia<sep>Maldives<sep>Mali<sep>Malta<sep>Marshall Islands<sep>Martinique<sep>Mauritania<sep>Mauritius<sep>Mayotte<sep>Mexico<sep>Micronesia, Federated States of<sep>Moldova, Republic of<sep>Monaco<sep>Mongolia<sep>Montserrat<sep>Morocco<sep>Mozambique<sep>Myanmar<sep>Namibia<sep>Nauru<sep>Nepal<sep>Netherlands<sep>Netherlands Antilles<sep>New Caledonia<sep>New Zealand<sep>Nicaragua<sep>Niger<sep>Nigeria<sep>Niue<sep>Norfolk Island<sep>Northern Mariana Islands<sep>Norway<sep>Oman<sep>Pakistan<sep>Palau<sep>Palestinian Territory, Occupied<sep>Panama<sep>Papua New Guinea<sep>Paraguay<sep>Peru<sep>Philippines<sep>Pitcairn<sep>Poland<sep>Portugal<sep>Puerto Rico<sep>Qatar<sep>Reunion<sep>Romania<sep>Russian Federation<sep>Rwanda<sep>Saint Helena<sep>Saint Kitts and Nevis<sep>Saint Lucia<sep>Saint Pierre and Miquelon<sep>Saint Vincent and The Grenadines<sep>Samoa<sep>San Marino<sep>Sao Tome and Principe<sep>Saudi Arabia<sep>Senegal<sep>Serbia and Montenegro<sep>Seychelles<sep>Sierra Leone<sep>Singapore<sep>Slovakia<sep>Slovenia<sep>Solomon Islands<sep>Somalia<sep>South Africa<sep>South Georgia and The South Sandwich Islands<sep>Spain<sep>Sri Lanka<sep>Sudan<sep>Suriname<sep>Svalbard and Jan Mayen<sep>Swaziland<sep>Sweden<sep>Switzerland<sep>Syrian Arab Republic<sep>Taiwan, Province of China<sep>Tajikistan<sep>Tanzania<sep>Thailand<sep>Timor-leste<sep>Togo<sep>Tokelau<sep>Tonga<sep>Trinidad and Tobago<sep>Tunisia<sep>Turkey<sep>Turkmenistan<sep>Turks and Caicos Islands<sep>Tuvalu<sep>Uganda<sep>Ukraine<sep>United Arab Emirates<sep>United Kingdom<sep>United States<sep>United States Minor Outlying Islands<sep>Uruguay<sep>Uzbekistan<sep>Vanuatu<sep>Venezuela<sep>Viet Nam<sep>Virgin Islands, British<sep>Virgin Islands, U.S.<sep>Wallis and Futuna<sep>Western Sahara<sep>Yemen<sep>Zambia<sep>Zimbabwe");
			setVar("number", "itemHeight",30);
			setVar("number","fontSize",18);
			setVar("uint","numberOfItemsShown",0);
			setVar("string","label","label");
			super.setVariables(list);
		}
		
		private function createStim():void
		{
			comboBox = new ComboBox(null,0,0,getVar("label"),null,getVar("fontSize"));
			comboBox.setSize(pic.width,pic.height);
			comboBox.listItemHeight=getVar("itemHeight") as Number;
			
			if(getVar("numberOfItemsShown")!=0)comboBox.numVisibleItems=getVar("numberOfItemsShown");
			
			
			for each(var item:String in (getVar("items") as String).split("<sep>")){
				comboBox.addItem(item);
			}
			pic.graphics.clear();
			pic.addChild(comboBox);

		}
		
	
		override public function RunMe():uberSprite {
			pic.graphics.drawRect(0,0,1,1); //needs to have A size as setUniversalVariables does resizing and cannot resize 
			super.setUniversalVariables();
			
			createStim()
			
			pic.scaleX=1;
			pic.scaleY=1;
			return (super.pic);
		}

	}
}