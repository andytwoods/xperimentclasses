﻿package  com.xperiment.stimuli{

	import flash.display.*;
	import fl.controls.Button;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.EventDispatcher;
	import flash.text.TextFormat;
	import flash.events.TouchEvent;
	import flash.ui.Multitouch;
	import flash.ui.MultitouchInputMode;
	import flash.events.TransformGestureEvent;
	import com.xperiment.codeRecycleFunctions;
	import com.xperiment.uberSprite;
	import com.stimuli.addButton;

	public class addSwipeMenu extends addButton {

		override public function returnsDataQuery():Boolean {
			return true;
		}

		public var button2:Button = new Button();

		//public function Trial_imageCollage(genAttrib:XMLList, specAttrib:XMLList) {
		override public function setVariables(list:XMLList) {

			setVar("string","textOfButtons","next trial&&back");
			setVar("string","orientation","horizontal");
			setVar("uint","seperation",10);
			setVar("boolean", "showButtons", false);
			super.setVariables(list);


		}


		private function swipeMade(e:TransformGestureEvent) {
			button1.visible=true;
			button2.visible=true;
			button1.addEventListener(MouseEvent.CLICK,button1Click);
			button2.addEventListener(MouseEvent.CLICK,button2Click);
			theStage.removeEventListener(TransformGestureEvent.GESTURE_SWIPE, swipeMade);
		}


		override public function RunMe():uberSprite {

			theStage.addEventListener(TransformGestureEvent.GESTURE_SWIPE, swipeMade);


			_simpleTextFormat.color=getVar("colour");
			_simpleTextFormat.size=getVar("size");

			button1.visible=false;
			button2.visible=false;

			button1.width=uint(codeRecycleFunctions.multipleTrialCorrection(getVar("width"),"&&",0));
			button1.height=uint(codeRecycleFunctions.multipleTrialCorrection(getVar("height"),"&&",0));
			button1.name=codeRecycleFunctions.multipleTrialCorrection(getVar("resultFileName"),"&&",0);
			button1.setStyle("textFormat", _simpleTextFormat);
			button1.label=codeRecycleFunctions.multipleTrialCorrection(getVar("textOfButtons"),"&&",0);
			button1.emphasized=Boolean(codeRecycleFunctions.multipleTrialCorrection(getVar("emphasized"),"&&",0));
			button1.x=button2.x+(button2.width)+.5*getVar("seperation");


			button2.width=uint(codeRecycleFunctions.multipleTrialCorrection(getVar("width"),"&&",1));
			button2.height=uint(codeRecycleFunctions.multipleTrialCorrection(getVar("height"),"&&",1));
			button2.name=codeRecycleFunctions.multipleTrialCorrection(getVar("resultFileName"),"&&",1);
			button2.setStyle("textFormat", _simpleTextFormat);
			button2.label=codeRecycleFunctions.multipleTrialCorrection(getVar("textOfButtons"),"&&",1);
			button2.emphasized=Boolean(codeRecycleFunctions.multipleTrialCorrection(getVar("emphasized"),"&&",1));
			button2.x=button2.x-(button2.width)-.5*getVar("seperation");

			if (getVar("showButtons")) {
				button1.visible=true;
				button2.visible=true;
			}

			if (getVar("orientation")=="horizontal") {
				button1.x=(returnStageWidth*getVar("xPosPercent")*.01)+(.5*getVar("seperation"));
				button2.x=(returnStageWidth*getVar("xPosPercent")*.01)-button2.width-(.5*getVar("seperation"));
				button1.y=(returnStageHeight*getVar("yPosPercent")*.01)-(.5*button1.height);
				button2.y=(returnStageHeight*getVar("yPosPercent")*.01)-(.5*button1.height);
			}
			else {
				button1.y=(returnStageHeight*getVar("yPosPercent")*.01)-button1.height-(.5*getVar("seperation"));
				button2.y=(returnStageHeight*getVar("yPosPercent")*.01)+(.5*getVar("seperation"));
				button1.x=(returnStageWidth*getVar("xPosPercent")*.01)-(.5*button1.width);
				button2.x=(returnStageWidth*getVar("xPosPercent")*.01)-(.5*button1.width);
			}

			super.pic.addChild(button1);
			super.pic.addChild(button2);

			button1.version=getVar("IdriveWhichEvent");//AW: a total BODGE which I've used to get past the annoying issue of not passing variables via Event.


			super.setUniversalVariables();
			return (pic);
		}



/*		override public function setPercentageSize() {
			if (getVar("widthPercent")!=0) {
				logger.log("truetrue");
				pic.width=returnStageWidth*Number(getVar("widthPercent"))*.01;
				pic.x=(returnStageWidth-pic.width)/2;
			}


			if (getVar("heightPercent")!=0) {
				pic.height=returnStageHeight*Number(getVar("heightPercent"))*.01;
				pic.y=(returnStageHeight-pic.height)/2;
			}
		}


		override public function setPosPercent() {

		}

*/

		public function button1Click(e:MouseEvent):void {
			var tempData:Array = new Array();
			theStage.addEventListener(TransformGestureEvent.GESTURE_SWIPE, swipeMade);
			tempData.event=String("buttonPressed_Count"+buttonCount);
			tempData.data=e.target.name;
			objectData.push(tempData);
			dispatchEvent(new Event("buttonWasClicked"));
			if (getVar("eventDriver")== true) {
				dispatchEvent(new Event("runDrivenEvent"));
			}
			logger.log("button pressed: "+e.target.name);
			buttonCount++;
		}

		public function button2Click(e:MouseEvent):void {
			theStage.addEventListener(TransformGestureEvent.GESTURE_SWIPE, swipeMade);
			button1.removeEventListener(MouseEvent.CLICK,button1Click);
			button2.removeEventListener(MouseEvent.CLICK,button2Click);
			button1.visible=false;
			button2.visible=false;



			buttonCount++;
		}

	}
}