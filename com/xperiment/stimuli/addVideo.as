﻿package  com.xperiment.stimuli{
	
	import com.bulkloader.preloadFilesFromWeb;
	import com.xperiment.ExptWideSpecs;
	import com.xperiment.uberSprite;
	
	import flash.events.*;
	import flash.media.SoundTransform;
	import flash.media.Video;
	import flash.net.NetStream;
	
	
	
	public class addVideo extends object_baseClass implements IgivePreloader{
		
		private var myVideo:Video;
		
		private var MyNS:NetStream;
		private var preloader:preloadFilesFromWeb;
		private var addedToStage:Boolean=false;
		private var repeatCount:uint=0;
		private var localStorageLocation:Object;
		
		override public function kill():void{
			if(pic.hasEventListener(Event.ADDED_TO_STAGE))pic.removeEventListener(Event.ADDED_TO_STAGE, startVideoWhenNotAddedToStage);
			if(MyNS.hasEventListener(NetStatusEvent.NET_STATUS))MyNS.removeEventListener(NetStatusEvent.NET_STATUS, detectEnd);
			myVideo = null;
			MyNS.close();
			MyNS = null;
		}
		
		override public function myUniqueActions(action:String):Function{
			switch(action){
				case "pause()": 		return function():void{MyNS.pause();} ; 			break;
				
				case "start()": 		return function():void{beginVideo();} ; 			break;
				
				case "togglePause()": 	return function():void{MyNS.togglePause();}; 		break;
				
			}
			return null;
		}
		
		
		//public function Trial_imageCollage(genAttrib:XMLList, specAttrib:XMLList) {
		override public function setVariables(list:XMLList):void {
			
			//setVar("string","soundTransform","");
			setVar("number","startVideoAtTime",0);
			setVar("number","volume",0);
			setVar("string","filename","");
			setVar("string","onFinish","hide","can equal hide,restart,repeatx2,repeatx3 etc");  
			super.setVariables(list);
			if(getVar("filename")=="" && OnScreenElements.filename!="")OnScreenElements.filename=list.@filename;
			
			if(!preloader)initVideo(false);
			if(preloader){
				if(preloader.isFinished())initVideo(true);
				else{
					//preloader not finished so must listen and wait...
					preloader.informWhenPreloaderFinished(function():void{ //passes a function to Preloader which is called when prelaoder is finished
						initVideo(true);
					})
				}
			}
		}
		
		public function getTime():String{	
			var tim:String=String(MyNS.time*1000);
			if(tim.indexOf(".")!=-1)tim=tim.substr(0,tim.indexOf("."));// on the very rare occasion, tim goes bananas and ends up with loads of digits.  Stupid flash bug.
			return tim;
		}
		
		public function passPreloader(preloader:preloadFilesFromWeb):void {
			this.preloader=preloader;
		}
		
		override public function RunMe():uberSprite {
			addedToStage=true;	
			if(myVideo)commence();
			return (pic);
		}
		
		private function commence():void
		{
			myVideo.visible=true;
			beginVideo();
			super.setUniversalVariables();
		}		
		
		private function beginVideo():void{
			MyNS.resume();
			if(!MyNS.hasEventListener(NetStatusEvent.NET_STATUS))MyNS.addEventListener(NetStatusEvent.NET_STATUS, detectEnd);
		}
		
		private function detectEnd(e:NetStatusEvent):void {
			
			if(e.info.code=="NetStream.Play.Stop")
			{
				//manageBehaviours.behaviourFinished(this);
				var fin:String=getVar("onFinish");
				switch(fin.substr(0,4)){
					case "rest":
						MyNS.seek(0);
						break;
					case "repe":
						
						if((fin.split(["x"] as Array).length)>1){
							if(!isNaN(fin.split("x")[1]) && int(fin.split("x")[1])<repeatCount) {
								MyNS.seek(0);
								repeatCount++;
							}
						}
						break;
					default:
						myVideo.visible=false;
						break;
					
				}
				
				
			}
		}
		
		
		private function initVideo(isLoaded:Boolean=false):void{
			
			myVideo=new Video;
			
			
			MyNS = preloader.give(getVar("filename"));
			MyNS.pause();
			pic.visible=false;
			var customClient:Object = new Object();
			customClient.onMetaData = function(infoObject:Object):void{
				if(getVar("width")=="0"){pic.width = myVideo.width = infoObject.width;setVar("string","width",pic.width);}
				if(getVar("height")=="0"){pic.height = myVideo.height = infoObject.height;setVar("string","height",pic.height);}
				setUniversalVariables();
				pic.visible=true;
				MyNS.resume();
			}
			
			MyNS.client = customClient;
			
			myVideo.attachNetStream(MyNS);
			MyNS.addEventListener(AsyncErrorEvent.ASYNC_ERROR, asyncErrorHandler); 
			
			localStorageLocation= ExptWideSpecs.ExptWideSpecs.fileInformation.stimuliFolder;
			if (localStorageLocation!="" && localStorageLocation!="assets/")localStorageLocation=localStorageLocation+"/";
			
			
			if(getVar("volume") as Number!=1) volume(getVar("volume") as Number);
			MyNS.pause();
			pic.addChild(myVideo);	
			if(!addedToStage){
				pic.addEventListener(Event.ADDED_TO_STAGE, startVideoWhenNotAddedToStage);
			}
			else {
				MyNS.play(localStorageLocation+getVar("filename"));
				commence();
				
			}
			
			
		}
		
		private function volume(vol:Number):void{
			var videoVolumeTransform:SoundTransform = new SoundTransform();
			videoVolumeTransform.volume = vol;
			MyNS.soundTransform = videoVolumeTransform;
		}
		
		
		private function asyncErrorHandler(event:AsyncErrorEvent):void 
		{
			trace("error in addVideo: "+event.text);
		}
		
		
		
		private function removeVideo(event:Event):void {
			super.pic.removeChild(myVideo);
			myVideo.removeEventListener("complete", removeVideo);
		}
		
		
		private function startVideoWhenNotAddedToStage(e:Event):void {			
			if(!addedToStage)commence();
			MyNS.play(localStorageLocation+getVar("filename"));
		}
		
		
	}
}