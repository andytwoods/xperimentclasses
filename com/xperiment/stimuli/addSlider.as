﻿package  com.xperiment.stimuli{
	
	import com.bit101.components.Style;
	import com.xperiment.codeRecycleFunctions;
	import com.xperiment.uberSprite;
	
	import flash.display.*;
	import flash.display.Stage;
	import flash.events.*;
	import flash.events.MouseEvent;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.utils.Timer;
	
	
	
	public class addSlider extends object_baseClass {
		
		private var j:uint;
		private var padding:uint=10;
		public var verticalSpacing:uint=30;
		public var track:uberSprite=new uberSprite  ;
		public var trackBG:uberSprite=new uberSprite  ;
		public var slider:uberSprite=new uberSprite  ;
		public var lowVal:Number=0;
		public var highVal:Number;
		public var range:Number;
		public var sliderAxis:String;
		public var startVal:Number;
		public var changeProp:String;
		public var triangleTLD:uint;
		public var triangleHeight:uint;
		public var flipPointerFix:int;
		public var startTime:Date;
		public var endTime:Date;
		public var timeWhenMouseReleased:Array=new Array  ;
		public var timer:Boolean=false;
		public var myTimer:Timer=new Timer(1);
		private var maxLabelHeight:uint=0;
		
		private var _labelList:Array;
		private var _labelLocation:Array;
		private var _myTextFormat:TextFormat=new TextFormat  ;
		private var scale:uberSprite=new uberSprite  ;
		private var sliderMoved:Boolean=false;
		private var sliderTouched:Boolean=false;
		private var myTextFormat:TextFormat=new TextFormat  ;
		private var trackTouched:Boolean=false;
		private var overlay:Sprite;
		
		/*	<addSlider x="50%" y="45%" width="50%" height="10%"  
		sliderTitle="How intense is this odour?" percentageScreenSizeFrom="horizontal"  hidePointerAtStart="false" 
		labelList="not detectable&&very strong---completely unknown&&very familar---disgusting&&extremely pleasant" startVal="50" 
		labelLocation="0&&100"distBetweenLabelsAndScale="2" length="85%" pointerSize="5%" resultFileName="intensity---familiarity---pleasantness" />  */
		
		
		//BEHAVIOURS:
		//mouseButtonUp
		override public function setVariables(list:XMLList):void {
			setVar("number","startVal",-1,"-1-100","-l means that the pointer is placed at a random location");
			setVar("string","sliderAxis","x","x||y");
			setVar("uint","pointerSize","20");
			setVar("uint","lineThickness",2);
			setVar("uint","triangleToLineDistance",5);
			setVar("boolean","flipPointerOver",true);
			setVar("string","timer","false","false||allEvents","allEvents means the time that everytime the scale is interacted with");
			setVar("string","labelList","not detectable&&very strong");
			setVar("string","labelLocation","0&&100","int&&int&&...int","seperate each label with &&");
			setVar("int","distBetweenLabelsAndScale",10);
			setVar("number","colour",0x111999);
			setVar("uint","size",10);
			setVar("uint","titleSize",20);
			//setVar("string","alignment","LEFT");
			setVar("uint","tagLength",5,"","the length of the dashes on the line scale");
			setVar("number","verticalJiggerTextPos",-12,"","allows you to shift the text position up and down");
			setVar("number","horizontalJiggerTextPos",-30,"","allows you to shift the text position left and right");
			setVar("number","sliderColour",Style.BUTTON_FACE);
			setVar("boolean","dontRecordWhenNoResponseMade",true);
			setVar("boolean","hidePointerAtStart",true);
			setVar("uint","backgroundClickBoxSize",30,"","the blank region around the scale where you can interact with the scale");
			setVar("uint","textInputBoxColour",0x000000);
			setVar("uint","textInputBoxFontSize",15);
			setVar("string","resultFileName","setMeNextTime");
			setVar("string","lineColour",0xffffff);
			setVar("string","sliderTitle","");
			setVar("uint","distBetweenScaleAndTitle",20);
			setVar("boolean","lockToLabels",false,"","forces your participants to select a value - no intermediary values");
			
			super.setVariables(list);		
			
			
			
			setVar("boolean","hideResults",true,"","you don't care about this data and it can be discarded");
		}
		
		override public function onLoaded():void {
			myTimer.start();
		}
		
		override public function returnsDataQuery():Boolean {
			return true;
		}
		
		
		/*		override public function setContainerSize():void {
		pic.myWidth=getVar("scaleX")*(track.width+track.x);
		pic.myHeight=getVar("scaleY")*codeRecycleFunctions.biggestInArray([track.height+track.y+maxLabelHeight,slider.height+slider.y+maxLabelHeight]);
		}
		
		*/
		override public function myScore():String {
			var dat:Number;
			if (sliderMoved) {
				var currentVal:Number=Math.abs(slider[sliderAxis]-(track[sliderAxis]/track[changeProp]));
				dat=Math.round(10000*currentVal/getVar("length"))/100;
				if (getVar("lockToLabels")) {
					currentVal=nearestLabel(currentVal);
					dat=_labelList[_labelLocation.indexOf(currentVal)];
				}
			}
			else {
				dat=1000000;
			}
			return String(dat);
		}
		
		override public function storedData():Array {
			
			var tempData:Array=new Array  ;
			tempData.event="Slider-"+getVar("id");
			tempData.data=myScore();
			
			super.objectData.push(tempData);
			
			if (timer) {
				tempData=new Array  ;
				tempData.event="SliderTimer-"+getVar("resultFileName");
				if (sliderMoved) {
					if (getVar("timer")=="allEvents") {
						tempData.data=String(timeWhenMouseReleased);
					}
					else {
						tempData.data=timeWhenMouseReleased[0];
					}
				}
				else {
					tempData.data="slider not moved";
				}
				super.objectData.push(tempData);
			}
			
			
			slider.removeEventListener(MouseEvent.MOUSE_DOWN,handleMouseDown);
			if(theStage && super.theStage.hasEventListener(MouseEvent.MOUSE_UP))super.theStage.removeEventListener(MouseEvent.MOUSE_UP,handleMouseUp);
			if (timer) {
				myTimer.stop();
			}
			
			return super.objectData;
		}
		
		public function isVertical():Boolean {
			if (sliderAxis=="x") {
				return false;
			}
			else {
				return true;
			}
		}
		
		override public function RunMe():uberSprite {
			pic.graphics.drawRect(0,0,1,1);
			super.setUniversalVariables();
			overlay=new Sprite;
			assignVariables();
			createSlider();
			composeLabels();
			if (getVar("lockToLabels")) lockToL();
			pic.scaleX=1;
			pic.scaleY=1;
			pic.addChild(overlay);
	
			
			//overlay.x=pic.width*.5-overlay.width*.5;
			//overlay.y=pic.height*.5-overlay.height*.5;
			
			
			return pic;
		}
		
		
		
		
		public function assignVariables():void {
			
			_myTextFormat.color=getVar("colour");
			_myTextFormat.size=getVar("size");
			_myTextFormat.align=TextFormatAlign.CENTER;
			
			_labelList=(getVar("labelList") as String).split("&&");
			_labelLocation=(getVar("labelLocation") as String).split("&&");
			//for (var i:uint=0;i<_labelLocation.length
			OnScreenElements.length=pic.width;
			sliderAxis=getVar("sliderAxis");
			highVal=getVar("length");
			startVal=getVar("startVal");
			triangleTLD=getVar("triangleToLineDistance");
			triangleHeight=getVar("pointerSize");
			flipPointerFix=0;
			if (getVar("flipPointerOver")==true) {
				flipPointerFix=-1;
			}
			if (getVar("timer")!="false") {
				timer=true;
			}
		}
		
		
		private function createTriangle(col:Number,size:uint):uberSprite {
			var tempTriangle:uberSprite=new uberSprite  ;
			tempTriangle.graphics.beginFill(col);
			if (sliderAxis=="x") {
				tempTriangle.graphics.moveTo(0,size);
				tempTriangle.graphics.lineTo(size/2,size*flipPointerFix+size);
				tempTriangle.graphics.lineTo(-size/2,size*flipPointerFix+size);
				tempTriangle.graphics.lineTo(0,size);
			}
			else {
				
				tempTriangle.graphics.moveTo(0,size);
				tempTriangle.graphics.lineTo(size*flipPointerFix,- size/2+size);
				tempTriangle.graphics.lineTo(size*flipPointerFix,size/2+size);
				tempTriangle.graphics.lineTo(0,size);
			}
			return tempTriangle;
		}
		
		public function createSlider():void {
			track.graphics.lineStyle(getVar("lineThickness"),getVar("lineColour"));
			trackBG.graphics.lineStyle(getVar("lineThickness"),getVar("lineColour"));
			
			if (sliderAxis=="x") {
				track.graphics.lineTo(highVal,0);
				trackBG.graphics.lineTo(highVal,0);
				track.graphics.moveTo(triangleHeight/2,0);
				trackBG.graphics.moveTo(triangleHeight/2,0);
			}
			else {
				track.graphics.lineTo(0,highVal);
				trackBG.graphics.lineTo(0,highVal);
				track.graphics.moveTo(0,-triangleHeight/2);
				trackBG.graphics.moveTo(0,-triangleHeight/2);
			}
			slider=createTriangle(getVar("sliderColour"),getVar("pointerSize"));
			
			
			if (getVar("hidePointerAtStart")) {
				slider.visible=false;
				trackBG.addEventListener(MouseEvent.MOUSE_DOWN,handleMouseDownTrack);
			}
			changeProp=sliderAxis=="x"?"width":"height";
			slider.buttonMode=true;
			
			if (getVar("startVal")==-1) {
				startVal=Math.random()*100;
			}
			
			startVal=highVal*startVal/100;
			
			if (sliderAxis=="x") {
				slider.x=track.x*track[changeProp]+startVal;
			}
			else {
				slider.y=track.y*track[changeProp]+startVal;
				slider.y=slider.y-startVal;
			}
			
			track.y=track.y+getVar("pointerSize");
			trackBG.y=trackBG.y+getVar("pointerSize");
			overlay.addChild(track);
			//super.pic.addChild(trackBG);
			overlay.addChild(slider);
			overlay.addChild(scale);
			
			slider.addEventListener(MouseEvent.MOUSE_DOWN,handleMouseDown);
			if(theStage)super.theStage.addEventListener(MouseEvent.MOUSE_UP,handleMouseUp);
		}
		
		private function handleMouseDownTrack(evt:MouseEvent):void {
			slider.visible=true;
			removeEventListener(MouseEvent.MOUSE_DOWN,handleMouseDownTrack);
			
			sliderMoved=true;
			sliderTouched=true;
			
			if (sliderAxis=="x") {
				slider.x=evt.localX;
			}
			else {
				slider.y=evt.localY;
			}
			
			if (sliderAxis=="x") {
				slider.startDrag(false,new Rectangle(0,0,highVal,0));
			}
			else {
				slider.startDrag(false,new Rectangle(0,0,0,highVal));
			}
			super.theStage.addEventListener(MouseEvent.MOUSE_UP,handleMouseUp);
			
			
		}
		
		
		private function handleMouseDown($evt:MouseEvent):void {
			super.theStage.addEventListener(MouseEvent.MOUSE_UP,handleMouseUp);
			trackTouched=true;
			sliderMoved=true;
			sliderTouched=true;
			if (sliderAxis=="x") {
				slider.startDrag(false,new Rectangle(0,0,highVal,0));
			}
			else {
				slider.startDrag(false,new Rectangle(0,0,0,highVal));
			}
			
		}
		/**
		 * Stops the slider dragging and timer.
		 */
		private function handleMouseUp($evt:MouseEvent):void {
			if (trackTouched) {
				manageBehaviours.actionHappened(this,"mouseButtonUp");
				lockToL();
				slider.stopDrag();
				super.theStage.removeEventListener(MouseEvent.MOUSE_UP,handleMouseUp);
				//logger.log(myTimer.currentCount);
				if (timer&&sliderTouched) {
					timeWhenMouseReleased.push(myTimer.currentCount);
				}
				sliderTouched=false;
				trackTouched=false;
			}
		}
		
		private function lockToL():void{
			if (getVar("lockToLabels")) {
				if (getVar("sliderAxis")=="x") {
					slider.x=nearestLabel(slider.x);
				}
				else {
					slider.y=nearestLabel(slider.y);
				}
				
			}
		}
		
		private function nearestLabel(pos:int):int {
			var returnMe:int=pos;
			var min:int=1000000;
			for (var i:uint=0; i<_labelLocation.length; i++) {
				if (min>Math.sqrt(Math.pow(_labelLocation[i]-pos,2))) {
					min=Math.sqrt(Math.pow(_labelLocation[i]-pos,2));
					returnMe=_labelLocation[i];
				}
			}
			return returnMe;
		}
		
		public function composeLabels():void {
			
			scale.graphics.lineStyle(getVar("lineThickness"),getVar("lineColour"));
			var tagL:int=getVar("tagLength");
			for (var i:uint; i<_labelLocation.length; i++) {
				_labelLocation[i]=codeRecycleFunctions.roundUpToNearest(highVal-(100-_labelLocation[i])/100*highVal,2);
				//has to round up as we've had to bodge _labelLocation[i] by subtracting 100 to get the right positioning.
				
				if (getVar("sliderAxis")=="x") {
					scale.graphics.moveTo(_labelLocation[i],- tagL/2+getVar("pointerSize"));
					scale.graphics.lineTo(_labelLocation[i],+ tagL/2+getVar("pointerSize"));
				}
				else {
					scale.graphics.moveTo(- tagL/2,_labelLocation[i]+getVar("pointerSize"));
					scale.graphics.lineTo(+ tagL/2,_labelLocation[i]+getVar("pointerSize"));
				}
			}
			for (i=0; i<_labelList.length; i++) {
				var tempTextField:TextField=scaleLabel(_labelList[i]);
				//tempTextField.width=200;
				if (getVar("sliderAxis")=="x") {
					tempTextField.x=_labelLocation[i]-(tempTextField.width/2);
					tempTextField.y=getVar("distBetweenLabelsAndScale")+getVar("pointerSize");
				}
				else {
					tempTextField.y=_labelLocation[i]+getVar("verticalJiggerTextPos")+getVar("pointerSize");
					tempTextField.x=getVar("distBetweenLabelsAndScale");
				}
				
				tempTextField.setTextFormat(myTextFormat);
				
				if (tempTextField.height>maxLabelHeight) {
					maxLabelHeight=tempTextField.height;
				}
				
				scale.addChild(tempTextField);
				
			}
			
			if (getVar("sliderTitle")!="") {
				tempTextField= new TextField;
				tempTextField.textColor=Style.LABEL_TEXT;
				tempTextField=scaleLabel(getVar("sliderTitle"));
				myTextFormat.size=getVar("titleSize");
				tempTextField.setTextFormat(myTextFormat);
				if (getVar("sliderAxis")=="x") {
					tempTextField.x=highVal-50/100*highVal-(tempTextField.width/2);
					tempTextField.y=getVar("distBetweenScaleAndTitle")+getVar("pointerSize");
				}
				else {
					tempTextField.y=highVal-50/100*highVal+getVar("verticalJiggerTextPos")+getVar("pointerSize");
					tempTextField.x=getVar("distBetweenScaleAndTitle");
				}
				scale.addChild(tempTextField);
			}
			
		}
		
		
		public function scaleLabel(t:String):TextField {
			var tt:TextField=new TextField  ;
			tt.textColor=Style.LABEL_TEXT;
			tt.text=t;
			tt.autoSize=TextFieldAutoSize.CENTER;
			tt.selectable=false;
			return tt;
		}
	}
}