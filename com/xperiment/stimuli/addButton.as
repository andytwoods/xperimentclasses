﻿package com.xperiment.stimuli{
	import com.bit101.components.PushButton;
	import com.xperiment.events.GotoTrialEvent;
	import com.xperiment.stimuli.object_baseClass;
	import com.xperiment.uberSprite;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	
	
	public class addButton extends object_baseClass {
		
		public var buttonCount:uint=1;
		private var button:PushButton;
		private var mouseDown4BehavBoss:Function;
		
		////BehaviourBoss Events links.
		////If BehaviourBoss finds the approp named function, passes it a function to call when the
		////given behaviour occurs.
		////
		
		
		override public function myUniqueActions(action:String):Function{
			
			switch(action){
				//case "banana": return function banana():void{trace ("baaaa");}
				
			}
			return null;
		}
		
		override public function myUniqueProps(prop:String):Function{
			
			switch(prop){
				case "text"	:		return function(what:String=null,to:String=null):String{
					if(what) button.label=to;
					return button.label;
				}; 	
					break;
			}
			return null;
		}
		
		public function mouseDown(md:Function):void{mouseDown4BehavBoss=md;}
		////
		////
		
		override public function setUpTrialSpecificVariables(trialObjs:Object):void {
			super.setUpTrialSpecificVariables(trialObjs);
		}
		
		
		
		
		override public function setVariables(list:XMLList):void {
			setVar("string","text","demo button");
			setVar("string","goto", "nextTrial");
			setVar("number","alpha",.9,"0-1");
			setVar("uint","fontSize",10);
			setVar("uint","width",80);
			setVar("uint","height",22);
			setVar("int","key",""); // use codes from here: http://www.asciitable.com/
			setVar("boolean","disableMouse",false);
			super.setVariables(list);
			
			
			if(getVar("behaviours")!="")setVar("string","goto","")
		}
		
		override public function RunMe():uberSprite {
			this.button=new PushButton(null,0,0,"",null,getVar("fontSize") as int);
			button.label=getVar("text");
			
			button.name="buttonAction";
			//button.setSize(getVar("width"),getVar("height"));
			events(true);
			pic.addChild(button);
			
			if(theStage)theStage.focus=pic;
			super.setUniversalVariables();
			button.width=pic.width/pic.scaleX;
			button.height=pic.height/pic.scaleY;
			
			return (pic);
		}
		
		protected function keyPressedUP(e:KeyboardEvent):void
		{
			if(e.keyCode==getVar("key") as int) {		
				button.dispatchEvent(new MouseEvent(MouseEvent.MOUSE_UP));
				
			}
		}
		
		protected function keyPressedDOWN(e:KeyboardEvent):void
		{
			//trace(e.type, e.keyCode, String.fromCharCode(e.keyCode),333,e.keyCode,getVar("key"));
			
			if(button && objectData && e.keyCode==getVar("key") as int) {
				
				if(getVar("ignoreData") as Boolean == false){
					var tempData:Array = new Array;
					tempData.event=String("buttonPressed_Count"+buttonCount);
					tempData.data=getVar("id");
					objectData.push(tempData);
				}
				
				checkIfGoto();
				
				if(button)button.dispatchEvent(new MouseEvent(MouseEvent.MOUSE_DOWN));
				if(button)button.dispatchEvent(new MouseEvent(MouseEvent.ROLL_OVER));		
			}
		}
		
		override public function kill():void {
			mouseDown4BehavBoss=null;
			removeEvents();
			button.removeListeners();
			button=null;
			super.kill();
		}
		
		override public function events(active:Boolean):void
		{
			if(active)addListeners();
			else removeEvents();
		}
		
		private function removeEvents():void
		{
			if(button.hasEventListener(MouseEvent.MOUSE_DOWN))this.removeEventListener(MouseEvent.MOUSE_DOWN, MouseDown);
			if(theStage && theStage.hasEventListener(KeyboardEvent.KEY_DOWN))theStage.removeEventListener(KeyboardEvent.KEY_DOWN,keyPressedDOWN);
			if(theStage && theStage.hasEventListener(KeyboardEvent.KEY_UP))theStage.removeEventListener(KeyboardEvent.KEY_UP,keyPressedUP);
		}
		
		private function addListeners():void
		{
			
			
			if(!(getVar("disableMouse") as Boolean))button.addEventListener(MouseEvent.MOUSE_DOWN, MouseDown,false,0,true);
			if(getVar("key")!="" && theStage){
				theStage.addEventListener(KeyboardEvent.KEY_DOWN,keyPressedDOWN,false,0,true);
				theStage.addEventListener(KeyboardEvent.KEY_UP,keyPressedUP,false,0,true);
			}			
		}
		
		public function MouseDown(e:MouseEvent):void {
			
			if(mouseDown4BehavBoss!=null)mouseDown4BehavBoss();
			
			e.stopPropagation();
			var tempData:Array = new Array;
			
			if(!(getVar("ignoreData") as Boolean)){
				//trace(getVar("ignoreData"),222);
				tempData.event=String("buttonPressed_Count"+buttonCount);
				tempData.data=getVar("id");
				objectData.push(tempData);
			}
			
			checkIfGoto();
			logger.log("button pressed: "+getVar("id"));
			buttonCount++;
		}
		
		private function checkIfGoto():void
		{
			
			if(this.actions && this.actions.hasOwnProperty("doBefore")){
				//manageBehaviours.doBehaviourFirst(this);
			}
			
			if (getVar("goto")!="" && theStage) {
				events(false);
				parentPic.dispatchEvent(new GotoTrialEvent(GotoTrialEvent.TRIAL_PING_FROM_OBJECT,getVar("goto")));
			}
		}
		
	}
}

