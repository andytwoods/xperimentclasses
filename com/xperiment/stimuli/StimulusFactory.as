package com.xperiment.stimuli
{
	import com.xperiment.admin.adminConditionList;

	import com.xperiment.stimuli.addBarGraph;
	import com.xperiment.stimuli.addBlank;
	import com.xperiment.stimuli.addButton;
	import com.xperiment.stimuli.addCode;
	import com.xperiment.stimuli.addColourArray;
	import com.xperiment.stimuli.addColourArrayXgoodXbad;
	import com.xperiment.stimuli.addComboBox;
	import com.xperiment.stimuli.addDropDownList;
	import com.xperiment.stimuli.addInputTextBox;
	import com.xperiment.stimuli.addJPG;
	import com.xperiment.stimuli.addJPGs;
	import com.xperiment.stimuli.addKeyPress;
	import com.xperiment.stimuli.addLoadingIndicator;
	import com.xperiment.stimuli.addMechanicalTurk;
	import com.xperiment.stimuli.addMultiNumberSelector;
	import com.xperiment.stimuli.addMultipleChoice;
	import com.xperiment.stimuli.addNextTrial;
	import com.xperiment.stimuli.addNumberSelector;
	import com.xperiment.stimuli.addQAs;
	import com.xperiment.stimuli.addSaveDataLocally;
	import com.xperiment.stimuli.addSequentialCounter;
	import com.xperiment.stimuli.addShapeMatrix;
	import com.xperiment.stimuli.addSlider;
	import com.xperiment.stimuli.addStyle;
	import com.xperiment.stimuli.addText;
	import com.xperiment.stimuli.addTickBox;
	import com.xperiment.stimuli.addVideo;
	import com.xperiment.behaviour.behavBevel;
	import com.xperiment.behaviour.behavBlur;
	import com.xperiment.behaviour.behavClick;
	import com.xperiment.behaviour.behavDrag;
	import com.xperiment.behaviour.behavDragToShiftingArea;
	import com.xperiment.behaviour.behavGotoTrial;
	import com.xperiment.behaviour.behavGradientFill;
	import com.xperiment.behaviour.behavHide;
	import com.xperiment.behaviour.behavIf;
	import com.xperiment.behaviour.behavLanguage;
	import com.xperiment.behaviour.behavLoad;
	import com.xperiment.behaviour.behavLogic;
	import com.xperiment.behaviour.behavNextTrial;
	import com.xperiment.behaviour.behavOpacity;
	import com.xperiment.behaviour.behavOutline;
	import com.xperiment.behaviour.behavPause;
	import com.xperiment.behaviour.behavRT;
	import com.xperiment.behaviour.behavRand;
	import com.xperiment.behaviour.behavRestart;
	import com.xperiment.behaviour.behavRotate;
	import com.xperiment.behaviour.behavSave;
	import com.xperiment.behaviour.behavSaveData;
	import com.xperiment.behaviour.behavSaveImage;
	import com.xperiment.behaviour.behavSearch;
	import com.xperiment.behaviour.behavShadow;
	import com.xperiment.behaviour.behavShake;
	import com.xperiment.behaviour.behavShufflePropertiesOfObjects;
	import com.xperiment.behaviour.behavSize;
	import com.xperiment.behaviour.behavSwap;
	import com.xperiment.behaviour.behavTextFilter;
	import com.xperiment.behaviour.behavTimer;
	import com.xperiment.container.containerHorizontal;
	import com.xperiment.container.containerHorizontalGrid;
	import com.xperiment.container.containerVertical;
	import com.xperiment.container.containerVerticalGrid;
	

	public class StimulusFactory
	{
		public static function Stimulus(stimName:String):IStimulus
		{
			//a hack to allow add and behav prefixes in the script
			if(stimName.substr(0,3)=="add")stimName=stimName.substr(3);
			else if (stimName.substr(0,5)=="behav")stimName=stimName.substr(5);
			
			//so, behaviours can be suffixed with B for simplicity
			else if (stimName.substr(0,1)=="B")stimName=stimName.substr(1);	

			
			switch(stimName.toLowerCase()){
				
				//case "svg": return new addSVG;
				//case "inconsole": return new adminConsole;
				//case "c0omplex_baseclass": return new addC0omplex_baseClass;
				//case "mergingsounds": return new addMergingSounds;
				//case "inputradiobuttons": return new addInputRadioButtons;
				//case "lms": return new addLMS;
				//case "dragtozoneobject": return new addDragToZoneObject;
				//case "youtube": return new addYouTube;
				//case "updateoverexperiment": return new addUpdateOverExperiment;
				//case "lams": return new addLAMS;
				//case "twodimensionalemotionscale": return new addTwoDimensionalEmotionScale;
				//case "text_quitexperimentsavedata": return new addText_QuitExperimentSaveData;
				//case "webcam": return new addWebCam;
				//case "distortioncam": return new addDistortionCam;
				//case "lscreenmode": return new fullScreenMode;
				//case "fixationcross": return new addFixationCross;
				//case "shape": return new addShape;
				case "bargraph": return new addBarGraph;
				case "blank": return new addBlank;
				case "button": return new addButton;
				case "code": return new addCode;
				case "colourarray": return new addColourArray;
				case "colourarrayxgoodxbad": return new addColourArrayXgoodXbad;
				case "combobox": return new addComboBox;
				case "dropdownlist": return new addDropDownList;
				case "inputtextbox": return new addInputTextBox;
				case "jpg": return new addJPG;
				case "jpgs": return new addJPGs;
				case "keypress": return new addKeyPress;
				case "loadingindicator": return new addLoadingIndicator;
				case "mechanicalturk": return new addMechanicalTurk;
				case "multinumberselector": return new addMultiNumberSelector;
				case "multiplechoice": return new addMultipleChoice;
				case "nexttrial": return new addNextTrial;
				case "numberselector": return new addNumberSelector;
				case "qas": return new addQAs;
				case "savedatalocally": return new addSaveDataLocally;
				case "sequentialcounter": return new addSequentialCounter;
				case "shapematrix": return new addShapeMatrix;
				case "slider": return new addSlider;
				case "style": return new addStyle;
				case "text": return new addText;
				case "tickbox": return new addTickBox;
				case "video": return new addVideo;
					
				case "conditionlist": return new adminConditionList;
					
				//behaviours
				case "bevel": return new behavBevel;
				case "blur": return new behavBlur;
				case "click": return new behavClick;
				case "drag": return new behavDrag;
				case "dragtoshiftingarea": return new behavDragToShiftingArea;
				case "gototrial": return new behavGotoTrial;
				case "gradientfill": return new behavGradientFill;
				case "hide": return new behavHide;
				case "if": return new behavIf;
				case "language": return new behavLanguage;
				case "load": return new behavLoad;
				case "logic": return new behavLogic;
				case "nexttrial": return new behavNextTrial;
				case "opacity": return new behavOpacity;
				case "outline": return new behavOutline;
				case "pause": return new behavPause;
				case "rand": return new behavRand;
				case "restart": return new behavRestart;
				case "rotate": return new behavRotate;
				case "rt": return new behavRT;
				case "save": return new behavSave;
				case "savedata": return new behavSaveData;
				case "saveimage": return new behavSaveImage;
				case "search": return new behavSearch;
				case "shadow": return new behavShadow;
				case "shake": return new behavShake;
				case "shufflepropertiesofobjects": return new behavShufflePropertiesOfObjects;
				case "size": return new behavSize;
				case "swap": return new behavSwap;
				case "textfilter": return new behavTextFilter;
				case "timer": return new behavTimer;
					
				case "containerhorizontal": return new containerHorizontal;
				case "containerhorizontalgrid": return new containerHorizontalGrid;
				case "containervertical": return new containerVertical;
				case "containerverticalgrid": return new containerVerticalGrid;
					
				

			}

			return null;
		}
	}
}



