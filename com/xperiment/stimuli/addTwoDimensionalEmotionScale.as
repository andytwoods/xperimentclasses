﻿package  com.xperiment.stimuli{


	import flash.display.*;
	import flash.display.Stage;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	import flash.utils.Timer;
	import flash.events.*;
	import flash.text.TextFormatAlign;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.geom.Matrix;
	import flash.utils.getTimer;
	import com.xperiment.uberSprite;
	import com.xperiment.stimuli.addButton;

	public class addTwoDimensionalEmotionScale extends addButton {

		private var j:uint;
		private var padding:uint=10;

		public var verticalSpacing:uint=30;
		public var track:uberSprite=new uberSprite  ;
		public var trackBG:uberSprite=new uberSprite  ;
		public var face:uberSprite=new uberSprite  ;
		public var lowVal:Number=0;
		public var highVal:Number;
		public var range:Number;
		public var startVal:Number;
		public var changeProp:String;
		public var triangleTLD:uint;
		public var triangleHeight:uint;
		public var flipPointerFix:int;
		public var startTime:Date;
		public var endTime:Date;
		public var timeWhenMouseReleased:Array=new Array  ;
		private var _XlabelList:Array;
		private var _XlabelLocation:Array;
		private var _YlabelList:Array;
		private var _YlabelLocation:Array;
		private var _myTextFormat:TextFormat=new TextFormat  ;
		private var scale:uberSprite=new uberSprite  ;
		private var ghostsArray:Array;
		var myTextFormat:TextFormat=new TextFormat  ;
		var eyes:uberSprite=new uberSprite  ;
		var mouth:uberSprite=new uberSprite  ;
		var my2DTimer:Timer;
		private var startingTime:Number;
		var eyeSize:int;
		var eyePositionShift:int=0;
		var shiftEyesUp:Number=0;
		var flipAxes:Boolean;
		var adjustEyeSize:Number=1;
		var Mouth:Number=1;
		var mouthSize:uint;
		var shiftMouthUp:int;
		var mouthRatio:Number;

		var thicknessofEyesLine:int;
		var eyeLinerColour:int;
		var thicknessofMouthLine:int;
		var mouthColour:int;
		var eyeColour:Number;
		var slowDownTail:uint;


		var myText:TextField=new TextField  ;//DELETE!

		//public function Trial_imageCollage(genAttrib:XMLList, specAttrib:XMLList) {
		override public function setVariables(list:XMLList) {
			setVar("number","length",200);
			setVar("number","startVal",-1);
			setVar("uint","lineThickness",2);
			setVar("string","XlabelList","");
			setVar("string","XlabelLocation","0&&100");
			setVar("string","YlabelList","");
			setVar("string","YlabelLocation","0&&100");
			setVar("uint","distBetweenLabelsAndScale",10);
			setVar("number","colour",0x111999);
			setVar("uint","size",10);
			setVar("string","alignment","LEFT");
			setVar("uint","tagLength",5);
			setVar("number","verticalJiggerTextPos",-12);
			setVar("number","faceColour",0x2147AB);
			setVar("boolean","dontRecordWhenNoResponseMade",true);
			setVar("boolean","hidePointerAtStart",true);
			setVar("uint","backgroundClickBoxSize",30);
			setVar("uint","textColour",0x000000);
			setVar("uint","textBackgroundColour",0xFFFFFF);
			setVar("uint","textFontSize",15);
			setVar("string","resultFileName","setMeNextTime");
			setVar("uint","numberTailSegments",10);
			setVar("number","tailDecayRate",96);
			setVar("uint","tickFreq",500);
			setVar("uint","thicknessofFaceLine",1);
			setVar("uint","thicknessofEyesLine",1);
			setVar("uint","thicknessofMouthLine",1);
			setVar("number","eyeColour",0x000000);
			setVar("number","mouthColour",0x000000);
			setVar("number","faceLineColour",0x000000);
			setVar("number","eyeLinerColour",0x000000);
			setVar("uint","eyeSize",10);
			setVar("int","shiftEyesRight",0);
			setVar("int","shiftEyesApart",20);
			setVar("string","flipTheAxes","no");
			setVar("uint","faceSize",20);
			setVar("uint","faceLocationX",0);
			setVar("uint","faceLocationY",0);
			setVar("uint","mouthSize",40);
			setVar("uint","shiftMouthUp",-20);
			setVar("uint","shiftEyesUp",-20);
			setVar("number","mouthRatio",1);
			setVar("uint", "slowDownTail", 1);
			setVar("number", "lineColour",0x000000);
			setVar("string", "showLocationOverTime", "false");
			setVar("number", "tailColour",0xFFF000);
			setVar("int","shiftYtextRight",-20);
			setVar("int","shiftXtextUp",20);
			super.setVariables(list);
			setVar("boolean","hideResults",true);
			//startTime=Date();
		}


		override public function returnsDataQuery():Boolean {
			if (getVar("hideResults")) return false;	
			else return true;
		}



		override public function onLoaded() {
			my2DTimer=new Timer(getVar("tickFreq"));
			my2DTimer.addEventListener(TimerEvent.TIMER, logData);
			my2DTimer.start();
			startingTime=getTimer();

			if (getVar("showLocationOverTime")=="true") {
				myText.text="hello";
				myText.x=-150;
				myText.y=-150;
				myText.textColor=getVar("textColour");
				myText.width=200;
				pic.addChild(myText);
			}
		}

		var twoDiStoredData:Array=new Array  ;


		function logData(event:TimerEvent):void {
			var temptwoDiStoredData:Array=new Array  ;

			var xVal:Number =int(((face.x)/highVal)*1000)/10;
			var yVal:Number =int((1-((face.y)/highVal))*1000)/10;

			temptwoDiStoredData.push(my2DTimer.currentCount);
			temptwoDiStoredData.push(xVal);
			temptwoDiStoredData.push(yVal);
			temptwoDiStoredData.push(getTimer()-startingTime);
			twoDiStoredData.push(temptwoDiStoredData);

			if (getVar("showLocationOverTime")=="true") {
				myText.text=("@" + (getTimer()-startingTime) + "ms" + ",X" + xVal+",Y"+yVal);
			}
		}




		override public function storedData():Array {//ppp
			my2DTimer.stop();
			var tempData:Array=new Array  ;
			var combinedStoredDataString:String=new String  ;

			for (var datTypes:uint=0; datTypes<twoDiStoredData[0].length; datTypes++) {
				for (var numPoints:uint=0; numPoints<twoDiStoredData.length; numPoints++) {
					combinedStoredDataString=combinedStoredDataString+","+twoDiStoredData[numPoints][datTypes];

				}
			}

			tempData.event="face_"+getVar("id");
			tempData.data=combinedStoredDataString;
			super.objectData.push(tempData);

			face.removeEventListener(MouseEvent.MOUSE_DOWN,handleMouseDown);
			//super.theStage.removeEventListener(MouseEvent.MOUSE_UP,handleMouseUp);

			return super.objectData;
			//return objectData;
		}



		override public function RunMe():uberSprite {

			myTextFormat.color=getVar("textColour");
			myTextFormat.size=getVar("textFontSize");
			eyePositionShift=getVar("shiftEyesApart");
			mouthRatio=getVar("mouthRatio");
			eyeSize=getVar("eyeSize");
			mouthSize=getVar("mouthSize");
			shiftMouthUp=-1*getVar("shiftMouthUp");
			eyeColour=getVar("eyeColour");
			slowDownTail=getVar("slowDownTail");

			if (getVar("flipTheAxes")=="no") {
				flipAxes=false;
			} else {
				flipAxes=true;
			}
			shiftEyesUp=getVar("shiftEyesUp");
			assignVariables();

			createTrack();
			createFace();
			composeLabels();


			super.pic.addChild(scale);
			super.pic.addChild(track);
			super.pic.addChild(trackBG);
			super.pic.addChild(face);

			super.setUniversalVariables();

			return pic;

		}


		public function assignVariables() {

			_myTextFormat.color=getVar("textColour");
			_myTextFormat.size=getVar("size");
			

			_XlabelList=generateArray(getVar("XlabelList"),"&&");
			_YlabelList=generateArray(getVar("YlabelList"),"&&");

			var tempText:String=getVar("XlabelLocation");
			_XlabelLocation=tempText.split("&&");
			tempText=getVar("YlabelLocation");
			_YlabelLocation=tempText.split("&&");
			highVal=getVar("length");
			startVal=getVar("startVal");

			thicknessofEyesLine=getVar("thicknessofEyesLine");
			eyeLinerColour=getVar("eyeLinerColour");
			thicknessofMouthLine=getVar("thicknessofMouthLine");
			mouthColour=getVar("mouthColour");


		}


		public function generateArray(t:String,divider:String):Array {
			return t.split(divider);
		}



		private function createFace() {

			face.graphics.lineStyle(getVar("thicknessofFaceLine"),getVar("faceLineColour"));
			face.graphics.beginFill(getVar("faceColour"));
			face.graphics.drawCircle(0, 0, getVar("faceSize"));

			range=highVal;
			face.buttonMode=true;

			if (getVar("startVal")==-1) {
				startVal=Math.random()*100;
			}
			startVal=highVal*startVal/100;

			face.x=track.x+(.5*track.width)+getVar("faceLocationX");
			face.y=track.y+(.5*track.height)-getVar("faceLocationY");
			face.addChild(eyes);
			face.addChild(mouth);
			addEyesAndMouth();
			//face.addEventListener(MouseEvent.MOUSE_DOWN,handleMouseDown);
			super.theStage.addEventListener(MouseEvent.MOUSE_DOWN,handleMouseDown);


		}


		public function createTrack() {//xxx


			track.graphics.lineStyle(getVar("lineThickness"),getVar("lineColour"));
			trackBG.graphics.lineStyle(getVar("backgroundClickBoxSize"),0,0);

			track.graphics.moveTo(0,highVal/2);
			trackBG.graphics.moveTo(0,highVal/2);


			track.graphics.lineTo(highVal,highVal/2);
			trackBG.graphics.lineTo(highVal,highVal/2);

			track.graphics.moveTo(highVal/2,0);
			trackBG.graphics.moveTo(highVal/2,0);

			track.graphics.lineTo(highVal/2,highVal);
			trackBG.graphics.lineTo(highVal/2,highVal);

		}


		var Tail:Array=new Array  ;
		var tailCounter:uint=1;

		private function addTail() {

			if (tailCounter==slowDownTail||slowDownTail==1) {


				for (var i:uint=0; i<Tail.length; i++) {

					if (i>=getVar("numberTailSegments")) {
						pic.removeChild(Tail[i]);
						Tail.pop();
					} else {
						var xshift:uint=Tail[i].width;
						var yshift:uint=Tail[i].height;
						Tail[i].scaleX=Tail[i].scaleX*getVar("tailDecayRate")/100;
						Tail[i].scaleY=Tail[i].scaleY*getVar("tailDecayRate")/100;

						Tail[i].alpha=Tail[i].alpha-(1-getVar("tailDecayRate")/100);

						xshift =(xshift- Tail[i].width) / 2;
						yshift =(yshift- Tail[i].height) / 2;
						Tail[i].x=Tail[i].x+xshift;
						Tail[i].y=Tail[i].y+yshift;
					}
				}

				var segment:uberSprite=new uberSprite  ;

				segment.graphics.lineStyle(2);
				segment.graphics.beginFill(getVar("tailColour"),.5);
				segment.graphics.drawCircle(0, 0, getVar("faceSize"));
				segment.alpha=.9;
				segment.graphics.endFill();
				//segment.scaleX=.05*getVar("faceSize");
				//segment.scaleY=.05*getVar("faceSize");
				segment.x=face.x;
				segment.y=face.y;

				Tail.unshift(segment);
				pic.addChildAt(segment,0);


			}

			if (slowDownTail!=1) {
				tailCounter++;
			}


			if (tailCounter>slowDownTail&&slowDownTail!=1) {
				tailCounter=1;
			}
		}

		private function updateFace(e:MouseEvent) {
			addTail();
			addEyesAndMouth();
		}

		function addEyesAndMouth() {
			if (flipAxes) {
				addEyes(face.x/highVal);
				addMouth(face.y/highVal);

			} else {
				addEyes(1-(face.y/highVal));
				addMouth(face.x/highVal);

			}
		}


		function addEyes(adjust:Number) {

			eyes.graphics.clear();

			eyes.graphics.lineStyle(thicknessofEyesLine,eyeLinerColour);
			eyes.graphics.beginFill(eyeColour);
			//eyes.graphics.drawEllipse(45+eyePositionShift,shiftEyesUp+47-(adjustEyeSize*.5),eyeSize,adjustEyeSize);
			//eyes.graphics.drawEllipse(124+eyePositionShift,shiftEyesUp+47-(adjustEyeSize*.5),eyeSize,adjustEyeSize);
			eyes.graphics.drawEllipse(-(eyePositionShift/2)-(.5*eyeSize),0-(.5*eyeSize*adjust)-shiftEyesUp,eyeSize,eyeSize*adjust);
			eyes.graphics.drawEllipse(+(eyePositionShift/2)-(.5*eyeSize),0-(.5*eyeSize*adjust)-shiftEyesUp,eyeSize,eyeSize*adjust);

			//eyes.graphics.drawEllipse(+eyePositionShift/2,shiftEyesUp,eyeSize,adjustEyeSize);
			eyes.graphics.endFill();

		}

		function addMouth(mouthShift:Number) {
			mouth.graphics.clear();
			mouth.graphics.lineStyle(thicknessofMouthLine,mouthColour);
			mouth.graphics.moveTo(-mouthSize/2,shiftMouthUp);
			mouth.graphics.curveTo(0,(mouthShift-.5)*mouthSize+shiftMouthUp,mouthSize/2,shiftMouthUp);
		}


		function handleMouseDown(evt:MouseEvent):void {
			face.startDrag(false,new Rectangle(0,0,highVal,highVal));
			super.theStage.addEventListener(MouseEvent.MOUSE_UP,handleMouseUp);
			super.theStage.addEventListener(MouseEvent.MOUSE_MOVE, updateFace);

		}


		function handleMouseUp($evt:MouseEvent):void {
			face.stopDrag();
			super.theStage.removeEventListener(MouseEvent.MOUSE_UP,handleMouseUp);
			super.theStage.removeEventListener(MouseEvent.MOUSE_MOVE, updateFace);
		}


		public function composeLabels() {
			scale.graphics.lineStyle(getVar("lineThickness"));
			var tempNum:Number;
			var tagL=getVar("tagLength");



			for (var i:uint=0; i<_YlabelLocation.length; i++) {
				tempNum=_XlabelLocation[i];
				_XlabelLocation[i]=highVal-tempNum/100*highVal;

				scale.graphics.moveTo(_XlabelLocation[i],- tagL/2+(highVal*.5));
				scale.graphics.lineTo(_XlabelLocation[i],+ tagL/2+(highVal*.5));

			}

			for (i=0; i<_YlabelLocation.length; i++) {
				tempNum=_YlabelLocation[i];
				_YlabelLocation[i]=highVal-tempNum/100*highVal;
				scale.graphics.moveTo(- tagL/2+(highVal*.5),_YlabelLocation[i]);
				scale.graphics.lineTo(+ tagL/2+(highVal*.5),_YlabelLocation[i]);
			}

			for (i=0; i<_XlabelList.length; i++) {
				var XtempTextField:TextField=scaleLabel(_XlabelList[i]);
				XtempTextField.autoSize = "left";
				XtempTextField.background=true;
				XtempTextField.backgroundColor=getVar("textBackgroundColour");
				XtempTextField.x=_XlabelLocation[i]+getVar("verticalJiggerTextPos");
				XtempTextField.y=getVar("distBetweenLabelsAndScale")+(highVal*.5)-getVar("shiftXtextUp");
				XtempTextField.setTextFormat(myTextFormat);
				//XtempTextField.rotation=45;
				scale.addChild(XtempTextField);
			}

			for (i=0; i<_YlabelList.length; i++) {
				var YtempTextField:TextField=scaleLabel(_YlabelList[i]);
				XtempTextField.background=true;
				YtempTextField.backgroundColor=getVar("textBackgroundColour");
				YtempTextField.autoSize = "left";
				YtempTextField.y=_YlabelLocation[i]+getVar("verticalJiggerTextPos");
				YtempTextField.x=getVar("distBetweenLabelsAndScale")+(highVal*.5)+getVar("shiftYtextRight");
				YtempTextField.setTextFormat(myTextFormat);
				scale.addChild(YtempTextField);

			}

		}



		public function scaleLabel(t:String):TextField {
			var tt:TextField=new TextField  ;
			tt.background=true;
			tt.autoSize = "left";
			tt.backgroundColor=getVar("textBackgroundColour");
			tt.text=t;
			tt.selectable=false;
			return tt;
		}
	}
}