﻿package com.xperiment.stimuli{
	
	import com.bit101.components.Style;
	import com.xperiment.uberSprite;
	import com.xperiment.stimuli.object_baseClass;
	import flash.display.*;
	import flash.display.BitmapData;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextLineMetrics;
	
	public class addText extends object_baseClass {
		
		public var myTextFormat:TextFormat=new TextFormat  ;
		public var myText:TextField=new TextField;
		private var border:Shape;
		
		override public function setVariables(list:XMLList):void {
			
			setVariablesChild();
			super.setVariables(list);
			
			
		}
		
		private var preSortedHTML:String;
		
		public function text(str:String=""):String{
			if(arguments.length!=0){
				str=getVar("prefix")+str+getVar("suffix");
				preSortedHTML=str;
				myText.htmlText=sortOutHTML(preSortedHTML);
				//trace(myText.htmlText);
			}
			return myText.text;
		}
		
		public function htmlText(str:String=""):String{
			if(arguments.length!=0)text(str);
			return myText.htmlText;
		}
		
		public function concatText(str:String):void{
			//trace("concatText:"+str);
			if(str!="")
			{
				if(myText.htmlText=="")myText.htmlText=sortOutHTML(str);
				else myText.htmlText+=sortOutHTML(str);
			}
		}
		
		public function setVariablesChild():void{
			setVar("string","text","demo text");
			setVar("string","font",""); //can equal _sans, _serif, _typewriter
			setVar("string","colour",Style.LABEL_TEXT);
			setVar("number","background", "");
			setVar("number","border","0");
			setVar("boolean","multiLine",true);
			setVar("boolean","selectable",false);
			setVar("boolean","wordWrap",false);
			setVar("uint","fontSize",15);
			setVar("string","font",""); // _sans, _serif, _typewriter, ios:times
			setVar("string","autoSize","true"); //if false, ensures correct size
			setVar("string","align","");
			setVar("Number","borderAlpha",Style.borderAlpha);
			setVar("Number","borderThickess",Style.borderWidth);
			setVar("Number","borderColour",Style.borderColour);
			setVar("number","exactSpacing",0);
			setVar("string","replace1","");
			setVar("string","suffix","");
			setVar("string","prefix","");
			setVar("uint","multiplySpaces",1);
		}
		
		
		public function giveBasicStimulus(obj:Object):Sprite {
			setVariablesChild();
			for (var i:* in obj){
				OnScreenElements[i]=obj[i];
			}	
			setupText();
			var tempSpr:Sprite=new Sprite;
			if(obj.name)myText.name=obj.name;
			tempSpr.addChild(myText);
			if(obj.name)tempSpr.name=obj.name;
			autoSize(false);
			return tempSpr;
		}
		
		//override public function doScaling(scaleX:Number,scaleY:Number):void{
		//if(getVar("fontSize")=="")pic.scaleX=scaleX; pic.scaleY=scaleY;
		//}
		
		
		public function giveBasicBMPStimulus(obj:Object):Sprite {
			setVariablesChild();
			for (var i:* in obj){
				OnScreenElements[i]=obj[i];
			}
			setupText();
			if(!obj.width)obj.width=myText.width;
			if(!obj.height)obj.height=myText.height;
			var myBitmapData:BitmapData = new BitmapData(obj.width, obj.height, true, 0x00000000);
			myBitmapData.draw(myText);
			var bmp:Bitmap = new Bitmap(myBitmapData);
			var tempSpr:Sprite=new Sprite;
			tempSpr.addChild(bmp);
			autoSize(false);
			return tempSpr;
		}	
		
		override public function setContainerSize():void {
			if(getVar("exactSpacing")==0){
				myText.width=pic.width;
				myText.height=pic.height;
				
				pic.width=getVar("scaleX")*myText.width;
				pic.height=getVar("scaleY")*myText.height;
				myText.x=0;
				myText.y=0;
			}
			super.setContainerSize();
		}
		
		public function replaceText(str:String,params:String,count:uint=1):String {
			var arr:Array = params.split("with");
			if(arr.length==2){
				var toReplace:String=arr[0];
				var replaceWith:String=arr[1];
				
				while (str.indexOf(toReplace)!=-1) {
					str=str.replace(toReplace,replaceWith);
				}
			}
			else if(logger)logger.log("!You've tried to run this replace action ("+params+") but you specified it wrongly.  Should be e.g. rwithx.");
			if(OnScreenElements.hasOwnProperty("replace"+String(count+1))){
				str=replaceText(str,OnScreenElements["replace"+String(count+1)],count+1);
			}
			return str;
		}
		
		override public function mouseTransparent():void{
			myText.mouseEnabled = false;
			super.mouseTransparent();
		}
		
		public function setupText():void {
			
			
			myText.multiline=getVar("multiLine");
			text(sortOutHTML(getVar("text")));
			if(getVar("replace1")!="")text(replaceText(myText.text,getVar("replace1")));
			myText.wordWrap=getVar("wordWrap");
			if(getVar("font")!="")myTextFormat.font=getVar("font");
			//myTextFormat.letterSpacing = 10;
			//if(getVar("fontSize")!="")myTextFormat.size=Number(getVar("fontSize"));
			
			if (getVar("colour")!=undefined && getVar("colour")!=0){
				myTextFormat.color=sortOutColours(getVar("colour"));
			}
			myTextFormat.size=uint(getVar("fontSize"));
			if(getVar("font")!="")myTextFormat.font=getVar("font");
			
			align();
		}
		
		private function align():void
		{
			switch((getVar("align")as String).toLowerCase()){
				case "left":
					myTextFormat.align="left";
					break;
				case "right":
					myTextFormat.align="right";
					break;
				case "center":
				case "centre":
				case "middle":
					//trace("here");
					myTextFormat.align="center";
					break;
			}
			
			//trace(getVar("textSize"));
			myTextFormat.size=getVar("fontSize") as int;
			myText.selectable=getVar("selectable");
			myText.setTextFormat(myTextFormat);
			myText.defaultTextFormat=myTextFormat;
			
			if (getVar("background")!="") {
				myText.background=true;
				myText.backgroundColor=getVar("background");
			}		
			if(getVar("exactSpacing")!=0){
				var txt:String=getVar("text");
				var spr:Sprite=new Sprite;
				var subSpr:Sprite;
				var t:TextField;
				
				
				for (var i:uint=0;i<txt.length;i++){
					subSpr=new Sprite;
					t = new TextField();
					t.text=txt.charAt(i);
					myTextFormat.size=getVar("fontSize");
					t.setTextFormat(myTextFormat);
					t.defaultTextFormat=myTextFormat;
					t.autoSize=TextFieldAutoSize.CENTER;
					subSpr.addChild(t);
					/*subSpr.graphics.beginFill(0x000fff,1);
					subSpr.graphics.drawRect(t.x,0,2,2);*/
					subSpr.x=getVar("exactSpacing")*i;
					spr.addChild(subSpr);
					//myText=null;
				}
				
				
				pic.addChild(spr);			
				
			}
		}
		
		private function getSize(txtf:TextField):Rectangle{
			var rect:Rectangle = new Rectangle(0,0,0,0);
			var metrics:TextLineMetrics;
			for(var i:uint=0;i<txtf.numLines;i++){
				metrics=txtf.getLineMetrics(i);
				if(metrics.width>rect.width)rect.width=metrics.width;
				rect.height+=metrics.height;
			}
			return rect;
		}
		
		public function autoSize(scaleText:Boolean=true):void {
			switch((getVar("autoSize") as String).toLowerCase()){
				case "left":
					myText.autoSize=TextFieldAutoSize.LEFT;
					break;
				case "center":
				case "centre":
					
					myText.autoSize=TextFieldAutoSize.CENTER;
					break;
				case "right":
					myText.autoSize=TextFieldAutoSize.RIGHT;
					break;
				case "autoleft":
				case "autoright":
					if(scaleText)sizer((getVar("autoSize") as String).toLowerCase());
					break;
				case "autocenter":
				case "autocentre":
				default:
					if(scaleText)sizer("autocentre");					
			}
		}
		
		private function sizer(justify:String):void{
			
			var f:TextFormat=myText.getTextFormat();
			switch(justify){
				case "autoleft":
					f.align = "left";
					break;
				case "autoright":
					f.align = "right";
					break;
				case "autocentre":
				case "autocenter":
				default:
					f.align = "center";
			}
			
			var rect:Rectangle=new Rectangle(0,0,int(myText.textWidth),myText.textHeight);
			var doContinue:Boolean=true;
			f.size=1;
			//var startTime:Date = new Date();
			var counter:uint=0; //there's an issue in Chrome's pepperflash with autosize.  This should stop xperiment crashing.
			
			//logger.log("a2"); //*******************need better algorithm.  Keeps crashing in Chrome.
			while (doContinue && counter<10) {
				//logger.log("a3 "+counter+String(doContinue));
				counter++;
				f.size=int(f.size)+5;
				myText.setTextFormat(f);
				rect=getSize(myText);
				doContinue=rect.width < myText.width-3 && rect.height < myText.height-3;
				//note, did a quick simulation in excel and it turns out that +5 is the best to use.
				/*size=1 av=-9.4 total=-188
				size=2 av=-5.25 total=-105
				size=3 av=-4.8 total=-96
				size=4 av=-4 total=-80
				size=5 av=-3.7 total=-74
				size=6 av=-4.4 total=-88
				size=7 av=-4.45 total=-89
				size=8 av=-3.95 total=-79
				size=9 av=-4.5 total=-90
				size=10 av=-5.05 total=-101*/
			}
			if(counter>90 && logger)logger.log("!!Problem with autoSize in addText. Could be due to using PepperFlash in googleChrome");
			counter=0;
			while (myText.textWidth > myText.width-3 || myText.textHeight > myText.height-3 || counter<2) {
				counter++
					f.size=int(f.size)-1;
				myText.setTextFormat(f);
				rect=getSize(myText);
			}
			if(counter>90 && logger)logger.log("!!Problem with autoSize in addText. Could be due to using PepperFlash in googleChrome");
			f.size=int(f.size)-1;
			myText.setTextFormat(f);
			
			//trace((startTime.time -new Date().time));
			
			//below, a staircase procedure I was trying out for text resizing.  Doesn't work though.
			/*	
			var hor:Number=0; var ver:Number=0; // difference from good size where +1 means too big by +1.  Both values need to be below 0.
			var pastSizeDiscrepency:Number = 1;
			var currentSizeDiscrepency:Number = 1;
			
			//where rect=the size the text should be
			//where myText.size is the current textSize.
			var switchCount:uint=0;
			var origTextSize:int=int(f.size);
			
			var sizeMultiplierOverGoes:Array=[.5];
			var rect:Rectangle=getSize(myText);
			
			
			while (switchCount<3) {
			
			
			hor=((myText.width-3)-rect.width);
			ver=((myText.height-3)-rect.height);
			if(hor>ver)currentSizeDiscrepency = hor;
			else currentSizeDiscrepency=ver;  //need to reduce the biggest value here.
			
			f.size=int(f.size)*sizeMultiplierOverGoes[sizeMultiplierOverGoes.length-1];
			myText.setTextFormat(f);
			
			
			if(currentSizeDiscrepency<pastSizeDiscrepency){
			sizeMultiplierOverGoes.push(sizeMultiplierOverGoes[sizeMultiplierOverGoes.length-1]*.5);
			}
			else{
			sizeMultiplierOverGoes.push(sizeMultiplierOverGoes[sizeMultiplierOverGoes.length-1]*1.5);//note should be 2 not 1.5 but we need a discrepency with the
			//prev .5 transform to ensure gradual scaling.
			}
			if(sizeMultiplierOverGoes.length>7)break;
			trace(int(f.size)+" "+hor+" "+ver+" "+currentSizeDiscrepency)
			pastSizeDiscrepency=currentSizeDiscrepency;
			
			rect=getSize(myText);	
			f.size=origTextSize;
			myText.setTextFormat(f);
			
			}	
			trace(sizeMultiplierOverGoes);*/
		}
		
		
		public function sortOutHTML(str:String):String {
			
			str=str.replace(new RegExp("{","g"), "<");
			str=str.replace(new RegExp("}","g"), ">");
			
			return str;
		}
		
		public function sortBorder():void
		{
			if(border)border.graphics.clear();
			else border = new Shape;
			border.graphics.lineStyle(getVar("borderThickness") as Number, getVar("borderColour"),getVar("borderAlpha"));
			border.graphics.drawRect(myText.x,myText.y,myText.width-1,myText.height-1);
			border.graphics.endFill();
			pic.addChild(border);
		}
		
		override public function RunMe():uberSprite {
			
			/*if(getVar("multiplySpaces")as uint!=1){
			
			var str:String=OnScreenElements.text as String;
			var newStr:String="";
			var newGap:String="";
			var siz:uint=getVar("multiplySpaces") as uint;
			//trace(siz,44);
			
			for(var i:uint=0;i<siz;i++){
			newGap=newGap+"            ";
			//trace(i,newGap,22);
			}
			
			for(i=0;i<str.length;i++){
			if(str.charCodeAt(i)==32){
			newStr=newStr+newGap;
			}
			else newStr=newStr+str.charAt(i);
			}
			OnScreenElements.text=newStr;
			}*/
			
			setupText();
			
			if(getVar("exactSpacing")==0)super.pic.addChild(myText);
			setUniversalVariables();		
			if(getVar("autoSize")!="false" && myText.text!=""){
				autoSize();
			}
				
			else align();
			if (getVar("border")!="") sortBorder();
			
			
			return (super.pic);
			
		}
		
	}
}