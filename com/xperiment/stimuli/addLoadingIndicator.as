﻿package  com.xperiment.stimuli{

	import flash.display.*;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.EventDispatcher;
	import flash.events.TimerEvent;
	import com.xperiment.uberSprite;
	import com.bulkloader.BulkLoader;
	import com.bulkloader.BulkProgressEvent;
	import com.bulkloader.preloadFilesFromWeb;
	import com.xperiment.stimuli.addText;
	import com.xperiment.codeRecycleFunctions;
	import flash.utils.Timer;
	import com.underscore._;
	

	public class addLoadingIndicator extends object_baseClass implements IgivePreloader {

		private var txt:Sprite;
		private var myBackground:Shape;
		private var myBar:Shape;
		private var combined:Sprite;
		private var loader:BulkLoader;
		private var bckground:Shape;
		private var tempTxt:addText

		//public function Trial_imageCollage(genAttrib:XMLList, specAttrib:XMLList) {
		override public function setVariables(list:XMLList):void {
			setVar("number","backBarColour",0xFFFFFF);
			setVar("number","backBarLineColour",0xc5c5c7);
			setVar("number","backBarLineThickness",6);
			setVar("number","barColour",0x0738f2);
			setVar("string","errorMessage","Unfortunately there has been an error loading the stimuli.  The server may be down, in which case, could you try again later?  If this repeatedly happens, could you let us know by visiting www.xperiment.mobi?  Many apologies for this inconvenience.");
			super.setVariables(list);
		}
		
		private function onAllDownloaded():void{
			//combined.visible=false;
			this.dispatchEvent(new Event("behaviourFinished",true));
		}

		override public function RunMe():uberSprite {
			combined= new Sprite;
			myBackground=new Shape  ;
			myBackground.graphics.beginFill(getVar("backBarColour"));
			myBackground.graphics.lineStyle(getVar("backBarLineThickness"),getVar("backBarLineColour"));
			myBackground.graphics.drawRect(0,0,200,20);
			myBackground.graphics.endFill();
			combined.addChild(myBackground);

			myBar=new Shape  ;
			myBar.graphics.beginFill(getVar("barColour"));
			myBar.graphics.drawRoundRect(2,2,198,16,15,15);
			myBar.scaleX=0;
			combined.addChild(myBar);
			  
			combined.visible=false;
			
			super.pic.addChild(combined);
			super.setUniversalVariables();
			return (super.pic);
		}

		public function passPreloader(preloader:preloadFilesFromWeb):void {
			if(preloader)loader=preloader.giveBulkloader();
			else{
				logger.log("Problem: you have used a 'addLoadingIndicator' but have not switched on the loader (add this to the SETUP bit <loadFilesInAdvance/>). Pretending now the stimuli have indeed been loaded... .");
				
			
				var timer:Timer = new Timer(1000, 1);
				timer.addEventListener(TimerEvent.TIMER_COMPLETE,
					function(e:Event):void {
						e.currentTarget.removeEventListener(TimerEvent.TIMER_COMPLETE,arguments.callee);
						timer=null;
						onAllDownloaded();
					});
				timer.start();
				
				// underscore.as
				_(onAllDownloaded).delay(1000);
			
			}
		}
		
		
	
		
		
		private function addInfo(message:String):void{
			if (txt){
				combined.removeChild(bckground);
				combined.removeChild(txt);
			}
			tempTxt=new addText;
			var obj:Object=new Object  ;
			obj.autoSize="left";
			obj.text=message;
			obj.textSize=30;
			obj.colour=0x000000;
			txt=tempTxt.giveBasicStimulus(obj);
			txt.y-=combined.height/3;
			
			
			bckground =new Shape  ;
			bckground.graphics.beginFill(getVar("backBarColour"));
			combined.addChild(txt);
			bckground.graphics.drawRoundRect(myBackground.x,myBackground.y,txt.width,myBackground.height*.8,13,13);
			bckground.graphics.endFill();
			bckground.alpha=.5;
			combined.addChild(bckground);
			
			combined.swapChildrenAt(combined.numChildren-1,combined.numChildren-2);
		}
		

		override public function appearedOnScreen(e:Event):void{
			super.appearedOnScreen(e);
				if(loader && loader.isFinished)onAllDownloaded()
				else{
				combined.visible=true;
				// attaching events to all items:
				// this will fire once all items have been loaded
				if(loader)loader.addEventListener(BulkLoader.COMPLETE, onAllLoaded,false,0,true);
				// this will fire on progress for any item;
				// the event , BulkProgress is a subclass of ProgressEvent (with extra information)
				if(loader)loader.addEventListener(BulkLoader.PROGRESS, onAllProgress,false,0,true);
				// this will fire if any item fails to load:;
				// the event is BulkErrorEvent and holds an array (errors) with all failed LoadingItem instances
				if(loader)loader.addEventListener(BulkLoader.ERROR, onAllError,false,0,true);
			}
		}


		private function onAllLoaded(evt : Event):void {
			trace("every thing is loaded!");
			addInfo("loaded all stimuli ("+ codeRecycleFunctions.roundToPrecision(Number(loader.bytesLoaded)/1000000,4) +" MB in total)");
			removeListeners();
			onAllDownloaded();
		}
		
		private function removeListeners():void{
			if(loader.hasEventListener(BulkLoader.ERROR))loader.removeEventListener(BulkLoader.ERROR, onAllError);
			if(loader.hasEventListener(BulkLoader.PROGRESS))loader.removeEventListener(BulkLoader.PROGRESS, onAllProgress);
			if(loader.hasEventListener(BulkLoader.COMPLETE))loader.removeEventListener(BulkLoader.COMPLETE, onAllLoaded);
		}

		public function onAllProgress(evt : BulkProgressEvent):void {
			myBar.scaleX=uint(100*evt.itemsLoaded/evt.itemsTotal )/100;
			if(evt.itemsTotal!=0)addInfo("loading stimuli ("+ codeRecycleFunctions.roundToPrecision(Number(loader.bytesLoaded)/1000000,4) +" MB loaded so far)");
		}

		public function onAllError(evt : BulkProgressEvent):void {
			trace(evt.loadingStatus());
			logger.errorMessage(getVar("errorMessage"),evt.loadingStatus());
		}
		
		override public function kill():void{
			pic.removeChild(combined);
			txt=null;
			myBackground=null;
			myBar=null;
			combined=null;
			super.kill();
			
		}
	}
}