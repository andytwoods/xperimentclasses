﻿
package com.xperiment.stimuli{

	import com.graphics.pattern.Box;
	import com.mTurk.mTurk;
	import com.xperiment.uberSprite;
	import com.xperiment.stimuli.addText;
	import com.xperiment.stimuli.object_baseClass;
	
	import flash.display.*;
	import flash.events.Event;

	public class addMechanicalTurk extends object_baseClass {
		private var passcode:String;
		private var myID:String;
		private var spr:Sprite;
		private var doFinisBehav:Boolean = false;
		private var box:Sprite;

		override public function setVariables(list:XMLList):void {
			//setVar("int","textSize",12); // mucks up sizing.  Leaving this up to autosize.
			setVar("Number","colour",0x000000);
			setVar("boolean","hideSuccess",false);
			setVar("string","linkedup_Success","successfully linked up with Mechanical Turk");
			setVar("string","linkedup_Fail","Sorry, there has been an error communicating with Mechanical Turk and we cannot proceed.");
			setVar("string","send_Fail","There has been an error communicating with Mechanical Turk please email Andy@xperiment.mobi citing this code:");
			
			setVar("string","finish_message","Dear Mechanical Turker.  Thankyou for taking part in this study. Please use the following code to register with Mechanical Turk that you have finished this study: ");
			setVar("string","purpose","linkedUp","linkedUp||giveCode");//tellFinished
			setVar("boolean","backgroundBox",true);
			super.setVariables(list);
		}
		
		override public function returnsDataQuery():Boolean {return true;}
		override public function myScore():String {return myID;}
		override public function storedData():Array {
			var tempData:Array=new Array  ;
			tempData.event="mturkID";
			tempData.data=myScore();
			super.objectData.push(tempData);
			return super.objectData;
		}
		


		override public function RunMe():uberSprite {
			var mechTurk:mTurk = new mTurk(theStage, logger);
			var success:Boolean=false;
			var obj:Object= new Object;
			spr=new Sprite()
			passcode=mechTurk.getPasscode();
			myID=mechTurk.getID();
	
			if(getVar("purpose")=="linkedUp"){
				if(passcode=="0"){
				obj.text=getVar("linkedup_Fail");				
				}
				else{
					obj.text=getVar("linkedup_Success");
					doFinisBehav=true;
					success=true;
				}
			}
			else{
				if(passcode=="0"){
				obj.text=getVar("send_Fail")+"<b>{FONT SIZE='15' COLOR='#c00505'}failedToSend";//fail code
				
				}
				
				else{
					mechTurk.submitTurkEnd();
					obj.text=getVar("finish_message")+"<b>{FONT SIZE='20' COLOR='#c00505'}"+mechTurk.getPasscode();
					doFinisBehav=true;
					success=true;
				}
				
			}
			obj.wordWrap=true;
			obj.autoSize=getVar("autoSize");
			obj.colour=getVar("colour");
			var tempTxt:addText=new addText  ;
			var temp:Sprite=tempTxt.giveBasicStimulus(obj);
			spr.addChild(temp);
			if(getVar("backgroundBox")){
				box=new Sprite;
				obj= new Object();
				obj.fillColour=0xFFFFFF;
				obj.width=temp.width;
				obj.height=temp.height;
				obj.autosize="true";
				box.addChild(Box.myBox(obj));
				spr.addChildAt(box,0);
			}
			if(!success)pic.addChild(spr);
			if(!pic.contains(spr) && !getVar("hideSuccess"))pic.addChild(spr);
			setUniversalVariables();
			return (super.pic);
		}
		
		override public function appearedOnScreen(e:Event):void{
			if(pic.contains(spr) && box && getVar("backgroundBox"))box.height=spr.height;box.width=spr.width;
			if(doFinisBehav)this.dispatchEvent(new Event("behaviourFinished",true));
			super.appearedOnScreen(e);
		}
		
		override public function kill():void {
			if(spr && pic.contains(spr))pic.removeChild(spr);
			box=null;
			spr=null;
		}

	}
}