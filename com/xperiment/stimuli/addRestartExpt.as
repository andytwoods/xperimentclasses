package com.xperiment.stimuli
{
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	import com.xperiment.uberSprite;

	public class addRestartExpt extends object_baseClass
	{

		override public function setVariables(list:XMLList):void {
			setVar("number","timeToRestart",1000);
			super.setVariables(list);
		}
		
		override public function RunMe():uberSprite{

			
			pic.graphics.drawRect(0,0,1,1);
			return (super.pic);
		}
	}
}