package com.xperiment.stimuli.primitives
{
	import com.bit101.components.PushButton;
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;

	public class MultipleChoice extends Sprite
	{
		private var buttons:Array;
		public var scores:Array=[];
		public var ActualAnswer:String="";
		public var PossibleAnswers:Array =[];
		public var questionLabel:String;
		
		public function getData():String
		{
			for(var i:uint=0;i<buttons.length;i++){
				if((buttons[i] as PushButton).selected){
					
					return (buttons[i] as PushButton).label;
					break;
				}
			}
			return "";
		}
		
		public function MultipleChoice(params:Object)
		{

	
			if(params.distanceApart==undefined)params.distanceApart=10;
			if(params.width==undefined)params.width=200;
			if(params.height==undefined)params.height=200;
			if(params.seperation==undefined)params.seperation="vertical";
			if(params.fontSize==undefined)params.fontSize=20;
			if(params.PossibleAnswers==undefined)PossibleAnswers=["label1","label2","label3"]; else if(params.PossibleAnswers as Array) PossibleAnswers=params.PossibleAnswers;
			if(params.scores==undefined)params.scores=1; else if (params.scores as Array) scores=params.scores;
			if(params.questionLabel)questionLabel=params.questionLabel;
			/*
			scores can either be an array OR a number: e.g. scores:3 or [0,0,1,1]
			if a number, that number refers to the correct answer in the params.PossibleAnswers Array, assigning that score a 1 and all others 0
			*/
			//trace(params.PossibleAnswers)

			if(params.scores as Number){
				var temp:Number=params.scores;
				//trace("in here",params.scores);
				for (var i:uint=0;i<PossibleAnswers.length;i++)scores.push(0);
				if(temp-1<=scores.length)scores[temp-1]=1;
			}
			
			params.number=params.PossibleAnswers.length;
			
			var distApart:Number=params.distanceApart as Number;
			
			buttons = [];
			var number:uint=params.number;

			var origWidth:Number=params.width;
			var origHeight:Number=params.height;
			
			//this.graphics.beginFill(0x006644,.5);
			//this.graphics.drawRect(0,0,origWidth,origHeight);
			
			var button:PushButton;
			
			
			for(i=0;i<number;i++){
				
				button=new PushButton(null,0,0,"",null,params.fontSize as int,true);
				button.label=PossibleAnswers[i%PossibleAnswers.length];
				button.addEventListener(MouseEvent.CLICK,mouseClicked);
				button.toggle=true;
				
				if(params.seperation=="horizontal"){
					button.width=origWidth/number-(distApart*(number-1));
					button.height=origHeight;
					button.x=(origWidth/number*i)+i*distApart;
				}
				else{
					button.width=origWidth;
					button.height=origHeight/number-(distApart*(number-1));
					button.y=(origHeight/number*i)+i*distApart;
					
				}
				
				button.name=PossibleAnswers[i];
				buttons.push(button);
				
				this.addChild(button);				
			}
		}
		
		public function score():Number{
			var pos:uint=PossibleAnswers.indexOf(ActualAnswer)
			//trace(pos,ActualAnswer,PossibleAnswers[pos], scores[pos]);
			return scores[pos];
		}
		
		protected function mouseClicked(e:MouseEvent):void
		{
			(e.target as PushButton).selected=true;
			ActualAnswer=(e.target as PushButton).name;
			//trace(ActualAnswer,score(),2222,PossibleAnswers,"scores:",scores);
			for(var i:uint=0;i<buttons.length;i++){
				if(buttons[i] as PushButton!=e.target as PushButton){
					(buttons[i] as PushButton).selected=false;
				}
			}
			
		}
		
		public function kill():void{
			for(var i:uint=0;i<buttons.length;i++){
				buttons[i].removeEventListener(MouseEvent.CLICK,mouseClicked);
				this.removeChild(buttons[i]);
				buttons[i] = null;
			}
			buttons=null;
		}
	}
}