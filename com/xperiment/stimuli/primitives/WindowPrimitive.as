package com.xperiment.stimuli.primitives
{
	import com.bit101.components.PushButton;
	import com.bit101.components.Window;
	import com.xperiment.events.GotoTrialEvent;
	
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.system.System;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import com.bit101.components.TextArea;
	
	public class WindowPrimitive extends Sprite
	{
		private var _obj:Object;
		public var myWindow:Window;
		private var txt:String;
		private var pushButton:PushButton
		private var he:TextField;
		private var tx:TextArea;
		
		public function WindowPrimitive(obj:Object)
		{
			_obj=obj;
			setVar("title","");
			myWindow=new Window(this,0, 0, _obj.title);
			myWindow.draggable=false;
			myWindow.hasCloseButton=true;
			
			//trace(obj.width,obj.height);
			myWindow.width=obj.width as int;
			myWindow.height=obj.height as int;

		}
		
		private function setVar(nam:String,dat:String):void{
			if(_obj[nam]==undefined)_obj[nam]=dat;
			
		}
		
		public function addMessage(header:String,str:String):void{
			he=new TextField;
			he.wordWrap=true;
			he.width=myWindow.width*.8;
			he.x=myWindow.width*.1;
			he.htmlText=header;
			he.autoSize=TextFieldAutoSize.LEFT;
			myWindow.addChild(he);
			
			txt=str;
			
			
			
			tx=new TextArea;
			tx.wordWrap=true;
			tx.width=myWindow.width*.8;
			
			tx.x=myWindow.width*.1;
			
			tx.text=str;
			tx.selectable=true;
			
			pushButton = new PushButton(myWindow,0,0,"copy to clipboard");
			pushButton.addEventListener(MouseEvent.CLICK,clipboard,false,0,true);
			pushButton.width=200;
			pushButton.x=myWindow.width*.5-pushButton.width*.5
			pushButton.y=30;
			
			
			
			myWindow.addChild(tx);
			he.y=pushButton.height+60;
			tx.y=he.x+he.height+30;
			
			tx.height=myWindow.height-he.y-he.height-30;
			
		}
		
		public function kill():void{
			
			myWindow.removeChildren();
			he=null;
			tx=null;
			pushButton=null;
			myWindow = null;
		}
		
		protected function clipboard(event:MouseEvent):void
		{
			System.setClipboard(txt);
		}
	}
}