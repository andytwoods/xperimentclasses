﻿package com.xperiment.stimuli.primitives{

	import com.xperiment.stimuli.addText;
	
	import flash.display.*;
	import flash.display.Shape;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	public class numberSelector extends Sprite {
		private var shapes:Array;
		private var num:Sprite;
		private var container:Sprite;
		private var obj:Object= new Object;
		private var selectedNumber:uint=0;
		private var myList:Array;
		private var info:Object = new Object;
		private var xOffset:uint;
		private var top:Shape;
		private var bottom:Shape;
		private var mouseShape:Shape;

		public function run(info:Object):void {
			this.info=info;
		
			myList=(info.list as String).split(",");

			container = new Sprite;
			shapes = new Array;
			top=makeTriangle(info.lineThickness,info.lineColour,info.lineAlpha, 
							   info.triangleColour, info.triangleWidth, -info.triangleHeight);
			top.x=info.textBoxWidth/2-info.triangleWidth/2;
			top.y=+info.textBoxHeight/2;
			top.name="top"
			container.addChild(top);
			shapes.push(top);
			
			bottom=makeTriangle(info.lineThickness,info.lineColour,info.lineAlpha, 
							   info.triangleColour, info.triangleWidth, info.triangleHeight);
			bottom.x=info.textBoxWidth/2-info.triangleWidth/2;
			bottom.y=info.textBoxHeight+info.triangleDistFromBox*2+info.textBoxHeight/2;
			bottom.name="bottom";

			container.addChild(bottom);
			//the below lets one use the mouse wheel anywhere between the triangles.  Bit hacky but does the job.
			container.graphics.beginFill(0x000000,0);
			container.graphics.drawRect(bottom.x,bottom.height,container.width,bottom.y-bottom.height);
			shapes.push(shape);

			
			
			//obj.backgroundColor=0x00FFFF as Number
			obj.autoSize="left" as String;
			obj.fontSize=info.fontSize;
			
			if(myList.indexOf(info.startVal)!=-1)selectedNumber=info.startVal;

			addNumber(myList[selectedNumber]);
			
			container.addChild(num);
			container.addEventListener(MouseEvent.MOUSE_DOWN, selected,false,0,true);
			container.addEventListener(MouseEvent.MOUSE_WHEEL, wheel,false,0,true);
			
			mouseShape=new Shape;
			mouseShape.graphics.beginFill(0x3355FF,0);
			mouseShape.graphics.drawCircle(0,0,1);
			container.addChild(mouseShape);
			
			this.addChild(container);
		}
		
		protected function wheel(e:MouseEvent):void
		{
			if(e.delta>0)escalateNumber(1);
			else escalateNumber(-1);
			
			
		}
		
		private function addNumber(str:String):void{
			obj.text=str;
			obj.colour=info.textColour;
			obj.align="center";
			var tempTxt:addText = new addText();
			num=tempTxt.giveBasicStimulus(obj);
			num.y=info.triangleHeight+info.textBoxHeight*.5-num.height*.5;
			num.x=info.textBoxWidth/2-num.width/2;
		}


		private function makeTriangle(lineThickness:uint, lineColour:Number, transparency:Number, colour:Number, width:uint,height:int):Shape {
			var sha:Shape=new Shape  ;
			sha.graphics.lineStyle(lineThickness,lineColour,transparency);
			sha.graphics.beginFill(colour,1);
			sha.graphics.moveTo(0,0);
			sha.graphics.lineTo(width,0);
			sha.graphics.lineTo(Math.round(width)*.5,height);
			sha.graphics.lineTo(0,0);
			sha.graphics.endFill();
					
			return sha;
		}

		private function selected(e:MouseEvent):void {
			
			mouseShape.x=e.localX;
			mouseShape.y=e.localY;
			
			if (mouseShape.hitTestObject(top)) {
				escalateNumber(1);
			}
				if (mouseShape.hitTestObject(bottom)) {
				escalateNumber(-1);
			}
		}
		
		private function escalateNumber(up:int):void{
			
			if(up==-1 && selectedNumber==0)selectedNumber=myList.length-1;
			else{
				selectedNumber+=up;
				selectedNumber=selectedNumber % myList.length;
			}
			container.removeChild(num);
			addNumber(myList[selectedNumber]);
			addNumber(String(myList[selectedNumber]));
			container.addChild(num);
		
		}
		
		public function giveData():uint{
			return selectedNumber;
		}
		

		public function kill():void {
			container.removeChild(mouseShape);mouseShape=null;
			container.removeChild(top);top=null;
			container.removeChild(bottom);bottom=null;
			container.removeEventListener(MouseEvent.MOUSE_DOWN,selected);
			container.removeEventListener(MouseEvent.MOUSE_WHEEL,wheel);
			for (var i:uint=0;i<shapes.length;i++){
				shapes[i]=null;
			}
			shapes=null;
		}
	}
}