package com.xperiment.stimuli.primitives.graphs
{

	import com.bit101.components.Style;
	import com.xperiment.codeRecycleFunctions;
	import com.greensock.TweenMax;
	
	import flash.display.Graphics;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	
	public class BarChart extends Sprite
	{
		private var thisWidth:Number;
		private var thisHeight:Number;
		private var lineGap:Number = 100;
		private var barGap:Number = 10;
		private var barWidth:Number = 30;
		private var chartVal:Number;
		private var chartData:Object;
		private var xAxisLabels1:Array;
		private var xAxisLabels2:Array;
		private var yAxisUnit:Number;
		private var bars:Sprite;
		private var barsArr:Array;
		private var dp:int;
		private var params:Object;
		private var yAxisTitleWidth:int;
		private var xAxisTitleHeight:int;
		private var xAxisTitle:TextField = new TextField();
		private var yAxisTitle:TextField = new TextField();
		private var mainTitle:TextField
		private var graphWidth:Number;
		private var graphHeight:Number;
		private var subXLabelsHeight:int=10;
		private var TxtTitleArr:Array;
		private var linesArr:Array;
		private var titleGap:int=0;

		public function BarChart(chartData:Array, params:Object)
		{

			this.chartData = chartData;
			this.params=params;
			this.dp=int(params.dp);

			this.xAxisLabels1=params.xAxisLabels1;
			this.xAxisLabels2=params.xAxisLabels2;
			if(params.title as String !=""){mainTitle=new TextField(); mainTitle.text=params.title; mainTitle.textColor=Style.LABEL_TEXT;}
			
			if(params && params.yAxisTitleWidth && params.yAxisTitleWidth!="")this.yAxisTitleWidth=Number(params.yAxisTitleWidth);
			else this.yAxisTitleWidth=20;
			if(params && params.xAxisTitleHeight && params.xAxisTitleHeight!="")this.xAxisTitleHeight=Number(params.xAxisTitleHeight);
			else this.xAxisTitleHeight=20;
			
			thisWidth = Number(params.width);
			thisHeight = Number(params.height);
			
			buildTitles();
			
			this.graphWidth=thisWidth-yAxisTitle.width;
			this.graphHeight=thisHeight-xAxisTitle.height*1.4-subXLabelsHeight;
			if(xAxisLabels2.length!=0 && xAxisLabels2[0]!="")this.graphHeight-=subXLabelsHeight*1.5;
			if(mainTitle){this.titleGap=subXLabelsHeight*1.5; this.graphHeight-=titleGap;}
			
			if(params && params.yAxisMax && params.yAxisMax!=0){yAxisUnit=graphHeight/params.yAxisMax;}
			else {
				yAxisUnit=0;
				for(var i:uint=0;i<chartData.length;i++){
					if(!isNaN(chartData[i]) && chartData[i]!="" && Number(chartData[i])>yAxisUnit)yAxisUnit=chartData[i];
				}
				if(yAxisUnit==0)yAxisUnit=10;
				
				yAxisUnit=graphHeight/(1.2*yAxisUnit); // chart is always 20% taller than tallest bar
			}
			
			buildXaxis();
			drawLines();
		}
		
		private function buildTitles():void
		{			
			
			xAxisTitle.text = params.x;
			
			for(var i:uint=0;i<params.y.length;i++){
				yAxisTitle.appendText((params.y as String).charAt(i) + "   \n");
			}
			xAxisTitle.textColor=Style.LABEL_TEXT
			xAxisTitle.autoSize = TextFieldAutoSize.CENTER;
			yAxisTitle.autoSize = TextFieldAutoSize.LEFT;
			xAxisTitle.selectable = yAxisTitle.selectable = false;
			xAxisTitle.setTextFormat(getTxtFormat(Style.LABEL_TEXT,12));
			yAxisTitle.setTextFormat(getTxtFormat(Style.LABEL_TEXT,12));
			xAxisTitle.x = thisWidth *.5 - xAxisTitle.width *.5;
			xAxisTitle.y = thisHeight-xAxisTitle.height+titleGap;
			yAxisTitle.y = thisHeight*.5-yAxisTitle.height*.5+titleGap;
			this.addChild(xAxisTitle);
			this.addChild(yAxisTitle);
			
			if(mainTitle){
				mainTitle.autoSize = TextFieldAutoSize.CENTER;
				mainTitle.selectable = yAxisTitle.selectable = false;
				mainTitle.setTextFormat(getTxtFormat(Style.LABEL_TEXT,16));
				mainTitle.x=thisWidth *.5-mainTitle.width*.5;
				this.addChild(mainTitle);
			}
			
		}
	
		public function init():void{
			if(chartData){
				var y:Number;
				for(var i:uint=0;i<barsArr.length;i++){;
					TweenMax.fromTo(barsArr[i],2,{alpha:0,scaleY:.1},{alpha:1,scaleY:1});
				}
			}
		}
		
		private function buildXaxis():void
		{
	
			var totalBarWidth:Number = ((graphWidth - lineGap - 20) );
			var xPosCount:Number = lineGap + barWidth;
			var val:Number=0;
			var varName:String;
			this.bars = new Sprite;
			var TxtValue:TextField;
			var TxtTitle:TextField;
			barsArr=new Array;
			TxtTitleArr=new Array;
			linesArr=new Array;
			var rect:Shape;
			
			for (var i:uint=0; i<chartData.length;i++)
			{
				rect = new Shape();
				rect.graphics.beginFill(0x00ff33);
				
				var dataBarText:String;
				
				if(isNaN(chartData[i]) || chartData==""){
					chartData[i]=0.001;
					dataBarText="no data";

				}
				else{
					dataBarText=String(codeRecycleFunctions.dp(chartData[i] as Number,dp));
				}
		
				rect.graphics.drawRect(0, 0, graphWidth/xAxisLabels1.length,yAxisUnit*Number(chartData[i]));
				rect.graphics.endFill();
				rect.x = yAxisTitleWidth+ graphWidth/xAxisLabels1.length*i+barGap*i;
				rect.y=-rect.height;
				bars.addChild(rect);
				barsArr.push(bars);
	
				TxtValue = new TextField();
				TxtValue.text = dataBarText;
				TxtValue.autoSize = TextFieldAutoSize.CENTER;
				TxtValue.selectable = false;
				TxtValue.setTextFormat(getTxtFormat(0xFF0066,16));
				TxtValue.x = rect.x+(rect.width-TxtValue.width)*.5;
				TxtValue.y = rect.y-TxtValue.height;
				bars.addChild(TxtValue);
			
				TxtTitle = new TextField();
				if(i<xAxisLabels1.length)TxtTitle.text = xAxisLabels1[i];
				TxtTitle.autoSize = TextFieldAutoSize.CENTER;
				TxtTitle.selectable = false;
				TxtTitle.setTextFormat(getTxtFormat(0xFF0066,16));
				TxtTitle.x = rect.x+(rect.width-TxtTitle.width)*.5;
				TxtTitle.y=graphHeight+titleGap;
				bars.y+=titleGap;
				
				TxtTitleArr.push(TxtTitle);
				this.addChild(TxtTitle);
				this.addChild(bars);
				bars.alpha=0;
				
				if(i<chartData.length-1){
					var line:Sprite = new Sprite;
					line.graphics.lineStyle(2,0x000000,.2);
					line.graphics.lineTo(0,subXLabelsHeight);
					line.x=rect.x+rect.width+barGap*.5;
					line.y=graphHeight+1+titleGap;
					this.addChild(line);
					linesArr.push(line);
					
				}
			}
			
			if(xAxisLabels2.length!=0){
				var seperation:Number=barsArr.length/xAxisLabels2.length;
				
					for (i=0; i<xAxisLabels2.length;i++){
						if(TxtTitleArr.length>i*seperation && TxtTitleArr[i*seperation]){
							TxtTitle = new TextField();
							TxtTitle.text = xAxisLabels2[i];
							TxtTitle.autoSize = TextFieldAutoSize.CENTER;
							TxtTitle.selectable = false;
							TxtTitle.setTextFormat(getTxtFormat(0xFF0066,16));
							TxtTitle.x = TxtTitleArr[i*seperation].x+TxtTitleArr[i*seperation].width*.5-TxtTitle.width*.5+graphWidth/xAxisLabels1.length*.5;
							TxtTitle.y=graphHeight+subXLabelsHeight*1.5+titleGap;
							this.addChild(TxtTitle);		
							if(i<xAxisLabels2.length-1 && linesArr[i*seperation+1]){
								line = new Sprite;
								line.graphics.lineStyle(2,0x000000,.2);
								line.graphics.lineTo(0,subXLabelsHeight*3);
								
								line.x=linesArr[i*seperation+1].x
								line.y=graphHeight+1+titleGap;
								this.removeChild(linesArr[i*seperation+1]);
								this.addChild(line);
							
							}
						}
				}
			}
			
			
			bars.y=graphHeight+titleGap;
		}
		
		private function drawLines():void
		{
			var shape:Shape = new Shape();	
			shape.graphics.lineStyle(2,0x000000,.2);
			shape.graphics.moveTo(yAxisTitleWidth,graphHeight+titleGap);
			shape.graphics.lineTo(yAxisTitleWidth+bars.width,graphHeight+titleGap);
			
			shape.graphics.moveTo(yAxisTitleWidth,graphHeight+titleGap);
			shape.graphics.lineTo(yAxisTitleWidth,0+titleGap);
			
			this.addChild(shape);
		}
		
		private function getTxtFormat(txtColor:uint,txtSize:Number):TextFormat
		{
			var tf:TextFormat = new TextFormat();
			tf.color = txtColor;
			tf.size = txtSize;
			return tf;
		}
	}
	
}
