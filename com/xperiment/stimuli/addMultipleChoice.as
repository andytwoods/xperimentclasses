﻿package  com.xperiment.stimuli{
	
	import com.bit101.components.PushButton;
	import com.bit101.components.Style;
	import com.xperiment.uberSprite;
	import com.xperiment.stimuli.primitives.MultipleChoice;
	
	import flash.display.*;
	import flash.display.Shape;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	
	public class addMultipleChoice extends object_baseClass {
		private var buttons:Array;
		private var multipleChoice:MultipleChoice;
		
		override public function setVariables(list:XMLList):void {			
			setVar("string","seperation","vertical");//vertical, horizontal
			setVar("number","distanceApart",1);
			setVar("uint","number",2);
			setVar("string","labels","label1,label2,labels3","string,string...string");
			setVar("uint","fontSize",20);
			super.setVariables(list);
			
		}
		
		override public function storedData():Array {
			var tempData:Array = new Array();
			tempData=new Array();
			if(getVar("id")!="")tempData.event=getVar("id");
			else tempData.event = "multipleChoiceSelected"
			tempData.data=multipleChoice.getData();
			super.objectData.push(tempData);
			return objectData;
		}
		
		override public function returnsDataQuery():Boolean {
			return true;
		}
	
		
		
		override public function RunMe():uberSprite {
			
			pic.graphics.drawRect(0,0,1,1);
			setUniversalVariables();
			
		
			multipleChoice = new MultipleChoice({fontSize:getVar("fontSize"),distanceApart:getVar("distanceApart"),
				number:getVar("number"),width:pic.width,height:pic.height,PossibleAnswers:(getVar("labels") as String).split(","),seperation:getVar("seperation")})
			
			this.name="multipleChoiceAction";
			pic.scaleX=1;
			pic.scaleY=1;
			pic.addChild(multipleChoice);
			multipleChoice.x=pic.width*.5-multipleChoice.width*.5;
			multipleChoice.y=pic.height*.5-multipleChoice.height*.5;
			//overlay.scaleX=pic.width/overlay.width;
			//overlay.scaleY=pic.height/overlay.height;
			//pic.addChild(overlay);
			
			return pic;
		}
	
		
		override public function kill():void {

			multipleChoice.kill();
			pic.removeChild(multipleChoice);
			multipleChoice=null;
			super.kill();
		}
	}
}