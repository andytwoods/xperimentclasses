package com.xperiment.stimuli
{
	import com.Logger.Logger;
	import com.xperiment.behaviour.BehaviourBoss;
	import com.xperiment.uberSprite;

	public interface IStimulus
	{
		
		function passLogger(logger:Logger):void;
		function giveBehavBoss(manageBehaviours:BehaviourBoss):void;
		function setUpTrialSpecificVariables(trialObjs:Object):void;
		function setVariables(list:XMLList):void;
		function returnsDataQuery():Boolean;
		function getVar(str:String):*;
		function setStartEndTimes(start:uint,end:uint):void;
		function RunMe():uberSprite;
		
	}
}