package com.xperiment.stimuli
{
	import com.xperiment.OnScreenBoss;

	public interface IgiveCurrentDisplay
	{
		function passOnScreenBoss(CurrentDisplay:OnScreenBoss):void;
	}
}