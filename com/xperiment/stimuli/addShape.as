﻿package  com.xperiment.stimuli{

	import com.xperiment.uberSprite;
	
	import flash.display.Shape;
	import flash.geom.Matrix;



	public class addShape extends object_baseClass {

		private var myShape:Shape;
		
		//public function Trial_imageCollage(genAttrib:XMLList, specAttrib:XMLList) {
		override public function setVariables(list:XMLList):void {
			setVar("number","colour",0x0000FF);
			setVar("uint","ellipseWidth", 20);
			setVar("uint","ellipseHeight",20);
			setVar("uint","lineThickness",5);
			setVar("uint","border",0);
			setVar("uint","lineColour",0x000000);
			setVar("int","transparency",1,"0-1");
			setVar("string","shape","rectangle","roundedRectangle||rectangle||ellipse||circle||triangle||square||tick||smile");
			setVar("uint","radius",40);
			setVar("string","gradient",""); // e.g. "type:linear,colors:[0x000000,0xffffff],alphas:[1,1],ratios:[1,255],angle:90"
			super.setVariables(list);
			if(getVar("shape")=="")setVar("string","shape",getVar("myShape"));
		}

		override public function RunMe():uberSprite {
			
			myShape=new Shape  ;
			
			var myGradient:Object;
			if(getVar("gradient")) myGradient = processGradient(getVar("gradient"));
			
			myShape=makeShape(getVar("shape"),getVar("lineThickness"),getVar("lineColour"),getVar("transparency"),getVar("colour"),100,100,getVar("border"),myGradient);
			super.pic.addChild(myShape);
			
			super.setUniversalVariables();
			return (super.pic);
		}
		

		public function processGradient(dat:String,orientation:Number=10000):Object
		{
			var obj:Object=new Object;
	
			obj.type = rip("type:",dat,",","linear") as String; //linear or radial
			obj.colors = (rip("colors:",dat,"]","0x000000,0xffffff","[]") as String).split(",") as Array; 
			obj.alphas = (rip("alphas:",dat,"]","1,1","[]") as String).split(",") as Array; 
			obj.ratios = (rip("ratios:",dat,"]","0,255","[]") as String).split(",") as Array; 
			var angle:Number = Number(rip("angle:",dat,",","0")); //linear or radial
			if(orientation!=10000)angle=orientation;
			obj.mat = gradBox(100,100,angle);
			
			return obj;
		}
		
		public function gradBox(width:Number,height:Number,angle:Number):Matrix{
			var mat:Matrix = new Matrix;
			mat.createGradientBox(width, height,angle*Math.PI/180);
			return mat;
		}
		
		public function rip(nam:String,dat:String,endTag:String, myDefault:String, trim:String=""):String{
			
			var loc:int=dat.search(nam);
			if(loc==-1)return myDefault;
			var temp:String=dat.substr(loc+nam.length);
			loc=temp.search(endTag);
			if(loc!=-1)temp=temp.substr(0,loc);
			for(var i:uint=0;i<trim.length;i++){
				temp=temp.split(trim.charAt(i)).join("");

			}
			return temp;
		}
		
		static public  function makeShape(type:String, lineThickness:Number, lineColour:Number, transparency:Number, colour:Number, width:int,height:int,line:Boolean=true,gradient:Object=null):Shape {
			var sha:Shape=new Shape  ;
			if(line)sha.graphics.lineStyle(lineThickness,lineColour,transparency);
			sha.graphics.moveTo(0,0);
			switch(type.toLowerCase()){
				case "circle":
					sha.graphics.beginFill(colour,transparency);
					if(gradient)sha.graphics.beginGradientFill(gradient.type, gradient.colors, gradient.alphas, gradient.ratios,gradient.mat);
					sha.graphics.drawEllipse(0, 0,width,height);
					break;
				case "triangle":
					sha.graphics.beginFill(colour,transparency);
					if(gradient)sha.graphics.beginGradientFill(gradient.type, gradient.colors, gradient.alphas, gradient.ratios,gradient.mat);
					sha.graphics.lineTo(width,0);
					sha.graphics.lineTo(Math.round(width)*.5,height);
					sha.graphics.lineTo(0,0);
					break;
				case "rectangle":
				case "roundedRectangle":
				case "square":
					
					sha.graphics.beginFill(colour,transparency);
					if(gradient)sha.graphics.beginGradientFill(gradient.type, gradient.colors, gradient.alphas, gradient.ratios,gradient.mat);
					sha.graphics.lineTo(width,0);
					sha.graphics.lineTo(width,height);
					sha.graphics.lineTo(0,height);
					sha.graphics.lineTo(0,0);
					break;
				case "tick":
					sha.graphics.moveTo(0, height*.25);
					sha.graphics.lineTo(width*.25, 0);
					sha.graphics.curveTo(width/2, height/2,width, height);
					sha.y=-height*.8;
					sha.x=width*.2
					break;
					
				case "smile":
				    sha.graphics.moveTo(width*.2, height*.75);
					sha.graphics.drawCircle(width*.2, height*.5, height*.2);
					sha.graphics.drawCircle(width*.6, height*.5, height*.2);
					sha.graphics.moveTo(0, 0);
					   sha.graphics.curveTo(width/2.3, -height/2,width*.75, 0);
					sha.graphics.drawCircle(width*.2, height*.5, .1);
					sha.graphics.drawCircle(width*.6, height*.5, .1);
					sha.height*=.9;
					sha.y-=height/1.5;
					sha.x+=width/20;
					break;
				case "sad":
				
					break;
			}
			sha.graphics.endFill();
			
			return sha;
		}
		
	}
}