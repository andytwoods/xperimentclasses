﻿package com.xperiment.stimuli{
	import com.Logger.Logger;
	import com.graphics.pattern.Box;
	import com.graphics.pattern.Target;
	import com.xperiment.StimEvents;
	import com.xperiment.codeRecycleFunctions;
	import com.xperiment.uberSprite;
	import com.xperiment.behaviour.BehaviourBoss;
	import com.xperiment.stimuli.primitives.boxLabel;
	import com.xperiment.trial.overExperiment;
	
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Point;
	import flash.system.System;
	import flash.utils.getQualifiedClassName;
	
	public class object_baseClass extends uberSprite implements iBehav, IStimulus{
		public var driveEvent:String="";
		public var OnScreenElements:Array=new Array;
		public var pic:uberSprite;
		public var theStage:*;
		public var duplicateTrialNumber:uint=new uint  ;
		public var iteration:uint;
		public var finishedSettingVariables:Boolean=false;
		public var overEntireExperiment:overExperiment;
		public var storedVariables:Array=new Array  ;
		public var temporaryStoredVariables:Array=new Array  ;
		public var returnStageHeight:int;
		public var returnStageWidth:int;
		public var containerX:int;
		public var containerY:int;
		public var trialWithinBlockPosition:int;
		public var orderOfTrial:int;
		public var orderWithinBlock:int;
		public var behaviours:Array;
		public var objectData:Array=new Array  ;
		//public var myBehaviours:Array;
		public var listOfDrivenEvents:Array;
		public var listOfStopEvents:Array;
		import flash.system.System;
		public var alive:Boolean=true;
		public var outLineBoxes:Array;
		public var percentageScreenSizeFrom:String="both";
		private var myBoxLabel:boxLabel;
		public var logger:Logger;
		public var myName:String;
		public var GUItrialObjs:Object = new Object;
		public var parentPic:Sprite;
		public var manageBehaviours:BehaviourBoss;
		private var classLevel:uint=0;
		public var stimEvents:StimEvents;

		public var disallowedActions:Vector.<String>;
		public var disallowedEvents:Vector.<String>;
		public var disallowedSettings:Vector.<String>;
		
		public function passLogger(logger:Logger):void{
			this.logger=logger;
		}
		
/*		override public function onBefore():void{
			trace("hererer");
			if(behav && behav.hasOwnProperty("doBefore"))this.manageBehaviours.doBehaviourFirst(this);
		}
		
		override public function onAfter():void{
			if(behav && behav.hasOwnProperty("doAfter"))this.manageBehaviours.doBehaviourAfter(this);
		}*/
		
		public function mouseTransparent():void{
			pic.mouseEnabled=false;
		}
		
		/*override public function myUniqueActions(action:String):Function{

			return null
		}*/
		

		public function events(active:Boolean):void{};

		public function getVar(nam:String):* {
			if (OnScreenElements && OnScreenElements[nam]!=undefined) {
				return OnScreenElements[nam];
				if(logger)logger.log("retrieved variable from baseClass");
			}
			else {
				if(logger)logger.log("! variable ["+nam+"] asked for but you have not specified it");
				return "";
			}
		}
		


		public function getTempStoredVars():Array {
			//logger.log("gave stored vars");
			return temporaryStoredVariables;
		}

		public function setStoredVars(arr:Array):void {
			storedVariables=arr;
		}

		public function setVar(typ:String, nam:String, val:*,defaultVals:String="",info:String=""):void {
			if(!OnScreenElements.hasOwnProperty(nam))OnScreenElements[nam]=returnType(typ.toLowerCase(),val,nam);
			//OnScreenElements[nam]=returnType(typ.toLowerCase(),val,nam);
			if(!theStage)setVarGUI(typ,nam,val,info);
		}
		
		public function setVarBase(typ:String, nam:String, val:*,defaultVals:String="",info:String=""):void {
			if(!OnScreenElements.hasOwnProperty(nam))OnScreenElements[nam]=returnType(typ.toLowerCase(),val,nam);
			if(!theStage)setVarGUI(typ,nam,val,info);
		}
		private function setVarGUI(typ:String, nam:String, val:*,defaultVals:String="",info:String=""):void{
			GUItrialObjs[nam]=new Object;
			GUItrialObjs[nam].defaul=returnType(typ,val,nam);
			GUItrialObjs[nam].inputMask=defaultVals;
			GUItrialObjs[nam].info=info;
		}

		public function setUpTrialSpecificVariables(trialObjs:Object):void {
			theStage=trialObjs.theStage;
			if(trialObjs.parent)this.parentPic=trialObjs.parent as Sprite;
			myName=trialObjs.name;
			returnStageHeight=trialObjs.h;
			returnStageWidth=trialObjs.w;
			containerX=trialObjs.containerX;
			containerY=trialObjs.containerY;

			if (trialObjs && trialObjs.perSize &&  trialObjs.perSize!="")percentageScreenSizeFrom=trialObjs.perSize.toLowerCase;
			storedVariables=trialObjs.storedVariables;
			if (trialObjs.oe!=null) {
				overEntireExperiment=trialObjs.oe;
			}

			iteration=trialObjs.i;
			trialWithinBlockPosition=trialObjs.trialBlockPositionStart;
		}
		
		
		public function showBox(depth:uint=0):void{
			drawBox(pic,0x46d1ff);
		}
			
		public function drawBox(obj:*,col:Number):void{
				drawMovePoint(obj.x-1-getVar("padding-left"),obj.y+1-getVar("padding-top"),col);
						
				var myObj:Object = new Object();
				myObj.lineColour=col;myObj.fillColour=0;
				myObj.lineThickness=1;
				myObj.width=obj.myWidth;myObj.height=obj.myHeight;
				

				var showBox:Shape=Box.myBox(myObj);
				showBox.x=obj.localToGlobal(new Point()).x-getVar("padding-left"); 
				showBox.y=obj.localToGlobal(new Point()).y-getVar("padding-top");
								
				addToStage(showBox);
				
				if (!outLineBoxes) outLineBoxes = new Array;
				outLineBoxes.push(showBox);
				
		}
		
		public function drawMovePoint(xPos:int,yPos:int,col:Number):void{
			var size:uint=7;
						
			var obj:Object = new Object();
			obj.lineColourCross=col;obj.lineColourCircle=col;obj.fillColour=0;
			obj.lineCrossThickness=1;obj.lineCircleThickness=2;
			obj.radius=7;
			var movePoint:Shape=Target.myTarget(new Object);
			movePoint.x=xPos; movePoint.y=yPos;
			
			addToStage(movePoint);
			if (!outLineBoxes) outLineBoxes = new Array;
			outLineBoxes.push(movePoint);
			obj=null;
		}
		
		private function addToStage(sha:Shape):void{
			addToBoxStuff(sha);
			theStage.addChildAt(sha,theStage.numChildren-1);
		}
		
	
		public function updateMe(str:String):void {
			logger.log("-----PROBLEM: you asked to update me with these parameters: "+str+". Afraid I do know what to do with them!");
		}

		public function onLoaded():void {
		}// probably redundant

		public function setVariables(list:XMLList):void {
			pic=this;
			//setVarBase("boolean","continuouslyShown",true);
			setVarBase("boolean","mouseTransparent",false);
			setVarBase("string","peg","");
			setVarBase("string","behaviours","","needs a good description"); // start,onFinish,onCk,OnCli,onEnd, OnShow,now			
			setVarBase("string","timeStart","");
			setVarBase("string","timeEnd","forever");
			setVarBase("number","rotation",0);
			setVarBase("int","scaleX",1);
			setVarBase("int","scaleY",1);
			setVarBase("int","scale", 1);
			//setVarBase("uint","howMany",1); //note, totally rendundant, but included it to stop these annoying 'this var does not exist' error appearing all the time.
			setVarBase("string","width","50%");
			setVarBase("string","height","50%");
			//setVarBase("uint","widthPercent",0);
			//setVarBase("uint","heightPercent",0);
			setVarBase("string","id","","needs a description");
			setVarBase("uint","numberOfTrialsInThisBlock",0,"hide");
			//setVarBase("String","boxLabel","");
			setVarBase("boolean","showBox",false,"","hide");
			setVarBase("boolean","hideResults",true,"","if you don't want to collect data for this object, specify false");
			setVarBase("boolean","ignoreData",true);
			setVarBase("String","x","50%","","the x-position of the object on the screen.  Use a % sign if you want specify this as a % of screen size");
			setVarBase("String","y","50%","","the x-position of the object on the screen.  Use a % sign if you want specify this as a % of screen size");
			setVarBase("string","horizontalPosition0","middle","left||middle||right","use to change the '0,0' point on the object");
			setVarBase("string","verticalPosition0","middle","top||middle||bottom","use to change the '0,0' point on the object");
			setVarBase("int","padding-left",0,"","if you want some blank space to the left of your object");
			setVarBase("int","padding-right",0,"","if you want some blank space to the right of your object");
			setVarBase("int","padding-top",0,"","if you want some blank space on the top of your object");
			setVarBase("int","padding-bottom",0,"","if you want some blank space at the bottom of your object");
			setVarBase("int","padding",0,"if you want some blank space around your object");
			setVarBase("string","name","");
			setVarBase("String","percentageScreenSizeFrom","both","vertical||both||horizontal","this is useful if you ensure that your object is never 'offscreen'");
			setVarBase("String","containerCell-verticalAlign","middle");
			setVarBase("String","containerCell-horizontalAlign","middle");
			setVarBase("Number","opacity",1,"0-1");
			setVarBase("boolean","visible",true);
			setVarBase("string","present","");
			setVarBase("uint","howMany",1);
			setVarBase("string","delay","");
			setVarBase("string","duration","");
			setVarBase("string","results1","");
			setVarBase("string","if","");
			
			setVarBase("string","deepID","");//needed for 'xperimentMaker'

			XMLListObjectPropertyAssigner(list);

			
			
			var logMessage:Function = function():void{if(logger)logger.log("!Please stop using startID and stopID and use peg instead.  Phasing out the first 2 terms")};
			
			if(getVar("peg")=="" && (OnScreenElements.startID || OnScreenElements.stopID)){
				if(getVar("startID")!=""){setVarBase("string","peg",getVar("startID"));logMessage();}
				else if(getVar("stopID")!=""){setVarBase("string","peg",getVar("stopID"));logMessage();}
			}
							
			if(OnScreenElements.behaviours2!=undefined){
				var contin:Boolean = true;
				var behav:String=getVar("behaviours");
				var counter:uint=2;
				var joiner:String;
				//if (behav=="") contin=false;
				while(contin){
					if(getVar("behaviours"+String(counter))!=""){
						//if(counter==2)trace(123,getVar("behaviours"),getVar("behaviours")=="",OnScreenElements.behaviours.substr(OnScreenElements.behaviours.length-1,1)==",");
						if(OnScreenElements.behaviours.substr(OnScreenElements.behaviours.length-1,1)==",")joiner="";
						else if(counter==2 && getVar("behaviours")==""){
							//trace("I",getVar("behaviours"));
							joiner="";
						}
						//else if(getVar("behaviours"+String(counter-1))=="")joiner="";
						else joiner=",";
						OnScreenElements.behaviours=OnScreenElements.behaviours+joiner+getVar("behaviours"+String(counter));
						//trace(OnScreenElements.behaviours)
						counter++;
					}
					else {contin=false; break;}
				}	
			}

			//trace(this.rawBehav,2);
			
			//this.behavID=getVar("behaviourID");
			//this.startID=getVar("startID");
			//this.stopID=getVar("stopID");
			//this.doID=getVar("doID");
			this.peg=getVar("peg");
			pic.peg=peg;
			//trace("eeeee",peg);
			this.name=getVar("name");
			this.id=getVar("id");
			pic.deepID=getVar("deepID");
			//pic.start=getVar("timeStart");
			//pic.end=getVar("timeEnd");
			
			percentageScreenSizeFrom=getVar("percentageScreenSizeFrom");

			pic.addEventListener(Event.ADDED_TO_STAGE,appearedOnScreen,false,0,true);
			if(getVar("visible")==false)pic.visible=false;
			
			if (getVar("padding")!=0){
				if (getVar("padding-left")==0) setVarBase("int","padding-left",getVar("padding"));
				if (getVar("padding-right")==0) setVarBase("int","padding-right",getVar("padding"));
				if (getVar("padding-top")==0) setVarBase("int","padding-top",getVar("padding"));
				if (getVar("padding-bottom")==0) setVarBase("int","padding-bottom",getVar("padding"));
				
				}
			//stimEvents=new StimEvents(peg,getQualifiedClassName(this),true);
		}
		
		public function setStartEndTimes(start:uint,end:uint):void{
			if(pic){
				pic.start=start;
				pic.end=end;
			}
		}
		
		
		private function removeSpacesNotInsideQuotes(str:String):String{
			//trace(this,peg,str);
			if((str.split("'")as Array).length%2==1){
				
				var quot:Boolean=false;
				var char:String;
				var newStr:String="";
				for(var i:int=str.length-1;i>=0;i--){
					char=str.charAt(i);
					if(char==String.fromCharCode(39))quot=!quot;
					if(quot || char!=" ")newStr=char+newStr;
				}
				return newStr
			}
			else {
				if(logger) logger.log("!Problem with logic ["+str+"] having an uneven number of single quotation marks [peg="+peg+"].");
				return "";
			}
		}
		
		
		
		public function appearedOnScreen(e:Event):void{
			pic.removeEventListener(Event.ADDED_TO_STAGE,appearedOnScreen);
			sortOutShowBox();
			//sortOutShowBoxLabel();
			
		}
		
		public function sortOutShowBox():void{
			if (getVar("showBox"))showBox();
		}
		
/*		public function sortOutShowBoxLabel():void{
			if (getVar("boxLabel")!=""){
				myBoxLabel = new boxLabel(getVar("boxLabel"), pic.myWidth,pic.myHeight);
				pic.addChild(myBoxLabel);	
			}
		}*/

		public function storedData():Array {
			
			if((getVar("results1") as String).length!=0){
				//trace("herererer");
				var count:int=1;
				var tempData:Array
				var arr:Array;
				while(true){
					var str:String= getVar("results"+String(count));
					if(str=="")break;
					if(str.indexOf("property(")==-1){
						tempData=[];
						if(str.indexOf(":")!=-1){
							arr=str.split(":")
							tempData.event=arr[0];
							if(arr.length>1)tempData.data=arr[1];
						}
						else {
							tempData.event="results"+String(count);
							tempData.data=str;
						}
						objectData.push(tempData);
					}
					else{//where we use the value from a property specified within property(here)

						arr=str.split(":");
						if(arr.length>=2){
							tempData=[];
							tempData.event=arr[0];
							arr[1]=(arr[1] as String).replace("property(","").replace(")","");
							//trace(arr[1],22);
							tempData.data=getVar(arr[1]);
							//trace(tempData.data);
							objectData.push(tempData);
						}
						
						
					}
					
					count++
					
				}
			}		
			return objectData;
		}
	
		public function returnsDataQuery():Boolean {
			return !(getVar("ignoreData") as Boolean);
		}
		
		public function putInResults(event:String,data:String):void{
			var tempData:Array
			tempData = new Array();
			tempData.event=event; 
			tempData.data=data;
			objectData.push(tempData);
		}

		public function updateStoredVariablesEachTrial():Boolean {
			return false;
		}
		
		public function myScore():String{
			return new String;
		}

		public function XMLListObjectPropertyAssigner(textinfo:XMLList):void {
			duplicateTrialNumber=uint(textinfo.duplicateTrialNumber);
			
						
			var multObjCorSym:String="---";
			var multiObjFixedSym:String="|||"
			var multTriCorSym:String=";";	
			var multTriFixedSym:String="~";
			
			
			var attNamesList:XMLList=textinfo.@*;
			for (var i:int=0; i<attNamesList.length(); i++) {
				var tag:String=attNamesList[i].name();// id and color
				var tagValue:String=attNamesList[i]  ;

				if (tagValue.indexOf(multTriCorSym)!=-1)	{
					tagValue=codeRecycleFunctions.multipleTrialCorrection(tagValue,multTriCorSym,duplicateTrialNumber);
				}
				if (tagValue.indexOf(multObjCorSym)!=-1)	tagValue=codeRecycleFunctions.multipleTrialCorrection(tagValue,multObjCorSym,iteration);
				if (tagValue.indexOf(multTriFixedSym)!=-1)	tagValue=codeRecycleFunctions.multipleTrialCorrection(tagValue,multTriFixedSym,trialWithinBlockPosition);
				if (tagValue.indexOf(multiObjFixedSym)!=-1)	tagValue=codeRecycleFunctions.multipleTrialCorrection(tagValue,multiObjFixedSym,iteration);				
				
				tagValue=suckOutPutInStoredVariables(tagValue);

				//var a:Array=tag.split('.');
				//trace(tag,22);
				var message:String = new String;
				if (OnScreenElements[tag]!=undefined) {
					if(this.toString()=="[object addShapeMatrix]"){
						//trace(OnScreenElements[a[0]],a,tagValue,typeof OnScreenElements[a[0]]);
					}
					OnScreenElements[tag]=returnType(typeof OnScreenElements[tag],tagValue,tag);
				}
				else {
					OnScreenElements[tag]=tagValue as String;
					message=". BTW, there are no default settings for this variable so the variable may be redundant.";
				}
				
				if(logger)logger.log("["+attNamesList[i].name()+"="+tagValue+"]" +message);
			}
		}
		
		//removed as mucked up Text when there was a percentage sign in it.
		//dont see the purpose of this function!
			//AW NOW DO! useful for when there is a % sign in other variables. E.g. used heavily in addSlider
		private function sortOutPercentageSign(str:String,objType:String):String{

			if(objType!="x" && objType!="y" && objType!="width" && objType!="height" &&
			   objType!="padding-left" &&  objType!="padding-right" &&  objType!="padding-top" &&  objType!="padding-bottom" && 
			   str.indexOf("%")!=-1 &&
			   objType!="split" && objType!="splitY" && objType!="splitX")str=numFromPercentage(str);
			else if (objType=="padding-left" && str.indexOf("%")!=-1){
				str=String(int(str.replace("%",""))*returnStageWidth/100);
			}
			else if(objType=="padding-right" && str.indexOf("%")!=-1){
				str=String(int(str.replace("%",""))*returnStageWidth/100);
			}
			else if(objType=="padding-top" && str.indexOf("%")!=-1){
				str=String(int(str.replace("%",""))*returnStageHeight/100);
			}
			else if(objType=="padding-bottom"&& str.indexOf("%")!=-1){
				str=String(int(str.replace("%",""))*returnStageHeight/100);
			}
			return str;
		}
		
		public function numFromPercentage(str:String):String{
			switch(getVar("percentageScreenSizeFrom")){
				case "both":
					str=String(int(str.replace("%",""))*(returnStageWidth+returnStageHeight)/200);
					break;
				case "horizontal":
					str=String(int(str.replace("%",""))*returnStageWidth/100);
					break;
				case "vertical":
					str=String(int(str.replace("%",""))*returnStageHeight/100);
					break;
				default:
					str=String(int(str.replace("%","")));
					logger.log("you have entered a value of percentageFromWidthOrHeightOrBoth that was not recognised (can be: 'horizontal', 'vertical', 'both')");
			}
			return str;
		}

		private function returnType(type:String,value:*, objType:String):* {
			type=type.toLowerCase();
			value=sortOutPercentageSign(value,objType);
			var returnType:*;
			switch (type) {
				case "string" :

					returnType=String(value);
					break;
				case "int" :
					returnType=int(value);
					break;
				case "number" :
					returnType=Number(value);
					break;
				case "boolean" :
					if(value=="true" || value==true)returnType=Boolean(true);
					else returnType=Boolean(false);
					break;
				case "uint" :
					returnType=uint(value);
					break;
				case "array" :
					returnType=value as Array;
					break;
				default :
					logger.log("incorrect variable type-----------------!!!!!!!");
			}

			return returnType;
		}

		public function RunMe():uberSprite {
			return pic;
		}

		public function setUniversalVariables():void {		
			sortOutScaling();
			sortOutWidthHeight();
			setContainerSize();	
			setRotation();
			setPosPercent();
			sortOutPadding();
			if(getVar("mouseTransparent") as Boolean)mouseTransparent();
			if(getVar("opacity")!=1)pic.alpha=getVar("opacity");
			
		}
			
		public function sortOutPadding():void{			
			var tempShift:uint=getVar("padding-left");
			pic.x=pic.x+tempShift;
			tempShift=getVar("padding-top");
			pic.y=pic.y+tempShift;
		}
		
		public function sortOutScaling():void{
				
			if (getVar("scale")!=1) {
				setVar("string", "scaleX", getVar("scale"));
				setVar("string", "scaleY", getVar("scale"));
			}
			doScaling(getVar("scaleX"),getVar("scaleY"));
		}
		
		public function doScaling(scaleX:Number,scaleY:Number):void{
			pic.scaleX=scaleX; pic.scaleY=scaleY;
		}
		
		public function sortOutWidthHeight():void{
			var arr:Array=getWidthHeight();
			
			
			if(!isNaN(arr[0]))pic.width=arr[0];
			if(!isNaN(arr[1]))pic.height=arr[1];
		}
		
		
		public function getWidthHeight():Array{
			var staWidth:uint=returnStageWidth;
			var staHeight:uint=returnStageHeight;
			
			var hor:Number; var ver:Number;
			var tempStr:String = getVar("width");
			if (tempStr!="0" && tempStr!="aspectRatio"){
				if(tempStr.indexOf("%")!=-1) hor=staWidth*int(tempStr.replace("%",""))/100;
				else hor=int(tempStr);
			}

			tempStr = getVar("height");
			if (tempStr!="0" && tempStr!="aspectRatio"){
				if(tempStr.indexOf("%")!=-1) ver=staHeight*int(tempStr.replace("%",""))/100;
				else ver=int(tempStr);
			}
			
			if(getVar("width")=="aspectRatio" && getVar("height")!="aspectRatio"){
				hor=ver;
			}
				
			else if(getVar("height")=="aspectRatio" && getVar("width")!="aspectRatio"){
				ver=hor;
			//	trace(ver,22)
			}
			else if(getVar("height")=="aspectRatio" && getVar("width")=="aspectRatio"){
				logger.log(getVar("height")+" you have specified both your width and height as 'aspectRatio' - you can only specify one as 'aspectRatio'");
			}


			return [hor,ver];
		}
		
	/*	private function setWidthHeight(staWidth:uint, staHeight:uint):void{
			var hor:Number; var ver:Number;
			var tempStr:String = getVar("width");
			if (tempStr!="0"){
				if(tempStr.indexOf("%")!=-1) hor=staWidth*int(tempStr.replace("%",""))/100;
				else hor=int(tempStr);
			}
			tempStr = getVar("height");
			if (tempStr!="0"){
				if(tempStr.indexOf("%")!=-1) ver=staHeight*int(tempStr.replace("%",""))/100;
				else ver=int(tempStr);
			}
			pic.width=hor;
			pic.height=ver;
			
			
			
		}*/
		
		
		public function sortOutColours(obj:*):*{
			var num:Number;
				if(obj as Number || isNaN(Number(obj))==false)num=Number(obj);
				else{
					switch(obj){
						case "red":
							num=0xFF0000;
							break;
						case "green":
							num=0x008000;
							break;
						case "blue":
							num=0x0000FF;
							break;
						case "yellow":
							num=0xFFFF00;
							break;
						case "pink":
							num=0xFFC0CB;
							break;
						case "orange":
							num=0xFFA500;
							break;
						case "white":
							num=0xFFFFFF;
							break;
						case "brown":
							num=0xA52A2A;
							break;
						case "black":
							num=0x000000;
							break;
						default:
							num=0x000000;
							if(logger)logger.log("you have specified a colour for the '"+myName+"' object incorrectly or have used text I don't understand (so using black):"+num);
							
	//colours defined here http://www.htmlgoodies.com/tutorials/colors/article.php/3478961/So-You-Want-A-Basic-Color-Code-Huh.htm
					}
				}
			return num;
		}
		


		
		public function setContainerSize():void{
			pic.myWidth=pic.width+getVar("padding-left")+getVar("padding-right");
			pic.myHeight=pic.height+getVar("padding-top")+getVar("padding-bottom");
		}

		public function setPosPercent():void {
			var tempPos:int;
		
			if ((getVar("x")as String).indexOf("%")!=-1) {
				tempPos=int((getVar("x") as String).replace("%",""));
				switch (getVar("horizontalPosition0")) {
					case ("left") :
						pic.x=containerX+(tempPos*.01*returnStageWidth);
						break;
					case ("right") :
						pic.x=containerX+(tempPos*.01*returnStageWidth)-(pic.myWidth);
						break;
					default :
						pic.x=containerX+(tempPos*.01*returnStageWidth)-(pic.myWidth/2);
				}
			}else {
				tempPos=containerX+int(getVar("x"));
				switch (getVar("horizontalPosition0")) {
					case ("left") :
						pic.x=tempPos;
						break;
					case ("right") :
						pic.x=tempPos-(pic.myWidth);
						break;
					default :
						pic.x=tempPos-(pic.myWidth/2);
				}
			}
		
			if ((getVar("y") as String).indexOf("%")!=-1) {
				tempPos=int((getVar("y")as String).replace("%",""));
				switch (getVar("verticalPosition0")) {
					case "top" :
						pic.y=containerY+(tempPos*.01*returnStageHeight);
						break;
					case "bottom" :
						pic.y=containerY+(tempPos*.01*returnStageHeight)-(pic.myHeight);
						break;
					default :
						pic.y=containerY+(tempPos*.01*returnStageHeight)-(pic.myHeight/2);
						break;
		
				}
			}else {
				tempPos=containerY+int(getVar("y"));
				switch (getVar("verticalPosition0")) {
					case ("top") :
						pic.y=tempPos;
						break;
					case ("bottom") :
						pic.y=tempPos-(pic.myHeight);
						break;
					default :
						pic.y=tempPos-(pic.myHeight/2);
				}
			}
			pic.myX=pic.x;
			pic.myY=pic.y;
		}

		
		
		public function setRotation():void {
			pic.rotation=getVar("rotation");
		}


		public function suckOutPutInStoredVariables(str:String):String {
			var returnStr:String;
			var tempSuckOutOfMem:Array=str.split("-GET-");
			var tempPutInMem:Array=str.split("-SET-");
			if (tempSuckOutOfMem.length==1&&tempPutInMem.length==1) {//this just asks if there are any 'memory' commands.  If no, do nothing.  
				returnStr=str;
			}
			else if (tempPutInMem.length>1) {
				temporaryStoredVariables.push(tempPutInMem);
				logger.log("have put this variable and value in memory: ["+tempPutInMem[0]+","+tempPutInMem[1]+"]");
				returnStr=tempPutInMem[1];
			}
			else if (tempSuckOutOfMem.length>1) {
				returnStr=getFromStoredVariables(tempSuckOutOfMem[0]);
				logger.log("have taken this variable and value from memory: ["+tempSuckOutOfMem[0]+","+returnStr+"]");
				if (tempSuckOutOfMem[1].substr(0,1)=="[") {
					returnStr=sortOutLogic(returnStr,tempSuckOutOfMem[1]);
				}
			}
			else {
				logger.log("-----Problem with the storing / retrieving variables code");
			}
			return returnStr;
		}
		

		private function sortOutLogic(str:String,logicTxt:String):String {
			var returnStr:String;
			var tmpString:String=logicTxt.substring(logicTxt.indexOf("[")+1,logicTxt.indexOf("]"));
			var tempArr:Array=tmpString.split(",");
			if (tempArr[0]=="=") {
				logger.log("LOGIC: you asked if "+str+"="+tempArr[1]+".  If so, return "+tempArr[2]+", if not, return "+tempArr[3]);
				if (tempArr[1]==str) {
					returnStr=tempArr[2];
				}
				else {
					returnStr=tempArr[3];
					returnStr=tempArr[3];
				}
				logger.log("LOGIC: returned "+returnStr);
			}
			else {
				returnStr="";
				logger.log("there is an error with your logic");
			}
			return returnStr;
		}

		public function getFromStoredVariables(str:String):String {
			var returnStr:String=new String  ;

			for (var i:uint=0; i<storedVariables.length; i++) {

				if (storedVariables[i][0]==str) {
					returnStr=storedVariables[i][1];
				}
			}
			for (i=0; i<temporaryStoredVariables.length; i++) {
				if (temporaryStoredVariables[i][0]==str) {
					returnStr=temporaryStoredVariables[i][1];
				}
			}

			if (returnStr=="") {
				logger.log("no variable stored of this name: "+str);
			}

			return returnStr;
		}

		override public function kill():void {
			
			parentPic=null;
			alive=false;
			if(pic.hasEventListener(Event.ADDED_TO_STAGE)) pic.removeEventListener(Event.ADDED_TO_STAGE,appearedOnScreen);
			if (outLineBoxes){
				while(outLineBoxes.length!=0){
					theStage.removeChild(outLineBoxes[outLineBoxes.length-1]);
					outLineBoxes.pop();
				}
			}
			
			if(!myBoxLabel){
				myBoxLabel=null;
			}
			
			
			driveEvent=null;
			super.kill();
			pic=null;
			System.gc();
		}
			
		public function getClassName(o:Object):String{
        	var fullClassName:String = getQualifiedClassName(o);
        	return fullClassName.slice(fullClassName.lastIndexOf("::") + 1);
    	}
		
		
		public function behav_getVar(variable:String):*{
			//trace("getVar:"+variable);
			if(this.hasOwnProperty(variable) && String(typeof((this[variable]))).toLocaleLowerCase()=="function") { //else if a special function set up/
				//trace(55,variable,this.hasOwnProperty(variable) , String(typeof((this[variable]))).toLocaleLowerCase()=="function");
				return this[variable]();
			}
				
			else if(this.OnScreenElements.hasOwnProperty(variable)){ //if saved as a property	
				return this.OnScreenElements[variable];
			}
			else {
				if(logger)logger.log("!You asked me (peg="+peg+") for a variable's ("+variable+") value, but that variable does not exist");
				return "";
			}
		}
		
		public function behav_setVar(variable:String,what:*):void{
			//trace(111,variable,what,peg,this);
			if(what!=null){
				if(this.hasOwnProperty(variable) && String(typeof((this[variable]))).toLocaleLowerCase()=="function") { //else if a special function set up
					this[variable](what);
				}
					
				else if(this.OnScreenElements.hasOwnProperty(variable)){ //if saved as a property	
					this.OnScreenElements[variable]=what;
					//trace("in here",variable,what,peg);
				}
				else{
					if (logger)logger.log("!You asked me (peg="+peg+") to set a variable's ("+variable+") value (to this: "+what+"), but that variable does not exist");	
				}
			}

		}
		
		////////////////////////
		////////////////////////
		
		//below could be used to set an objects variable from another.  Decided not to use it as do see where one could use it!  Done so in this fashion: behaviours="myPeg[myPegOrigVar>newPegVar]
		//the below functions pull the myPegOrigVar value and put it into the newPegVar.
		//n.b. to use you need to rename givenObjects to givenObjectsPreStep in behaviourBoss;
		
/*		override public function input(str:String,obj:uberSprite, id:String=""):void{
			//trace("objInput",this,str,id);
			var arr:Array=str.split(">");
			
			if(str.indexOf(">")==-1 || arr.length!=2){if(logger)logger.log("!Cannot change a variable (input="+str+") as it is incorrectly formatted in your script.  Should be e.g. onFinish=input[data>text].");}
			else{

				var receiveVar:String=arr[1];
				var sendVar:String=arr[0];
				sendVar=sendVar.replace("+","").replace("-","");
				var val:String=getVar(sendVar);
				
				//trace("in hrerererererer",str,(obj as object_baseClass).OnScreenElements[sendVar],3,sendVar,this,this.peg);
				
				if((obj as object_baseClass).OnScreenElements.hasOwnProperty(sendVar)){
					
					var giveVar:String=(obj as object_baseClass).OnScreenElements[sendVar];
				
				
					if(this.hasOwnProperty(receiveVar) && String(typeof((this[receiveVar]))).toLocaleLowerCase()=="function") { //else if a special function set up
						//trace("11111111111",String(typeof((this[receiveVar]))).toLocaleLowerCase()=="function");
						
						if(str.indexOf("+")!=-1 || str.indexOf("-")!=-1){
							if(logger)logger.log("!Problem, you have specified you want to concatenate a variable but this is not possible for "+sendVar);
						}
							else this[receiveVar](giveVar); //this calls a function of a given name (with the val property);
						//Could at a later date let other things (basic variables) be set here but not for time being (use switch not if then).
	
					}
					
					else if(this.OnScreenElements.hasOwnProperty(receiveVar)){ //if saved as a property	
						//if(String(this)=="[object behavTextFilter]")trace("in hrerererererer",str,(obj as object_baseClass).OnScreenElements[sendVar],3,sendVar,this);
						if(str.indexOf("+")!=-1 || str.indexOf("-")!=-1){
							if(this.OnScreenElements[receiveVar]=="")this.OnScreenElements[receiveVar]=giveVar;
							else this.OnScreenElements[receiveVar]+=","+giveVar;
						}
						else this.OnScreenElements[receiveVar]=giveVar;
						//trace("setting variable:",this.peg+"."+receiveVar,"=",this.OnScreenElements[receiveVar]);
					}
					
					else if(logger)logger.log("!You have asked to change a variable that DOES NOT EXIST ["+receiveVar+"] in an object "+this+", to a new value ["+giveVar+"] as taken from this object "+obj+" and this object property ["+sendVar+"]."); 
						
					
				}
				else {if(logger)logger.log("!You have asked to change a variable ["+receiveVar+"] in an object "+this+", to a new value, but this variable ["+sendVar+"] does not exist in this object "+obj+".");}; 
			}	
			
		}
		
		override public function reverseInput(str:String,obj:uberSprite, id:String=""):void{
			//trace("objInput",this,str,id);
			var arr:Array=str.split("{");

			
			if(str.indexOf("{")==-1 || arr.length!=2){if(logger)logger.log("!Cannot change a variable (input="+str+") as it is incorrectly formatted in your script.  Should be e.g. onFinish=input[data<text].");}
			else{
				
				var receiveVar:String=arr[0];
				var sendVar:String=arr[1];
				var val:String=getVar(sendVar);
				
				
				trace("in hrerererererer",str,this.OnScreenElements[sendVar],3,receiveVar,sendVar,this,sendVar,this,this.OnScreenElements.hasOwnProperty(sendVar));
				
				if(this.OnScreenElements.hasOwnProperty(receiveVar)){
					var giveVar:String=this.OnScreenElements[receiveVar];
					trace("in erererer");
					if(String(typeof(((obj as object_baseClass)[receiveVar]))).toLocaleLowerCase()=="function") { //else if a special function set up
						//trace("11111111111",String(typeof((this[receiveVar]))).toLocaleLowerCase()=="function");
						
						(obj as object_baseClass)[receiveVar](giveVar); //this calls a function of a given name (with the val property);
						//Could at a later date let other things (basic variables) be set here but not for time being (use switch not if then).
						
					}
						
					else if((obj as object_baseClass).OnScreenElements.hasOwnProperty(receiveVar)){ //if saved as a property		
						//trace("in ere");
						(obj as object_baseClass).OnScreenElements[receiveVar]=giveVar;
					}
						
					else if(logger)logger.log("!You have asked to change a variable that DOES NOT EXIST ["+receiveVar+"] in an object "+this+", to a new value ["+giveVar+"] as taken from this object "+obj+" and this object property ["+sendVar+"]."); 
				}
				else if(1==1){
					//trace(5555555,this[sendVar]);
			
					
				}
				else {if(logger)logger.log("!You have asked to change a variable ["+receiveVar+"] in an object "+this+", to a new value, but this variable ["+sendVar+"] does not exist in this object "+obj+".");}; 
			}	
			
		}*/
		
		public function giveBehavBoss(manageBehaviours:BehaviourBoss):void{
			this.manageBehaviours=manageBehaviours;
			//manageBehaviours.NOWbehaviours()
		}
		
		public function getValFromAction(txt:String):String{ //objects can be passed data regarding the OBJECT to the Behav this way.  E.g. the object may want to tell the data it is of a specific type.
			//how to use this function in other behavs: (see behavText for eg).
			/*			switch(getValFromAction((obj as object_baseClass).getVar("behaviours")){
			case "giveText":
			break;
			}*/
			var nam:String=getVar("peg")+"[";
			var pos:int=txt.indexOf(nam);
			if(pos!=-1){
				txt=txt.substr(pos+nam.length)
				pos=txt.indexOf("]");
				if(pos!=-1){
					txt=txt.substr(0,pos);
					return txt;
				}
			}
			return "";
		}
		
		/*
		public function customListener(listen:String, callee:Function):Listener
		{
			var L:Listener
			switch(listen){
				case "banana":
					L=new Listener;
					L.killF = function():void{
						this.removeEventListener	
					}
					this.addEventListener(MouseEvent.CLICK
					break;
			}
		}*/
	}
}