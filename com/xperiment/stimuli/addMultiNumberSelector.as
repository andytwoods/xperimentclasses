﻿package com.xperiment.stimuli {

	import com.bit101.components.Style;
	import com.xperiment.uberSprite;
	import com.xperiment.stimuli.object_baseClass;
	import com.xperiment.stimuli.primitives.numberSelector;
	import flash.display.*;
	import flash.display.Shape;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	
	public class addMultiNumberSelector extends object_baseClass {
		private var selectors:Array = new Array;
		private var sha:Shape;
		private var myInfo:Object;
		
		override public function setVariables(list:XMLList):void {
			
			setVar("uint","selectors",2);
			setVar("string","list","0,1,2,3,4,5,6,7,8,9","int,int...int");
			setVar("number","triangleColour",Style.BUTTON_FACE);
			setVar("number","lineAlpha",Style.borderAlpha);
			setVar("uint","fontSize",50);
			setVar("int","lineThickness",Style.borderWidth);
			setVar("number","lineColour",Style.borderColour);
			setVar("uint","triangleWidth",100);
			setVar("uint","triangleHeight",50);
			setVar("int","triangleDistFromBox",0);
			setVar("uint","triangleSeperation",5);
			setVar("uint","textBoxWidth",100);
			setVar("uint","textBoxHeight",100);
			setVar("uint","autoSize","center");
			setVar("uint","textColour",Style.LABEL_TEXT);
			setVar("number","textBoxColour",0xFFFFFF);
			setVar("string","startingVal","22");
			
			
			super.setVariables(list);
			
			if(getVar("id")=="")setVar("string","id","numberSelector");

			make();
		}
		
		private function make():void {
			makeSquare();
			var sel:numberSelector;
			for(var i:uint=0;i<getVar("selectors");i++){
				OnScreenElements.xOffset=i*getVar("textBoxWidth");
				OnScreenElements.startVal=(getVar("startingVal") as String).substr(i,1);
				sel=new numberSelector();
				sel.run(OnScreenElements);
				sel.x=i*getVar("textBoxWidth");
				selectors.push(sel);
				pic.addChild(sel);
			}
			
		}
		
		override public function storedData():Array {
			var tempData:Array = new Array();
			tempData.event=String(getVar("id")) as String;
			tempData.data = "";
			for(var i:uint=0;i<selectors.length;i++){
				tempData.data=tempData.data+String(selectors[i].giveData());
			}
			super.objectData.push(tempData);
			return objectData;
		}
	

		override public function returnsDataQuery():Boolean {
			return true;
		}


		private function makeSquare():void {
			var height:uint=getVar("textBoxHeight");
			var width:uint=getVar("textBoxWidth")*getVar("selectors");
			
			
			sha=new Shape  ;
			sha.graphics.lineStyle(getVar("lineThickness"),getVar("lineColour"),getVar("lineAlpha"));
			sha.graphics.beginFill(getVar("textBoxColour"),1);
			sha.graphics.drawRect(0,0,width,height);
				
			sha.graphics.endFill();
			
			sha.x=0;
			sha.y=+getVar("triangleDistFromBox")+getVar("textBoxHeight")/2;
					
			pic.addChild(sha);


		}
		
		
		override public function RunMe():uberSprite {
			setUniversalVariables();			
			return (super.pic);
		}

		override public function kill():void {
			for (var i:uint=0;i<selectors.length;i++){
				pic.removeChild(selectors[i]);
				selectors[i].kill();
				selectors[i]=null;
			}
			selectors=null;
			pic.removeChild(sha);
			super.kill();
		}
	}
}