﻿package com.xperiment{
	
	import flash.display.Graphics;
	import flash.display.Shape;
	import flash.display.Sprite;
	
	public class uberSprite extends Sprite{
		public var myX:int;
		public var myY:int;
		public var myHeight:int;
		public var myWidth:int;
		public var boxStuff:Array;
		public var info:String;
		//public var behavID:String;
		//public var startID:String;
		//public var stopID:String;
		//public var doID:String;
		public var peg:String;
		public var actions:Array; //onShow:String(rotate[start],rt[start]), onClick:String([rotate[start],rt[start])
		public var behavJustStopped:uberSprite;
		public var usePegs:String;
		public var id:String="";
		public var deepID:String="";//for xperimentMaker
		public var start:uint;
		public var end:uint;
		private var _myGraphics:Graphics;
		public var scratchData:Array;
		
		

		public function get myGraphics():Graphics{return _myGraphics;}
		public function set myGraphics(value:Graphics):void{_myGraphics = value;}

/*		public function onBefore():void{};
		public function onAfter():void{};*/
		
		//used to get private functions for FlexUnit testing
		public function getFunct(nam:String):Function{
			if(this.hasOwnProperty(nam) && this[nam] as Function) return this[nam];
			else return null;
		}
		
		public function scratchpad(vari:String,obj:* =""):String{ //pass a data value.  If exists in scratchdata return true.  If not save value and return false.  If str is blank, just inform whether str exists in data or not.
			
			if (!scratchData){scratchData = new Array;trace(123);}
	//trace(peg,scratchData.length);
			if(obj==""){	
				if(scratchData[vari]==undefined){ 
					//trace(333,scratchData[vari]);
					return "false";
				}
				else return scratchData[vari];
			}
			
			else if(obj=="kill"){
				for (var i:uint=0;i<scratchData.length;i++){
					scratchData[i]=null;
				}
				scratchData=null;
				return "";
			}
			
			else{
				//trace("in here");
				scratchData[vari]=obj;
				//trace(	scratchData[vari],8);
				return "";
			}	
		}
		
		public function addToBoxStuff(sha:Shape):void{
		if(!boxStuff)boxStuff = new Array;
			boxStuff.push(sha);
		}
		
		public function wipeObjects():void{
		}
		
		public function kill():void{
			if(boxStuff)boxStuff=null;
			//if(scratchData)scratchData=null;
		}

		public function uberSprite():void {
			super();
		}
		
		public function myUniqueActions(action:String):Function{return null;}
		public function myUniqueProps(property:String):Function{return null;}
		
		public function runBehav(id:String):void{
		}
		
		public function stopBehav(id:String):void{
		}
		
		public function givenObjects(pic:uberSprite):void{//rename to givenObjects	
		}
		
		
		/*public function action(id:String){
			trace("you are calling the action function in the uberSprite class.  You shouldn't be!");
		}*/

	}
	
}
