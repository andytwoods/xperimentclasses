﻿package com.xperiment{
	/**
	         * Simple script to convert a Datagrid to a HTML table and then 
	         * pass it on to an external excel exporter
	         *
	         * @author: S.Radovanovic (With help of Tracy Spratt through the post on
	         *          http://www.cflex.net/showFileDetails.cfm?ObjectID=298&Object=File&ChannelID=1)
	         */

	//Libs that are mostly used 
	//(only a number are necessary for the datagrid conversion and export)
	
	import flash.errors.*;
	import flash.events.*;
	import flash.external.*;
	import flash.net.URLLoader;
	import flash.net.URLVariables;
	import flash.net.URLRequest;
	import fl.controls.DataGrid;
	import flash.net.URLRequestMethod;
	import flash.net.navigateToURL;
	public class exportToExcel 
	{
//////////////////////
        public var urlExcelExport:String = "exportXMLtoExcel.php";

         function convertDGToHTMLTable(dg:DataGrid):String {
        	//Set default values
        	var font:String = String(dg.getStyle('fontFamily'));
        	var size:String = String(dg.getStyle('fontSize'));
        	var str:String = '';
        	var colors:String = '';
        	var style:String = String('style="font-family:'+font+';font-size:'+size+'pt;"');				
        	var hcolor:Number;
        	
        	//Retrieve the headercolor
        	if(dg.getStyle("headerColor") as Object != undefined) {
        		hcolor = Number([dg.getStyle("headerColor")]);
        	} else {
        		hcolor = Number(dg.getStyle("headerColors"));
        	}				
        	
        	//Set the htmltabel based upon knowlegde from the datagrid
        	str+= '<table width="'+dg.width+'"><thead><tr width="'+dg.width+'" style="background-color:#' +hcolor.toString(16)+'">';
        	
        	//Set the tableheader data (retrieves information from the datagrid header				
        	for(var i:int = 0;i<dg.columns.length;i++) {
        		colors = String(dg.getStyle("themeColor"));
        			
        		if(dg.columns[i].headerText != undefined) {
        			str+="<th "+style+">"+dg.columns[i].headerText+"</th>";
        		} else {
        			str+= "<th "+style+">"+dg.columns[i].dataField+"</th>";
        		}


        	}
        	str += "</tr></thead><tbody>";
        	colors = String(dg.getStyle("alternatingRowColors"));
        	
        	//Loop through the records in the dataprovider and 
        	//insert the column information into the table
        	for(var j:int =0;j<dg.dataProvider.length;j++) {					
        		str+="<tr width=\""+Math.ceil(dg.width)+"\">";
        			
        		for(var k:int=0; k < dg.columns.length; k++) {
        			
        			//Do we still have a valid item?						
        			if(dg.dataProvider.getItemAt(j) as Object != undefined && dg.dataProvider.getItemAt(j) != null) {
        				
        				//Check to see if the user specified a labelfunction which we must
        				//use instead of the dataField
        				if(dg.columns[k].labelFunction != undefined) {
        					str += "<td width=\""+Math.ceil(dg.columns[k].width)+"\" "+style+">"+dg.columns[k].labelFunction(dg.dataProvider.getItemAt(j),dg.columns[k].dataField)+"</td>";
        					
        				} else {
        					//Our dataprovider contains the real data
        					//We need the column information (dataField)
        					//to specify which key to use.
        					str += "<td width=\""+Math.ceil(dg.columns[k].width)+"\" "+style+">"+dg.dataProvider.getItemAt(j)[dg.columns[k].dataField]+"</td>";
        				}

        			}
        		}
        		str += "</tr>";
        	}
        	str+="</tbody></table>";
        
        	return str;
        }
        

			
			public function exportToExcel(dg:DataGrid):void {

			//Pass the htmltable in a variable so that it can be delivered
			//to the backend script
			var variables:URLVariables = new URLVariables(); 
			variables.htmltable	= convertDGToHTMLTable(dg);
			
			//Setup a new request and make sure that we are 
			//sending the data through a post
			var u:URLRequest = new URLRequest(urlExcelExport);
			u.data = variables; //Pass the variables
			u.method = URLRequestMethod.POST; //Don't forget that we need to send as POST
			
			//Navigate to the script
			//We can use _self here, since the script will through a filedownload header
			//which results in offering a download to the user (and still remaining in you Flex app.)
            navigateToURL(u,"_self");
        }       
	}

////////////////////////
}