﻿	
package com.xperiment.trial{
	import com.adventuresInActionscript.utils.SuperTimer;
	import com.adventuresInActionscript.graphics.SuperStage;
	import fl.controls.Button;
	import fl.controls.Slider;
	import fl.controls.SliderDirection;
	import flash.text.TextFieldAutoSize;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.display.*;
	import flash.events.*;
	import flash.text.*;
	import com.xperiment.OnScreenBoss;
	import flash.display.MovieClip;
	import flash.display.Loader;
	import flash.net.URLRequest;
	import flash.geom.*;



	public class Trial_instructions extends Trial {

		var instructions:String = new String;
		var colour:Number=0x000000;

		override public function setup(theSta:Stage, genAttrib:XMLList,specAttrib:XMLList) {
		super.OnScreenElements.Stim=new SuperStage;
		super.OnScreenElements.exptButtons=new Array;
		
			super.setup(theSta, genAttrib,specAttrib);
			//instructions[0]=genAttrib.instructions1;
			instructions=String(genAttrib.instructions1);
			//trace("..."+genAttrib.instructions1);
			if (genAttrib.textColour.text() != null){colour = genAttrib.textColour.text();}
		}
		////////////////////////////////////////////////////////////////////////
		//TRIAL TYPE SPECIFIC VARIABLES AND FUNCTIONS
		////////////////////////////////////////////////////////////////////////


		/////////////////////////////
		//BOSS VARIABLES
		/////////////////////////////



		/////////////////////////////
		//BOSS FUNCTIONS
		/////////////////////////////
		override function trialEvents():void {//where trial type specific stuff is specified.

			super.OnScreenElements.exptButtons.push(super.makeButton("continue",super.OnScreenElements.returnStageWidth/2,super.OnScreenElements.returnStageHeight/1.5));
			
			//super.CurrentDisplay.addElement(addText(instructions, 0x111111, 12, "CENTER",super.returnStageWidth/2,(1)*super.returnStageHeight/5));
			super.OnScreenElements.CurrentDisplay.addElement(addText(instructions, colour, 12, "CENTER",super.OnScreenElements.returnStageWidth/2,(1)*super.OnScreenElements.returnStageHeight/5));
				
			super.OnScreenElements.CurrentDisplay.addElement(OnScreenElements.exptButtons[0]);
			super.OnScreenElements.CurrentDisplay.runTimedEvents();
		}




		override function endOfTrial():void {
			trace(super.trialData.Slider1+" "+super.trialData.Slider1);
			super.OnScreenElements.Stim=null;
			super.endOfTrial();
		}

		/////////////////////////////
		//SUB FUNCTIONS
		/////////////////////////////

	}


}