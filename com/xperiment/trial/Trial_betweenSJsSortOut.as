﻿package com.xperiment.trial{
	import flash.events.Event;
	import flash.display.*;
	import com.xperiment.uberSprite;
	import com.xperiment.trial.Trial;

	public class Trial_betweenSJsSortOut extends Trial {

		public function introSetup(genAttrib:XMLList, exptScript:XML):void{
			this.exptScript=exptScript;
			trialNumber=0;
			objects=genAttrib;
			trialBlockPositionStart=0;
			//duplicateTrialNumber=0;
			trialLabel="betweenSJs_conditionSelector";
			theStage.addEventListener("betweenSJsTrialEvent_Trial", betweenSJsTrialEvent,false,0,true);

		}
		
		public var stuffToReturn:String;

		
		private function betweenSJsTrialEvent(e:Event):void{
			theStage.removeEventListener("betweenSJsTrialEvent_Trial", betweenSJsTrialEvent);
			stuffToReturn=e.target.info;
			theStage.dispatchEvent(new Event("betweenSJsTrialEvent_Runner",true));
			generalCleanUp();
			
		}
			
		public function introInfo():String{return stuffToReturn;}
	}
}