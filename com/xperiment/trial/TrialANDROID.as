﻿package com.xperiment.trial {
	//import flash.display.Sprite;
	//import com.adventuresInActionscript.utils.SuperTimer;
	//import com.adventuresInActionscript.graphics.SuperStage;


	import com.xperiment.admin.adminConsole;
	import com.xperiment.admin.adminData;
	import com.xperiment.admin.adminSetVariable;
	import com.xperiment.behaviour.behavQuit;
	import com.xperiment.stimuli.IStimulus;
	import com.xperiment.stimuli.StimulusFactory;
	import com.xperiment.stimuli.addButtonToCloseProgram;
	import com.xperiment.stimuli.addTouch;

	public class TrialANDROID extends Trial {
		public var _dummyaddButtonToCloseProgram:addButtonToCloseProgram;
		//public var _dummyVarA2:addSwipeMenu;
		public var _dummyaddTouch:addTouch;
		public var _dummybehavQuit:behavQuit;
		public var _dummyadminData:adminData;
		public var _dummyadminSetVariable:adminSetVariable;
		public var _dummyadminConsole:adminConsole;
		//public var _dummyadminDropBox:adminDropBox;
	
	
		override public function stimulusFactory(name:String):IStimulus{
			switch(name.toLowerCase()){
				case "touch": 				return new addTouch;	
				case "quit": 				return new behavQuit;
				case "admindata": 			return new adminData;
				case "adminsetvariable": 	return new adminSetVariable;
				case "adminconsole": 		return new adminConsole;
			}

			return StimulusFactory.Stimulus(name);
		}
	
	}
		

	


}