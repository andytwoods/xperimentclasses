﻿
package com.xperiment.trial{
	import com.adventuresInActionscript.utils.SuperTimer;
	import com.adventuresInActionscript.graphics.SuperStage;
	import fl.controls.Button;
	import fl.controls.Slider;
	import fl.controls.SliderDirection;
	import flash.text.TextFieldAutoSize;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.display.*;
	import flash.events.*;
	import flash.text.*;
	import com.xperiment.OnScreenBoss;

	public class Trial_oneBall extends Trial {
		public function Trial_oneBall() {
			super();
		}
		////////////////////////////////////////////////////////////////////////
		//TRIAL TYPE SPECIFIC VARIABLES AND FUNCTIONS
		////////////////////////////////////////////////////////////////////////


		/////////////////////////////
		//BOSS VARIABLES
		/////////////////////////////
		var slider:Slidebar=new Slidebar  ;

		/////////////////////////////
		//BOSS FUNCTIONS
		/////////////////////////////
		override function trialEvents():void {//where trial type specific stuff is specified.
			super.makeButton("testing testing 1234");
			addSlider(2);
			circleVolStim(100);
			;
		}

		override function endOfTrial():void {
			super.trialData.Slider1=slider.returnSliderPosition(1);
			super.trialData.Slider2=slider.returnSliderPosition(2);
			trace(super.trialData.Slider1+" "+super.trialData.Slider1);
			slider=null;
			Stim=null;
			super.endOfTrial();
		}

		/////////////////////////////
		//SUB FUNCTIONS
		/////////////////////////////

		function addSlider(numSlides:uint):void {
			if (numSlides!=2||numSlides!=1) {
				numSlides=2;
			}
			slider.initVal1=20;
			slider.initVal2=20;
			slider.textDownArrow="Minimum possible";
			slider.textUpArrow="Maximum possible value";
			slider.numSli=2;
			slider.setup();
			// Add slider to display list
			slider.x+=200;
			slider.y+=200;
			super.CurrentDisplay.addElement(slider);
		}

	var Stim:SuperStage=new SuperStage  ;
		function circleVolStim(vol:int) {
			
			Stim.graphics.lineStyle(3,0x00ff00);
			Stim.graphics.beginFill(0x0000FF);
			Stim.graphics.drawCircle(super.theStage.stageWidth/2,super.theStage.stageHeight/3,vol);
			Stim.graphics.endFill();
			Stim.addEventListener(Event.ENTER_FRAME,animateBall);
			super.CurrentDisplay.addTimedElement(Stim,1000);
		}


		function animateBall(e:Event):void {
			var thisBall:Object=e.target;
			//apply randomness to velocity


			//var maxheight:int=Math.floor(stage.stageHeight*.8);
			//var maxwidth:int=Math.floor(stage.stageWidth*.8);
			//var minheight:int=Math.floor(stage.stageHeight*.0);
			//var minwidth:int=Math.floor(stage.stageWidth*.0);

			thisBall.y=thisBall.y+1-Math.random()*2;
			thisBall.x=thisBall.x+1-Math.random()*2;
		}






	}
}