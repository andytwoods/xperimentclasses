﻿
package com.xperiment.trial{

	import flash.display.*;
	import flash.geom.*;



	public class Trial_imageCollage_Blocks extends Trial_imageCollage {
		
		//override public function setup(theSta:Stage, genAttrib:XMLList, specAttrib:XMLList) {
			//super.setup(theSta, genAttrib, specAttrib);
		//}
		////////////////////////////////////////////////////////////////////////
		//TRIAL TYPE SPECIFIC VARIABLES AND FUNCTIONS
		////////////////////////////////////////////////////////////////////////


		/////////////////////////////
		//BOSS VARIABLES
		/////////////////////////////


		/////////////////////////////
		//BOSS FUNCTIONS
		/////////////////////////////


		/////////////////////////////
		//SUB FUNCTIONS
		/////////////////////////////


		override public function RunMe():Sprite {
			trace("here-------------------------");
			var numHappyStimNumber:Number=getVar("numPicsAlongYaxis")*getVar("numPicsAlongXaxis")*getVar("ratioHappySad");
			var numSadStimNumber:Number = getVar("numPicsAlongXaxis")*getVar("numPicsAlongXaxis")*(1-getVar("ratioHappySad"));
			var numHappyStimUint:uint=uint(numHappyStimNumber);
			var numSadStimUint:uint=uint(numSadStimNumber);
			var emotionArray:Array=new Array  ;
			var tempEmotion:String=new String  ;

			for (var happy:uint=0; happy<=numHappyStimUint; happy++) {
				tempEmotion="happy";
				emotionArray.push(tempEmotion);
			}
			for (var sad:uint=0; sad<numSadStimUint; sad++) {
				tempEmotion="sad";
				emotionArray.push(tempEmotion);
			}


			emotionArray=shuffleArray(emotionArray);
			var tempSprite:Sprite=new Sprite  ;
			var tempBox:Rectangle;
			var counter:uint=0;
			for (var i:uint=0; i<getVar("numPicsAlongXaxis"); i++) {
				var tempArray:Array=new Array  ;
				for (var j:uint=0; j<getVar("numPicsAlongYaxis"); j++) {
					//var imageObject:Object=new Object  ;


					tempBox=getBoundingBox(i,j);

					if (emotionArray[counter]=="sad") {
						tempSprite.graphics.beginFill(getVar("colour1"));
						tempSprite.graphics.drawRect(tempBox.x, tempBox.y, tempBox.width, tempBox.height);
						tempSprite.graphics.endFill();
					} else {
						tempSprite.graphics.beginFill(getVar("colour2"));
						tempSprite.graphics.drawRect(tempBox.x, tempBox.y, tempBox.width, tempBox.height);
						tempSprite.graphics.endFill();
					}
					counter++;
				}
				


			}
						
			return tempSprite;

		}
		private function shuffleArray(arr:Array):Array {
			var arr2:Array=[];
			while (arr.length > 0) {
				arr2.push(arr.splice(Math.round(Math.random() * (arr.length - 1)), 1)[0]);
			}
			return arr2;
		}

	}
}