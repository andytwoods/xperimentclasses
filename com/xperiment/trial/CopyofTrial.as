﻿package com.xperiment.trial {
	import com.Logger.Logger;
	import com.bulkloader.preloadFilesFromWeb;
	import com.xperiment.ExptWideSpecs;
	import com.xperiment.XMLstuff.XMLObjectPropertyAssigner;
	import com.xperiment.admin.adminConditionList;
	import com.xperiment.admin.adminConsole;
	import com.xperiment.admin.admin_baseClass;
	import com.xperiment.behaviour.behavBevel;
	import com.xperiment.behaviour.behavBlur;
	import com.xperiment.behaviour.behavClick;
	import com.xperiment.behaviour.behavDrag;
	import com.xperiment.behaviour.behavDragToShiftingArea;
	import com.xperiment.behaviour.behavGotoTrial;
	import com.xperiment.behaviour.behavGradientFill;
	import com.xperiment.behaviour.behavHide;
	import com.xperiment.behaviour.behavIf;
	import com.xperiment.behaviour.behavLanguage;
	import com.xperiment.behaviour.behavLoad;
	import com.xperiment.behaviour.behavLogic;
	import com.xperiment.behaviour.behavNextTrial;
	import com.xperiment.behaviour.behavOpacity;
	import com.xperiment.behaviour.behavOutline;
	import com.xperiment.behaviour.behavPause;
	import com.xperiment.behaviour.behavRT;
	import com.xperiment.behaviour.behavRand;
	import com.xperiment.behaviour.behavRestart;
	import com.xperiment.behaviour.behavRotate;
	import com.xperiment.behaviour.behavSave;
	import com.xperiment.behaviour.behavSaveData;
	import com.xperiment.behaviour.behavSaveImage;
	import com.xperiment.behaviour.behavSearch;
	import com.xperiment.behaviour.behavShadow;
	import com.xperiment.behaviour.behavShake;
	import com.xperiment.behaviour.behavShufflePropertiesOfObjects;
	import com.xperiment.behaviour.behavSize;
	import com.xperiment.behaviour.behavSwap;
	import com.xperiment.behaviour.behavTextFilter;
	import com.xperiment.behaviour.behavTimer;
	import com.xperiment.behaviour.behav_baseClass;
	import com.xperiment.behaviour.BehaviourBoss;
	import com.xperiment.codeRecycleFunctions;
	import com.xperiment.container.container;
	import com.xperiment.container.containerHorizontal;
	import com.xperiment.container.containerHorizontalGrid;
	import com.xperiment.container.containerVertical;
	import com.xperiment.container.containerVerticalGrid;
	import com.xperiment.events.GotoTrialEvent;
	import com.xperiment.exptWideAction.ExptWideAction;
	import com.xperiment.interfaces.IGiveScript;
	import com.xperiment.interfaces.IgiveTrialInfo;
	import com.xperiment.OnScreenBoss;
	import com.xperiment.stimuli.addBarGraph;
	import com.xperiment.stimuli.addBlank;
	import com.xperiment.stimuli.addButton;
	import com.xperiment.stimuli.addCode;
	import com.xperiment.stimuli.addColourArray;
	import com.xperiment.stimuli.addColourArrayXgoodXbad;
	import com.xperiment.stimuli.addComboBox;
	import com.xperiment.stimuli.addDropDownList;
	import com.xperiment.stimuli.addInputTextBox;
	import com.xperiment.stimuli.addJPG;
	import com.xperiment.stimuli.addJPGs;
	import com.xperiment.stimuli.addKeyPress;
	import com.xperiment.stimuli.addLoadingIndicator;
	import com.xperiment.stimuli.addMechanicalTurk;
	import com.xperiment.stimuli.addMultiNumberSelector;
	import com.xperiment.stimuli.addMultipleChoice;
	import com.xperiment.stimuli.addNextTrial;
	import com.xperiment.stimuli.addNumberSelector;
	import com.xperiment.stimuli.addQAs;
	import com.xperiment.stimuli.addSaveDataLocally;
	import com.xperiment.stimuli.addSequentialCounter;
	import com.xperiment.stimuli.addShapeMatrix;
	import com.xperiment.stimuli.addSlider;
	import com.xperiment.stimuli.addStyle;
	import com.xperiment.stimuli.addText;
	import com.xperiment.stimuli.addTickBox;
	import com.xperiment.stimuli.addVideo;
	import com.xperiment.stimuli.object_baseClass;
	import com.xperiment.uberSprite;
	
	import flash.display.*;
	import flash.events.*;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.system.ApplicationDomain;
	import flash.system.System;
	import flash.utils.Timer;
	import flash.utils.getDefinitionByName;
	
	
	//import com.xperiment.stimuli.addSVG;
	
	
	public class Trial extends uberSprite {
		
		public static var language:String = "";
		public var trialInfo:Object;
		public var _dummyobject_baseClass:object_baseClass;
		//public var _dummyVar3:fullScreenMode;
		public var _dummyaddText:addText;
		//public var _dummyVar5:addFixationCross;
		public var _dummyaddJPG:addJPG;
		public var _dummyaddButton:addButton;
		public var _dummyVar8:addVideo;
		//public var _dummyVar9:addShape;
		public var _dummyaddInputTextBox:addInputTextBox;
		//public var _dummyVar11:addMergingSounds;
		//public var _dummyVar12:addInputRadioButtons;
		public var _dummyaddSlider:addSlider;
		//public var _dummyVar14:addLMS;
		//public var _dummyVar15:addDragToZoneObject;
		//public var _dummyVar16:addYouTube;
		//public var _dummyVar17:addUpdateOverExperiment;
		//public var _dummyVar18:addLAMS;
		//public var _dummyVar19:addTwoDimensionalEmotionScale;
		public var _dummyaddColourArray:addColourArray;
		public var _dummyaddSequentialCounter:addSequentialCounter;
		public var _dummyaddColourArrayXgoodXbad:addColourArrayXgoodXbad;
		//public var _dummyVar23:addText_QuitExperimentSaveData;
		//public var _dummyVar24:addWebCam;
		//public var _dummyVar25:addDistortionCam;
		public var _dummyaddMultipleChoice:addMultipleChoice;
		public var _dummyaddNumberSelector:addNumberSelector;
		public var _dummyaddMultiNumberSelector:addMultiNumberSelector;
		public var _dummyaddTickBox:addTickBox;
		public var _dummyaddDropDownList:addDropDownList;
		public var _dummyaddNextTrial:addNextTrial;
		public var _dummyaddLoadingIndicator:addLoadingIndicator;
		public var _dummyaddMechanicalTurk:addMechanicalTurk;
		//public var _dummyComplexStim0:addC0omplex_baseClass;
		public var _dummyaddBlank:addBlank;
		public var _dummyaddJPGs:addJPGs;
		public var _dummyaddComboBox:addComboBox;
		public var _dummyaddKeys:addKeyPress;
		public var _dummyaddShapeMatrix:addShapeMatrix;
		public var _dummyaddBarGraph:addBarGraph;
		//public var _dummyaddSVG:addSVG;
		public var _dummyaddCode:addCode;
		public var _dummyaddQAs:addQAs;
		public var _dummyaddSaveDataLocally:addSaveDataLocally;
		
		public var _dummyaddStyle:addStyle;
		
		
		public var _dummybehav_baseClass:behav_baseClass;
		public var _dummybehavDrag:behavDrag;
		public var _dummybehavDragToShiftingArea:behavDragToShiftingArea;
		public var _dummybehavBlur:behavBlur;
		public var _dummybehavOutline:behavOutline;
		public var _dummybehavSwap:behavSwap;
		public var _dummybehavSize:behavSize;
		public var _dummybehavHide:behavHide;
		public var _dummybehavOpacity:behavOpacity;
		public var _dummybehavNextTrial:behavNextTrial;
		public var _dummybehavLogic:behavLogic;
		public var _dummybehavClick:behavClick;
		public var _dummybehavRT:behavRT;
		public var _dummybehavGotoTrial:behavGotoTrial;
		public var _dummybehavShadow:behavShadow;
		public var _dummybehavIf:behavIf;
		public var _dummybehavBevel:behavBevel;
		public var _dummybehavGradientFill:behavGradientFill;
		public var _dummybehavRotate:behavRotate;
		public var _dummybehavShake:behavShake;
		public var _dummybehavPause:behavPause;
		public var _dummybehavSaveImage:behavSaveImage;
		public var _dummybehavRand:behavRand;
		public var _dummybehavShufflePropertiesOfObjects:behavShufflePropertiesOfObjects;
		public var _dummybehavTextFilter:behavTextFilter;
		public var _dummybehavSave:behavSave;
		public var _dummybehavLoad:behavLoad;
		public var _dummybehavSearch:behavSearch;
		public var _dummybehavTimer:behavTimer;
		public var _dummybehavLanguage:behavLanguage;
		public var _dummybehavRestartExpt:behavRestart;
		public var _dummybehavSaveData:behavSaveData;
		
		public var _dummycontainer:container;
		public var _dummycontainerHorizontal:containerHorizontal;
		public var _dummycontainerVertical:containerVertical;
		public var _dummycontainerHorizontalGrid:containerHorizontalGrid;
		public var _dummycontainerVerticalGrid:containerVerticalGrid;
		
		public var _dummyadmin_baseClass:admin_baseClass;
		public var _dummyadminConditionList:adminConditionList;
		//public var _dummyadminConsole:adminConsole;
		
		public var ITItimer:Timer;
		
		public var fixedLocation:String="999";
		private var ListofObjectsReturningData:Array;
		private var ListofObjectsUpdatedVariables:Array;
		//var trialOrder:String=new String  ;
		
		public var OnScreenElements:Object;
		public var objects:XMLList=new XMLList;
		
		private var onScreenChild:uberSprite=new uberSprite  ;
		public var trialData:XML=new XML  ;
		public var CurrentDisplay:OnScreenBoss;
		public var manageBehaviours:BehaviourBoss;
		//var objectsOnScreen = new Array();
		public var Order:uint=new uint  ;
		static public var theStage:Stage;
		static public var returnStageHeight:int;
		static public var returnStageWidth:int;
		public var trialLabel:String=new String  ;
		//public var duplicateTrialNumber:uint=new uint  ;
		public var trialNumber:uint;
		private var TrialRawIdentity:Array = new Array();
		private var onLoadedObjects:Array = new Array();
		public var trialBlockPositionStart:uint;
		public var trialProtocolList:XML;
		
		
		private static var _overEntireExperiment:overExperiment;
		
		private var myContainers:Array;
		
		public var ITI:int=-1;
		private var tempString:String="";
		public var exptScript:XML;
		public var logger:Logger;
		
		public static var preloader:preloadFilesFromWeb;
		public var submitData:Boolean = false;
		
		public var pic:Sprite;
		
		private var exptWideAction:ExptWideAction;
		
		private var _ignoreData:Boolean = false;
		
		public static function set overEntireExperiment(value:overExperiment):void	{_overEntireExperiment = value;}
		
		
		public function get ignoreData():Boolean{return _ignoreData;}public function set ignoreData(value:Boolean):void{_ignoreData = value;}
		
		public function giveObjToXptMaker(nam:String):Class{
			var tempClassReference:Class;
			
			if (ApplicationDomain.currentDomain.hasDefinition("com.xperiment.stimuli."+nam) || 
				ApplicationDomain.currentDomain.hasDefinition("com.xperiment.behaviour."+nam) ||
				ApplicationDomain.currentDomain.hasDefinition("com.xperiment.container."+nam) ||
				ApplicationDomain.currentDomain.hasDefinition("com.xperiment.admin."+nam)){
				
				switch(nam.substr(0,3)){
					case "add":
						tempClassReference = getDefinitionByName("com.xperiment.stimuli."+nam) as Class;
						break;
					case "beh":
						tempClassReference = getDefinitionByName("com.xperiment.behaviour."+nam) as Class;
						break;
					case "con":
						tempClassReference = getDefinitionByName("com.xperiment.container."+nam) as Class;
						break;
					case "adm":
						tempClassReference = getDefinitionByName("com.xperiment.admin."+nam) as Class;
						break;
				}
			}
			return tempClassReference;
		}
		
		public function passExptScriptXML(trialProtocolList:XML):void{
			this.trialProtocolList=trialProtocolList;
		}
		
		public function setup(genAttrib:XMLList,info:Object):void {		
			trialInfo=info;
			
			info.storageFolder=ExptWideSpecs.ExptWideSpecs.fileInformation.saveToPortableDevice;
			
			
			
			
			
			this.logger=Logger.getInstance();		
			
			
			this.ITI=ExptWideSpecs.ExptWideSpecs.defaults.ITI;
			
			trialNumber=info.order;
			//trace(trialNumber)
			TRIAL_ID=info.blockNum;
			
			objects=genAttrib;
			
			trialBlockPositionStart=info.trialBlockPositionStart;
			//trace(trialBlockPositionStart,222,TRIAL_ID);
			//duplicateTrialNumber=info.duplicateTrialNumber;
			
			
			
		}
		
		private var tempTimeStart:Number=-1;
		private var tempTimeEnd:Number=-1;
		private var behaviours:String;
		private var tempResponse:String;
		///////////////////////////////////////////////
		
		public function elementSetup(obj:XML,inContainer:container, level:uint):void {
			//trace(1111,kinder,level);
			for each(var kinder:XML in obj.*) {
				
				//HERE, add if objectType="con"
				// take out the code below and put into a seperate function :)  Allows the container to do stuff. 
				//If container, setup container, THEN cycle through elements in container, passing them the container:Sprite as well as the stage. 
				//note, this will have to be iterative ... containers within containers...
				// prob will need newfunction(a,b,c,etc,)
				var isSuitableToCheckForChildren:Boolean=false;
				var lowerLevelContainer:container;
				var kinderNam:String=kinder.name().toString();
				//trace(kinderNam,222,kinderNam=="addBlank");
				
				
				if(kinder.hasOwnProperty("@exptWideAction") && kinder.@["exptWideAction"]!=""){//where this value follows this pattern: id,property,action
					this.exptWideAction=ExptWideAction.getInstance();
					var stuffToUpdate:Object= exptWideAction.pass(kinder);
					
					for (var nam:String in stuffToUpdate){
						if(kinder.hasOwnProperty("@"+nam)){
							kinder.@[nam]=stuffToUpdate[nam];
						}
					}
				}
				
				switch (kinderNam.substr(0,3)){
					case("add"):
					case("beh"):
					case("adm"):
						iterateObjectsForComposition(kinder,inContainer);isSuitableToCheckForChildren=true;
						break;
					case("con"):
						if(!myContainers)myContainers=new Array;
						kinder.@numInContainer=countChildren(kinder);
						lowerLevelContainer=composeObject(kinder, 1,inContainer);
						myContainers.push(lowerLevelContainer); //put this array in kill();
						isSuitableToCheckForChildren=true;
						break;
					default:
						//maybe in the future we want a default behaviour
						break;
					
				}
				
				//if has children, self call;
				
				if(isSuitableToCheckForChildren && kinder.length()!=0 && kinder.children().length() >0 && (kinder.children()[0].name().toString())!=""){
					//above, utter madness that TWO checks are required to determine if kinder has children... must be a better way. Oh well. 
					elementSetup(kinder,lowerLevelContainer,level+1);
				}
			}
		}
		
		
		private function countChildren(kinder:XML):uint{
			var count:uint=0;
			var howMany:String;
			var nam:String;
			for (var i:uint=0;i<kinder.children().length();i++){
				howMany=kinder.children()[i].@howMany;
				nam=kinder.children()[i].name().toString().substr(0,3);
				if(nam=="add" || nam=="con"){
					if (howMany.length==0 && howMany!="0") count++;
					else if (uint(howMany)>0) count+=uint(howMany)
				}
			}
			return count;
		}
		
		
		private function iterateObjectsForComposition(obj:XML,inContainer:container):void{
			var iterations:uint=1;
			
			if (String(obj.attribute("howMany")).length!=0) {
				iterations=obj.attribute("howMany");
			}
			for (var i:uint=1;i<=iterations;i++) {
				if (obj.attribute("overExperiment")=="true") {
					_overEntireExperiment.composeALLexptObject(obj, i-1,objects);
				}
				else {
					composeObject(obj, i-1,inContainer);
					
				}
			}
		}
		
		private var TimeStartLog:int=0;
		private var TimeEndLog:int=0;
		private var influencesTime:Boolean=false;
		public var TRIAL_ID:int;
		
		
		public function composeObject(obj:XML, iteration:uint,inContainer:container):container {
			var tempObj:*;
			var tempClassReference:Class;
			//using the 'present' attribute (where 0 means 'do not show');
			var makeStimulus:Boolean=codeRecycleFunctions.multipleTrialCorrection(obj.attribute("present"),";",trialBlockPositionStart)!="0";
			
			//trace("ggg:"+codeRecycleFunctions.multipleTrialCorrection(obj.attribute("present"),";",duplicateTrialNumber)+" "+obj.attribute("present"));
			//trace("ggg:"+makeStimulus+" "+obj.name()+" "+duplicateTrialNumber);
			if(makeStimulus){
				var objName:String=obj.name();
				if (ApplicationDomain.currentDomain.hasDefinition("com.xperiment.stimuli."+objName) || 
					ApplicationDomain.currentDomain.hasDefinition("com.xperiment.behaviour."+objName) ||
					ApplicationDomain.currentDomain.hasDefinition("com.xperiment.container."+objName) ||
					ApplicationDomain.currentDomain.hasDefinition("com.xperiment.admin."+objName)){
					if(logger)logger.log("-----------------'"+objName+"' object-----------------");
					if (inContainer!=null)logger.log("---->>>---in container:'"+inContainer+"' ------");
					
					switch(objName.substr(0,3)){
						case "add":
							tempClassReference = getDefinitionByName("com.xperiment.stimuli."+objName) as Class;
							break;
						case "beh":
							tempClassReference = getDefinitionByName("com.xperiment.behaviour."+objName) as Class;
							
							break;
						case "con":
							tempClassReference = getDefinitionByName("com.xperiment.container."+objName) as Class;
						case "adm":
							tempClassReference = getDefinitionByName("com.xperiment.admin."+objName) as Class;
					}
					
					tempObj = new(tempClassReference);
					if(logger)tempObj.passLogger(logger);
					OnScreenElements.push(tempObj);
					
					var TrialVarObj:Object=new Object  ;
					TrialVarObj.theStage=theStage;
					TrialVarObj.name=obj.name();
					
					if(inContainer==null){
						TrialVarObj.h=returnStageHeight;
						TrialVarObj.w=returnStageWidth;
						TrialVarObj.containerX=0;
						TrialVarObj.containerY=0;
					}
					else{
						var fromContainer:Object = inContainer.returnContainerInfo();
						TrialVarObj.h=fromContainer.myHeight;
						TrialVarObj.w=fromContainer.myWidth;
						TrialVarObj.containerX=fromContainer.x;
						TrialVarObj.containerY=fromContainer.y;
					}
					
					
					
					if (tempObj as IGiveScript){
						tempObj.giveExptScript(exptScript);
					}
					
					if(tempObj as IgiveTrialInfo){
						tempObj.giveTrialInfo(trialInfo);
					}
					
					
					//if (obj.name().toString().substr(0,3)=="beh"){ // 12.9.2012 decided to give behaviourBoss to everything
					tempObj.giveBehavBoss(manageBehaviours);
					//}
					
					if (["behavShufflePropertiesOfObjects"].indexOf(obj.name().toString())!=-1){
						tempObj.passOnScreenBoss(CurrentDisplay);
					}
					
					if (["addJPG","addJPGs","addLoadingIndicator","addLoadingIndicator","addVideo"].indexOf(obj.name().toString())!=-1){
						tempObj.passPreloader(preloader);
					}
					
					
					TrialVarObj.i=iteration;
					TrialVarObj.perSize=ExptWideSpecs.ExptWideSpecs.screen.percentageFromWidthOrHeightOrBoth as String;
					TrialVarObj.trialBlockPositionStart=trialBlockPositionStart;
					TrialVarObj.parent=pic;
					TrialVarObj.order=trialInfo.order;
					
					tempObj.setUpTrialSpecificVariables(TrialVarObj);
					
					obj.duplicateTrialNumber=trialBlockPositionStart;
					tempObj.setVariables(XMLList(obj));
					behaviours="";
					tempResponse="";
					
					if (String(obj.@showBox).length!=0 && obj.@showBox=="true" && (String(obj.attribute("showBox")).length==0 || obj.attribute("showBox")=="false")){
						obj.@showBox="true";
					}
					
					if (String(objects.@verticalPosition0).length!=0 && String(obj.attribute("verticalPosition0")).length==0){
						obj.@verticalPosition0=String(objects.@verticalPosition0);
					}
					
					if (String(objects.@horizontalPosition0).length!=0 && String(obj.attribute("horizontalPosition0")).length==0){
						obj.@horizontalPosition0=String(objects.@horizontalPosition0);
					}
					
					influencesTime=true;//this is to do with a 'rolling start time' where an object's start/end time is determined by a previous object
					if (codeRecycleFunctions.multipleTrialCorrection(obj.attribute("timeEnd"),"---",iteration)=="false") {
						influencesTime=false;
					}
					/////////////////////////////////////////
					
					var tempTimeVal:String=tempObj.getVar("timeStart");
					if (tempTimeVal!="" && tempTimeVal.substring(0,8)!="withPrev" && tempTimeVal.substring(0,9)!="afterPrev") {
						tempTimeStart=Number(tempTimeVal)+codeRecycleFunctions.mathVal(tempTimeVal.substring(tempTimeStart));
					}
					else if (tempTimeVal.substring(0,8)=="withPrev") {
						tempTimeStart=TimeStartLog+codeRecycleFunctions.mathVal(tempTimeVal.substring(tempTimeStart));
					}
					else if (tempTimeVal.substring(0,9)=="afterPrev") {
						tempTimeStart=TimeEndLog+codeRecycleFunctions.mathVal(tempTimeVal.substring(tempTimeStart));
					}					
					
					tempTimeVal=tempObj.getVar("timeEnd");
					if (tempTimeVal.toLowerCase()=="forever") {
						tempTimeEnd=uint(0-1);
					}
					else if (tempTimeVal!=""  && tempTimeVal.substring(0,8)!="withPrev" && tempTimeVal.substring(0,9)!="afterPrev") {
						tempTimeEnd=Number(tempTimeVal);//+tempTimeStart+codeRecycleFunctions.mathVal(tempTimeVal.substring(tempTimeStart));
					}
					else if (tempTimeVal.substring(0,8)=="withPrev") {
						tempTimeEnd=TimeStartLog+codeRecycleFunctions.mathVal(tempTimeVal.substring(tempTimeStart));
					}
					else if (tempTimeVal.substring(0,9)=="afterPrev") {
						tempTimeEnd=TimeEndLog+codeRecycleFunctions.mathVal(tempTimeVal.substring(tempTimeStart));
					}
					else {
						tempTimeVal=codeRecycleFunctions.multipleTrialCorrection(obj.attribute("duration"),"---",iteration);
						if (tempTimeVal!="") {
							tempTimeEnd=Number(tempTimeVal)+tempTimeStart+codeRecycleFunctions.mathVal(tempTimeVal.substring(tempTimeStart));
						}
					}
					
					if (TimeStartLog<tempTimeStart && influencesTime) {
						TimeStartLog=tempTimeStart;
					}
					
					if (TimeEndLog<tempTimeEnd && tempTimeEnd !=999999999 && influencesTime) {
						TimeEndLog=tempTimeEnd;
					}
					
					if (tempTimeEnd<tempTimeStart) {
						tempTimeEnd=999999999;
						if(logger)logger.log("PROBLEM----------'"+obj.name()+"' Ends before it begins :S ------------!!!!!!! ");
					}
					
					if (tempTimeEnd!=-1 && tempTimeEnd<0) {
						tempTimeEnd=999999999;
						if(logger)logger.log("PROBLEM----------'"+obj.name()+"' Ends before 0 :S ------------!!!!!!! ");
					}
					
					if (tempTimeStart!=-1 && tempTimeStart<0 ) {
						tempTimeEnd=0;
						if(logger)logger.log("PROBLEM----------'"+obj.name()+"' Starts before 0 :S ------------!!!!!!! ");
					}
					
					
					
					behaviours=codeRecycleFunctions.multipleTrialCorrection(obj.attribute("behaviours"),"---",iteration);
					tempResponse=codeRecycleFunctions.multipleTrialCorrection(obj.attribute("responseRecorder"),"---",iteration);
					
					
					if (obj.name()=="addUpdateOverExperiment") {
						TrialVarObj._overEntireExperiment=_overEntireExperiment;
					}
					
					if (behaviours!="") {
						tempObj.addEventListener("runDrivenEvent",runDrivenEvent,false,0,true);
						tempObj.addEventListener("stopEvent",stopEvent,false,0,true);
					}
					
					
					manageBehaviours.behaviourLogic(tempObj,{
						iteration:	iteration,
						start:		tempTimeStart,
						end:		tempTimeEnd,
						logicStart:	codeRecycleFunctions.multipleTrialCorrection(obj.attribute("timeStart"),"---",iteration),
						logicEnd:	codeRecycleFunctions.multipleTrialCorrection(obj.attribute("timeEnd"),"---",iteration),					
						myPeg:		tempObj.getVar("peg")
					});
					//note that logicStart and logicEnd are a hack.  Ideally should only need to provide start and end but there is some issue where these values default to 0
					//if no value is specified in the Script (see object_baseclass where timeStart default is 0... thought not to change this as it IS the default).
					
					
					if (tempObj.returnsDataQuery() || (tempObj.getVar("results1") as String).length!=0) ListofObjectsReturningData.push(tempObj);
					if(inContainer!=null)inContainer.passChildObject(tempObj.pic);
					
					
					tempObj.setStartEndTimes(tempTimeStart,tempTimeEnd);
					
					
				}
			}
			
			if(String(obj.name()).substr(0,3)=="con")return tempObj;
			else return inContainer;
		}
		
		
		
		/*		private function endResponseGoToNext(event:Event):void {
		endResponseGoToTrial(event, "next");
		}
		
		private function endResponseGoToPrev(event:Event):void {
		endResponseGoToTrial(event, "prev");
		}*/
		
		
		
		private function runDrivenEvent(event:Event):void {
			var myArr:Array=event.target.getListOfDrivenEvents();
			logger.log("running driven event(s):"+myArr);
			event.target.removeEventListener("runDrivenEvent",runDrivenEvent);
			//////
			for (var i:uint=0; i<myArr.length; i++) {
				CurrentDisplay.runDrivenEvent(myArr[i]);
			}
		}
		
		private function stopEvent(event:Event):void {
			logger.log("stopping event...");
			event.target.removeEventListener(event.type,runDrivenEvent);
			var myArr:Array=event.target.getListOfstopEvents();
			//////
			for (var i:uint=0; i<myArr.length; i++) {
				CurrentDisplay.stopObj(myArr[i]);
			}
		}
		private function saveDataEndStudy(event:Event):void{
			dispatchEvent(new Event("saveDataEndStudy"));
		}
		
		public function compileOutputForTrial():void {
			var tempResults:XML=new XML  ;
			var tempArray:Array=new Array();
			var tempTrialData:XMLList=new XMLList  ;
			
			tempTrialData+= XML(<order>{Order}</order>);
			
			if(ListofObjectsReturningData){
				for (var i:uint=0; i<ListofObjectsReturningData.length; i++) {
					tempArray=ListofObjectsReturningData[i].storedData();
					for (var j:uint=0; j<tempArray.length; j++) {
						tempString=String(tempArray[j].event);
						
						tempResults=new XML  ;
						tempResults=<{tempString}>
						{tempArray[j].data}
						</{tempString}>;
						tempTrialData+=tempResults;
					}
				}
				
				//trialData = <trialData name={trialLabel} trialReplication={duplicateTrialNumber}>
				trialData = <trialData name={trialLabel}>
				{tempTrialData}
				</trialData>;
				var temp:Array;
				
				for (i=0; i<ListofObjectsUpdatedVariables.length; i++) {
					temp=ListofObjectsReturningData[i].getTempStoredVars();
					//var identifier:String=temp[temp.length-1][0];
					//var val=temp[temp.length-1][1];
					//logger.log("222 "+identifier+" --- "+ val); //only takes the last value (the new value)
					//varsStoredEachTrial[identifier]=val;
					//varsStoredEachTrial.push(temp[temp.length-1]);
				}
			}
		}
		
		
		////////////////////////////////////////////////////////////////////////
		//GENERIC VARIABLES AND FUNCTIONS
		////////////////////////////////////////////////////////////////////////
		
		
		public function instantiateVars():void
		{
			if(!pic)pic=new Sprite;
			if(theStage)theStage.addChild(pic);
			OnScreenElements=new Array;
			ListofObjectsReturningData = new Array();
			ListofObjectsUpdatedVariables = new Array();
		}
		
		public function prepare(Ord:uint,trial:XML):void {
			
			objects=trial.children();
			Order=Ord;
			
			if(trial.hasOwnProperty('@ITI'))			ITI=int(trial.@ITI);
			if(trial.hasOwnProperty('@submitData'))		submitData=true; //for the v end of the experiment
			if(trial.hasOwnProperty('@ignoreData'))		ignoreData=true;
			if(trial.hasOwnProperty('@trialName'))		trialLabel=codeRecycleFunctions.multipleTrialCorrection(trial.@trialName,";",trialBlockPositionStart);
			
			var con_b:container;
			instantiateVars();
			if (!CurrentDisplay) CurrentDisplay=new OnScreenBoss(pic,logger); // slight bodge to allow for stuff to be added when sorting out a multiExperiment Trial.
			manageBehaviours=new BehaviourBoss(pic,CurrentDisplay,logger);
			//trace(objects,con_b)
			elementSetup(XML(<objects>{objects}</objects>),con_b,0);
			
			if(trialInfo.ITI){
				tempString=codeRecycleFunctions.multipleTrialCorrection(trialInfo.ITI,"~",Order-trialBlockPositionStart);//n.b. the left objects formula is identical to 'trialWithinBlockPosition' in objectBaseClass.
				ITI=int(codeRecycleFunctions.multipleTrialCorrection(tempString,";",trialBlockPositionStart));
			}
			
			manageBehaviours.init();
			
			ITItimer=new Timer(ITI);
			//trace("ITI",ITI);
			ITItimer.addEventListener( TimerEvent.TIMER, ITInextTrial,false,0,true);
			ITItimer.start();
			
		}
		
		public function giveTrialData():XML{
			return trialData;
		}
		
		private function ITInextTrial(e:TimerEvent):void{
			//trace("..");
			if(ITItimer && ITItimer.hasEventListener(TimerEvent.TIMER))ITItimer.removeEventListener(TimerEvent.TIMER,ITInextTrial);
			run();
		}
		
		public function run():void {
			pic.addEventListener(GotoTrialEvent.TRIAL_PING_FROM_OBJECT,gotoTrial,false,0,false);//note false here for weak refs.  NEEDED.  If the SJ is on a trial long enough, the eventlistener will just be cleaned up...
			CurrentDisplay.commenceDisplay();
		}
		
		public function gotoTrial(e:GotoTrialEvent):void
		{
			pic.removeEventListener(GotoTrialEvent.RUNNER_PING_FROM_TRIAL,gotoTrial);
			generalCleanUp();
			theStage.dispatchEvent(new GotoTrialEvent(GotoTrialEvent.RUNNER_PING_FROM_TRIAL,e.action));
			
		}
		
		
		public function addOnScrEle(nam:String, obj:*, ... rest):void {
			if (rest.length==1) {
				OnScreenElements[nam]=rest[0];
			}
		}
		
		public function generalCleanUp(forced:Boolean=false):void {
			if(!submitData || forced){
				compileOutputForTrial();
				if(OnScreenElements){
					for (var i:uint=0; i<OnScreenElements.length; i++) {
						OnScreenElements[i].kill();
						OnScreenElements[i]=null;
					}
					
					OnScreenElements=null;
					if (CurrentDisplay)CurrentDisplay.cleanUpScreen();
					CurrentDisplay=null;
					if(manageBehaviours)manageBehaviours.kill();
					manageBehaviours=null;
					
					
					ITItimer=null;
					
					//theStage.swapChildren(theStage.getChildAt(0),theStage.getChildAt(theStage.numChildren-1));
					
					onScreenChild=null;
					ListofObjectsReturningData=null;
					ListofObjectsUpdatedVariables=null;
					OnScreenElements=null;
					//objects=null;
					TrialRawIdentity=null;
					onLoadedObjects=null;
					//varsStoredEachTrial=null;
					if(pic && theStage && theStage.contains(pic))theStage.removeChild(pic);
					//else trace("prob",pic);
					if(pic)pic=null;
					System.gc();
					if (logger)logger.log("screen cleared");
				}
			}
		}
	}
	
	
}