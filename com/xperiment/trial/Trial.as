﻿package com.xperiment.trial {
	import com.Logger.Logger;
	import com.bulkloader.preloadFilesFromWeb;
	import com.xperiment.ExptWideSpecs;
	import com.xperiment.OnScreenBoss;
	import com.xperiment.codeRecycleFunctions;
	import com.xperiment.uberSprite;
	import com.xperiment.behaviour.BehaviourBoss;
	import com.xperiment.container.container;
	import com.xperiment.events.GotoTrialEvent;
	import com.xperiment.exptWideAction.ExptWideAction;
	import com.xperiment.interfaces.IGiveScript;
	import com.xperiment.interfaces.IgiveTrialInfo;
	import com.xperiment.stimuli.IStimulus;
	import com.xperiment.stimuli.IgiveCurrentDisplay;
	import com.xperiment.stimuli.IgivePreloader;
	import com.xperiment.stimuli.StimulusFactory;
	import com.xperiment.stimuli.object_baseClass;
	
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.system.System;
	import flash.utils.Timer;


	
	
	//import com.xperiment.stimuli.addSVG;
	
	
	public class Trial extends AbstractTrial {
		
		public static var language:String = "";
		public var trialInfo:Object;

		
		public var ITItimer:Timer;
		
		public var fixedLocation:String="999";
		private var ListofObjectsReturningData:Array;
		private var ListofObjectsUpdatedVariables:Array;
		//var trialOrder:String=new String  ;
		
		public var OnScreenElements:Object;
		public var objects:XMLList=new XMLList;
		
		private var onScreenChild:uberSprite=new uberSprite  ;
		public var trialData:XML=new XML  ;
		public var CurrentDisplay:OnScreenBoss;
		//var objectsOnScreen = new Array();
		public var Order:uint=new uint  ;
		static public var theStage:Stage;
		static public var returnStageHeight:int;
		static public var returnStageWidth:int;
		public var trialLabel:String=new String  ;
		//public var duplicateTrialNumber:uint=new uint  ;
		public var trialNumber:uint;
		private var TrialRawIdentity:Array = new Array();
		private var onLoadedObjects:Array = new Array();
		public var trialBlockPositionStart:uint;
		public var trialProtocolList:XML;
		
		
		private static var _overEntireExperiment:overExperiment;
		
		private var myContainers:Array;
		
		public var ITI:int=-1;
		private var tempString:String="";
		public var exptScript:XML;
		
		
		public static var preloader:preloadFilesFromWeb;
		public var submitData:Boolean = false;
		
		public var pic:Sprite;
		
		private var exptWideAction:ExptWideAction;
		
		private var _ignoreData:Boolean = false;
		
		public static function set overEntireExperiment(value:overExperiment):void	{_overEntireExperiment = value;}
		
		
		public function get ignoreData():Boolean{return _ignoreData;}public function set ignoreData(value:Boolean):void{_ignoreData = value;}
		
/*		public function giveObjToXptMaker(nam:String):Class{
			var tempClassReference:Class;
			
			if (ApplicationDomain.currentDomain.hasDefinition("com.xperiment.stimuli."+nam) || 
				ApplicationDomain.currentDomain.hasDefinition("com.xperiment.behaviour."+nam) ||
				ApplicationDomain.currentDomain.hasDefinition("com.xperiment.container."+nam) ||
				ApplicationDomain.currentDomain.hasDefinition("com.xperiment.admin."+nam)){
				
				switch(nam.substr(0,3)){
					case "add":
						tempClassReference = getDefinitionByName("com.xperiment.stimuli."+nam) as Class;
						break;
					case "beh":
						tempClassReference = getDefinitionByName("com.xperiment.behaviour."+nam) as Class;
						break;
					case "con":
						tempClassReference = getDefinitionByName("com.xperiment.container."+nam) as Class;
						break;
					case "adm":
						tempClassReference = getDefinitionByName("com.xperiment.admin."+nam) as Class;
						break;
				}
			}
			return tempClassReference;
		}*/
		
		public function passExptScriptXML(trialProtocolList:XML):void{
			this.trialProtocolList=trialProtocolList;
		}
		
		public function setup(genAttrib:XMLList,info:Object):void {		
			trialInfo=info;
			

			info.storageFolder=ExptWideSpecs.ExptWideSpecs.fileInformation.saveToPortableDevice;
			
			
			
			
			
			this.logger=Logger.getInstance();		
			
			
			this.ITI=ExptWideSpecs.ExptWideSpecs.defaults.ITI;
			
			trialNumber=info.order;
			//trace(trialNumber)
			TRIAL_ID=info.blockNum;
			
			objects=genAttrib;
			
			trialBlockPositionStart=info.trialBlockPositionStart;
			//trace(trialBlockPositionStart,222,TRIAL_ID);
			//duplicateTrialNumber=info.duplicateTrialNumber;
			
			
			
		}
		
		
		private var behaviours:String;
		private var tempResponse:String;
		///////////////////////////////////////////////
		
		public function elementSetup(obj:XML,inContainer:container, level:uint):void {
			//trace(1111,kinder,level);
			for each(var kinder:XML in obj.*) {
				
				//HERE, add if objectType="con"
				// take out the code below and put into a seperate function :)  Allows the container to do stuff. 
				//If container, setup container, THEN cycle through elements in container, passing them the container:Sprite as well as the stage. 
				//note, this will have to be iterative ... containers within containers...
				// prob will need newfunction(a,b,c,etc,)
				var isSuitableToCheckForChildren:Boolean=false;
				var lowerLevelContainer:container;
				var kinderNam:String=kinder.name().toString();
				//trace(kinderNam,222,kinderNam=="addBlank");
				
				
				if(kinder.hasOwnProperty("@exptWideAction") && kinder.@["exptWideAction"]!=""){//where this value follows this pattern: id,property,action
					this.exptWideAction=ExptWideAction.getInstance();
					var stuffToUpdate:Object= exptWideAction.pass(kinder);
					
					for (var nam:String in stuffToUpdate){
						if(kinder.hasOwnProperty("@"+nam)){
							kinder.@[nam]=stuffToUpdate[nam];
						}
					}
				}
				
				switch (kinderNam.substr(0,3)){
					case("add"):
					case("beh"):
					case("adm"):
						iterateObjectsForComposition(kinder,inContainer);isSuitableToCheckForChildren=true;
						break;
					case("con"):
						if(!myContainers)myContainers=new Array;
						kinder.@numInContainer=countChildren(kinder);
						lowerLevelContainer=composeObject(kinder, 1,inContainer);
						myContainers.push(lowerLevelContainer); //put this array in kill();
						isSuitableToCheckForChildren=true;
						break;
					default:
						//maybe in the future we want a default behaviour
						break;
					
				}
				
				//if has children, self call;
				
				if(isSuitableToCheckForChildren && kinder.length()!=0 && kinder.children().length() >0 && (kinder.children()[0].name().toString())!=""){
					//above, utter madness that TWO checks are required to determine if kinder has children... must be a better way. Oh well. 
					elementSetup(kinder,lowerLevelContainer,level+1);
				}
			}
		}
		
		
		private function countChildren(kinder:XML):uint{
			var count:uint=0;
			var howMany:String;
			var nam:String;
			for (var i:uint=0;i<kinder.children().length();i++){
				howMany=kinder.children()[i].@howMany;
				nam=kinder.children()[i].name().toString().substr(0,3);
				if(nam=="add" || nam=="con"){
					if (howMany.length==0 && howMany!="0") count++;
					else if (uint(howMany)>0) count+=uint(howMany)
				}
			}
			return count;
		}
		
		
		private function iterateObjectsForComposition(obj:XML,inContainer:container):void{
			var iterations:uint=1;	
			
			if (String(obj.attribute("howMany")).length!=0) {
				iterations=obj.attribute("howMany");
			}
			for (var i:uint=1;i<=iterations;i++) {
				if (obj.attribute("overExperiment")=="true") {
					_overEntireExperiment.composeALLexptObject(obj, i-1,objects);
				}
				else {
					composeObject(obj, i-1,inContainer);
					
				}
			}
		}
	
		public var TRIAL_ID:int;
		
		//seperated for overwriting reasons (in TrialAndroid etc);
		public function stimulusFactory(name:String):IStimulus{
			return StimulusFactory.Stimulus(name);
		}
		
		public function composeObject(obj:XML, iteration:uint,inContainer:container):container {
			
			//using the 'present' attribute (where 0 means 'do not show');
			var makeStimulus:Boolean=codeRecycleFunctions.multipleTrialCorrection(obj.attribute("present"),";",trialBlockPositionStart)!="0";
			
			var tempObj:IStimulus = stimulusFactory(obj.name());
			if(makeStimulus && tempObj){
				{
					if(logger)tempObj.passLogger(logger);
					OnScreenElements.push(tempObj);
					
					var TrialVarObj:Object=new Object  ;
					TrialVarObj.theStage=theStage;
					TrialVarObj.name=obj.name();
					
					if(inContainer==null){
						TrialVarObj.h=returnStageHeight;
						TrialVarObj.w=returnStageWidth;
						TrialVarObj.containerX=0;
						TrialVarObj.containerY=0;
					}
					else{
						var fromContainer:Object = inContainer.returnContainerInfo();
						TrialVarObj.h=fromContainer.myHeight;
						TrialVarObj.w=fromContainer.myWidth;
						TrialVarObj.containerX=fromContainer.x;
						TrialVarObj.containerY=fromContainer.y;
					}
					
					// 12.9.2012 decided to give behaviourBoss to everything
					tempObj.giveBehavBoss(manageBehaviours);
					
					if (tempObj is IGiveScript)(tempObj as IGiveScript).giveExptScript(exptScript);
					if(tempObj is IgiveTrialInfo)(tempObj as IgiveTrialInfo).giveTrialInfo(trialInfo);
					if(tempObj is IgiveCurrentDisplay)(tempObj as IgiveCurrentDisplay).passOnScreenBoss(CurrentDisplay);
					if(tempObj is IgivePreloader)(tempObj as IgivePreloader).passPreloader(preloader);
					

					TrialVarObj.i=iteration;
					TrialVarObj.perSize=ExptWideSpecs.ExptWideSpecs.screen.percentageFromWidthOrHeightOrBoth as String;
					TrialVarObj.trialBlockPositionStart=trialBlockPositionStart;
					TrialVarObj.parent=pic;
					TrialVarObj.order=trialInfo.order;
					
					tempObj.setUpTrialSpecificVariables(TrialVarObj);
					
					obj.duplicateTrialNumber=trialBlockPositionStart;
					tempObj.setVariables(XMLList(obj));
					behaviours="";
					tempResponse="";
					
					if (String(obj.@showBox).length!=0 && obj.@showBox=="true" && (String(obj.attribute("showBox")).length==0 || obj.attribute("showBox")=="false")){
						obj.@showBox="true";
					}
					
					if (String(objects.@verticalPosition0).length!=0 && String(obj.attribute("verticalPosition0")).length==0){
						obj.@verticalPosition0=String(objects.@verticalPosition0);
					}
					
					if (String(objects.@horizontalPosition0).length!=0 && String(obj.attribute("horizontalPosition0")).length==0){
						obj.@horizontalPosition0=String(objects.@horizontalPosition0);
					}
					
					

					behaviours=codeRecycleFunctions.multipleTrialCorrection(obj.attribute("behaviours"),"---",iteration);
					tempResponse=codeRecycleFunctions.multipleTrialCorrection(obj.attribute("responseRecorder"),"---",iteration);
					
					
					if (obj.name()=="addUpdateOverExperiment") TrialVarObj._overEntireExperiment=_overEntireExperiment;
						
					
					
					if(tempObj.returnsDataQuery() || (tempObj.getVar("results") as String).length!=0) ListofObjectsReturningData.push(tempObj);
					if(inContainer!=null)inContainer.passChildObject((tempObj as object_baseClass).pic);
					
					
					/////////////timing stuff
					/////////////created the timing object var for unit testing
					var duration:String = tempObj.getVar("duration")
					if(duration!="") duration = codeRecycleFunctions.multipleTrialCorrection(duration,"---",iteration);
					var timing:Object = super.sortoutTiming(tempObj.getVar("timeStart"), tempObj.getVar("timeEnd"),duration,tempObj.getVar("peg"));
					manageBehaviours.passObject(tempObj as object_baseClass,timing);
					tempObj.setStartEndTimes(timing.start,timing.end);
					/////////////
					/////////////
					
					tempObj.RunMe();
					
				}
			}
			
			if(String(obj.name()).substr(0,3)=="con")return tempObj as container;
			else return inContainer;
		}
		
		
		
		/*		private function endResponseGoToNext(event:Event):void {
		endResponseGoToTrial(event, "next");
		}
		
		private function endResponseGoToPrev(event:Event):void {
		endResponseGoToTrial(event, "prev");
		}*/
		
		
		/*		
		private function runDrivenEvent(event:Event):void {
		var myArr:Array=event.target.getListOfDrivenEvents();
		logger.log("running driven event(s):"+myArr);
		event.target.removeEventListener("runDrivenEvent",runDrivenEvent);
		//////
		for (var i:uint=0; i<myArr.length; i++) {
		CurrentDisplay.runDrivenEvent(myArr[i]);
		}
		}
		
		private function stopEvent(event:Event):void {
		logger.log("stopping event...");
		event.target.removeEventListener(event.type,runDrivenEvent);
		var myArr:Array=event.target.getListOfstopEvents();
		//////
		for (var i:uint=0; i<myArr.length; i++) {
		CurrentDisplay.stopObj(myArr[i]);
		}
		}*/
		private function saveDataEndStudy(event:Event):void{
			dispatchEvent(new Event("saveDataEndStudy"));
		}
		
		public function compileOutputForTrial():void {
			var tempResults:XML=new XML  ;
			var tempArray:Array=new Array();
			var tempTrialData:XMLList=new XMLList  ;
			
			tempTrialData+= XML(<order>{Order}</order>);
			
			if(ListofObjectsReturningData){
				for (var i:uint=0; i<ListofObjectsReturningData.length; i++) {
					tempArray=ListofObjectsReturningData[i].storedData();
					for (var j:uint=0; j<tempArray.length; j++) {
						tempString=String(tempArray[j].event);
						
						tempResults=new XML  ;
						tempResults=<{tempString}>
						{tempArray[j].data}
						</{tempString}>;
						tempTrialData+=tempResults;
					}
				}
				
				//trialData = <trialData name={trialLabel} trialReplication={duplicateTrialNumber}>
				trialData = <trialData name={trialLabel}>
				{tempTrialData}
				</trialData>;
				var temp:Array;
				
				for (i=0; i<ListofObjectsUpdatedVariables.length; i++) {
					temp=ListofObjectsReturningData[i].getTempStoredVars();
					//var identifier:String=temp[temp.length-1][0];
					//var val=temp[temp.length-1][1];
					//logger.log("222 "+identifier+" --- "+ val); //only takes the last value (the new value)
					//varsStoredEachTrial[identifier]=val;
					//varsStoredEachTrial.push(temp[temp.length-1]);
				}
			}
		}
		
		
		////////////////////////////////////////////////////////////////////////
		//GENERIC VARIABLES AND FUNCTIONS
		////////////////////////////////////////////////////////////////////////
		
		
		public function instantiateVars():void
		{
			if(!pic)pic=new Sprite;
			if(theStage){
				theStage.addChild(pic);
			}
			OnScreenElements=new Array;
			ListofObjectsReturningData = new Array();
			ListofObjectsUpdatedVariables = new Array();
		}
		
		public function prepare(Ord:uint,trial:XML):void {
			
			objects=trial.children();
			Order=Ord;
			
			if(trial.hasOwnProperty('@ITI'))			ITI=int(trial.@ITI);
			if(trial.hasOwnProperty('@submitData'))		submitData=true; //for the v end of the experiment
			if(trial.hasOwnProperty('@ignoreData'))		ignoreData=true;
			if(trial.hasOwnProperty('@trialName'))		trialLabel=codeRecycleFunctions.multipleTrialCorrection(trial.@trialName,";",trialBlockPositionStart);
			
			var con_b:container;
			instantiateVars();
			if (!CurrentDisplay) CurrentDisplay=new OnScreenBoss(pic,logger); // slight bodge to allow for stuff to be added when sorting out a multiExperiment Trial.
			manageBehaviours=new BehaviourBoss(pic,CurrentDisplay,logger);
			//trace(objects,con_b)
			elementSetup(XML(<objects>{objects}</objects>),con_b,0);
			
			if(trialInfo.ITI){
				tempString=codeRecycleFunctions.multipleTrialCorrection(trialInfo.ITI,"~",Order-trialBlockPositionStart);//n.b. the left objects formula is identical to 'trialWithinBlockPosition' in objectBaseClass.
				ITI=int(codeRecycleFunctions.multipleTrialCorrection(tempString,";",trialBlockPositionStart));
			}
			
			manageBehaviours.init();
			
			ITItimer=new Timer(ITI);
			ITItimer.addEventListener(TimerEvent.TIMER, ITIf);
			ITItimer.start();
		}
		
		protected function ITIf(e:TimerEvent):void
		{
			if(ITItimer && ITItimer.hasEventListener(TimerEvent.TIMER))ITItimer.removeEventListener(TimerEvent.TIMER,arguments.callee);
			run();
		}
		
		public function giveTrialData():XML{
			return trialData;
		}
		
		
		public function run():void {
			pic.addEventListener(GotoTrialEvent.TRIAL_PING_FROM_OBJECT,gotoTrial,false,0,false);//note false here for weak refs.  NEEDED.  If the SJ is on a trial long enough, the eventlistener will just be cleaned up...
			CurrentDisplay.commenceDisplay();
		}
		
		public function gotoTrial(e:GotoTrialEvent):void
		{
			pic.removeEventListener(GotoTrialEvent.RUNNER_PING_FROM_TRIAL,gotoTrial);
			generalCleanUp();
			theStage.dispatchEvent(new GotoTrialEvent(GotoTrialEvent.RUNNER_PING_FROM_TRIAL,e.action));
			
		}
		
		
		public function addOnScrEle(nam:String, obj:*, ... rest):void {
			if (rest.length==1) {
				OnScreenElements[nam]=rest[0];
			}
		}
		
		public function generalCleanUp(forcedSubmitData:Boolean=false):void {
			if(ITItimer.hasEventListener(TimerEvent.TIMER)){
				ITItimer.stop();
				ITItimer.removeEventListener(TimerEvent.TIMER,ITIf);
			}
			ITItimer=null;
			
			if(!submitData || forcedSubmitData){
				compileOutputForTrial();
				if(OnScreenElements){
					for (var i:uint=0; i<OnScreenElements.length; i++) {
						OnScreenElements[i].kill();
						OnScreenElements[i]=null;
					}
					
					OnScreenElements=null;
					if (CurrentDisplay)CurrentDisplay.cleanUpScreen();
					CurrentDisplay=null;
					if(manageBehaviours)manageBehaviours.kill();
					manageBehaviours=null;
					
					
					
					
					//theStage.swapChildren(theStage.getChildAt(0),theStage.getChildAt(theStage.numChildren-1));
					
					onScreenChild=null;
					ListofObjectsReturningData=null;
					ListofObjectsUpdatedVariables=null;
					OnScreenElements=null;
					//objects=null;
					TrialRawIdentity=null;
					onLoadedObjects=null;
					//varsStoredEachTrial=null;
					if(pic && theStage && theStage.contains(pic))theStage.removeChild(pic);
					//else trace("prob",pic);
					if(pic)pic=null;
					System.gc();
					if (logger)logger.log("screen cleared");
				}
			}
		}
	}
	
	
}