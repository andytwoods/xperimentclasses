package com.xperiment.events
{
	import flash.events.Event;
	
	public class runGlobalFunctionsEvent extends Event
	{
		
		public static var COMMAND:String="command";
		public var theFunction:String;
		public var values:*;
		
		
		
		public function runGlobalFunctionsEvent(type:String, theFunction:String,values:*="")
		{
			this.theFunction=theFunction;
			this.values=values;
			super(type);
		}
	}
}