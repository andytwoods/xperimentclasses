package com.xperiment.events
{
	import flash.events.Event;
	
	public class GotoTrialEvent extends Event
	{
		
		public var action:String; // nextTrial, prevTrial reserved, else use trialname
		
		public static const TRIAL_PING_FROM_OBJECT:String="trialPingFromObject";
		public static const RUNNER_PING_FROM_TRIAL:String="runnerPingFromTrial";
		
		
		public function GotoTrialEvent(type:String, action:String)
		{
			this.action=action;

			super(type);
		}
		
	}
}

